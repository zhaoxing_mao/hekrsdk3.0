package me.hekr.hekrsdk.bean;

/**
 * Created by OkamiHsu on 16/4/25.
 */
public class LANBean {

    String ip;
    int port;
    String devTid;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDevTid() {
        return devTid;
    }

    public void setDevTid(String devTid) {
        this.devTid = devTid;
    }

    @Override
    public boolean equals(Object o) {
        if (o.toString().equals(devTid))
            return true;
        else
            return false;
    }
}