package me.hekr.hekrsdk.bean;

/**
 * Created by hekr_jds on 4/19 0019.
 **/
public class GroupInfoBean {

    /**
     * groupId : gid1234
     * groupName : 联动小组001
     */

    private String groupId;
    private String groupName;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
