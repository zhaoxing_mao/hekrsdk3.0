package me.hekr.hekrsdk.bean;

/**
 * Created by hekr_jds on 5/11 0011.
 **/
public class OAuthListBean {

    /**
     * devTid : ESP_xxx
     * ctrlKey : xxxx
     * grantor : uid1
     * grantorName : 小李
     * grantee : uid2
     * granteeName : 小明
     * grantTime : 1460219502699
     * updateTime : 1460219502699
     * expire : 3600
     * instructionExpire : null
     * enableIFTTT : true
     * enableSchedule : true
     * desc : test
     */

    private String devTid;
    private String ctrlKey;
    private String grantor;
    private String grantorName;
    private String grantee;
    private String granteeName;
    private long grantTime;
    private long updateTime;
    private int expire;
    private Object instructionExpire;
    private boolean enableIFTTT;
    private boolean enableSchedule;
    private String desc;

    public OAuthListBean() {
    }

    public String getDevTid() {
        return devTid;
    }

    public void setDevTid(String devTid) {
        this.devTid = devTid;
    }

    public String getCtrlKey() {
        return ctrlKey;
    }

    public void setCtrlKey(String ctrlKey) {
        this.ctrlKey = ctrlKey;
    }

    public String getGrantor() {
        return grantor;
    }

    public void setGrantor(String grantor) {
        this.grantor = grantor;
    }

    public String getGrantorName() {
        return grantorName;
    }

    public void setGrantorName(String grantorName) {
        this.grantorName = grantorName;
    }

    public String getGrantee() {
        return grantee;
    }

    public void setGrantee(String grantee) {
        this.grantee = grantee;
    }

    public String getGranteeName() {
        return granteeName;
    }

    public void setGranteeName(String granteeName) {
        this.granteeName = granteeName;
    }

    public long getGrantTime() {
        return grantTime;
    }

    public void setGrantTime(long grantTime) {
        this.grantTime = grantTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public int getExpire() {
        return expire;
    }

    public void setExpire(int expire) {
        this.expire = expire;
    }

    public Object getInstructionExpire() {
        return instructionExpire;
    }

    public void setInstructionExpire(Object instructionExpire) {
        this.instructionExpire = instructionExpire;
    }

    public boolean isEnableIFTTT() {
        return enableIFTTT;
    }

    public void setEnableIFTTT(boolean enableIFTTT) {
        this.enableIFTTT = enableIFTTT;
    }

    public boolean isEnableSchedule() {
        return enableSchedule;
    }

    public void setEnableSchedule(boolean enableSchedule) {
        this.enableSchedule = enableSchedule;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public OAuthListBean(String devTid, String ctrlKey, String grantor, String grantorName, String grantee, String granteeName, long grantTime, long updateTime, int expire, Object instructionExpire, boolean enableIFTTT, boolean enableSchedule, String desc) {
        this.devTid = devTid;
        this.ctrlKey = ctrlKey;
        this.grantor = grantor;
        this.grantorName = grantorName;
        this.grantee = grantee;
        this.granteeName = granteeName;
        this.grantTime = grantTime;
        this.updateTime = updateTime;
        this.expire = expire;
        this.instructionExpire = instructionExpire;
        this.enableIFTTT = enableIFTTT;
        this.enableSchedule = enableSchedule;
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "OAuthListBean{" +
                "devTid='" + devTid + '\'' +
                ", ctrlKey='" + ctrlKey + '\'' +
                ", grantor='" + grantor + '\'' +
                ", grantorName='" + grantorName + '\'' +
                ", grantee='" + grantee + '\'' +
                ", granteeName='" + granteeName + '\'' +
                ", grantTime=" + grantTime +
                ", updateTime=" + updateTime +
                ", expire=" + expire +
                ", instructionExpire=" + instructionExpire +
                ", enableIFTTT=" + enableIFTTT +
                ", enableSchedule=" + enableSchedule +
                ", desc='" + desc + '\'' +
                '}';
    }
}
