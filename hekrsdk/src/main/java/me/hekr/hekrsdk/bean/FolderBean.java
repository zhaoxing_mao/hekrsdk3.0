package me.hekr.hekrsdk.bean;

import java.util.List;

/**
 * Created by hekr_jds on 4/19 0019.
 **/
public class FolderBean {

    /**
     * folderId : xxxx
     * folderName : {folderName}
     * devTidList : []
     */

    private String folderId;
    private String folderName;
    private List<?> devTidList;

    public FolderBean() {
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public List<?> getDevTidList() {
        return devTidList;
    }

    public void setDevTidList(List<?> devTidList) {
        this.devTidList = devTidList;
    }
}
