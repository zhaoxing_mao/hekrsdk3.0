package me.hekr.hekrsdk.bean;

import android.webkit.WebView;

import org.json.JSONObject;

import me.hekr.hekrsdk.action.HekrData;

/**
 * Created by hekr_xm on 2016/5/9.
 **/
public class FilterBean {

    private WebView objectWeakReference;
    private JSONObject filter;
    private long timeStamp;
    private boolean once;
    private HekrData.dataReceiverListener dataReceiverListener;

    public FilterBean(WebView objectWeakReference, long timeStamp, JSONObject filter, boolean once, HekrData.dataReceiverListener dataReceiverListener) {
        this.objectWeakReference = objectWeakReference;
        this.timeStamp = timeStamp;
        this.filter = filter;
        this.once = once;
        this.dataReceiverListener = dataReceiverListener;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public JSONObject getFilter() {
        return filter;
    }

    public void setFilter(JSONObject filter) {
        this.filter = filter;
    }

    public boolean isOnce() {
        return once;
    }

    public void setOnce(boolean once) {
        this.once = once;
    }

    public HekrData.dataReceiverListener getDataReceiverListener() {
        return dataReceiverListener;
    }

    public void setDataReceiverListener(HekrData.dataReceiverListener dataReceiverListener) {
        this.dataReceiverListener = dataReceiverListener;
    }

    public WebView getObjectWeakReference() {
        return objectWeakReference;
    }

    public void setObjectWeakReference(WebView objectWeakReference) {
        this.objectWeakReference = objectWeakReference;
    }

    @Override
    public String toString() {
        return "FilterBean{" +
                "filter=" + filter +
                ", timeStamp=" + timeStamp +
                ", once=" + once +
                '}';
    }
}
