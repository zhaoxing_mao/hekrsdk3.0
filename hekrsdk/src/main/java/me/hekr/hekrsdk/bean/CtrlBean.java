package me.hekr.hekrsdk.bean;



import android.webkit.WebView;

import org.json.JSONObject;

import me.hekr.hekrsdk.action.HekrData;

/**
 * Created by hekr_xm on 2016/4/27.
 * 发送控制命令
 **/
public class CtrlBean {

    private WebView x5WebView;
    private String devTid;
    private JSONObject data;
    private HekrData.dataReceiverListener dataReceiverListener;

    public CtrlBean(WebView x5WebView, String devTid, JSONObject data, HekrData.dataReceiverListener dataReceiverListener) {
        this.x5WebView=x5WebView;
        this.devTid = devTid;
        this.data = data;
        this.dataReceiverListener = dataReceiverListener;
    }

    public WebView getObject() {
        return x5WebView;
    }

    public void setObject(WebView object) {
        this.x5WebView = object;
    }

    public String getDevTid() {
        return devTid;
    }

    public void setDevTid(String devTid) {
        this.devTid = devTid;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public HekrData.dataReceiverListener getDataReceiverListener() {
        return dataReceiverListener;
    }

    public void setDataReceiverListener(HekrData.dataReceiverListener dataReceiverListener) {
        this.dataReceiverListener = dataReceiverListener;
    }

    @Override
    public String toString() {
        return "CtrlBean{" +
                "WebView=" + x5WebView +
                ", devTid='" + devTid + '\'' +
                ", data=" + data +
                ", dataReceiverListener=" + dataReceiverListener +
                '}';
    }
}
