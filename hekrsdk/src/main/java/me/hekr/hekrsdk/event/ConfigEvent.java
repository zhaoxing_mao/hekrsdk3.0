package me.hekr.hekrsdk.event;


import me.hekr.hekrsdk.bean.DeviceBean;

/**
 * Created by hekr_xm on 2016/5/16.
 **/
public class ConfigEvent {
    private DeviceBean deviceBean;

    public ConfigEvent(DeviceBean deviceBean) {
        this.deviceBean = deviceBean;
    }

    public DeviceBean getDeviceBean() {
        return deviceBean;
    }

    public void setDeviceBean(DeviceBean deviceBean) {
        this.deviceBean = deviceBean;
    }

    @Override
    public String toString() {
        return "ConfigEvent{" +
                "deviceBean=" + deviceBean +
                '}';
    }
}
