package me.hekr.hekrsdk.event;

/**
 * Created by hekr_xm on 2016/4/28.
 **/
public class NetworkEvent {
    private int netStatus;

    public NetworkEvent(int netStatus) {
        this.netStatus = netStatus;
    }

    public int getNetStatus() {
        return netStatus;
    }

    public void setNetStatus(int netStatus) {
        this.netStatus = netStatus;
    }

    @Override
    public String toString() {
        return "NetworkEvent{" +
                "netStatus=" + netStatus +
                '}';
    }
}
