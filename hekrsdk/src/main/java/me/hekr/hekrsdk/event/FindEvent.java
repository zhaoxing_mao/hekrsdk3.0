package me.hekr.hekrsdk.event;

/**
 * Created by hekr_jds on 4/19 0019.
 **/
public class FindEvent {
    private boolean isFind;

    public FindEvent(boolean isFind) {
        this.isFind = isFind;
    }

    public boolean getFind() {
        return isFind;
    }

    public void setFind(boolean find) {
        isFind = find;
    }

    @Override
    public String toString() {
        return "FindEvent{" +
                "isFind=" + isFind +
                '}';
    }
}
