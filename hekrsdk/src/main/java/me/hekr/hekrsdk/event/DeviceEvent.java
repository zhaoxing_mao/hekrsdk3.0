package me.hekr.hekrsdk.event;


import me.hekr.hekrsdk.bean.DeviceBean;

/**
 * Created by hekr_jds on 4/20 0020.
 **/
public class DeviceEvent {
    private DeviceBean deviceBean;

    public DeviceBean getDeviceBean() {
        return deviceBean;
    }

    public void setDeviceBean(DeviceBean deviceBean) {
        this.deviceBean = deviceBean;
    }

    @Override
    public String toString() {
        return "DeviceEvent{" +
                "deviceBean=" + deviceBean +
                '}';
    }

    public DeviceEvent(DeviceBean deviceBean) {
        this.deviceBean = deviceBean;
    }
}
