package me.hekr.hekrsdk.event;


import me.hekr.hekrsdk.bean.DeviceBean;

/**
 * Created by hekr_xm on 2016/4/28.
 **/
public class DownLoadEvent {
    private DeviceBean deviceBean;

    public DownLoadEvent(DeviceBean deviceBean) {
        this.deviceBean = deviceBean;
    }

    public DeviceBean getDeviceBean() {
        return deviceBean;
    }

    public void setDeviceBean(DeviceBean deviceBean) {
        this.deviceBean = deviceBean;
    }

    @Override
    public String toString() {
        return "DownLoadEvent{" +
                "deviceBean=" + deviceBean +
                '}';
    }
}
