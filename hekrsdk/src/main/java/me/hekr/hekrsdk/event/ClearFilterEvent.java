package me.hekr.hekrsdk.event;

import android.webkit.WebView;


/**
 * Created by hekr_xm on 2016/5/14.
 **/
public class ClearFilterEvent {
    private boolean clear;
    private WebView x5WebView;

    public ClearFilterEvent(boolean clear, WebView x5WebView) {
        this.clear = clear;
        this.x5WebView = x5WebView;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public WebView getX5WebView() {
        return x5WebView;
    }

    public void setX5WebView(WebView x5WebView) {
        this.x5WebView = x5WebView;
    }

    @Override
    public String toString() {
        return "ClearFilterEvent{" +
                "clear=" + clear +
                ", x5WebView=" + x5WebView +
                '}';
    }
}
