package me.hekr.hekrsdk.event;

/**
 * Created by hekr_xm on 2016/4/25.
 **/
public class PingMessageEvent {
    private String str;

    public PingMessageEvent(String str) {
        this.str = str;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}
