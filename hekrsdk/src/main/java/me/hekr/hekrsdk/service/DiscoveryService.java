package me.hekr.hekrsdk.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import me.hekr.hekrsdk.bean.Global;
import me.hekr.hekrsdk.event.NetworkEvent;
import me.hekr.hekrsdk.event.StartDiscoverEvent;
import me.hekr.hekrsdk.util.ServiceDscDevUtil;


public class DiscoveryService extends Service {

    private static final String TAG="DiscoveryService";

    private BroadcastReceiver connectionReceiver;
    private ServiceDscDevUtil serviceDscDevUtil;

    //判断是否acquire wifi,并且只能和release一一对应
    private int isStartFlag=0;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG,"---onCreate---");
        initData();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG,"---onStartCommand---");
        return super.onStartCommand(intent, flags, startId);
    }

    private void initData(){
        createReceiver();
        EventBus.getDefault().register(this);
        serviceDscDevUtil =new ServiceDscDevUtil(this);
    }

    public void createReceiver() {
        // 创建网络监听广播
        if (connectionReceiver == null) {
            connectionReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                        ConnectivityManager mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = mConnectivityManager.getActiveNetworkInfo();
                        if (netInfo != null && netInfo.isAvailable()) {
                            EventBus.getDefault().postSticky(new NetworkEvent(3));
                        } else {
                            EventBus.getDefault().postSticky(new NetworkEvent(4));
                        }
                    }
                }
            };
            // 注册网络监听广播
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(connectionReceiver, intentFilter);
        }
    }

    //EventBus 接收数据
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(NetworkEvent event) {
        if(event!=null){
            switch (event.getNetStatus()){
                case 3:
                    android.util.Log.i(TAG, "收到网络变换动作:有网:开始发现服务");
                    Global.lanList.clear();
                    serviceDscDevUtil.startSearch();
                    isStartFlag=1;
                    break;
                case 4:
                    android.util.Log.i(TAG, "收到网络变换动作:无网:停止发现服务");
                    Global.lanList.clear();
                    if(isStartFlag==1) {
                        //serviceDscDevUtil.stopSearch();
                        isStartFlag=0;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    //EventBus 接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserEvent(StartDiscoverEvent event) {
        if(event!=null){
            if(event.isStart()){
                serviceDscDevUtil.startSearch();
            }
            else{
                serviceDscDevUtil.stopSearch();
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG,"---onDestroy---");
        super.onDestroy();

        if(connectionReceiver!=null){
            unregisterReceiver(connectionReceiver);
        }

        EventBus.getDefault().unregister(this);
    }
}
