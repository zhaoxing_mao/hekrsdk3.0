package me.hekr.hekrsdk.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;

import com.litesuits.common.assist.Network;
import com.litesuits.common.utils.TelephoneUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import de.tavendo.autobahn.WebSocket;
import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import me.hekr.hekrsdk.action.HekrData;
import me.hekr.hekrsdk.action.HekrUserAction;
import me.hekr.hekrsdk.bean.FilterBean;
import me.hekr.hekrsdk.event.ClearFilterEvent;
import me.hekr.hekrsdk.event.CtrlDataEvent;
import me.hekr.hekrsdk.event.NetworkEvent;
import me.hekr.hekrsdk.event.WebServiceSendEvent;
import me.hekr.hekrsdk.util.ConstantsUtil;
import me.hekr.hekrsdk.util.ProtocolFilterUtil;

/**
 * Created by hekr_xm on 2015/12/24.
 **/
public class WebSocketService extends Service implements WebSocket.ConnectionHandler {

    private static final String TAG = "WebSocketService";
    public static final int COMMAND_CONNECT = 1;
    public static final int COMMAND_SEND = 2;
    public static final int NETWORK_AVAILABLE = 3;
    public static final int NETWORK_NO = 4;
    public static final int DATA_SEND_WHAT = 5;
    public static final int DATA_RECEIVE_WHAT = 6;

    //每秒扫描一次过滤器队列
    private static final int TIMEOUT = 1000;

    private final WebSocket mConnection = new WebSocketConnection();

    private PingTask pingTask;
    private Timer timer;

    private int MSG_COUNT = 0;
    private HekrUserAction hekrUserAction;
    private String IMEI;

    private ArrayList<FilterBean> filterQueue = new ArrayList<>();

    private Timer filterClearTimer = new Timer();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        initData();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    private void initData() {
        EventBus.getDefault().register(this);
        hekrUserAction = HekrUserAction.getInstance(this);
        IMEI = TelephoneUtil.getIMEI(this);
        startTimeOut();
    }

    //EventBus 接收ws连接
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserEvent(CtrlDataEvent event) {
        if (event != null) {
            switch (event.getCommand()) {
                case COMMAND_CONNECT:
                    Log.i(TAG, "收到WebSocket开始命令:URL:" + ConstantsUtil.UrlUtil.APP_WEBSOCKET_CONNECT_CLOUD_URL);

                    if (!TextUtils.isEmpty(ConstantsUtil.UrlUtil.APP_WEBSOCKET_CONNECT_CLOUD_URL)) {
                        if (!mConnection.isConnected()) {
                            connect(ConstantsUtil.UrlUtil.APP_WEBSOCKET_CONNECT_CLOUD_URL);
                        }
                        else {
                            mConnection.isConnected();
                            connect(ConstantsUtil.UrlUtil.APP_WEBSOCKET_CONNECT_CLOUD_URL);
                        }
                    }
                    break;

                case COMMAND_SEND:
                    /*if (event.getCtrlBean() != null) {
                        sendMsg(event.getCtrlBean().getAction(), event.getCtrlBean().getDevTid(), event.getCtrlBean().getCtrlKey(), event.getCtrlBean().getData());
                    }*/
                    break;
                default:
                    break;
            }
        }
    }

    //EventBus 接收发送协议
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserEvent(WebServiceSendEvent event) {
        if (event != null) {
            switch (event.getCommand()) {

                case DATA_SEND_WHAT:
                    if (event.getCtrlBean() != null) {
                        sentMsg(event.getCtrlBean().getObject(), event.getCtrlBean().getDevTid(), event.getCtrlBean().getData(), event.getCtrlBean().getDataReceiverListener());
                    }
                    break;
                case DATA_RECEIVE_WHAT:
                    if (event.getCtrlBean() != null) {
                        receiveMsg(event.getCtrlBean().getObject(), event.getCtrlBean().getData(), event.getCtrlBean().getDataReceiverListener());
                    }
                default:
                    break;
            }
        }
    }

    //EventBus 收到外部webView销毁消息,清除接收过滤器
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserEvent(ClearFilterEvent event) {
        if (event != null) {
            if(event.isClear()){
                for(int i=filterQueue.size()-1;i>=0;i--){
                    if(!filterQueue.get(i).isOnce()&&filterQueue.get(i).getObjectWeakReference().equals(event.getX5WebView())){
                        filterQueue.remove(i);
                    }
                }
            }
        }
    }

    //EventBus 接收网络状态变化控制ws切换
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(NetworkEvent event) {
        if (event != null) {
            switch (event.getNetStatus()) {
                case NETWORK_AVAILABLE:
                    Log.i(TAG, "收到网络变换动作:有网");
                    if (!mConnection.isConnected()) {
                        Log.i(TAG, "wifi变化:重连webSocket");
                        connect(ConstantsUtil.UrlUtil.APP_WEBSOCKET_CONNECT_CLOUD_URL);
                    }
                    break;
                case NETWORK_NO:
                    Log.i(TAG, "收到网络变换动作:无网");
                    stopTimer();
                    if (mConnection.isConnected()) {
                        Log.i(TAG, "无网:断开webSocket");
                        mConnection.disconnect();
                    }
                    break;
                default:
                    break;
            }
        }
    }

    //开启心跳包
    private void startTimer() {
        if (timer == null) {
            timer = new Timer();
        }

        if (pingTask == null) {
            pingTask = new PingTask();
        }

        if (timer != null)
            timer.schedule(pingTask, 0, 25000);
    }

    //关闭心跳包
    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        if (pingTask != null) {
            pingTask.cancel();
            pingTask = null;
        }
    }

    //打开ws连接，并发送登录云端ws服务协议
    @Override
    public void onOpen() {
        try {
            Log.i(TAG, "onOpen " + ConstantsUtil.UrlUtil.APP_WEBSOCKET_CONNECT_CLOUD_URL);
            JSONObject jsonObject = new JSONObject();
            JSONObject result = new JSONObject();

            jsonObject.put("appTid", IMEI);
            jsonObject.put("token", hekrUserAction.getJWT_TOKEN());

            result.put("msgId", MSG_COUNT);
            result.put("action", "appLogin");
            result.put("params", jsonObject);
            Log.i(TAG, "param:" + result.toString());
            if (mConnection.isConnected()) {
                mConnection.sendTextMessage(result.toString());
            } else {
                connect(ConstantsUtil.UrlUtil.APP_WEBSOCKET_CONNECT_CLOUD_URL);
            }
            addMsgCount();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param code   code:2>>>reason:Host is unresolved: asia.app.hekr.me
     *               code:2>>>reason:Could not connect to WebSocket server
     *               code:3>>>reason:WebSockets connection lost
     *               code:5>>>reason:WebSockets internal error (java.nio.channels.AsynchronousCloseException)
     * @param reason reason:Host is unresolved: asia.app.hekr.me
     *               reason:Could not connect to WebSocket server
     *               reason:WebSockets connection lost
     */
    @Override
    public void onClose(int code, String reason) {
        Log.i(TAG, "code:" + code + ">>>reason:" + reason);
        if (code == 3) {
            stopTimer();
            if(Network.isConnected(this)){
                connect(ConstantsUtil.UrlUtil.APP_WEBSOCKET_CONNECT_CLOUD_URL);
            }
        }
    }

    //ws服务返回协议
    @Override
    public void onTextMessage(String payload) {
        try {
            JSONObject jsonObject = null;
            if (!TextUtils.isEmpty(payload)) {
                jsonObject = new JSONObject(payload);

            }
            if (jsonObject != null && jsonObject.has("action")) {

                if ("appLoginResp".equals(jsonObject.getString("action"))) {
                    Log.i(TAG, "ws连接成功");
                    stopTimer();
                    startTimer();
                }
                JSONObject receiveObject = new JSONObject(payload);
                if(!"heartbeatResp".equals(jsonObject.getString("action"))) {
                    Log.i("Protocol", "ws接收到云端返回的data:" + payload+"\n");
                    for (int i = filterQueue.size() - 1; i >= 0; i--) {
                        if (ProtocolFilterUtil.dictMatch(filterQueue.get(i).getFilter(), receiveObject)) {

                            Log.i("Protocol","sentFilterQueue.get>>>"+i+">>>:"+filterQueue.get(i).toString());
                            Log.i("Protocol","回调数据"+i+">>>:"+receiveObject.toString());
                            filterQueue.get(i).getDataReceiverListener().onReceiveSuccess(receiveObject.toString());
                            if (filterQueue.get(i).isOnce()) {
                                filterQueue.remove(i);
                            }
                        }
                    }
                    Log.i("Protocol", "onTextMessage:sentFilterQueue个数:" + filterQueue.size() + "数值:" + filterQueue.toString() + "\n");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void sentMsg(WebView objectWeakReference, String devTid, JSONObject data, HekrData.dataReceiverListener dataReceiverListener) {
        try {

            data.put("msgId", MSG_COUNT);
            data.getJSONObject("params").put("appTid", IMEI);

            if (mConnection.isConnected()) {
                //Log.i(TAG, "ws发送到云端的data:" + data.toString());
                mConnection.sendTextMessage(data.toString());

                //协议过滤器
                JSONObject filterObject = new JSONObject();
                filterObject.put("msgId", MSG_COUNT);
                filterObject.put("action", data.getString("action")+"Resp");
                JSONObject params = new JSONObject();
                params.put("devTid", devTid);
                filterObject.put("params", params);

                //Log.i(TAG, "filter:" + filterObject.toString());

                Log.i(TAG,"System.currentTimeMillis():"+System.currentTimeMillis());
                FilterBean filterBean = new FilterBean(objectWeakReference, System.currentTimeMillis() + (10 * 1000), filterObject, true, dataReceiverListener);

                //Log.i(TAG,"sentMsg:filterBean:"+filterBean.toString());

                filterQueue.add(filterBean);

                Log.i("Protocol", "sentMsg:sentFilterQueue个数:" + filterQueue.size() + "数值:" + filterQueue.toString() + "\n");

                addMsgCount();
            } else {
                connect(ConstantsUtil.UrlUtil.APP_WEBSOCKET_CONNECT_CLOUD_URL);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void receiveMsg(WebView object, JSONObject filter, HekrData.dataReceiverListener dataReceiverListener) {

        String sDt = "01/01/2999 00:00:00";
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss",getResources().getConfiguration().locale);
        long maxTime=0;
        Date dt;
        try {
            dt = sdf.parse(sDt);
            maxTime = dt.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        FilterBean filterBean = new FilterBean(object, maxTime, filter, false, dataReceiverListener);
        //Log.i(TAG,"receiveMsg:filterBean:"+filterBean.toString());

        filterQueue.add(filterBean);
        Log.i("Protocol", "receiveMsg:sentFilterQueue个数:" + filterQueue.size() + "数值:" + filterQueue.toString() + "\n");
    }

    private void startTimeOut() {

        filterClearTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                long nowTime = System.currentTimeMillis();
                for (int i = filterQueue.size() - 1; i >= 0; i--) {
                    if (nowTime > filterQueue.get(i).getTimeStamp()) {

                        filterQueue.get(i).getDataReceiverListener().onReceiveTimeout();
                        Log.i("Protocol", "超时..." + filterQueue.get(i).toString());

                        //if (sentFilterQueue.get(i).isOnce() || sentFilterQueue.get(i).getObjectWeakReference().get() == null) {
                        if (filterQueue.get(i).isOnce()) {
                            filterQueue.remove(i);
                        }
                    /*} else if (sentFilterQueue.get(i).getObjectWeakReference().get() == null) {
                        sentFilterQueue.remove(i);
                    }*/
                    }
                }
            }
        }, TIMEOUT);
    }

    private void connect(final String wsUrl) {
        try {
            mConnection.connect(wsUrl, this);
        } catch (WebSocketException e) {
            Log.e(TAG, e.toString());
        }
    }

    //心跳包发送
    class PingTask extends TimerTask {
        @Override
        public void run() {
            if (mConnection.isConnected()) {
                sendPing(mConnection);
            } else {
                EventBus.getDefault().post(new CtrlDataEvent(1, null));
            }
        }
    }

    public void sendPing(WebSocket mConnection) {
        try {
            JSONObject jo = new JSONObject();
            jo.put("msgId", MSG_COUNT);
            jo.put("action", "heartbeat");
            //com.litesuits.android.log.Log.i(TAG, "向云端发送心跳包!" + jo.toString());

            mConnection.sendTextMessage(jo.toString());
            addMsgCount();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addMsgCount() {
        MSG_COUNT++;
        if (MSG_COUNT >= 65535) {
            MSG_COUNT = 0;
        }
    }

    @Override
    public void onBinaryMessage(byte[] payload) {

    }

    @Override
    public void onRawTextMessage(byte[] payload) {

    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "WS服务结束");
        EventBus.getDefault().unregister(this);
        if (mConnection.isConnected())
            mConnection.disconnect();
        super.onDestroy();
    }

}
