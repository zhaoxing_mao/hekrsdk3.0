package me.hekr.hekrsdk.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.litesuits.common.utils.TelephoneUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import me.hekr.hekrsdk.bean.CtrlBean;
import me.hekr.hekrsdk.bean.FindDeviceBean;
import me.hekr.hekrsdk.bean.Global;
import me.hekr.hekrsdk.bean.LanUtilBean;
import me.hekr.hekrsdk.event.ClearFilterEvent;
import me.hekr.hekrsdk.event.CreateSocketEvent;
import me.hekr.hekrsdk.event.LanServiceSendEvent;
import me.hekr.hekrsdk.event.NetworkEvent;
import me.hekr.hekrsdk.event.WebServiceSendEvent;
import me.hekr.hekrsdk.util.LANUtil;

/**
 * Created by hekr_xm on 16/5/10.
 **/
public class LANService extends Service {

    private static final String TAG = "LANService";
    public static final int NETWORK_AVAILABLE = 3;
    public static final int NETWORK_NO = 4;
    public ArrayList<LanUtilBean> lanUtilList = new ArrayList<>();

    private String IMEI;

    @Override
    public void onCreate() {
        com.litesuits.android.log.Log.i(TAG, "---onCreate---");
        super.onCreate();
        initData();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        com.litesuits.android.log.Log.i(TAG, "---onStartCommand---");
        return super.onStartCommand(intent, flags, startId);
    }

    private void initData() {
        //Log.i(TAG,"局域网内的设备:"+ MyApplication.lanList.toString());
        EventBus.getDefault().register(this);
        IMEI = TelephoneUtil.getIMEI(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(CreateSocketEvent event) {
        // UI updates must run on MainThread

        if (event != null && event.getFindDeviceBean() != null) {
            Log.i(TAG, "局域网内的设备:" + event.getFindDeviceBean());
            FindDeviceBean findDeviceBean=event.getFindDeviceBean();

            if (lanUtilList != null) {
                if (!lanUtilList.isEmpty()) {
                    int i = lanUtilList.size()-1;
                    //遍历已有list中的设备
                    while (i >=0) {
                        if (TextUtils.equals(findDeviceBean.getDevTid(), lanUtilList.get(i).getFindDeviceBean().getDevTid())) {
                            lanUtilList.get(i).getLanUtil().close();
                            lanUtilList.remove(i);

                            LANUtil lanUtil = new LANUtil(this,findDeviceBean.getServiceIp(), findDeviceBean.getServicePort(), findDeviceBean.getDevTid());
                            LanUtilBean lanUtilBean = new LanUtilBean(findDeviceBean, lanUtil);
                            lanUtilList.add(lanUtilBean);
                            break;
                        }
                        if (i == 0) {
                            Log.i(TAG,"findDeviceBean.getServiceIp():"+findDeviceBean.getServiceIp()+">>>:findDeviceBean.getServicePort():"+findDeviceBean.getServicePort()+
                            ">>>findDeviceBean.getDevTid():"+findDeviceBean.getDevTid());
                            LANUtil lanUtil = new LANUtil(this,findDeviceBean.getServiceIp(), findDeviceBean.getServicePort(), findDeviceBean.getDevTid());
                            LanUtilBean lanUtilBean = new LanUtilBean(findDeviceBean, lanUtil);
                            lanUtilList.add(lanUtilBean);
                            break;
                        }
                        i--;
                    }
                } else {
                    Log.i(TAG,"findDeviceBean.getServiceIp():"+findDeviceBean.getServiceIp()+">>>:findDeviceBean.getServicePort():"+findDeviceBean.getServicePort()+
                            ">>>findDeviceBean.getDevTid():"+findDeviceBean.getDevTid());
                    LANUtil lanUtil = new LANUtil(this,findDeviceBean.getServiceIp(), findDeviceBean.getServicePort(), findDeviceBean.getDevTid());
                    LanUtilBean lanUtilBean = new LanUtilBean(findDeviceBean, lanUtil);
                    lanUtilList.add(lanUtilBean);
                }
            }
        }
    }

    //EventBus 接受数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserEvent(LanServiceSendEvent event) {
        if (event != null) {
            switch (event.getCommand()) {
                case 5:
                    if (isHaveLanDev(Global.lanList, event.getCtrlBean()) != null) {
                        FindDeviceBean findDeviceBean = isHaveLanDev(Global.lanList, event.getCtrlBean());
                        for (int i = 0; i < lanUtilList.size(); i++) {
                            if (TextUtils.equals(findDeviceBean.getDevTid(), lanUtilList.get(i).getFindDeviceBean().getDevTid())) {
                                lanUtilList.get(i).getLanUtil().send(event.getCtrlBean().getObject(), event.getCtrlBean().getDevTid(), event.getCtrlBean().getData(), IMEI,
                                        event.getCtrlBean().getDataReceiverListener());
                                Log.i(TAG,"111");
                            }
                        }
                    } else {
                        Log.i(TAG,"222");
                        EventBus.getDefault().post(new WebServiceSendEvent(5, new CtrlBean(event.getCtrlBean().getObject(), event.getCtrlBean().getDevTid(), event.getCtrlBean().getData(), event.getCtrlBean().getDataReceiverListener())));
                    }
                    break;
                case 6:
                    if (isHaveLanDev(Global.lanList, event.getCtrlBean()) != null) {
                        FindDeviceBean findDeviceBean = isHaveLanDev(Global.lanList, event.getCtrlBean());
                        for (int i = 0; i < lanUtilList.size(); i++) {
                            if (TextUtils.equals(findDeviceBean.getDevTid(), lanUtilList.get(i).getFindDeviceBean().getDevTid())) {
                                lanUtilList.get(i).getLanUtil().receiveMsg(event.getCtrlBean().getObject(), event.getCtrlBean().getData(),
                                        event.getCtrlBean().getDataReceiverListener());
                                Log.i(TAG,"333");
                            }
                        }
                    } else {
                        Log.i(TAG,"444");
                        //EventBus.getDefault().post(new WebServiceSendEvent(6, new CtrlBean(event.getCtrlBean().getObject(), event.getCtrlBean().getDevTid(), event.getCtrlBean().getData(), event.getCtrlBean().getDataReceiverListener())));
                    }

                default:
                    break;
            }
        }
    }

    //EventBus 收到外部webView销毁消息,清除接收过滤器
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserEvent(ClearFilterEvent event) {
        if (event != null) {
            if(event.isClear()){
                if(lanUtilList!=null&&!lanUtilList.isEmpty()) {
                    for (int i = lanUtilList.size() - 1; i >= 0; i--) {
                        lanUtilList.get(i).getLanUtil().clear(event.getX5WebView());
                    }
                }
            }
        }
    }

    //EventBus 接收网络状态变化控制ws切换
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(NetworkEvent event) {
        if (event != null) {
            switch (event.getNetStatus()) {
                case NETWORK_AVAILABLE:
                    Log.i(TAG, "收到网络变换动作:有网");

                    break;
                case NETWORK_NO:
                    Log.i(TAG, "收到网络变换动作:无网");
                    if (lanUtilList != null && !lanUtilList.isEmpty()) {
                        for (int i = lanUtilList.size()-1; i >=0; i--) {
                            lanUtilList.get(i).getLanUtil().close();
                            lanUtilList.remove(i);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private FindDeviceBean isHaveLanDev(ArrayList<FindDeviceBean> list, CtrlBean ctrlBean) {
        if(list==null || ctrlBean ==null)
            return null;
        String devTid= ctrlBean.getDevTid();
        if (list.isEmpty() || TextUtils.isEmpty(devTid))
            return null;
        for (int i = 0; i < list.size(); i++) {
            if (TextUtils.equals(devTid, list.get(i).getDevTid())) {
                return list.get(i);
            }
        }
        return null;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "WS服务结束");
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

}
