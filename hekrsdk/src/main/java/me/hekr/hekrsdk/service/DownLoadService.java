package me.hekr.hekrsdk.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.text.TextUtils;

import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import me.hekr.hekrsdk.event.DownLoadEvent;
import me.hekr.hekrsdk.util.DownLoadH5;
import me.hekr.hekrsdk.util.HekrCodeUtil;


public class DownLoadService extends Service {

    private static final String TAG="DownLoadService";
    private DownLoadH5 downLoadH5;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG,"onCreate--");
        initData();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG,"onStartCommand--");
        return super.onStartCommand(intent, flags, startId);
    }

    private void initData(){
        EventBus.getDefault().register(this);
        downLoadH5=new DownLoadH5(this);
    }

    //EventBus 接收下载前端页面动作
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserEvent(DownLoadEvent event) {
        if(event!=null&&event.getDeviceBean()!=null){
            ConnectivityManager mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = mConnectivityManager.getActiveNetworkInfo();
            //当在wifi状态下才下载
            if (netInfo != null && netInfo.isAvailable() && netInfo.getType() == ConnectivityManager.TYPE_WIFI) {

                if (!TextUtils.isEmpty(event.getDeviceBean().getAndroidPageZipURL()) && event.getDeviceBean().getAndroidPageZipURL().endsWith(".zip")) {
                    Log.i(TAG, "开始下载!");
                    downLoadH5.startDownLoadH5(event.getDeviceBean().getAndroidPageZipURL(), HekrCodeUtil.url2Folder(event.getDeviceBean().getAndroidPageZipURL()));
                }
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
