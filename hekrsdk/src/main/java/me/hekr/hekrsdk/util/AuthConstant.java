package me.hekr.hekrsdk.util;

/**
 * Created by hekr_xm on 2015/11/18.
 **/
public class AuthConstant {

    /**
     * 当前 DEMO 应用的 APP_KEY，第三方应用应该使用自己的 APP_KEY 替换该 APP_KEY
     */
    public static final String WEIBO_APP_KEY = "1051928323";

    public static final String WEIBO_APP_SECRET = "9c6743b48b9ee93e4ca211d68a695ea5";

    /**
     * 当前 DEMO 应用的回调页，第三方应用可以使用自己的回调页。
     * 建议使用默认回调页：https://api.weibo.com/oauth2/default.html
     */
    public static final String WEIBO_REDIRECT_URL = "https://api.weibo.com/oauth2/default.html";

    /**
     * WeiboSDKDemo 应用对应的权限，第三方开发者一般不需要这么多，可直接设置成空即可。
     * 详情请查看 Demo 中对应的注释。
     */
    public static final String WEIBO_SCOPE = "email,direct_messages_read,direct_messages_write,"
            + "friendships_groups_read,friendships_groups_write,statuses_to_me_read,"
            + "follow_app_official_microblog," + "invitation_write";

    public static final String WEIXIN_APP_ID = "wxe9cf3e3c92acfb6f";

    public static final String WEIXIN_APP_SECRET = "f3904a543fed8a30f041c5a63311dac0";

    public static final String QQ_APP_ID = "1104783890";
    //public static final String QQ_APP_ID="1105242567";

    public static final String QQ_APP_KEY = "WEZnNWilXqY89Yb7";

    //hekr posseidon中申请的appid以及appkey

    public static final String POSEIDON_APP_ID = "";

    public static final String POSEIDON_APP_KEY = "";
}
