package me.hekr.hekrsdk.util;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.WebView;

import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import me.hekr.hekrsdk.action.HekrData;
import me.hekr.hekrsdk.bean.CtrlBean;
import me.hekr.hekrsdk.bean.FilterBean;
import me.hekr.hekrsdk.event.WebServiceSendEvent;


public class LANUtil {

    private static final String TAG = "LANUtil";

    private String ip;

    private int port;

    private String device_tid;

    //private X5WebView x5WebView;

    //局域网接收数据超时时间
    private static final int TIMEOUT = 1000;

    //心跳包发送间隔时间
    private static final int HEART_BEAT_RATE = 60 * 1000;

    //消息id标识
    private int MSG_COUNT = 0;

    //消息过滤器
    private ArrayList<FilterBean> udpFilterQueue = new ArrayList<>();

    Timer timer = new Timer();//超时计时器，当获取到设备信息后刷新

    private Boolean onTimeout = false;//标示超时器状态

    Timer heartbeat = new Timer();//心跳计时器

    private Boolean onHeartOut = false;//标示超时器状态

    //接收数据字节数组
    private byte[] buffer = new byte[1024];

    //udp套接字
    private DatagramSocket ds = null;

    private Context context;

    /**
     * 构造函数，创建UDP客户端
     */
    public LANUtil(Context context, final String ip, final int port, String devTid) {
        this.context = context;
        this.ip = ip;
        this.port = port;
        this.device_tid = devTid;

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "InetAddress.getByName(ip):" + ip);

                try {
                    ds = new DatagramSocket();
                    try {
                        ds.connect(InetAddress.getByName(ip), port);
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }

                    if (ds != null && ds.isConnected()) {

                        if(!onHeartOut)
                        startHeartBeat();

                        if(!onTimeout)
                        startTimeOut();
                    }

                    //接收数据
                    while (!ds.isClosed() && ds.isConnected()) {
                        //Log.i(TAG,"等待接收数据:");
                        receive();
                    }

                } catch (SocketException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * 向指定的服务端发送数据信息.
     *
     * @param msg 发送的数据信息
     * @throws IOException
     */
    private void send(String msg) throws IOException {
        if (ds != null) {
            byte[] buf = msg.getBytes("utf-8");
            DatagramPacket dp = new DatagramPacket(buf, buf.length, InetAddress.getByName(ip), port);
            if (!ds.isClosed() && ds.isConnected()) {
                ds.send(dp);
            } else {
                try {
                    ds.disconnect();
                    ds.connect(InetAddress.getByName(ip), port);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
            /*if (!onTimeout)
                startTimeOut();*/
        }
    }

    /**
     * @param devTid               设备tid
     * @param data                 需要发送的部分数据
     * @param IMEI                 appTid
     * @param dataReceiverListener 数据回调接口
     */
    public void send(final WebView x5WebView, final String devTid, final JSONObject data, String IMEI, final HekrData.dataReceiverListener dataReceiverListener) {
        try {
            if (ds != null) {
                data.put("msgId", MSG_COUNT);
                data.getJSONObject("params").put("appTid", IMEI);

                Log.i(TAG, "udp发送到设备data:" + data.toString());

                byte[] buf = (data.toString() + "\n").getBytes();
                DatagramPacket dp = null;
                try {
                    dp = new DatagramPacket(buf, buf.length, InetAddress.getByName(ip), port);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                //Log.i(TAG,"ip:"+ip+">>>port:"+port);

                final DatagramPacket finalDp = dp;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            if (!ds.isClosed() && ds.isConnected()) {
                                ds.send(finalDp);
                            } else {
                                try {
                                    ds.disconnect();
                                    ds.connect(InetAddress.getByName(ip), port);
                                } catch (UnknownHostException e) {
                                    e.printStackTrace();
                                }
                            }

                            //协议过滤器
                            JSONObject filterObject = new JSONObject();
                            try {
                                filterObject.put("msgId", MSG_COUNT);
                                filterObject.put("action", data.getString("action") + "Resp");
                                JSONObject params = new JSONObject();
                                params.put("devTid", data.getJSONObject("params").getString("devTid"));
                                filterObject.put("params", params);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Log.i(TAG, "filterObject:" + filterObject);
                            FilterBean filterBean = new FilterBean(x5WebView, System.currentTimeMillis() + 10 * 1000, filterObject, true, dataReceiverListener);
                            udpFilterQueue.add(filterBean);
                            Log.i("udp-Protocol", "udp-send:sentFilterQueue个数:" + udpFilterQueue.size() + "数值:" + udpFilterQueue.toString() + "\n");
                            addMsgCount();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 接收从指定的服务端发回的数据.
     */
    private void receive() {
        // 创建用来发送数据报包的套接字
        try {
            if (ds != null && !ds.isClosed()&&ds.isConnected()) {
                DatagramPacket dp = new DatagramPacket(buffer, buffer.length);
                ds.receive(dp);
                String msg = new String(dp.getData(), 0, dp.getLength());
                Log.i("udp-Protocol", "局域网接收到的数据：" + msg);
                try {
                    if (!TextUtils.isEmpty(msg)) {
                        JSONObject jsonObject = new JSONObject(msg);

                        if (!"heartbeatResp".equals(jsonObject.getString("action"))) {
                            for (int i = udpFilterQueue.size() - 1; i >= 0; i--) {
                                if (ProtocolFilterUtil.dictMatch(udpFilterQueue.get(i).getFilter(), jsonObject)) {

                                    android.util.Log.i("udp-Protocol", "sentFilterQueue.get>>>" + i + ">>>:" + udpFilterQueue.get(i).toString());
                                    android.util.Log.i("udp-Protocol", "回调数据" + i + ">>>:" + jsonObject.toString());
                                    udpFilterQueue.get(i).getDataReceiverListener().onReceiveSuccess(jsonObject.toString());
                                    if (udpFilterQueue.get(i).isOnce()) {
                                        udpFilterQueue.remove(i);
                                    }
                                }
                            }
                            Log.i("udp-Protocol", "receive:sentFilterQueue个数:" + udpFilterQueue.size() + "数值:" + udpFilterQueue.toString() + "\n");
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void receiveMsg(WebView object, final JSONObject filter, final HekrData.dataReceiverListener dataReceiverListener) {

        String sDt = "01/01/2999 00:00:00";
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", context.getResources().getConfiguration().locale);
        long maxTime = 0;
        Date dt;
        try {
            dt = sdf.parse(sDt);
            maxTime = dt.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        FilterBean filterBean = new FilterBean(object, maxTime, filter, false, dataReceiverListener);
        //Log.i(TAG,"receiveMsg:filterBean:"+filterBean.toString());

        udpFilterQueue.add(filterBean);
        Log.i("udp-Protocol", "receiveMsg:sentFilterQueue个数:" + udpFilterQueue.size() + "数值:" + udpFilterQueue.toString() + "\n");
    }

    public boolean isConnect() {
        return ds != null && ds.isConnected();
    }

    //外部webView关闭时,清除相关的消息过滤器
    public void clear(WebView x5WebView) {
        for (int i = udpFilterQueue.size() - 1; i >= 0; i--) {
            if (!udpFilterQueue.get(i).isOnce() && udpFilterQueue.get(i).getObjectWeakReference().equals(x5WebView)) {
                udpFilterQueue.remove(i);
            }
        }
    }

    /**
     * 关闭udp连接.
     */
    public void close() {
        try {
            //ds.disconnect();
            if (onHeartOut) {
                heartbeat.cancel();
                heartbeat = new Timer();
                onHeartOut = false;
            }

            if (onTimeout) {
                timer.cancel();
                timer = new Timer();
                onTimeout = false;
            }

            ds.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 启动超时任务
     */
    private void startTimeOut() {
        onTimeout = true;
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                //ds.close();
                //listener.onReceiveTimeout(devTid);
                Log.i(TAG, "局域网接收数据超时--使用ws发送!");
                //EventBus.getDefault().post(new WebServiceSendEvent(5, new NewCtrlBean(this,devTid, data, dataReceiverListener)));
                long nowTime = System.currentTimeMillis();
                for (int i = udpFilterQueue.size() - 1; i >= 0; i--) {
                    if (nowTime > udpFilterQueue.get(i).getTimeStamp()) {

                        //udpFilterQueue.get(i).getDataReceiverListener().onReceiveTimeout();
                        android.util.Log.i("udp-Protocol", "超时..." + udpFilterQueue.get(i).toString());

                        EventBus.getDefault().post(new WebServiceSendEvent(5, new CtrlBean(udpFilterQueue.get(i).getObjectWeakReference(), device_tid, udpFilterQueue.get(i).getFilter(), udpFilterQueue.get(i).getDataReceiverListener())));

                        if (udpFilterQueue.get(i).isOnce()) {
                            udpFilterQueue.remove(i);
                        }
                    }
                }
            }
        }, TIMEOUT);
    }

    /**
     * 启动心跳任务
     */
    private void startHeartBeat() {
        onHeartOut = true;

        heartbeat.schedule(new TimerTask() {
            @Override
            public void run() {
                JSONObject jo = new JSONObject();
                try {
                    jo.put("msgId", MSG_COUNT);
                    jo.put("action", "heartbeat");
                    Log.i(TAG, "向设备发送心跳包!" + jo.toString());
                    send(jo.toString() + "\n");
                    addMsgCount();
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }

            }
        }, 0, HEART_BEAT_RATE);
    }

    private void addMsgCount() {
        MSG_COUNT++;
        if (MSG_COUNT >= 65535) {
            MSG_COUNT = 0;
        }
    }
}