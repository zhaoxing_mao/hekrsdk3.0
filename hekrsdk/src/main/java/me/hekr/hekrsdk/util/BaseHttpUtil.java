package me.hekr.hekrsdk.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.entity.StringEntity;


/**
 * Created by jds on 2016/4/8.
 **/
public class BaseHttpUtil {
    private static final String TAG = "HEKR_BaseHttpUtil";
    private static AsyncHttpClient client = new AsyncHttpClient();

    static {
        client.setTimeout(5 * 1000);
        client.setConnectTimeout(5 * 1000);
    }

    public static AsyncHttpClient getClient() {
        return client;
    }


    /**
     * 普通get方法
     *
     * @param context                  context
     * @param url                      构造方法
     * @param asyncHttpResponseHandler 回调方法
     */
    public static void getData(Context context, String url, final AsyncHttpResponseHandler asyncHttpResponseHandler) {
        Log.d(TAG, url + "\n");
        client.get(context, url, asyncHttpResponseHandler);
    }


    /**
     * 普通post数据
     *
     * @param url                      构造网址
     * @param entity                   String类型的json
     * @param asyncHttpResponseHandler 回调方法
     */
    public static void postData(Context context, String url, String entity, final AsyncHttpResponseHandler asyncHttpResponseHandler) {
        Log.d(TAG, url + "\n" + entity);
        StringEntity entity_post = new StringEntity(entity, "utf-8");
        client.post(context, url, entity_post, "application/json", asyncHttpResponseHandler);
    }

    /**
     * 带token的get
     *
     * @param context                  context
     * @param JWT_TOKEN                指令格式中的参数token，是APP调用 认证授权API -> 登录 接口返回的JWT Token，有效期为12小时。如果APP和云端建立通道前token已过期，云端会提示token无效；如果联网之后连接不断，即使token过期，APP还能继续控制设备。
     * @param url                      构造网址
     * @param asyncHttpResponseHandler 回调方法
     */
    public static void getDataToken(Context context, String JWT_TOKEN, String url, final AsyncHttpResponseHandler asyncHttpResponseHandler) {
        Log.d(TAG, url + "\n" + JWT_TOKEN);
        client.addHeader("Authorization", "Bearer " + JWT_TOKEN);
        client.get(context, url, asyncHttpResponseHandler);
    }

    /**
     * 带token的网络post  runInUI
     *
     * @param context                  context
     * @param JWT_TOKEN                指令格式中的参数token，是APP调用 认证授权API -> 登录 接口返回的JWT Token，有效期为12小时。如果APP和云端建立通道前token已过期，云端会提示token无效；如果联网之后连接不断，即使token过期，APP还能继续控制设备。
     * @param url                      构造网址
     * @param entity                   String JSON形式的string
     * @param asyncHttpResponseHandler 回调方法
     */
    public static void postDataToken(Context context, String JWT_TOKEN, String url, String entity, final AsyncHttpResponseHandler asyncHttpResponseHandler) {

        StringEntity entity_post = null;
        if (!TextUtils.isEmpty(entity)) {
            entity_post = new StringEntity(entity, "utf-8");
            Log.d(TAG, url + "\n" + JWT_TOKEN + "\n" + entity);
        } else {
            Log.d(TAG, url + "\n" + JWT_TOKEN);
        }
        client.addHeader("Authorization", "Bearer " + JWT_TOKEN);

        client.post(context, url, entity_post, "application/json", asyncHttpResponseHandler);
    }


    /**
     * 带token的DELETE
     *
     * @param context                  context
     * @param JWT_TOKEN                指令格式中的参数token，是APP调用 认证授权API -> 登录 接口返回的JWT Token，有效期为12小时。如果APP和云端建立通道前token已过期，云端会提示token无效；如果联网之后连接不断，即使token过期，APP还能继续控制设备。
     * @param url                      构造网址
     * @param asyncHttpResponseHandler 回调方法
     */
    public static void deleteDataToken(Context context, String JWT_TOKEN, String url, final AsyncHttpResponseHandler asyncHttpResponseHandler) {
        Log.d(TAG, url + "\n" + JWT_TOKEN);
        client.addHeader("Authorization", "Bearer " + JWT_TOKEN);
        client.addHeader("Content-Type", "application/json");
        client.delete(context, url, asyncHttpResponseHandler);
    }


    /**
     * 带token的PATCH
     *
     * @param context                  context
     * @param JWT_TOKEN                指令格式中的参数token，是APP调用 认证授权API -> 登录 接口返回的JWT Token，有效期为12小时。如果APP和云端建立通道前token已过期，云端会提示token无效；如果联网之后连接不断，即使token过期，APP还能继续控制设备。
     * @param url                      构造网址
     * @param asyncHttpResponseHandler 回调方法
     */
    public static void patchDataToken(Context context, String JWT_TOKEN, String url, String entity, final AsyncHttpResponseHandler asyncHttpResponseHandler) {
        Log.d(TAG, url + "\n" + JWT_TOKEN + "\n" + entity);
        client.addHeader("Authorization", "Bearer " + JWT_TOKEN);
        StringEntity entity_post = new StringEntity(entity, "utf-8");
        client.patch(context, url, entity_post, "application/json", asyncHttpResponseHandler);
    }


    /**
     * 带token的PUT
     *
     * @param context                  context
     * @param JWT_TOKEN                指令格式中的参数token，是APP调用 认证授权API -> 登录 接口返回的JWT Token，有效期为12小时。如果APP和云端建立通道前token已过期，云端会提示token无效；如果联网之后连接不断，即使token过期，APP还能继续控制设备。
     * @param url                      构造网址
     * @param asyncHttpResponseHandler 回调方法
     */
    public static void putDataToken(Context context, String JWT_TOKEN, String url, String entity, final AsyncHttpResponseHandler asyncHttpResponseHandler) {
        Log.d(TAG, url + "\n" + JWT_TOKEN + "\n" + entity);
        client.addHeader("Authorization", "Bearer " + JWT_TOKEN);
        StringEntity entity_post = new StringEntity(entity, "utf-8");
        client.put(context, url, entity_post, "application/json", asyncHttpResponseHandler);
    }

    /**
     * 带token的网络post  runInUI
     *
     * @param context                  context
     * @param JWT_TOKEN                指令格式中的参数token，是APP调用 认证授权API -> 登录 接口返回的JWT Token，有效期为12小时。如果APP和云端建立通道前token已过期，云端会提示token无效；如果联网之后连接不断，即使token过期，APP还能继续控制设备。
     * @param url                      构造网址
     * @param params                   文件地址
     * @param asyncHttpResponseHandler 回调方法
     */
    public static void postFileToken(Context context, String JWT_TOKEN, String url, RequestParams params, final AsyncHttpResponseHandler asyncHttpResponseHandler) {
        Log.d(TAG, url + "\n" + JWT_TOKEN + "\n" + params);
        client.addHeader("Authorization", "Bearer " + JWT_TOKEN);
        client.post(context, url, params, asyncHttpResponseHandler);
    }

}
