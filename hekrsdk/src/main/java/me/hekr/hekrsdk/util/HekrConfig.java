/**
 *
 * Copyright (c) Hekr.me Co.ltd Hangzhou, 2013-2015
 * 
 * 杭州第九区科技（氦氪科技）有限公司拥有该文件的使用、复制、修改和分发的许可权
 * 如果你想得到更多信息，请访问 <http://www.hekr.me>
 *
 * Hekr Co.ltd Hangzhou owns permission to use, copy, modify and
 * distribute this documentation.
 * For more information, please see <http://www.hekr.me>
 * 
 */

package me.hekr.hekrsdk.util;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 
 * HekrConfig.java created by durianskh at Oct 30, 2015 6:49:19 PM 这里对类或者接口作简要描述
 *
 * @author durianskh
 * @version 1.0
 * 
 */

public class HekrConfig {

	/**
	 * 原子变量，决定是否需要继续发送ssid和password
	 */
	private AtomicBoolean isSsidAndPassOK = new AtomicBoolean(false);

	/**
	 * 获取发送经历的时间
	 * 
	 * @param beginTime
	 * @return
	 */
	private long getPassTime(long beginTime) {
		return System.currentTimeMillis() - beginTime;
	}

	/**
	 * 获取需要等待的时间，该变量变化不大，有时间可以多测试，写一个更好的公式
	 * 
	 * @param passTime
	 * @param length
	 * @return
	 */
	private long getSleepTime(long passTime, int length) {
		long param = passTime / 1000 - 3 > 0 ? passTime / 1000 - 3 : 0;
		return 100 / length * (1 + param / 6);
	}

	/**
	 * 具体执行配置的过程，启动两个线程，一个线程用于发送ssid和pass
	 * 
	 * @param ssid
	 * @param password
	 * @return
	 */
	public void config(final String ssid, final String password,int number) {

		isSsidAndPassOK.set(false);
		// 发送ssid和pass的线程
		new Thread() {
			public void run() {
				byte[] data = (ssid + '\0' + password + '\0').getBytes();
				long beginTime = System.currentTimeMillis();
				long passTime = getPassTime(beginTime);
				while (!isSsidAndPassOK.get()) {
					long sleepTime;
					if (passTime > 1000) {
						sleepTime = getSleepTime(passTime, data.length);
					} else {
						sleepTime = getSleepTime(passTime, data.length + 1);
					}
					try {
						hekrconfig(ssid + "", password + "", (int) sleepTime);
						Thread.sleep(sleepTime);
					} catch (Exception e) {
					}
                    passTime = getPassTime(beginTime);
				}
			}
		}.start();

		int count = 0;
		try {
			// number*1000毫秒是number分钟，这是配置的总体超时时间
			// 在number分钟内，不断去判断配置是否成功
			while ((!isSsidAndPassOK.get()) && count < number) {
				// 每次判断之后，主线程休眠1000毫秒
				Thread.sleep(1000);
				count++;
			}
		} catch (Exception e) {
		}

		isSsidAndPassOK.set(true);
	}

	/**
	 *
	 * @param ssid
	 * @param password
	 * @param time
	 *            每次发送之后休眠的时间，以免发送速度过快，路由器承受不住
	 * @throws IOException
	 */
	public void hekrconfig(String ssid, String password, int time) throws IOException {
		// 创建用来发送数据报包的套接字
		DatagramSocket ds = new DatagramSocket();
		byte[] ssidbs = ssid.getBytes("utf-8");
		byte[] passbs = password.getBytes("utf-8");
		int len = ssidbs.length + passbs.length + 2;
		byte[] d = "hekrconfig".getBytes("utf-8");
		DatagramPacket dp;
		dp = new DatagramPacket(d, d.length, InetAddress.getByName("224.127." + len + ".255"),
				7001);
		ds.send(dp);
		ds.close();

		byte[] data = (ssid + '\0' + password + '\0').getBytes("utf-8");

		for (int i = 0; i < data.length; i++) {
			ds = new DatagramSocket();
			dp = new DatagramPacket(d, d.length,
					InetAddress.getByName("224." + i + "." + unsignedByteToInt(data[i]) + ".255"),
					7001);
			ds.send(dp);
			ds.close();
			if (time > 0) {
				try {
					Thread.sleep(time);
				} catch (InterruptedException e) {
				}
			}
		}
	}

	public int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}

    public void stop() {
        isSsidAndPassOK.set(true);
    }
}
