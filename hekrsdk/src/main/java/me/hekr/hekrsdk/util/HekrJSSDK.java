package me.hekr.hekrsdk.util;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;


import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import me.hekr.hekrsdk.action.HekrData;
import me.hekr.hekrsdk.action.HekrUser;
import me.hekr.hekrsdk.action.HekrUserAction;
import me.hekr.hekrsdk.bean.DeviceBean;
import me.hekr.hekrsdk.event.ClearFilterEvent;


/**
 * Created by hekr_xm on 2015/11/9.
 **/
public class HekrJSSDK {

    private static final String TAG = "HekrJSSDK";
    private static final int SCAN_AUTH_REQUEST_CODE = 10001;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 10002;
    private static final int PICK_ACTIVITY_REQUEST_CODE = 10003;

    private AppCompatActivity activity;
    private Context mContext;

    private static HekrJSSDK hekrJSSDK = null;

    private WebViewJavascriptBridge bridge;

    private boolean initHEKRUserFlag = true;

    //primary_layout
    private RelativeLayout layout;

    //primary_WebView
    private WebView loginView;

    //current_WebView
    private WebView currentView;

    private DeviceBean deviceBean;

    //all_WebView
    private LinkedList<WebView> windows = new LinkedList<>();

    private BroadcastReceiver sleepReceiver;


    private HekrUserAction hekrUserAction;
    private String url;
    private int downloadTag = 0;
    private String h5Zip = null;


    public HekrJSSDK(Context mContext, AppCompatActivity activity, RelativeLayout layout, DeviceBean deviceBean) {
        this.mContext = mContext;
        this.activity = activity;
        this.layout = layout;
        this.h5Zip = deviceBean.getAndroidPageZipURL();
        this.url = deviceBean.getAndroidH5Page();
        this.deviceBean=deviceBean;
    }


    //HekrJSSDK-singleton-mode(applicationContext防止oom)
    public static synchronized HekrJSSDK getInstance(Context mContext, AppCompatActivity activity, RelativeLayout layout, DeviceBean deviceBean) {
        hekrJSSDK = new HekrJSSDK(mContext, activity, layout, deviceBean);
        setUpSDK(mContext, activity, layout, deviceBean);
        return hekrJSSDK;
    }

    public static void setUpSDK(Context mContext, AppCompatActivity activity, RelativeLayout layout, DeviceBean deviceBean) {
        //hekrJSSDK = new HekrJSSDK(mContext, activity, layout, h5Zip);
        Log.i(TAG, "url:" + deviceBean.getAndroidH5Page());
        hekrJSSDK.initSDK(mContext, deviceBean.getAndroidH5Page() + "?devTid=" + deviceBean.getDevTid() + "&ctrlKey=" + deviceBean.getCtrlKey() + "&ppk=" + deviceBean.getProductPublicKey());
        hekrJSSDK.initData(activity, deviceBean.getAndroidH5Page());
        hekrJSSDK.initViews(layout);
    }

    //init_sdk
    private void initSDK(Context mContext, String initUrl) {
        this.mContext = mContext;
        this.url = initUrl;
        createReceiver();
    }

    private void initData(Context mContext, String h5Zip) {
        hekrUserAction = HekrUserAction.getInstance(mContext);
        this.h5Zip = h5Zip;
    }

    //init_WebView
    private void initViews(RelativeLayout layout) {
        loginView = new WebView(mContext);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        bridge = getBridge(loginView);
        loginView.loadUrl(url);
        layout.addView(loginView, lp);
        currentView = loginView;
        windows.add(loginView);
    }

    //open_new_WebView(with_push_flag)
    private void openNewWindow(String url, boolean pushInLastFlag) {

        //Log.i("HekrJSSDK", "openNewWindow:URL:" + url.substring(0, url.length() - 13));
        WebView window = new WebView(mContext);
        bridge = getBridge(window);

        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);

       /* if (pushInLastFlag) {
            if (!TextUtils.isEmpty(url) && url.length() > 14) {
                window.loadUrl(url.substring(0, url.length() - 14));
            }
        } else {*/
        window.loadUrl(url);
        layout.addView(window, lp);

        if (currentView != null) {
            currentView.setVisibility(View.INVISIBLE);
        }
        currentView = window;
        windows.add(window);
    }

    //close-current-WebView
    private void closeWindow() {
        try {
            currentView.setVisibility(View.GONE);
            WebView view = null;
            if (windows.size() > 1) {
                view = windows.get(windows.size() - 2);
                view.setVisibility(View.VISIBLE);
            } else {
                if (windows != null && !windows.isEmpty() && windows.get(0) != null) {
                    //windows.get(0).setVisibility(View.GONE);
                    windows.get(0).destroy();
                    Intent intent = new Intent();
                    activity.setResult(Activity.RESULT_OK, intent);
                    EventBus.getDefault().post(new ClearFilterEvent(true,currentView));
                    activity.finish();
                }
            }
            windows.remove(currentView);
            if (currentView != null) {
                //currentView.setVisibility(View.GONE);
                currentView.destroy();
            }
            if (view != null) {
                currentView = view;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeAllWebView() {
        currentView.setVisibility(View.GONE);
        Iterator<WebView> it = windows.iterator();
        while (it.hasNext()) {
            WebView webView = it.next();
            it.remove();
            if (webView != null) {
                webView.setVisibility(View.GONE);
                webView.destroy();
            }
        }
        currentView = loginView;
        windows.add(currentView);
        currentView.setVisibility(View.VISIBLE);
    }

    //js桥接
    private WebViewJavascriptBridge getBridge(final WebView webView) {

        bridge = new WebViewJavascriptBridge(activity, webView, new UserServerHandler());
        bridge.setCustomWebViewClient(new myWebClient());
        bridge.setCustomWebChromeClient(new MyWebChromeClient());

        bridge.registerHandler("currentUser", new WebViewJavascriptBridge.WVJBHandler() {
            @Override
            public void handle(Object data, final WebViewJavascriptBridge.WVJBResponseCallback jsCallback) {
                if (null != jsCallback && hekrUserAction != null) {

                    JSONObject jsonObject = new JSONObject();
                    JSONObject user = new JSONObject();
                    try {
                        user.put("uid", hekrUserAction.getUserId());
                        user.put("access_token", hekrUserAction.getJWT_TOKEN());
                        jsonObject.put("obj", user);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    jsCallback.callback(jsonObject);

                }
            }
        });

        bridge.registerHandler("getDevices", new WebViewJavascriptBridge.WVJBHandler() {
            @Override
            public void handle(Object data, final WebViewJavascriptBridge.WVJBResponseCallback jsCallback) {
                if (null != jsCallback && hekrUserAction != null) {
                    hekrUserAction.getDevices(new HekrUser.GetDevicesListener() {
                        @Override
                        public void getDevicesSuccess(List<DeviceBean> hekrLists) {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("obj", hekrLists);
                                //Log.i(TAG, "getDevices:" + jsonObject.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            jsCallback.callback(jsonObject);
                        }

                        @Override
                        public void getDevicesFail(int errorCode) {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("obj", null);
                                //Log.i(TAG, "getDevices:" + jsonObject.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            jsCallback.callback(jsonObject);
                        }
                    });
                }
            }
        });
        bridge.registerHandler("login", new WebViewJavascriptBridge.WVJBHandler() {
            @Override
            public void handle(Object data, final WebViewJavascriptBridge.WVJBResponseCallback jsCallback) {
                if (null != jsCallback) {
                    if (data != null && !TextUtils.isEmpty(data.toString())) {
                        try {
                            JSONObject obj = new JSONObject(data.toString());
                            if (obj.has("userName") && obj.has("password") && hekrUserAction != null) {
                                hekrUserAction.login(obj.getString("userName"), obj.getString("password"), new HekrUser.LoginListener() {
                                    @Override
                                    public void loginSuccess(String str) {
                                        JSONObject jsonObject = new JSONObject();
                                        try {
                                            jsonObject.put("obj", str);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        jsCallback.callback(jsonObject);
                                    }

                                    @Override
                                    public void loginFail(int errorMsg) {
                                        JSONObject jsonObject = new JSONObject();
                                        try {
                                            jsonObject.put("error", errorMsg);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        jsCallback.callback(jsonObject);
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }

        });
        bridge.registerHandler("logout", new WebViewJavascriptBridge.WVJBHandler() {
            @Override
            public void handle(Object data, WebViewJavascriptBridge.WVJBResponseCallback jsCallback) {
                hekrUserAction.userLogout();
                jsCallback.callback("");
            }
        });

        bridge.registerHandler("close", new WebViewJavascriptBridge.WVJBHandler() {
            @Override
            public void handle(Object data, WebViewJavascriptBridge.WVJBResponseCallback jsCallback) {
                if (null != jsCallback) {
                    if (data != null && !TextUtils.isEmpty(data.toString())) {
                        Log.i(TAG, "close:param:" + data.toString());
                    }
                    Log.i(TAG, "调用close关闭WebView！");
                    closeWindow();
                    //jsCallback.callback("");
                }
            }
        });

        bridge.registerHandler("closeAll", new WebViewJavascriptBridge.WVJBHandler() {
            @Override
            public void handle(Object data, WebViewJavascriptBridge.WVJBResponseCallback jsCallback) {
                if (null != jsCallback) {
                    if (data != null && !TextUtils.isEmpty(data.toString())) {
                        Log.i(TAG, "closeAll:param:" + data.toString());
                    }
                    removeAllWebView();
                    Log.i(TAG, "调用closeAll关闭all_WebView！");
                    jsCallback.callback("");
                }
            }
        });

        bridge.registerHandler("send", new WebViewJavascriptBridge.WVJBHandler() {
            @Override
            public void handle(Object data, final WebViewJavascriptBridge.WVJBResponseCallback jsCallback) {

                if (data != null) {
                    Log.i("udp-Protocol", "send:data:" + data.toString());
                    try {
                        JSONObject obj = new JSONObject(data.toString());
                        if (obj.has("tid") && obj.has("command")) {
                            MsgUtil.sendMsg(currentView, obj.getString("tid"), new JSONObject(obj.getString("command")), new HekrData.dataReceiverListener() {
                                @Override
                                public void onReceiveSuccess(String msg) {
                                    Log.i("udp-Protocol", "send:onReceiveSuccess: " + msg);
                                    JSONObject jsonObject = new JSONObject();
                                    try {
                                        jsonObject.put("obj", new JSONObject(msg));
                                        if (jsCallback != null) {
                                            jsCallback.callback(jsonObject);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onReceiveTimeout() {
                                    if (jsCallback != null) {
                                        jsCallback.callback(null);
                                    }
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.i(TAG, "send:data:空");
                }

            }
        });

        bridge.registerHandler("recv", new WebViewJavascriptBridge.WVJBHandler() {
                    @Override
                    public void handle(Object data, final WebViewJavascriptBridge.WVJBResponseCallback jsCallback) {
                        if (data != null) {
                            Log.i("udp-Protocol", "recv:data:" + data.toString());
                            if (!TextUtils.isEmpty(data.toString())) {
                                final com.alibaba.fastjson.JSONObject object = com.alibaba.fastjson.JSONObject.parseObject(data.toString());
                                object.getJSONObject("filter");
                                object.getString("funcId");

                                MsgUtil.receiveMsg(currentView, new JSONObject(object.getJSONObject("filter")), new HekrData.dataReceiverListener() {
                                    @Override
                                    public void onReceiveSuccess(String msg) {
                                        Log.i("udp-Protocol", "recv:onReceiveSuccess:" + msg);
                                        JSONObject data = new JSONObject();

                                        try {
                                            data.put("funcId", object.getString("funcId"));
                                            data.put("obj", new JSONObject(msg));

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        bridge.callHandler("onRecv", data, new WebViewJavascriptBridge.WVJBResponseCallback() {
                                            @Override
                                            public void callback(Object data) {
                                                if (jsCallback != null) {
                                                    jsCallback.callback(data);
                                                }
                                            }
                                        });
                                    }

                                    @Override
                                    public void onReceiveTimeout() {

                                    }
                                });

                            }
                        }
                        if (jsCallback != null) {
                            jsCallback.callback(null);
                        }
                    }
                }
        );

        bridge.registerHandler("notify", new WebViewJavascriptBridge.WVJBHandler() {
            @Override
            public void handle(Object data, WebViewJavascriptBridge.WVJBResponseCallback jsCallback) {
                if (null != jsCallback && windows != null && windows.size() > 0) {
                    for (int i = 0; i < windows.size(); i++) {
                        bridge = getBridge(windows.get(i));
                        bridge.callHandler("onNotify", data, new WebViewJavascriptBridge.WVJBResponseCallback() {
                            @Override
                            public void callback(Object data) {

                            }
                        });
                    }
                }
            }
        });

        return bridge;
    }

    class myWebClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Log.i(TAG, "onPageStarted:URL:" + url);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.i(TAG, "shouldOverrideUrlLoading:URL:" + url);
            if (url.endsWith("openType=push") || openTypeWithPush(url)) {
                Log.i(TAG, "创建新WebView：URL:" + url);
                boolean pushInLast = false;
                if (url.endsWith("openType=push")) {
                    pushInLast = true;
                }
                openNewWindow(url, pushInLast);
                return true;
            } else {
                view.loadUrl(url);
                return true;
            }
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
           /* if (!TextUtils.isEmpty(url) && !TextUtils.isEmpty(h5Zip)) {
                WebResourceResponse response = null;
                File file = new File(ExternalStorage.getSDCacheDir(mContext, HekrCodeUtil.url2Folder(url)), "index.html");
                Log.d(TAG, "路径: " + ExternalStorage.getSDCacheDir(mContext, HekrCodeUtil.url2Folder(url)));
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);
                    response = new WebResourceResponse("text/html", "UTF-8", fis);
                    return response;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    //通知下载h5zip
                    if (downloadTag == 0) {
                        Log.d(TAG, "shouldInterceptRequest: 开始下载h5Zip！！！！！！");
                        DeviceBean deviceBean = new DeviceBean();
                        deviceBean.setAndroidPageZipURL(h5Zip);
                        EventBus.getDefault().post(new DownLoadEvent(deviceBean));
                    }
                    downloadTag = 1;
                    return super.shouldInterceptRequest(webView, url);
                }

            }*/
            return super.shouldInterceptRequest(webView, webResourceRequest);
        }

        public boolean openTypeWithPush(String str) {
            HashMap<String, String> data = new HashMap<String, String>();
            if (!TextUtils.isEmpty(str)) {
                int index = str.indexOf("?");
                if (index != -1) {
                    String reallyData = str.substring(index + 1);
                    //Log.i(TAG,"reallyData:"+reallyData);
                    if (reallyData.length() > 1) {
                        String[] param = reallyData.split("&");

                        for (String item : param) {
                            String[] key_vaule = item.split("=");
                            if (key_vaule.length == 2) {
                                data.put(key_vaule[0].trim(), key_vaule[1].trim());
                            }
                        }
                        if (data.containsKey("openType") && TextUtils.equals("push", data.get("openType"))) {
                            return true;
                        }
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        @Override
        public void onPageFinished(final WebView view, String url) {
            Log.i(TAG, "onPageFinished:URL:" + url);
            if (!view.getSettings().getLoadsImagesAutomatically()) {
                view.getSettings().setLoadsImagesAutomatically(true);
            }

            if (windows != null && !windows.isEmpty()) {
                Log.i(TAG, "当前WebView个数：" + windows.size());
            }

            if (hekrUserAction != null && initHEKRUserFlag) {
                //Log.i(TAG, "已经得到初始化用户所需的accesskey和uid,并不再进入初始化hekrUser除非logout");
                initHEKRUserFlag = false;
                initHEKRUser();
            }
            super.onPageFinished(view, url);
        }
    }

    class MyWebChromeClient extends WebChromeClient {
        public MyWebChromeClient() {
            super();
        }

        @Override
        public void onCloseWindow(WebView window) {
            super.onCloseWindow(window);
        }

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
            return super.onCreateWindow(view, isDialog, isUserGesture, resultMsg);
        }
    }

    private void initHEKRUser() {

        JSONObject jsonObject = new JSONObject();
        JSONObject uidObject = new JSONObject();
        try {
            uidObject.put("uid", hekrUserAction.getUserId());
            uidObject.put("access_token", hekrUserAction.getJWT_TOKEN());
            jsonObject.put("obj", uidObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        userChange(jsonObject);
    }

    class UserServerHandler implements WebViewJavascriptBridge.WVJBHandler {

        @Override
        public void handle(Object data, WebViewJavascriptBridge.WVJBResponseCallback jsCallback) {
            if (null != jsCallback) {
                jsCallback.callback("Java said:Right back atcha");
            }
        }

    }

    public void createReceiver() {
        // 创建网络监听广播
        if (sleepReceiver == null) {
            sleepReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action.equals("android.intent.action.SCREEN_OFF")) {
                        Log.i(TAG, "屏幕休眠动作触发!");
                        EventBus.getDefault().post(new ClearFilterEvent(true,currentView));
                    }
                }
            };
            // 注册网络监听广播
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Intent.ACTION_SCREEN_ON);
            intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
            intentFilter.addAction(Intent.ACTION_USER_PRESENT);
            mContext.registerReceiver(sleepReceiver, intentFilter);
        }
    }

    public void userChange(JSONObject jsonObject) {
        if (windows != null && !windows.isEmpty()) {
            for (int i = 0; i < windows.size(); i++) {
                bridge = getBridge(windows.get(i));
                if (bridge != null) {
                    bridge.callHandler("onUserChange", jsonObject, new WebViewJavascriptBridge.WVJBResponseCallback() {
                        @Override
                        public void callback(Object data) {

                        }
                    });
                } else {
                    Log.i(TAG, "updateUser->onUserChange:bridge为空!");
                }
            }
        } else {
            Log.i(TAG, "updateUser->onUserChange:windows为空!");
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SCAN_AUTH_REQUEST_CODE:

                break;
            case CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE:


                break;

            case PICK_ACTIVITY_REQUEST_CODE:

                break;

            default:
                break;
        }
    }

    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        if (!windows.isEmpty() && windows.size() > 1) {
            if ((keyCode == KeyEvent.KEYCODE_BACK) && currentView != null && currentView.canGoBack()) {
                currentView.goBack();
                Log.i(TAG, "多个WebView并且当前WebView中存在多个url页面！");
                return true;
            } else {
                Log.i(TAG, "多个WebView并且当前WebView中只剩下一个页面！");
                closeWindow();
                return true;
            }
        } else {
            if ((keyCode == KeyEvent.KEYCODE_BACK && currentView != null && currentView.canGoBack())) {
                Log.i(TAG, "一个WebView并且当前WebView中存在多个url页面！");
//                currentView.goBack();
                Intent intent = new Intent();
                activity.setResult(Activity.RESULT_OK, intent);
                activity.finish();
                return true;
            } else {
                Log.i(TAG, "一个WebView并且当前WebView中只剩下一个页面！");
                Intent intent = new Intent();
                activity.setResult(Activity.RESULT_OK, intent);
                activity.finish();
                return true;
            }
        }
    }

    public void onBackPressed() {
        if (currentView != null && currentView.canGoBack()) {
            currentView.goBack();
        } else {
            closeWindow();
        }
    }

    public void onDestroy() {
        try {

            Log.i(TAG, "loginView--onDestroy()");
            if (windows != null && !windows.isEmpty()) {
                for (int i = 0; i < windows.size(); i++) {
                    //windows.get(i).setVisibility(View.GONE);
                    windows.get(i).destroy();
                }
            }
            if (loginView != null) {
                //loginView.setVisibility(View.GONE);
                loginView.destroy();
            }
            if(sleepReceiver!=null){
                mContext.unregisterReceiver(sleepReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

