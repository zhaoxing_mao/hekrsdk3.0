package me.hekr.hekrsdk.util;

/**
 * Created by jds on 2016/4/10.
 **/
public class ConstantsUtil {
    public static final String NETWORK_ERROR = "Network is not available";
    public static final String TOKEN_OUT_ERROR = "Token expired, please re login";
    public static final String UNKNOWN_ERROR = "UNKNOWN_ERROR";
    public static final String SERVER_ERROR = "The server encountered an internal error and was unable to complete your request";

    /**
     *
     */
    public static class UrlUtil {
        public static final String BASE_QR_CODE_URL = "http://www.hekr.me?";
        public static final String BASE_CONFIG_HElP_URL = "https://hummingbird.daikeapp.com/";
        /**
         * 认证授权API
         */
        public static final String BASE_UAA_URL = "http://uaa.openapi.hekr.me/";
        public static final String UAA_GET_CODE_URL = "sms/getVerifyCode?phoneNumber=";
        public static final String UAA_CHECK_CODE_URL = "sms/checkVerifyCode?phoneNumber=";
        public static final String UAA_LOGIN_URL = "login";
        public static final String UAA_REGISTER_URL = "register?type=";
        public static final String UAA_RESET_PWD_URL = "resetPassword?type=";
        public static final String UAA_CHANGR_PWD_URL = "changePassword";
        public static final String UAA_CHANGE_PHONE_URL = "changePhoneNumber";
        public static final String UAA_REFRESH_TOKEN = "token/refresh";
        public static final String UAA_SEND_CHANGE_EMAIL = "sendChangeEmailStep1Email?email=";
        public static final String UAA_GROUP = "group";

        /**
         * 设备管理
         */
        public static final String BASE_USER_URL = "http://user.openapi.hekr.me/";
        public static final String BIND_DEVICE = "device";
        public static final String FOLDER = "folder";
        public static final String PROFILE = "user/profile";
        public static final String USER_FILE = "user/file";

        public static final String UAA_WEATHER = "weather/now?location=";

        public static final String BASE_CONSOLE_URL = "http://console.openapi.hekr.me/";
        public static final String DEVICE_BIND_STATUS = "deviceBindStatus";
        public static final String DEFAULT_STATIC = "external/device/default/static";
        public static final String CHECK_FW_UPDATE = "external/device/fw/ota/check";

        /**
         * APP连接云端
         */
        public static final String APP_WEBSOCKET_CONNECT_CLOUD_URL = "ws://asia.app.hekr.me:86";

        /**
         * 授权
         */
        public static final String OAuthUrl = "http://www.hekr.me?action=rauth&token=";
        public static final String OAuthREGISTER = "authorization/reverse/register";


    }

    public static class ActionStrUtil {
        public static final String ACTION_DISCOVER_START = "me.hekr.action.discover.start";
        public static final String ACTION_CTR_WEB_SOCKET = "me.hekr.action.ctrservice";

    }

    public static class ErrorCode {
        public static final int NETWORK_TIME_OUT = 0;
        public static final int TOKEN_TIME_OUT = 1;
        public static final int UNKNOWN_ERROR = 2;
        public static final int SERVER_ERROR = 500;
    }


}
