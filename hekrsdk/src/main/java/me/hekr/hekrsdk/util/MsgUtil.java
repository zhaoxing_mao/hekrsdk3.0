package me.hekr.hekrsdk.util;

import android.webkit.WebView;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import me.hekr.hekrsdk.action.HekrData;
import me.hekr.hekrsdk.bean.CtrlBean;
import me.hekr.hekrsdk.bean.Global;
import me.hekr.hekrsdk.event.LanServiceSendEvent;
import me.hekr.hekrsdk.event.WebServiceSendEvent;


/**
 * Created by hekr_xm on 2016/5/3.
 **/
public class MsgUtil {

    private static final String TAG = "MsgUtil";

    public static void sendMsg(WebView x5WebView, String devTid, JSONObject protocol, HekrData.dataReceiverListener dataReceiverListener) {

        if (Global.lanList != null && !Global.lanList.isEmpty()) {

            EventBus.getDefault().post(new LanServiceSendEvent(5, new CtrlBean(x5WebView, devTid, protocol, dataReceiverListener)));
        } else {
            EventBus.getDefault().post(new WebServiceSendEvent(5, new CtrlBean(x5WebView, devTid, protocol, dataReceiverListener)));
        }
    }


    public static void receiveMsg(WebView x5WebView, JSONObject filter, HekrData.dataReceiverListener dataReceiverListener) {

        if (Global.lanList != null && !Global.lanList.isEmpty()) {

            try {
                EventBus.getDefault().post(new LanServiceSendEvent(6, new CtrlBean(x5WebView, filter.getJSONObject("params").getString("devTid"), filter, dataReceiverListener)));
                EventBus.getDefault().post(new WebServiceSendEvent(6, new CtrlBean(x5WebView, filter.getJSONObject("params").getString("devTid"), filter, dataReceiverListener)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                EventBus.getDefault().post(new WebServiceSendEvent(6, new CtrlBean(x5WebView, filter.getJSONObject("params").getString("devTid"), filter, dataReceiverListener)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
