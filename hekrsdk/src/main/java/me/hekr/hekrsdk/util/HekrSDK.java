package me.hekr.hekrsdk.util;

import android.content.Context;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.litesuits.android.log.Log;

import org.json.JSONException;

import me.hekr.hekrsdk.bean.ConfigBean;


/**
 * Created by hekr_jds on 5/19 0019.
 **/
public class HekrSDK {

    private static final String TAG = "HekrSDK";

    /**
     * 初始化SDK
     *
     * @param context 请使用getApplicationContext()
     * @param id      R.raw.config.json
     */
    public static void init(Context context, int id) {
        //初始化sp
        SpCache.init(context.getApplicationContext());
        String jsonStr = HekrCommonUtil.convertStreamToString(context, id);
        try {
            if (!TextUtils.isEmpty(jsonStr)) {
                org.json.JSONObject jsonConfig = new org.json.JSONObject(jsonStr);
                if (jsonConfig.has("Hekr")) {
                    String pid = jsonConfig.getJSONObject("Hekr").getString("AppId");
                    SpCache.putString("pid", pid);
                    Log.d(TAG, "init: " + pid);
                }
                if (jsonConfig.has("Social")) {
                    JSONObject Social = JSON.parseObject(jsonConfig.getJSONObject("Social").toString());
                    ConfigBean weiBoBean = JSON.parseObject(Social.getString("Weibo"), ConfigBean.class);
                    ConfigBean qqBean = JSON.parseObject(Social.getJSONObject("QQ").toString(), ConfigBean.class);
                    ConfigBean weiXinBean = JSON.parseObject(Social.getJSONObject("Weixin").toString(), ConfigBean.class);
                    ConfigBean facebookBean = JSON.parseObject(Social.getJSONObject("Facebook").toString(), ConfigBean.class);
                    ConfigBean googleBean = JSON.parseObject(Social.getJSONObject("Google").toString(), ConfigBean.class);
                    ConfigBean twitterBean = JSON.parseObject(Social.getJSONObject("Twitter").toString(), ConfigBean.class);
                }
                if (jsonConfig.has("push")) {
                    ConfigBean pushBean = JSONObject.parseObject(jsonConfig.getJSONObject("push").toString(), ConfigBean.class);
                }
            } else {
                throw new NullPointerException("configJson is null");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
