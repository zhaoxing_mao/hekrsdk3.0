package me.hekr.hekrsdk.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.hekr.hekrsdk.bean.DeviceAndFolderBean;
import me.hekr.hekrsdk.bean.DeviceBean;
import me.hekr.hekrsdk.bean.ErrorMsgBean;


/**
 * Created by hekr_jds on 4/28 0028.
 **/
public class HekrCodeUtil {
    public static final int LANGUAGE_zh_Hans = 1;
    public static final int LANGUAGE_zh_Hant = 2;
    public static final int LANGUAGE_en = 3;

    /**
     * 从网址 中拿到路径
     */
    public static String url2Folder(String url) {
        Pattern p = Pattern.compile("android/(.*?)/");
        Matcher m = p.matcher(url);
        while (m.find()) {
            MatchResult mr = m.toMatchResult();
            return mr.group(1);
        }
        return null;
    }

    /**
     * 获取语言
     */
    public static int getLanguage(Context context) {
        if (context.getResources().getConfiguration().locale.getCountry().equals("CN")) {
            return LANGUAGE_zh_Hans;
        } else if (context.getResources().getConfiguration().locale.getCountry().equals("TW")) {
            return LANGUAGE_zh_Hant;
        } else if (context.getResources().getConfiguration().locale.getCountry().equals("US")) {
            return LANGUAGE_en;
        } else {
            return LANGUAGE_zh_Hans;
        }
    }




    /**
     * 把云端传过来的数据以folder的类型整合
     *
     * @param DeviceBeanList 云端传入的list
     * @return 带folderID的list
     */
    private static List<DeviceAndFolderBean> Devices2Folder(List<DeviceBean> DeviceBeanList) {
        List<DeviceAndFolderBean> DeviceAndFolderBeanLists = new ArrayList<>();
        try {
            if (DeviceBeanList != null && !DeviceBeanList.isEmpty()) {
                for (DeviceBean device : DeviceBeanList) {
                    //遍历云端的数据
                    String folderId = device.getFolderId();
                    String folderName = device.getFolderName();
                    //首次list为空，直接添加进去
                    if (DeviceAndFolderBeanLists.isEmpty()) {
                        ArrayList<DeviceBean> lists_temp = new ArrayList<>();
                        lists_temp.add(device);
                        DeviceAndFolderBeanLists.add(new DeviceAndFolderBean(folderId, folderName, lists_temp));
                    } else {
                        //不为空则开始遍历list
                        for (int i = 0; i < DeviceAndFolderBeanLists.size(); i++) {
                            DeviceAndFolderBean devicesAndFolder = DeviceAndFolderBeanLists.get(i);
                            if (folderId.equals(devicesAndFolder.getFolderId())) {
                                //如果两个id一样，直接添加进去
                                devicesAndFolder.getLists().add(device);
                                break;
                            } else if (i == DeviceAndFolderBeanLists.size() - 1) {
                                //遍历到最大后如果还没有 直接添加进去
                                ArrayList<DeviceBean> lists_temp = new ArrayList<>();
                                lists_temp.add(device);
                                DeviceAndFolderBeanLists.add(new DeviceAndFolderBean(folderId, folderName, lists_temp));
                                break;
                            }
                        }
                    }
                }
            }
            return DeviceAndFolderBeanLists;
        } catch (Exception e) {
            e.printStackTrace();
            return DeviceAndFolderBeanLists;
        }
    }

    /**
     * 转换成为list直接使用
     *
     * @param DeviceBeanList 从云端获取到的所有设备
     * @return 可以在主界面Adapter中使用的bean
     */
    public static List<DeviceBean> Folder2Lists(List<DeviceBean> DeviceBeanList) {
        List<DeviceBean> devices = new ArrayList<>();
        List<DeviceAndFolderBean> lists = Devices2Folder(DeviceBeanList);
        for (DeviceAndFolderBean deviceAndFolderBean : lists) {
            if ("0".equals(deviceAndFolderBean.getFolderId())) {
                devices.addAll(deviceAndFolderBean.getLists());
            } else {
                devices.add(new DeviceBean(deviceAndFolderBean));
            }
        }
        return devices;
    }


    /**
     * 错误码转换到错误信息！
     *
     * @param errorCode 错误码
     * @return 错误信息
     */
    public static String errorCode2Msg(int errorCode) {
        switch (errorCode) {
            case 3400001:
                return "手机号码无效";
            case 3400002:
                return "验证code错误";
            case 3400003:
                return "验证code过期";
            case 3400005:
                return "发送code次数过多";
            case 3400006:
                return "无效的请求类型";
            case 3400007:
                return "无效的旧密码";
            case 3400008:
                return "用户已注册";
            case 3400009:
                return "用户尚未验证";
            case 3400010:
                return "账号或密码错误";
            case 3400011:
                return "用户不存在";
            case 3400012:
                return "无效的邮件token";
            case 3400013:
                return "账户已认证";
            case 3400014:
                return "账户已经关联了某个三方账号";
            case 500:
            case 5200000:
                return "服务内部错误";
            case 5400001:
                return "内部错误";
            case 5400002:
                return "app重复登录";
            case 5400003:
                return "appTid不能为空";
            case 5400004:
                return "授权关系已存在";
            case 5400005:
                return "授权关系不存在";
            case 5400006:
                return "因网络原因绑定失败";
            case 5400007:
                return "因超时原因绑定失败";
            case 5400009:
                return "修改用户档案失败";
            case 5400010:
                return "校验code失败";
            case 5400011:
                return "设备授权次数达到上限";
            case 5400012:
                return "因内部错误绑定失败";
            case 5400013:
                return "因重复绑定绑定失败";
            case 5400014:
                return "设备不属于用户";
            case 5400015:
                return "没有这样的指令";
            case 5400016:
                return "设备无法重复登录";
            case 5400017:
                return "devTid不能为空";
            case 5400018:
                return "创建定时预约次数达到上限";
            case 5400019:
                return "授权的指令已过期";
            case 5400020:
                return "不支持该指令";
            case 5400021:
                return "不合法的邮件token";
            case 5400022:
                return "不合法的旧密码";
            case 5400023:
                return "不合法的校验code";
            case 5400024:
                return "由于内部错误设备无法找到，请重连";
            case 5400025:
                return "不存在该pid";
            case 5400026:
                return "没有对该指令的权限";
            case 5400027:
                return "指定模板不存在";
            case 5400028:
                return "由于内部不正确的状态导致设备无法被找到";
            case 5400035:
                return "指定任务不存在";
            case 5400036:
                return "无法创建重复模板";
            case 5400037:
                return "设备id 不匹配";
            case 5400039:
                return "用户不存在";
            case 5400040:
                return "校验code过期";
            case 5400041:
                return "校验code发送失败";
            case 5400042:
                return "校验code类型不合法";
            case 5400043:
                return "设备无法强制绑定";
            case 5500000:
                return "内部服务错误";
            case 6400001:
                return "指定id的反向注册申请不存在";
            case 6400002:
                return "不合法的反向授权请求";
            case 6400003:
                return "只有属主可以授权设备给其他人";
            case 6400004:
                return "指定devTid的设备不存在";
            case 6400005:
                return "达到文件夹所能容纳设备数量的上限";
            case 6400006:
                return "无法创建同名文件夹";
            case 6400007:
                return "指定id的文件夹不存在";
            case 6400008:
                return "达到创建文件夹数量上限";
            case 6400009:
                return "无法删除根目录";
            case 6400010:
                return "无法给根目录改名";
            case 6400011:
                return "指定的规则不存在";
            case 6400012:
                return "指定的定时预约任务不存在";
            case 6400013:
                return "无法创建相同的规则";
            case 6400014:
                return "无法创建相同的定时预约";
            case 6400015:
                return "不合法的prodPubKey";
            case 6400016:
                return "没有权限这样做";
            case 6400017:
                return "请求参数错误";
            case 6400018:
                return "指定的网盘文件不存在";
            case 6400020:
                return "找不到这个红外码";
            case 6400021:
                return "红外服务请求出错";
            case 6400022:
                return "无法找到指令集";
            case 6400023:
                return "参数不支持";
            case 6400024:
                return "解析json失败";
            case 6500001:
                return "删除网盘文件失败";
            case 6500002:
                return "上传网盘文件失败";
            case 8200000:
                return "成功";
            case 8400000:
                return "产品不存在";
            case 8400001:
                return "协议模板不存在";
            case 8400002:
                return "非法参数";
            case 8400003:
                return "平台参数错误";
            case 8400004:
                return "指定pid不存在";
            case 9200000:
                return "成功";
            case 9400000:
                return "错误";
            case 9400001:
                return "非法参数";
            case 9400002:
                return "action 不存在";
            case 9400003:
                return "产品不存在";
            case 9400004:
                return "超时";
            case 9400005:
                return "方法不支持";
            case 9500000:
                return "服务错误";
            case 9500001:
                return "服务应答错误";
            case 0:
                return "网络超时";
            case 1:
                return "登录信息过期,请重新登录！";
            case 2:
                return "未知错误！";
            default:
                return String.valueOf(errorCode);
        }
    }


    /**
     * 定义错误
     *
     * @param code  httpCode
     * @param bytes body
     * @return 错误码
     */
    public static int getErrorCode(int code, byte[] bytes) {
        String TAG = "HEKR_SDK_ERROR";
        switch (code) {
            case ConstantsUtil.ErrorCode.NETWORK_TIME_OUT:
                android.util.Log.e(TAG, ConstantsUtil.NETWORK_ERROR);
                return code;
            case ConstantsUtil.ErrorCode.TOKEN_TIME_OUT:
                android.util.Log.e(TAG, ConstantsUtil.TOKEN_OUT_ERROR);
                return code;
            case ConstantsUtil.ErrorCode.SERVER_ERROR:
                if (bytes != null && bytes.length > 0) {
                    android.util.Log.e(TAG, new String(bytes));
                }
                return code;
            default:
                android.util.Log.e(TAG, "HTTP-" + code + new String(bytes));
                return msg2Bean(bytes).getCode();
        }
    }


    /**
     * 转换错误信息
     *
     * @return ErrorMsgBean
     */
    public static ErrorMsgBean msg2Bean(byte[] bytes) {
        ErrorMsgBean errorMsgBean = new ErrorMsgBean(ConstantsUtil.ErrorCode.UNKNOWN_ERROR, ConstantsUtil.UNKNOWN_ERROR, System.currentTimeMillis());
        if (bytes == null || bytes.length == 0) {
            return errorMsgBean;
        }
        try {
            String errorMsg = new String(bytes);
            errorMsgBean = JSON.parseObject(errorMsg, ErrorMsgBean.class);
            return errorMsgBean;
        } catch (Exception e) {
            return errorMsgBean;
        }
    }
}
