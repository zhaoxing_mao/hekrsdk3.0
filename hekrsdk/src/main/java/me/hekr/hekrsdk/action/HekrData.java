package me.hekr.hekrsdk.action;

/**
 * Created by hekr_xm on 2016/5/9.
 **/
public class HekrData {

    public interface dataReceiverListener{

        void onReceiveSuccess(String msg);

        void onReceiveTimeout();
    }
}
