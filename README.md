HEKR 3.0

### 06/13
* 增加了用户修改密码后主动退出的功能 /jin123d
* 修改密码界面加入了进度框 /jin123d
* 修改用户名时如果用户名/性别/年龄没有改变则不再重新保存 /jin123d

### 06/12
* 修复了友盟上报上来的两个bug，一个验证码到期崩溃的问题，一个google登录的问题 /jin123d
* 增加了“第三方账号”与“已经注册的账号”绑定的功能 /jin123d
* 修复了一个本地头像加载逻辑的问题，先缓存到本地，再对外公开，保证本地数据最新 /jin123d
* 修复了定位的问题，默认“beijing” /jin123d
* 修复了重命名文件夹时特殊字符的问题 /jin123d
* 登录界面适配了960*540分辨率的手机 /jin123d

### 05/17
* 主界面拖入/拖出 /jin123d
* HekrUserAction重构 /jin123d
* 配网保存密码功能 /xm
* 配网文字抖动动画 /xm
* 反馈建议 /xm
* 手机信息复制 /xm
* 授权扫描界面 /xm
* 启动界面 /xm


### 05/04
* 位置相关 /jin123d
* 提高配网发现和服务发现的稳定性 /xm

### 05/03
* 重置的一些接口，手机号重置，邮箱重置 /jin123d
* 添加发送消息工具类（先局域网发送，超时则ws发送）、（局域网设备list里面没有则直接用ws发送）/xm
* 修改配网动画显示不出来的问题 /xm
* 提供第三方登录接口 /xm


###  04/26
* 个人中心的一些接口，上传图片的接口 /jin123d

### 04/25
* 增加设备共享信息界面、设备共享授权界面 /xm
* 增加网络诊断、手机信息查询 /xm
* 增加了刷新token的功能,并且可以自动管理token /jin123d


### 04/22
* 增加了几个界面
* 提供了一个uid供外部使用

### 04/20
* 修改密码完成 /jin123d
* 设备删除完成 /jin123d
* 系统设置界面 /jin123d


### 04/19
* 设备管理界面 /jin123d
* 设备管理第二个界面 /jin123d
* 关于界面 /jin123d


### 04/18
* 添加设备最终版页面 /xm
* 添加设备提示用户如何进入配网模式最终版页面 /xm
* WIFI配网最终版页面 /xm
* 点击连接按钮之后，配网交互页面 50% /xm
* 个人中心三个界面 /jin123d

### 04/17
* 状态栏适配，主题的更新 /jin123d

### 04/16
* 完成了一个3.0通用的titleBar /jin123d

### 04/15
* 注册登录界面基本完成 /jin123d

### 04/14
* 修复了一个导致空指针的bug /jin123d
* 删除设备接口通调 /jin123d
* 改名设备接口调通 /jin123d
* 绑定设备接口调通 /jin123d
* 集成QQ X5内核 /jin123d
* 配网测试 /xm

### 04/13

* 局域网发现 /xm
* 广播wifi密码 /xm
* 配网界面Demo页面 /xm
* 联调绑定（不能强制绑定/绑定超时）/xm/jin123d
* 主界面更新 /xm
* 基础http请求搭建（get/post）,post方式由form表单更改为entity /jin123d
* 基础请求类,验证码/校验/注册/登录/获取设备 （完成）/jin123d
* 列表页Demo /jin123d




//开发问题汇总
* 1.fragment中使用intent传递数据,startActivity必须使用getActivity.startActivity();
* 2.fragment中监听返回数据直接使用startActivityForResult,不可使用getActivity.startActivityForResult,如果父级拦截掉,则需要重写父级onActivityResult以便将数据传递给fragment
* 3.微信登录点击没有反应，首先检查开放平台的参数是否正确
* 4.完整的form表单必须包含name/type
* 5.获取资源文件 ContextCompat.getColor
* 6.获取Locale.getDefault()
* 7.git删除 git rm -r -n --cached ".idea/"
    git rm -r --cached  ".idea/"
* 8.空格 &#160;
* 9.string下划线 <u>下划线</u>
* 10.退出登录时，使用singleTask启动主界面，然后主界面再onNewIntent启动登录界面，finish自己，保证除了登录界面以外的所有的Activity都关闭掉
* 11.字体问题 http://www.jianshu.com/p/69a05f99a9ff