package me.hekr.hummingbird.event;

import me.hekr.hummingbird.bean.FindDeviceBean;

/**
 * Created by hekr_xm on 2016/5/10.
 * 局域网socket建立
 **/
public class CreateSocketEvent {

    private FindDeviceBean findDeviceBean;

    public CreateSocketEvent(FindDeviceBean findDeviceBean) {
        this.findDeviceBean = findDeviceBean;
    }

    public FindDeviceBean getFindDeviceBean() {
        return findDeviceBean;
    }

    public void setFindDeviceBean(FindDeviceBean findDeviceBean) {
        this.findDeviceBean = findDeviceBean;
    }

    @Override
    public String toString() {
        return "CreateSocketEvent{" +
                "findDeviceBean=" + findDeviceBean +
                '}';
    }
}
