package me.hekr.hummingbird.event;


import me.hekr.hummingbird.bean.CtrlBean;

/**
 * Created by hekr_xm on 2016/4/27.
 * ws命令发送
 **/
public class WebServiceSendEvent {

    private int command;
    private CtrlBean ctrlBean;

    public WebServiceSendEvent(int command, CtrlBean ctrlBean) {
        this.command = command;
        this.ctrlBean = ctrlBean;
    }

    public int getCommand() {
        return command;
    }

    public void setCommand(int command) {
        this.command = command;
    }

    public CtrlBean getCtrlBean() {
        return ctrlBean;
    }

    public void setCtrlBean(CtrlBean ctrlBean) {
        this.ctrlBean = ctrlBean;
    }

    @Override
    public String toString() {
        return "WebServiceSendEvent{" +
                "command=" + command +
                ", ctrlBean=" + ctrlBean +
                '}';
    }
}
