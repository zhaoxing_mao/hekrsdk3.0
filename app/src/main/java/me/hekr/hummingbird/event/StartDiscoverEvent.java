package me.hekr.hummingbird.event;

/**
 * Created by hekr_xm on 2016/5/4.
 * 服务的开启与关闭
 **/
public class StartDiscoverEvent {
    private boolean isStart;

    public StartDiscoverEvent(boolean isStart) {
        this.isStart = isStart;
    }

    public boolean isStart() {
        return isStart;
    }

    public void setStart(boolean start) {
        isStart = start;
    }

    @Override
    public String toString() {
        return "StartDiscoverEvent{" +
                "isStart=" + isStart +
                '}';
    }
}
