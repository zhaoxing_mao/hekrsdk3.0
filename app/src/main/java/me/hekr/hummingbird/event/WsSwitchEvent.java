package me.hekr.hummingbird.event;

/**
 * Created by hekr_xm on 2016/5/25.
 **/
public class WsSwitchEvent {

    //1、用户退出当前账号
    private int status;

    public WsSwitchEvent(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "WsSwitchEvent{" +
                "status=" + status +
                '}';
    }
}
