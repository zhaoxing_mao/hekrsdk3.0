package me.hekr.hummingbird.event;

import me.hekr.hummingbird.bean.DeviceBean;

/**
 * Created by hekr_xm on 2016/4/28.
 * h5页面下载
 **/
public class DownLoadEvent {
    private DeviceBean deviceBean;

    public DownLoadEvent(DeviceBean deviceBean) {
        this.deviceBean = deviceBean;
    }

    public DeviceBean getDeviceBean() {
        return deviceBean;
    }

    public void setDeviceBean(DeviceBean deviceBean) {
        this.deviceBean = deviceBean;
    }

    @Override
    public String toString() {
        return "DownLoadEvent{" +
                "deviceBean=" + deviceBean +
                '}';
    }
}
