package me.hekr.hummingbird.event;


/**
 * Created by hekr_xm on 2016/4/27.
 * ws服务连接
 **/
public class ConnectWsEvent {

    private int command;

    public ConnectWsEvent(int command) {
        this.command = command;
    }

    public int getCommand() {
        return command;
    }

    public void setCommand(int command) {
        this.command = command;
    }

    @Override
    public String toString() {
        return "ConnectWsEvent{" +
                "command=" + command +
                '}';
    }
}
