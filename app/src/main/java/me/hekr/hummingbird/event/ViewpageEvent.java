package me.hekr.hummingbird.event;

/**
 * Created by hekr_jds on 5/23 0023.
 **/
public class ViewpageEvent {
    private int currentItem;
    private String userName;

    public ViewpageEvent(int currentItem, String userName) {
        this.currentItem = currentItem;
        this.userName = userName;
    }

    public int getCurrentItem() {
        return currentItem;
    }

    public void setCurrentItem(int currentItem) {
        this.currentItem = currentItem;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
