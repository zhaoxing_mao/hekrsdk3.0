package me.hekr.hummingbird.event;

/**
 * Created by hekr_jds on 4/21 0021.
 **/
public class LogoutEvent {
    private boolean logout;

    public boolean isLogout() {
        return logout;
    }

    public void setLogout(boolean logout) {
        this.logout = logout;
    }

    public LogoutEvent(boolean logout) {
        this.logout = logout;
    }
}
