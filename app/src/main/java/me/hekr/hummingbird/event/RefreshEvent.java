package me.hekr.hummingbird.event;

/**
 * Created by hekr_xm on 2016/3/23.
 **/
public class RefreshEvent {
    public static final int REFRESH_DEVICE = 1;
    public static final int REFRESH_USER = 2;
    private int refreshTag;

    /**
     * 默认为0<br>
     * 刷新用户为1<br>
     * 刷新设备为2<br>
     */
    public RefreshEvent(int refreshTag) {
        this.refreshTag = refreshTag;
    }

    public int getRefreshTag() {
        return refreshTag;
    }

    public void setRefreshTag(int refreshTag) {
        this.refreshTag = refreshTag;
    }
}
