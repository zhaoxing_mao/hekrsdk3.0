package me.hekr.hummingbird.event;

import me.hekr.hummingbird.bean.DeviceBean;

/**
 * Created by hekr_jds on 4/20 0020.
 **/
public class DeviceEvent {
    private DeviceBean deviceBean;

    public DeviceBean getDeviceBean() {
        return deviceBean;
    }

    public void setDeviceBean(DeviceBean deviceBean) {
        this.deviceBean = deviceBean;
    }

    @Override
    public String toString() {
        return "DeviceEvent{" +
                "deviceBean=" + deviceBean +
                '}';
    }

    public DeviceEvent(DeviceBean deviceBean) {
        this.deviceBean = deviceBean;
    }
}
