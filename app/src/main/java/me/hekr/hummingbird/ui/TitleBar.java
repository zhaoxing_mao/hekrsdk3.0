package me.hekr.hummingbird.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import me.hekr.hummingbird.R;

/**
 * Created by hekr_jds on 4/16 0016.
 **/
public class TitleBar extends AppBarLayout {
    private BackListener titleBackListener;
    private String mTitle;
    TextView textView;
    private Toolbar toolbar;

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.titleBar);
        mTitle = typedArray.getString(R.styleable.titleBar_TitleString);
        typedArray.recycle();
        initView();
    }

    private void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li;
        li = (LayoutInflater) getContext().getSystemService(infService);
        li.inflate(R.layout.layout_title, this, true);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        textView = (TextView) findViewById(R.id.tv_title);
        toolbar.setNavigationIcon(R.drawable.ic_left_black);
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (titleBackListener != null) {
                    titleBackListener.click();
                }
            }
        });
        textView.setText(mTitle);
    }

    public Toolbar getToolbar() {
        if (toolbar != null) {
            return toolbar;
        }
        return null;
    }


    /**
     * 返回键监听
     *
     * @param titleBackListener 定义返回事件
     */
    public void setBack(BackListener titleBackListener) {
        this.titleBackListener = titleBackListener;
    }

    /**
     * 设置标题
     *
     * @param title
     */
    public void setTitle(String title) {
        textView.setText(title);
    }


    /**
     * 按下返回键接口
     */
    public interface BackListener {
        void click();
    }
}
