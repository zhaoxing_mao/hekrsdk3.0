package me.hekr.hummingbird.ui;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.RectF;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.LinearInterpolator;

import java.util.ArrayList;

import me.hekr.hummingbird.util.DensityUtils;

public class ConfirmView extends View {

    //绘制√和×及快速旋转弧形的动画时间
    private static final long NORMAL_ANIMATION_DURATION = 350L;
    //绘制正常弧形的动画时间
    private static final long NORMAL_ANGLE_ANIMATION_DURATION = 1000L;
    //绘制偏移角度的动画时间
    private static final long NORMAL_CIRCLE_ANIMATION_DURATION = 2000L;
    //绘制×需要的path数量
    private static final int PATH_SIZE_TWO = 2;

    public static int STROKEN_WIDTH;

    private int[] colors = {0xFFBEF0E6, 0xFFBEF0E6, 0xFFBEF0E6, 0xFFBEF0E6, 0xFFBEF0E6, 0xFFBEF0E6};
    private int colorCursor = 0;

    private State mCurrentState = State.Success;

    private Path mSuccessPath;
    private PathMeasure mPathMeasure;
    private ArrayList<Path> mRenderPaths;
    private Paint mPaint;

    private Paint progressBottomPaint;
    private Paint failBottomPaint;
    private Paint failXPaint;
    private Paint failMiddlePaint;
    private Paint successBottomPaint;
    private Paint successPaint;
    private Paint successMiddlePaint;

    private int mCenterX;
    private int mCenterY;

    private int mRadius;
    private int mSignRadius;
    private float mPhare;
    private float mStartAngle;
    private float mEndAngle;
    private float mCircleAngle;
    private RectF oval;

    private ValueAnimator mPhareAnimator;
    private ValueAnimator mStartAngleAnimator;
    private ValueAnimator mEndAngleAnimator;
    private ValueAnimator mCircleAnimator;

    public ConfirmView(Context context) {
        this(context, null);
    }

    public ConfirmView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ConfirmView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        STROKEN_WIDTH= DensityUtils.dp2px(context,10.0f);

        mSuccessPath = new Path();
        mPathMeasure = new PathMeasure(mSuccessPath, false);
        mRenderPaths = new ArrayList<>();

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(0xFF4BC8C8);
        mPaint.setStrokeWidth(STROKEN_WIDTH);
        mPaint.setStrokeCap(Paint.Cap.ROUND);

        progressBottomPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        progressBottomPaint.setStyle(Paint.Style.STROKE);
        progressBottomPaint.setColor(0xFFBEF0E6);
        progressBottomPaint.setStrokeWidth(STROKEN_WIDTH);
        progressBottomPaint.setStrokeCap(Paint.Cap.ROUND);

        //浅灰外边环
        failBottomPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        failBottomPaint.setStyle(Paint.Style.STROKE);
        failBottomPaint.setColor(0xFFD3D3D3);
        failBottomPaint.setStrokeWidth(STROKEN_WIDTH);
        failBottomPaint.setStrokeCap(Paint.Cap.ROUND);

        //x
        failXPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        failXPaint.setStyle(Paint.Style.STROKE);
        failXPaint.setColor(0xFFFFFFFF);
        failXPaint.setStrokeWidth(DensityUtils.dp2px(context,5.0f));
        //failXPaint.setStrokeCap(Paint.Cap.ROUND);

        //深灰中间圆
        failMiddlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        failMiddlePaint.setStyle(Paint.Style.FILL);
        failMiddlePaint.setColor(0xFFACACAE);

        //浅绿外边环
        successBottomPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        successBottomPaint.setStyle(Paint.Style.STROKE);
        successBottomPaint.setColor(0xFFBEF0E6);
        successBottomPaint.setStrokeWidth(STROKEN_WIDTH);
        successBottomPaint.setStrokeCap(Paint.Cap.ROUND);

        //勾
        successPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        successPaint.setStyle(Paint.Style.STROKE);
        successPaint.setColor(0xFFFFFFFF);
        successPaint.setStrokeWidth(DensityUtils.dp2px(context,5.0f));
        //failXPaint.setStrokeCap(Paint.Cap.ROUND);

        //深绿中间圆
        successMiddlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        successMiddlePaint.setStyle(Paint.Style.FILL);
        successMiddlePaint.setColor(0xFF4BC8C8);

        oval = new RectF();
    }


    private void setPhare(float phare) {
        mPhare = phare;
        updatePhare();
        invalidate();
    }

    private void setStartAngle(float startAngle) {
        this.mStartAngle = startAngle;
        invalidate();
    }

    private void setEndAngle(float endAngle) {
        this.mEndAngle = endAngle;
        invalidate();
    }

    private void setCircleAngle(float circleAngle) {
        this.mCircleAngle = circleAngle;
        invalidate();
    }

    ///////////////////////////////////////////////////////////////////////////
    // Phare Animation
    ///////////////////////////////////////////////////////////////////////////

    public void startPhareAnimation() {
        if (mPhareAnimator == null) {
            mPhareAnimator = ValueAnimator.ofFloat(0.0F, 1.0F);
            mPhareAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float value = (Float) animation.getAnimatedValue();
                    setPhare(value);
                }
            });

            mPhareAnimator.setDuration(NORMAL_ANIMATION_DURATION);
            mPhareAnimator.setInterpolator(new LinearInterpolator());
        }
        mPhare = 0;
        mPhareAnimator.start();
    }

    public void stopPhareAnimation() {
        if (mPhareAnimator != null) {
            mPhareAnimator.end();
        }
    }

    private void updatePhare() {

        if (mSuccessPath != null) {

            switch (mCurrentState) {
                case Success: {
                    if (mPathMeasure.getSegment(0, mPhare * mPathMeasure.getLength(), mRenderPaths.get(0), true)) {
                        mRenderPaths.get(0).rLineTo(0, 0);
                    }
                }
                break;
                case Fail: {
                    //i = 0,画一半，i=1,画另一半
                    float seg = 1.0F / PATH_SIZE_TWO;

                    for (int i = 0; i < PATH_SIZE_TWO; i++) {
                        float offset = mPhare - seg * i;
                        offset = offset < 0 ? 0 : offset;
                        offset *= PATH_SIZE_TWO;
                        Log.d("i:" + i + ",seg:" + seg, "offset:" + offset + ", mPhare:" + mPhare + ", size:" + PATH_SIZE_TWO);
                        boolean success = mPathMeasure.getSegment(0, offset * mPathMeasure.getLength(), mRenderPaths.get(i), true);

                        if (success) {
                            mRenderPaths.get(i).rLineTo(0, 0);
                        }

                        mPathMeasure.nextContour();
                    }
                    mPathMeasure.setPath(mSuccessPath, false);
                }
                break;
            }

        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Progressing Circle
    ///////////////////////////////////////////////////////////////////////////

    public void startCircleAnimation() {
        if (mCircleAnimator == null || mStartAngleAnimator == null || mEndAngleAnimator == null) {
            initAngleAnimation();
        }

        mStartAngleAnimator.setDuration(NORMAL_ANGLE_ANIMATION_DURATION);
        mEndAngleAnimator.setDuration(NORMAL_ANGLE_ANIMATION_DURATION);
        mCircleAnimator.setDuration(NORMAL_CIRCLE_ANIMATION_DURATION);
        mStartAngleAnimator.start();
        mEndAngleAnimator.start();
        mCircleAnimator.start();
    }


    private void initAngleAnimation() {

        mStartAngleAnimator = ValueAnimator.ofFloat(0.0F, 1.0F);
        mEndAngleAnimator = ValueAnimator.ofFloat(0.0F, 1.0F);
        mCircleAnimator = ValueAnimator.ofFloat(0.0F, 1.0F);

        mStartAngleAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (Float) animation.getAnimatedValue();
                setStartAngle(value);
            }
        });
        mEndAngleAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (Float) animation.getAnimatedValue();
                setEndAngle(value);
            }
        });

        mStartAngleAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

                if (mCurrentState == State.Progressing) {
                    if (mEndAngleAnimator != null) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEndAngleAnimator.start();
                            }
                        }, 400L);
                    }
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (mCurrentState != State.Progressing && mEndAngleAnimator != null && !mEndAngleAnimator.isRunning() && !mEndAngleAnimator.isStarted()) {
                    startPhareAnimation();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });

        mEndAngleAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (mStartAngleAnimator != null) {
                    if (mCurrentState != State.Progressing) {
                        mStartAngleAnimator.setDuration(NORMAL_ANIMATION_DURATION);
                    }
                    colorCursor++;
                    if (colorCursor >= colors.length) colorCursor = 0;
                    //mPaint.setColor(colors[colorCursor]);
                    mStartAngleAnimator.start();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });

        mCircleAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                setCircleAngle(value);
            }
        });


        mStartAngleAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        mEndAngleAnimator.setInterpolator(new AccelerateDecelerateInterpolator());


        mCircleAnimator.setInterpolator(new LinearInterpolator());
        mCircleAnimator.setRepeatCount(-1);
    }

    public void animatedWithState(State state) {

        if (mCurrentState != state) {
            mCurrentState = state;
            if (mPhareAnimator != null && mPhareAnimator.isRunning()) {
                stopPhareAnimation();
            }
            switch (state) {
                case Fail:
                case Success:
                    updatePath();
                    if (mCircleAnimator != null && mCircleAnimator.isRunning()) {
                        mCircleAngle = (Float) mCircleAnimator.getAnimatedValue();
                        mCircleAnimator.end();
                    }

                    if ((mStartAngleAnimator == null || !mStartAngleAnimator.isRunning() || !mStartAngleAnimator.isStarted()) &&
                            (mEndAngleAnimator == null || !mEndAngleAnimator.isRunning() || !mEndAngleAnimator.isStarted())) {
                        mStartAngle = 360;
                        mEndAngle = 0;
                        startPhareAnimation();
                    }

                    break;
                case Progressing:
                    mCircleAngle = 0;
                    startCircleAnimation();
                    break;
            }
        }

    }

    private void updatePath() {

        int offset = (int) (mSignRadius * 0.15F);
        mRenderPaths.clear();

        switch (mCurrentState) {
            case Success:
                mSuccessPath.reset();
                mSuccessPath.moveTo(mCenterX - mSignRadius, mCenterY + offset);
                mSuccessPath.lineTo(mCenterX - offset, mCenterY + mSignRadius - offset);
                mSuccessPath.lineTo(mCenterX + mSignRadius, mCenterY - mSignRadius + offset);
                mRenderPaths.add(new Path());
                break;
            case Fail:
                mSuccessPath.reset();
                float failRadius = mSignRadius * 0.8F;
                mSuccessPath.moveTo(mCenterX - failRadius, mCenterY - failRadius);
                mSuccessPath.lineTo(mCenterX + failRadius, mCenterY + failRadius);
                mSuccessPath.moveTo(mCenterX + failRadius, mCenterY - failRadius);
                mSuccessPath.lineTo(mCenterX - failRadius, mCenterY + failRadius);
                for (int i = 0; i < PATH_SIZE_TWO; i++) {
                    mRenderPaths.add(new Path());
                }
                break;
            default:
                mSuccessPath.reset();
        }

        mPathMeasure.setPath(mSuccessPath, false);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mCenterX = w / 2;
        mCenterY = h / 2;

        mRadius = mCenterX > mCenterY ? mCenterY : mCenterX;
        mSignRadius = (int) (mRadius * 0.55F);

        int realRadius = mRadius - (STROKEN_WIDTH / 2);

        oval.left = mCenterX - realRadius;
        oval.top = mCenterY - realRadius;
        oval.right = mCenterX + realRadius;
        oval.bottom = mCenterY + realRadius;

        updatePath();
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        switch (mCurrentState) {
            case Fail:
                failMiddleCircle(canvas);
                for (int i = 0; i < PATH_SIZE_TWO; i++) {
                    Path p = mRenderPaths.get(i);
                    if (p != null) {
                        canvas.drawPath(p, failXPaint);
                    }
                }
                failBottomCircle(canvas);
                //drawCircle(canvas);
                break;
            case Success:
                successMiddleCircle(canvas);
                Path p = mRenderPaths.get(0);
                if (p != null) {
                    canvas.drawPath(p, successPaint);
                }
                successBottomCircle(canvas);
                //drawCircle(canvas);
                break;
            case Progressing:
                drawProgressBottomCircle(canvas);
                drawCircle(canvas);
                break;
        }

    }

    private void drawProgressBottomCircle(Canvas canvas) {
        canvas.drawArc(oval, 0.0f, 360.0f, false, progressBottomPaint);
    }

    private void failBottomCircle(Canvas canvas) {
        canvas.drawArc(oval, 0.0f, 360.0f, false, failBottomPaint);
    }

    private void failMiddleCircle(Canvas canvas) {
        //Log.i("failMiddleCircle","mCenterX:"+mCenterX+">>>mCenterY:"+mCenterY+">>>mRadius:"+mRadius+">>>STROKEN_WIDTH:"+STROKEN_WIDTH);
        canvas.drawCircle(mCenterX, mCenterY, mRadius-STROKEN_WIDTH, failMiddlePaint);
    }

    private void successBottomCircle(Canvas canvas) {
        canvas.drawArc(oval, 0.0f, 360.0f, false, successBottomPaint);
    }

    private void successMiddleCircle(Canvas canvas) {
        //Log.i("failMiddleCircle","mCenterX:"+mCenterX+">>>mCenterY:"+mCenterY+">>>mRadius:"+mRadius+">>>STROKEN_WIDTH:"+STROKEN_WIDTH);
        canvas.drawCircle(mCenterX, mCenterY, mRadius-STROKEN_WIDTH, successMiddlePaint);
    }

    private void drawCircle(Canvas canvas) {
        float offsetAngle = mCircleAngle * 360;
        float startAngle = mEndAngle * 360;
        float sweepAngle = mStartAngle * 360;

        if (startAngle == 360)
            startAngle = 0;
        sweepAngle = sweepAngle - startAngle;
        startAngle += offsetAngle;

        if (sweepAngle < 0)
            sweepAngle = 1;

        canvas.drawArc(oval, startAngle, sweepAngle, false, mPaint);
    }

    public enum State {
        Success, Fail, Progressing
    }
}
