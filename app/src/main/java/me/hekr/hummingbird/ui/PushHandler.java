package me.hekr.hummingbird.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.bean.OAuthRequestBean;
import me.hekr.hummingbird.ui.CircleImageView;

import com.litesuits.common.assist.Toastor;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import me.hekr.hummingbird.R;

/**
 * Created by hekr_xm on 2016/5/11.
 **/
public class PushHandler {

    private Context context;
    private DisplayImageOptions options;
    private Toastor toastor;

    public PushHandler(Context context) {
        this.context = context;
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)//设置为RGB565比起默认的ARGB_8888要节省大量的内存
                .delayBeforeLoading(100)//载入图片前稍做延时可以提高整体滑动的流畅度
                .build();

        toastor = new Toastor(context);
    }

    public void messageHandler(final HekrUserAction hekrUserAction) {
        hekrUserAction.getOAuthInfoRequest("", 0, 1, "", new HekrUser.GetOauthInfoListener() {
            @Override
            public void getOauthInfoSuccess(final List<OAuthRequestBean> lists) {
                if (lists != null && !lists.isEmpty()) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    View view = View.inflate(context, R.layout.dialog_auth_tip, null);
                    CircleImageView circleImageView = (CircleImageView) view.findViewById(R.id.auth_user_icon);

                    if (lists.get(0).getGranteeAvater() != null && !TextUtils.isEmpty(lists.get(0).getGranteeAvater().getSmall())) {
                        ImageLoader.getInstance().displayImage(lists.get(0).getGranteeAvater().getSmall(), circleImageView, options);
                    }

                    TextView auth_user_name = (TextView) view.findViewById(R.id.auth_user_name);
                    if (!TextUtils.isEmpty(lists.get(0).getGranteeName())) {
                        StringBuilder userStr = new StringBuilder();
                        auth_user_name.setText(userStr.append(context.getResources().getString(R.string.push_auth_user_tip)).append(lists.get(0).getGranteeName()));
                    }

                    TextView auth_device_name = (TextView) view.findViewById(R.id.auth_device_name);
                    if (!TextUtils.isEmpty(lists.get(0).getDeviceName())) {
                        StringBuilder deviceStr = new StringBuilder();
                        auth_device_name.setText(deviceStr.append(context.getResources().getString(R.string.push_auth_device_tip)).append(lists.get(0).getDevTid()));
                    }

                    builder.setView(view)
                            .setMessage(context.getString(R.string.please_detail_request))
                            .setNegativeButton(context.getResources().getString(R.string.push_auth_disagree_tip), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    hekrUserAction.refuseOAuth(lists.get(0).getDevTid(), lists.get(0).getCtrlKey(), lists.get(0).getGrantee(), lists.get(0).getRegisterId(), new HekrUser.RefuseOAuthListener() {
                                        @Override
                                        public void refuseOauthSuccess() {
                                            messageHandler(hekrUserAction);
                                        }

                                        @Override
                                        public void refuseOauthFail(int errorCode) {
                                            toastor.showLongToast(context.getResources().getString(R.string.push_auth_disagree_fail_tip));
                                        }
                                    });
                                }
                            })
                            .setPositiveButton(context.getResources().getString(R.string.push_auth_agree_tip), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    hekrUserAction.agreeOAuth(lists.get(0).getDevTid(), lists.get(0).getCtrlKey(), lists.get(0).getRegisterId(), new HekrUser.AgreeOauthListener() {
                                        @Override
                                        public void AgreeOauthSuccess() {
                                            messageHandler(hekrUserAction);
                                        }

                                        @Override
                                        public void AgreeOauthFail(int errorCode) {
                                            toastor.showLongToast(context.getResources().getString(R.string.push_auth_agree_fail_tip));
                                        }
                                    });
                                }
                            }).create().show();
                }
            }

            @Override
            public void getOauthInfoFail(int errorCode) {

            }
        });
    }
}
