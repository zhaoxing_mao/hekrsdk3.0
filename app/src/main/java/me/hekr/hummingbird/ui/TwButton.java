//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package me.hekr.hummingbird.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import java.lang.ref.WeakReference;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.common.CommonUtils;

public class TwButton extends ImageView {
    final WeakReference<Activity> activityRef;
    volatile TwitterAuthClient authClient;
    OnClickListener onClickListener;
    Callback<TwitterSession> callback;

    public TwButton(Context context) {
        this(context, (AttributeSet) null);
    }

    public TwButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public TwButton(Context context, AttributeSet attrs, int defStyle) {
        this(context, attrs, defStyle, (TwitterAuthClient) null);
    }

    TwButton(Context context, AttributeSet attrs, int defStyle, TwitterAuthClient authClient) {
        super(context, attrs, defStyle);
        this.activityRef = new WeakReference(this.getActivity());
        this.authClient = authClient;
        this.checkTwitterCoreAndEnable();
        super.setOnClickListener(new TwButton.LoginClickListener());
    }


    public void setCallback(Callback<TwitterSession> callback) {
        if (callback == null) {
            throw new IllegalArgumentException("Callback cannot be null");
        } else {
            this.callback = callback;
        }
    }

    public Callback<TwitterSession> getCallback() {
        return this.callback;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == this.getTwitterAuthClient().getRequestCode()) {
            this.getTwitterAuthClient().onActivityResult(requestCode, resultCode, data);
        }

    }

    protected Activity getActivity() {
        if (this.getContext() instanceof Activity) {
            return (Activity) this.getContext();
        } else if (this.isInEditMode()) {
            return null;
        } else {
            throw new IllegalStateException("TwitterLoginButton requires an activity. Override getActivity to provide the activity for this button.");
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    TwitterAuthClient getTwitterAuthClient() {
        if (this.authClient == null) {
            Class var1 = TwButton.class;
            synchronized (TwButton.class) {
                if (this.authClient == null) {
                    this.authClient = new TwitterAuthClient();
                }
            }
        }

        return this.authClient;
    }

    private void checkTwitterCoreAndEnable() {
        if (!this.isInEditMode()) {
            try {
                TwitterCore.getInstance();
            } catch (IllegalStateException var2) {
                Fabric.getLogger().e("Twitter", var2.getMessage());
                this.setEnabled(false);
            }

        }
    }

    private class LoginClickListener implements OnClickListener {
        private LoginClickListener() {
        }

        public void onClick(View view) {
            this.checkCallback(TwButton.this.callback);
            this.checkActivity((Activity) TwButton.this.activityRef.get());
            TwButton.this.getTwitterAuthClient().authorize((Activity) TwButton.this.activityRef.get(), TwButton.this.callback);
            if (TwButton.this.onClickListener != null) {
                TwButton.this.onClickListener.onClick(view);
            }

        }

        private void checkCallback(Callback callback) {
            if (callback == null) {
                CommonUtils.logOrThrowIllegalStateException("Twitter", "Callback must not be null, did you call setCallback?");
            }

        }

        private void checkActivity(Activity activity) {
            if (activity == null || activity.isFinishing()) {
                CommonUtils.logOrThrowIllegalStateException("Twitter", "TwitterLoginButton requires an activity. Override getActivity to provide the activity for this button.");
            }

        }
    }
}
