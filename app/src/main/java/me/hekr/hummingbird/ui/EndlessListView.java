package me.hekr.hummingbird.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.litesuits.common.assist.Toastor;

;

public class EndlessListView extends ListView implements OnScrollListener {

    private boolean isLoading = false;
    private boolean isLoadFlag = false;
    private EndlessListener listener;
    private View footerView;
    private Context context;

    public EndlessListView(Context context) {
        super(context);
        setOnScrollListener(this);
        this.context = context;
    }

    public EndlessListView(Context context, AttributeSet arrs) {
        super(context, arrs);
        setOnScrollListener(this);
        this.context = context;
    }

    public EndlessListView(Context context, AttributeSet arrs, int defStyle) {
        super(context, arrs, defStyle);
        this.context = context;
    }

    public void setLoadingView(int resId) {
        LayoutInflater inflater = (LayoutInflater) super.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        footerView = (View) inflater.inflate(resId, null);
        addFooterView(footerView);
    }

    public void setListener(EndlessListener listener) {
        this.listener = listener;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {

        if (getAdapter() == null) {
            return;
        }

        if (getAdapter().getCount() == 0) {
            return;
        }

        int lastItemIndex = firstVisibleItem + visibleItemCount;
        if (lastItemIndex >= totalItemCount && !isLoading && isLoadFlag) {
            removeFooterView(footerView);
            addFooterView(footerView);
            isLoading = true;
            listener.loadData();
        }

    }

    public void updateLoadingStatus() {
        this.removeFooterView(footerView);
        isLoading = false;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // TODO Auto-generated method stub

    }

    public boolean isLoadFlag() {
        return isLoadFlag;
    }

    public void setLoadFlag(boolean loadFlag) {
        isLoadFlag = loadFlag;
    }


    public interface EndlessListener {
        void loadData();
    }
}
