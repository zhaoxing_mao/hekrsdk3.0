/*
 * Copyright (C) 2015 Paul Burke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.hekr.hummingbird.itemTouchHelper;

import android.graphics.Canvas;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;


public class FolderItemTouchHelperCallback extends ItemTouchHelper.Callback {

    public static final float ALPHA_FULL = 1.0f;
    private static final String TAG = "ItemTouchHelperCallback";
    private OnStartDragListener mOnStartDragListener;

    private final FolderItemTouchMoveListener mAdapter;

    public FolderItemTouchHelperCallback(FolderItemTouchMoveListener adapter, OnStartDragListener onStartDragListener) {
        mAdapter = adapter;
        mOnStartDragListener = onStartDragListener;
    }


    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

    }


    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
// Set movement flags based on the layout manager
        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            final int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
            final int swipeFlags = 0;
            return makeMovementFlags(dragFlags, swipeFlags);
        } else {
            final int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
            final int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
            return makeMovementFlags(dragFlags, swipeFlags);
        }

        //return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, final RecyclerView.ViewHolder source, final RecyclerView.ViewHolder target) {
        if (source == target) {
            return false;
        }
        return true;
    }


    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            View itemView = viewHolder.itemView;
            final float alpha = ALPHA_FULL - Math.abs(dX) / (float) itemView.getWidth();
            itemView.setAlpha(alpha);
        }
    }


    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        switch (actionState) {
            case ItemTouchHelper.ACTION_STATE_DRAG:
                mOnStartDragListener.onStartDrag(viewHolder);
                if (viewHolder instanceof ItemTouchHelperViewHolderListener) {
                    // Let the view holder know that this item is being moved or dragged
                    ItemTouchHelperViewHolderListener itemViewHolder = (ItemTouchHelperViewHolderListener) viewHolder;
                    itemViewHolder.onItemSelected();
                }
                break;
            case ItemTouchHelper.ACTION_STATE_IDLE:
                //this.viewHolder = viewHolder;
                break;

        }
        super.onSelectedChanged(viewHolder, actionState);
    }


    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);
        FolderItemTouchMoveListener mItemtouch = mAdapter;
        mItemtouch.onItemMoveOver(viewHolder, viewHolder.getAdapterPosition());
        mOnStartDragListener.onEndDrag(viewHolder);
        if (viewHolder instanceof ItemTouchHelperViewHolderListener) {
            // Tell the view holder it's time to restore the idle state
            ItemTouchHelperViewHolderListener itemViewHolder = (ItemTouchHelperViewHolderListener) viewHolder;
            itemViewHolder.onItemClear();
            mOnStartDragListener.onEndDrag(viewHolder);
        }
    }


}