

package me.hekr.hummingbird.itemTouchHelper;

public interface ItemTouchHelperViewHolderListener {
    //被选中后添加背景
    void onItemSelected();

    //清除背景
    void onItemClear();
}