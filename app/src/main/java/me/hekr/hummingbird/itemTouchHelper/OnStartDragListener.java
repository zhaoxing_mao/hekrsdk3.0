package me.hekr.hummingbird.itemTouchHelper;

import android.support.v7.widget.RecyclerView;

public interface OnStartDragListener {

    /**
     * 开始拖拽
     */

    void onStartDrag(RecyclerView.ViewHolder viewHolder);

    /**
     * 结束拖拽
     */

    void onEndDrag(RecyclerView.ViewHolder viewHolder);
}