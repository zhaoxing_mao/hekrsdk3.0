package me.hekr.hummingbird.itemTouchHelper;

import android.support.v7.widget.RecyclerView;

/**
 * Created by jin123d on 2016/3/3.
 */
public interface FolderItemTouchMoveListener {
    //拖拽  初始的viewHolder
    void onItemMoveOver(RecyclerView.ViewHolder viewHolder, int fromPosition);
}
