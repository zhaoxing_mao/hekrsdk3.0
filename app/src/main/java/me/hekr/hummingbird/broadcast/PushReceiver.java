package me.hekr.hummingbird.broadcast;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import me.hekr.hummingbird.action.HekrUserAction;

import com.igexin.sdk.PushConsts;
import com.igexin.sdk.PushManager;
import com.igexin.sdk.Tag;
import com.litesuits.common.assist.Toastor;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.activity.MainActivity;
import me.hekr.hummingbird.event.RefreshEvent;

public class PushReceiver extends BroadcastReceiver {

    private static final String TAG = "PushReceiver";
    private HekrUserAction hekrUserAction;
    private Toastor toastor;

    /**
     * 应用未启动, 个推 service已经被唤醒,保存在该时间段内离线消息(此时 GetuiSdkDemoActivity.tLogView == null)
     */
    public NotificationManager mNotificationManager;

    public NotificationCompat.Builder mBuilder;

    /**
     * Notification的ID
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle bundle = null;
        if (intent != null) {
            hekrUserAction = HekrUserAction.getInstance(context);
            toastor = new Toastor(context.getApplicationContext());
            bundle = intent.getExtras();
            Log.d(TAG, "onReceive() action=" + bundle.getInt("action"));

            mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            initNotify(context);
        }
        if (bundle != null) {
            switch (bundle.getInt(PushConsts.CMD_ACTION)) {
                case PushConsts.GET_MSG_DATA:
                    // 获取透传数据
                    // String appid = bundle.getString("appid");
                    byte[] payload = bundle.getByteArray("payload");

                    String taskid = bundle.getString("taskid");
                    String messageid = bundle.getString("messageid");

                    Log.d(TAG, "taskid:" + taskid);
                    Log.d(TAG, "messageid:" + messageid);

                    // smartPush第三方回执调用接口，actionid范围为90000-90999，可根据业务场景执行
                    boolean result = PushManager.getInstance().sendFeedbackMessage(context, taskid, messageid, 90001);
                    System.out.println("第三方回执接口调用" + (result ? "成功" : "失败"));
                    Log.i(TAG, "result:" + result);

                    if (payload != null) {
                        String data = new String(payload);
                        Log.i(TAG, "推送数据:" + data);
                        //Toast.makeText(context,"payload:"+data,Toast.LENGTH_LONG).show();

                        if (!TextUtils.isEmpty(data)) {
                            try {
                                JSONObject jsonObject = new JSONObject(data);
                                if (jsonObject.has("pushType")) {
                                    switch (jsonObject.getString("pushType")) {
                                        case "DEVICE_ALERT":

                                            break;
                                        case "REVERSE_AUTHORIZATION":
                                            //new PushHandler(context).messageHandler(hekrUserAction);
                                            showIntentActivityNotify(context);
                                            break;
                                        case "REVERSE_AUTHORIZATION_RESULT":
                                            if(jsonObject.has("devTid")&&jsonObject.has("result")){
                                                //授权用户允许授权
                                                if(TextUtils.equals(jsonObject.getString("result"),"ACCEPT")) {
                                                    showSuccessAuthNotify(context, jsonObject.getString("devTid"), 1);
                                                    EventBus.getDefault().post(new RefreshEvent(RefreshEvent.REFRESH_DEVICE));
                                                }
                                                //授权用户拒绝授权
                                                if(TextUtils.equals(jsonObject.getString("result"),"REJECT")) {
                                                    showSuccessAuthNotify(context, jsonObject.getString("devTid"), 2);
                                                }
                                            }
                                            break;
                                        case "INFORMATION":

                                            break;
                                    }
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                    break;

                case PushConsts.GET_CLIENTID:
                    // 获取ClientID(CID)
                    final String cid = bundle.getString("clientid");
                    Log.i(TAG, "cid:" + cid);

                    if (!TextUtils.isEmpty(cid)) {
                        String[] tags = new String[]{"00000000000"};
                        Tag[] tagParam = new Tag[tags.length];

                        for (int i = 0; i < tags.length; i++) {
                            Tag t = new Tag();
                            t.setName(tags[i]);
                            tagParam[i] = t;
                        }
                        int i = PushManager.getInstance().setTag(context, tagParam);
                        String text = "设置标签失败,未知异常";

                        switch (i) {
                            case PushConsts.SETTAG_SUCCESS:
                                text = "设置标签成功";
                                break;

                            case PushConsts.SETTAG_ERROR_COUNT:
                                text = "设置标签失败, tag数量过大, 最大不能超过200个";
                                break;

                            case PushConsts.SETTAG_ERROR_FREQUENCY:
                                text = "设置标签失败, 频率过快, 两次间隔应大于1s";
                                break;

                            case PushConsts.SETTAG_ERROR_REPEAT:
                                text = "设置标签失败, 标签重复";
                                break;

                            case PushConsts.SETTAG_ERROR_UNBIND:
                                text = "设置标签失败, 服务未初始化成功";
                                break;

                            case PushConsts.SETTAG_ERROR_EXCEPTION:
                                text = "设置标签失败, 未知异常";
                                break;

                            case PushConsts.SETTAG_ERROR_NULL:
                                text = "设置标签失败, tag 为空";
                                break;

                            default:
                                break;
                        }
                        Log.i(TAG, "text:" + text);
                        boolean flag = false;
                        if (hekrUserAction != null) {
                            Log.i(TAG, "Alias:" + hekrUserAction.getUserId());
                            flag = PushManager.getInstance().bindAlias(context, hekrUserAction.getUserId());
                        }
                        if (flag) {
                            Log.i(TAG, "bindAlias:" + true);
                        }
                    }
                    break;

                case PushConsts.THIRDPART_FEEDBACK:
                    break;

                default:
                    break;
            }
        }
    }

    /**
     * 点击推送，弹出授权对话框
     */
    public void showIntentActivityNotify(Context context) {
        // Notification.FLAG_ONGOING_EVENT --设置常驻 Flag;Notification.FLAG_AUTO_CANCEL 通知栏上点击此通知后自动清除此通知
        //notification.flags = Notification.FLAG_AUTO_CANCEL; //在通知栏上点击此通知后自动清除此通知

        mBuilder.setAutoCancel(true)//点击后让通知将消失
                .setContentText(context.getResources().getString(R.string.receiver_push_auth_have_a_message_tip));
        //点击的意图ACTION是跳转到Intent
        Intent resultIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);
        mNotificationManager.notify(1 + (int) (Math.random() * 5000000), mBuilder.build());
    }

    //被授权用户接收到授权用户允许或者拒绝之后的提示
    public void showSuccessAuthNotify(Context context, String devTid,int type) {
        // Notification.FLAG_ONGOING_EVENT --设置常驻 Flag;Notification.FLAG_AUTO_CANCEL 通知栏上点击此通知后自动清除此通知
        //notification.flags = Notification.FLAG_AUTO_CANCEL; //在通知栏上点击此通知后自动清除此通知

        if(type==1) {
            mBuilder.setAutoCancel(true)//点击后让通知将消失
                    .setContentText(context.getResources().getString(R.string.receiver_push_auth_tip));
            //点击的意图ACTION是跳转到Intent
            StringBuilder str = new StringBuilder();
            str.append(context.getResources().getString(R.string.receiver_push_auth_tip))
                    .append(context.getResources().getString(R.string.push_auth_user_tip))
                    .append(hekrUserAction.getUserCache().getLastName())
                    .append(context.getResources().getString(R.string.push_auth_device_tip))
                    .append(devTid);
            toastor.showSingleLongToast(String.valueOf(str));
            mNotificationManager.notify(1 + (int) (Math.random() * 5000000), mBuilder.build());
        }

        if(type==2){
            mBuilder.setAutoCancel(true)//点击后让通知将消失
                    .setContentText(context.getResources().getString(R.string.receiver_push_auth_disagree_tip));
            //点击的意图ACTION是跳转到Intent
            StringBuilder str = new StringBuilder();
            str.append(context.getResources().getString(R.string.receiver_push_auth_disagree_tip))
                    .append(context.getResources().getString(R.string.push_auth_user_tip))
                    .append(hekrUserAction.getUserCache().getLastName())
                    .append(context.getResources().getString(R.string.push_auth_device_tip))
                    .append(devTid);
            toastor.showSingleLongToast(String.valueOf(str));
            mNotificationManager.notify(1 + (int) (Math.random() * 5000000), mBuilder.build());
        }
    }

    /**
     * 初始化通知栏
     */
    private void initNotify(Context context) {
        mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setContentIntent(getDefaultIntent(context, Notification.FLAG_AUTO_CANCEL))
                .setSmallIcon(R.mipmap.appicon)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.appicon))
                .setWhen(System.currentTimeMillis())//通知产生的时间，会在通知信息里显示
                .setPriority(Notification.PRIORITY_DEFAULT)//设置该通知优先级
                .setOngoing(false)//ture，设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)
                .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)//向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合：
                // Notification.DEFAULT_ALL  Notification.DEFAULT_SOUND 添加声音 // requires VIBRATE permission
                .setSmallIcon(R.mipmap.ic_launcher);
    }

    public PendingIntent getDefaultIntent(Context context, int flags) {
        return PendingIntent.getActivity(context, 1, new Intent(), flags);
    }
}
