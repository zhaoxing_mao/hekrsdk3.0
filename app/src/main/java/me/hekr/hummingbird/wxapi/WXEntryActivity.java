package me.hekr.hummingbird.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;


import cz.msebera.android.httpclient.Header;
import me.hekr.hummingbird.application.MyApplication;
import me.hekr.hummingbird.event.AuthCodeEvent;
import me.hekr.hummingbird.util.AuthConstant;
import me.hekr.hummingbird.util.BaseHttpUtil;

import com.alibaba.fastjson.JSON;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import org.greenrobot.eventbus.EventBus;


/**
 * updatee by hekr_xm on 2016/5/3.
 **/
public class WXEntryActivity extends Activity implements IWXAPIEventHandler {

    private static final String TAG = "WXEntryActivity";
    private IWXAPI api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = WXAPIFactory.createWXAPI(this, AuthConstant.WEIXIN_APP_ID, false);
        handleIntent(getIntent());
    }

    private void handleIntent(Intent paramIntent) {
        api.handleIntent(paramIntent, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntent(intent);
    }

    @Override
    public void onReq(BaseReq baseReq) {
        finish();
    }

    @Override
    public void onResp(BaseResp baseResp) {
        final String code = ((SendAuth.Resp) baseResp).code;
        Log.i(TAG, "code:" + code);
        EventBus.getDefault().post(new AuthCodeEvent(AuthCodeEvent.TYPE_WEIXIN, code));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
