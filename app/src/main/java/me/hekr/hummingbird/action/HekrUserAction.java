package me.hekr.hummingbird.action;


import android.content.Context;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.litesuits.android.log.Log;
import com.litesuits.common.assist.Base64;
import com.litesuits.common.utils.MD5Util;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import me.hekr.hummingbird.R;
import me.hekr.hummingbird.bean.BindDeviceBean;
import me.hekr.hummingbird.bean.DeviceBean;
import me.hekr.hummingbird.bean.DeviceStatusBean;
import me.hekr.hummingbird.bean.FileBean;
import me.hekr.hummingbird.bean.FirmwareBean;
import me.hekr.hummingbird.bean.FolderBean;
import me.hekr.hummingbird.bean.FolderListBean;
import me.hekr.hummingbird.bean.GroupBean;
import me.hekr.hummingbird.bean.JWTBean;
import me.hekr.hummingbird.bean.MOAuthBean;
import me.hekr.hummingbird.bean.NewsBean;
import me.hekr.hummingbird.bean.OAuthBean;
import me.hekr.hummingbird.bean.OAuthListBean;
import me.hekr.hummingbird.bean.OAuthRequestBean;
import me.hekr.hummingbird.bean.ProfileBean;
import me.hekr.hummingbird.bean.UserBean;
import me.hekr.hummingbird.bean.WeatherBeanResultsNow;
import me.hekr.hummingbird.event.WsSwitchEvent;
import me.hekr.hummingbird.util.BaseHttpUtil;
import me.hekr.hummingbird.util.ConstantsUtil;
import me.hekr.hummingbird.util.HekrCodeUtil;
import me.hekr.hummingbird.util.HekrHttpUtil;
import me.hekr.hummingbird.util.SpCache;

/**
 * Created by jin123d on 2016/4/7.
 * hekr_Android_SDK
 **/
public class HekrUserAction {
    /**
     * 用户注册类型
     * 1为手机号注册
     * 2为Email注册
     */
    public static final int REGISTER_TYPE_PHONE = 1;
    public static final int REGISTER_TYPE_EMAIL = 2;
    /**
     * 用户注册数据节点
     */
    public static final int REGISTER_NODE_ASIA = 3;
    public static final int REGISTER_NODE_AMERICA = 4;
    public static final int REGISTER_NODE_EUROPE = 5;
    private static final String TAG = "HekrUserAction";

    /**
     * 发送验证码类型
     */
    public static final int CODE_TYPE_REGISTER = 1;
    public static final int CODE_TYPE_RE_REGISTER = 2;
    public static final int CODE_TYPE_CHANGE_PHONE = 3;


    public static final int OAUTH_QQ = 1;
    public static final int OAUTH_WECHAT = 2;
    public static final int OAUTH_SINA = 3;
    public static final int OAUTH_TWITTER = 4;
    public static final int OAUTH_FACEBOOK = 5;
    public static final int OAUTH_GOOGLE_PLUS = 6;


    private static Context mContext;
    private String JWT_TOKEN = null;
    private String refresh_TOKEN = null;
    private String userId = null;
    private static volatile HekrUserAction instance = null;
    private static String pid;


    public static HekrUserAction getInstance(Context context) {
        if (instance == null) {
            synchronized (HekrUserAction.class) {
                if (instance == null) {
                    instance = new HekrUserAction(context);
                }
            }
        }
        return instance;
    }

    private HekrUserAction(Context context) {
        SpCache.init(context.getApplicationContext());
        mContext = context.getApplicationContext();
        //初始化的时候先读取token
        JWT_TOKEN = SpCache.getString("JWT_TOKEN", "");
        refresh_TOKEN = SpCache.getString("refresh_TOKEN", "");
        if (TextUtils.isEmpty(SpCache.getString("pid", ""))) {
            throw new NullPointerException("you show invoke HekrSDK.init() before you use it ");
        } else {
            pid = SpCache.getString("pid", "00000000000");
        }
        userId = TokenToUid();
    }


    /**
     * 3.1 发送短信验证码--1
     *
     * @param phoneNumber           手机号码
     * @param type                  验证码用途
     * @param getVerifyCodeListener 获取验证码回调
     */
    public void getVerifyCode(String phoneNumber, int type, final HekrUser.GetVerifyCodeListener getVerifyCodeListener) {
        String registerType = "register";
        switch (type) {
            case CODE_TYPE_REGISTER:
                registerType = "register";
                break;
            case CODE_TYPE_RE_REGISTER:
                registerType = "resetPassword";
                break;
            case CODE_TYPE_CHANGE_PHONE:
                registerType = "changePhone";
                break;
            default:
                registerType = "register";
                break;
        }

        BaseHttpUtil.getData(mContext, ConstantsUtil.UrlUtil.BASE_UAA_URL + ConstantsUtil.UrlUtil.UAA_GET_CODE_URL + phoneNumber + "&pid=" + pid + "&type=" + registerType, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int i, Header[] headers, byte[] bytes) {
                        //获取成功
                        getVerifyCodeListener.getVerifyCodeSuccess();
                    }

                    @Override
                    public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                        getVerifyCodeListener.getVerifyCodeFail(HekrCodeUtil.getErrorCode(i, bytes));
                    }

                }

        );
    }

    /**
     * 3.2 校验短信验证码--1
     *
     * @param phoneNumber             用户手机号码
     * @param code                    用户收到的验证码，长度为6位
     * @param checkVerifyCodeListener 验证码校验回调
     */
    public void checkVerifyCode(String phoneNumber, String code, final HekrUser.CheckVerifyCodeListener checkVerifyCodeListener) {
        BaseHttpUtil.getData(mContext, ConstantsUtil.UrlUtil.BASE_UAA_URL + ConstantsUtil.UrlUtil.UAA_CHECK_CODE_URL + phoneNumber + "&code=" + code, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                JSONObject checkObject = JSON.parseObject(new String(bytes));
                //获取成功
                checkVerifyCodeListener.checkVerifyCodeSuccess(checkObject.get("phoneNumber").toString(), checkObject.get("verifyCode").toString(), checkObject.get("token").toString(), checkObject.get("expireTime").toString());
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                //获取验证码失败
                checkVerifyCodeListener.checkVerifyCodeFail(HekrCodeUtil.getErrorCode(i, bytes));
            }
        });
    }


    /**
     * 3.3 使用手机号注册用户--1
     *
     * @param password         用户的密码
     * @param phoneNumber      用户注册手机号
     * @param token            校验验证码返回的注册TokenToken
     * @param registerListener 注册接口
     */
    public void registerByPhone(String phoneNumber, String password, String token, final HekrUser.RegisterListener registerListener) {
        /*String node = null;
        int dataCenterNode = 3;
        switch (dataCenterNode) {
            case REGISTER_NODE_ASIA:
                node = "ASIA";
                break;
            case REGISTER_NODE_AMERICA:
                node = "AMERICA";
                break;
            case REGISTER_NODE_EUROPE:
                node = "EUROPE";
                break;
            default:
                node = "ASIA";
                break;
        }*/
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pid", pid);
        jsonObject.put("password", password);
        jsonObject.put("phoneNumber", phoneNumber);
        jsonObject.put("token", token);
        BaseHttpUtil.postData(mContext, ConstantsUtil.UrlUtil.BASE_UAA_URL + ConstantsUtil.UrlUtil.UAA_REGISTER_URL + "phone", jsonObject.toString(), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                Log.d(TAG, "onSuccess: " + new String(bytes));
                registerListener.registerSuccess(JSON.parseObject(new String(bytes)).getString("uid"));
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                registerListener.registerFail(HekrCodeUtil.getErrorCode(i, bytes));
            }
        });
    }

    /**
     * 3.4 使用邮箱注册用户
     *
     * @param password         用户的密码
     * @param email            用户邮箱
     * @param registerListener 注册回调
     */
    public void registerByEmail(String email, String password, final HekrUser.RegisterListener registerListener) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pid", pid);
        jsonObject.put("password", password);
        jsonObject.put("email", email);
        BaseHttpUtil.postData(mContext, ConstantsUtil.UrlUtil.BASE_UAA_URL + ConstantsUtil.UrlUtil.UAA_REGISTER_URL + "email", jsonObject.toString(), new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int i, Header[] headers, byte[] bytes) {
                        Log.d(TAG, "onSuccess: " + new String(bytes));
                        registerListener.registerSuccess(JSON.parseObject(new String(bytes)).getString("uid"));
                    }

                    @Override
                    public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                        registerListener.registerFail(HekrCodeUtil.getErrorCode(i, bytes));
                    }
                }

        );
    }


    /**
     * 3.5 用户登录--1
     *
     * @param loginListener 回调接口
     */

    public void login(final String userName, final String passWord, final HekrUser.LoginListener loginListener) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", userName);
        jsonObject.put("password", passWord);
        jsonObject.put("pid", pid);
        jsonObject.put("clientType", "ANDROID");
        BaseHttpUtil.postData(mContext, ConstantsUtil.UrlUtil.BASE_UAA_URL + ConstantsUtil.UrlUtil.UAA_LOGIN_URL, jsonObject.toString(), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                JWTBean jwtBean = JSON.parseObject(new String(bytes), JWTBean.class);
                UserBean userBean = new UserBean(pid, userName, passWord, jwtBean.getAccess_token(), jwtBean.getRefresh_token());
                //把相关的用户信息保存下来
                setUserCache(userBean);
                setTokenWIthCache(jwtBean);
                //执行登录
                loginListener.loginSuccess(new String(bytes));

            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                loginListener.loginFail(HekrCodeUtil.getErrorCode(i, bytes));
            }
        });
    }

    /**
     * 3.6 重置密码--1
     */
    public void resetPwd(String phoneNumber, String verifyCode, String password, final HekrUser.ResetPwdListener resetPwdListener) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("phoneNumber", phoneNumber);
        jsonObject.put("verifyCode", verifyCode);
        jsonObject.put("pid", pid);
        jsonObject.put("password", password);
        BaseHttpUtil.postData(mContext, ConstantsUtil.UrlUtil.BASE_UAA_URL + ConstantsUtil.UrlUtil.UAA_RESET_PWD_URL + "phone", jsonObject.toString(), new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int i, Header[] headers, byte[] bytes) {
                        resetPwdListener.resetSuccess();

                    }

                    @Override
                    public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                        resetPwdListener.resetFail(HekrCodeUtil.getErrorCode(i, bytes));
                    }
                }

        );
    }


    /**
     * 3.7 修改密码--1
     *
     * @param newPassword       新密码
     * @param oldPassword       旧密码
     * @param changePwdListener 回调接口
     */
    public void changePassword(String newPassword, String oldPassword, final HekrUser.ChangePwdListener changePwdListener) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pid", pid);
        jsonObject.put("newPassword", newPassword);
        jsonObject.put("oldPassword", oldPassword);
        String url = ConstantsUtil.UrlUtil.BASE_UAA_URL + ConstantsUtil.UrlUtil.UAA_CHANGR_PWD_URL;
        postHekrData(url, jsonObject.toJSONString(), new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                changePwdListener.changeSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                changePwdListener.changeFail(errorCode);
            }
        });
    }


    /**
     * 3.8 修改用户手机号
     *
     * @param token                     验证码token
     * @param verifyCode                验证码
     * @param phoneNumber               手机号
     * @param changePhoneNumberListener 回调接口
     */
    public void changePhoneNumber(String token, String verifyCode, String phoneNumber, final HekrUser.ChangePhoneNumberListener changePhoneNumberListener) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pid", pid);
        jsonObject.put("token", token);
        jsonObject.put("verifyCode", verifyCode);
        jsonObject.put("phoneNumber", phoneNumber);
        String url = ConstantsUtil.UrlUtil.BASE_UAA_URL + ConstantsUtil.UrlUtil.UAA_CHANGE_PHONE_URL;
        postHekrData(url, jsonObject.toJSONString(), new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                changePhoneNumberListener.changePhoneNumberSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                changePhoneNumberListener.changePhoneNumberFail(errorCode);
            }
        });
    }

    /**
     * 3.9 发送重置密码邮件--1
     *
     * @param email                          邮箱
     * @param sendResetPasswordEmailListener 回调邮箱
     */
    public void sendResetPwdEmail(String email, final HekrUser.SendResetPwdEmailListener sendResetPasswordEmailListener) {
        BaseHttpUtil.getData(mContext, ConstantsUtil.UrlUtil.BASE_UAA_URL + "sendResetPasswordEmail?email=" + email + "&pid=" + pid, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                sendResetPasswordEmailListener.sendSuccess();
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                sendResetPasswordEmailListener.sendFail(HekrCodeUtil.getErrorCode(i, bytes));
            }
        });
    }


    /**
     * 3.10 重新发送确认邮件--1
     *
     * @param email               邮箱
     * @param reSendVerifiedEmail 回调接口
     */
    public void reSendVerifiedEmail(@NotNull String email, final HekrUser.ReSendVerifiedEmailListener reSendVerifiedEmail) {
        BaseHttpUtil.getData(mContext, ConstantsUtil.UrlUtil.BASE_UAA_URL + "resendVerifiedEmail?email=" + email + "&pid=" + pid, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                reSendVerifiedEmail.reSendVerifiedEmailSuccess();
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                reSendVerifiedEmail.reSendVerifiedEmailFail(HekrCodeUtil.getErrorCode(i, bytes));
            }
        });
    }


    /**
     * 3.11 发送修改邮箱邮件
     *
     * @param email                   邮箱
     * @param sendChangeEmailListener 厂家唯一ID
     */
    public void sendChangeEmailStep1Email(@NotNull String email, final HekrUser.SendChangeEmailListener sendChangeEmailListener) {
        //http://uaa.openapi.hekr.me/sendChangeEmailStep1Email?email=test@hekr.me&pid=01698862200
        String url = ConstantsUtil.UrlUtil.BASE_UAA_URL + ConstantsUtil.UrlUtil.UAA_SEND_CHANGE_EMAIL + email + "&pid=" + pid;
        getHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                sendChangeEmailListener.sendChangeEmailSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                sendChangeEmailListener.sendChangeEmailFail(errorCode);
            }
        });
    }


    /**
     * 3.12 刷新Access Token
     */
    private void refresh_token() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("refresh_TOKEN", getRefreshToken());
        if (!TextUtils.isEmpty(getRefreshToken())) {
            String url = ConstantsUtil.UrlUtil.BASE_UAA_URL + ConstantsUtil.UrlUtil.UAA_REFRESH_TOKEN;
            BaseHttpUtil.postData(mContext, url, jsonObject.toString(), new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                    JWTBean jwtBean = JSON.parseObject(new String(bytes), JWTBean.class);
                    setTokenWIthCache(jwtBean);
                }

                @Override
                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                    if (i == 401) {
                        //401直接通知退出APP
                        //EventBus.getDefault().post(new LogoutEvent(true));
                    }
                }
            });
        }
    }

    /**
     * 3.13 移动端OAuth --1
     *
     * @param type           OOGLE, WECHAT, QQ, SINA, FACEBOOK, TWITTER	OAuth账号类型
     * @param certificate    移动端OAuth之后返回的code，或者Twitter返回的access_token
     * @param moAuthListener 回调接口
     */
    public void OAuthLogin(int type, @NotNull String certificate, final HekrUser.MOAuthListener moAuthListener) {
        String auth_type = null;
        switch (type) {
            case OAUTH_QQ:
                auth_type = "QQ";
                break;
            case OAUTH_SINA:
                auth_type = "SINA";
                break;
            case OAUTH_WECHAT:
                auth_type = "WECHAT";
                break;
            case OAUTH_FACEBOOK:
                auth_type = "FACEBOOK";
                break;
            case OAUTH_TWITTER:
                auth_type = "TWITTER";
                break;
            case OAUTH_GOOGLE_PLUS:
                auth_type = "GOOGLE";
                break;
        }
        String url = ConstantsUtil.UrlUtil.BASE_UAA_URL + "MOAuth?type=" + auth_type + "&pid=" + pid + "&clientType=ANDROID&certificate=" + certificate;
        BaseHttpUtil.getData(mContext, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                JSONObject jsonObject = JSONObject.parseObject(new String(bytes));
                if (jsonObject.containsKey("uid")) {
                    MOAuthBean moAuthBean = JSONObject.parseObject(new String(bytes), MOAuthBean.class);
                    moAuthListener.mOAuthSuccess(moAuthBean);
                } else {
                    JWTBean jwtBean = JSONObject.parseObject(new String(bytes), JWTBean.class);
                    UserBean userBean = new UserBean(pid, "", "", jwtBean.getAccess_token(), jwtBean.getRefresh_token());
                    //把相关的用户信息保存下来
                    setUserCache(userBean);
                    setTokenWIthCache(jwtBean);
                    moAuthListener.mOAuthSuccess(jwtBean);
                }
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                moAuthListener.mOAuthFail(HekrCodeUtil.getErrorCode(i, bytes));
            }
        });

    }


    /**
     * 3.14 将OAuth账号和主账号绑定
     *
     * @param token             绑定token
     * @param bindOAuthListener 绑定接口。使用此接口之前必须登录！
     */
    public void bindOAuth(@NotNull String token, final HekrUser.BindOAuthListener bindOAuthListener) {
        //http://uaa.openapi.hekr.me/account/bind?token=qazxswedcvfrtgbnhyujmkilop&pid=01698862200
        String url = ConstantsUtil.UrlUtil.BASE_UAA_URL + "account/bind?token=" + token + "&pid=" + pid;
        getHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                bindOAuthListener.bindSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                bindOAuthListener.bindFail(errorCode);
            }
        });
    }

    /**
     * 3.15 解除OAuth账号和主账号的绑定关系
     *
     * @param type              类型 QQ 微博 微信
     * @param bindOAuthListener 回调接口
     */
    public void unbindOAuth(int type, final HekrUser.BindOAuthListener bindOAuthListener) {
        String auth_type = null;
        switch (type) {
            case OAUTH_QQ:
                auth_type = "QQ";
                break;
            case OAUTH_SINA:
                auth_type = "SINA";
                break;
            case OAUTH_WECHAT:
                auth_type = "WECHAT";
                break;
        }
        String url = ConstantsUtil.UrlUtil.BASE_UAA_URL + "unbind?type=" + auth_type + "&pid=" + pid;
        getHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                bindOAuthListener.bindSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                bindOAuthListener.bindFail(errorCode);
            }
        });

    }

    /**
     * 4.1.1 绑定设备
     *
     * @param bindDeviceBean     绑定设备Bean
     * @param bindDeviceListener 回调接口
     */
    public void bindDevice(BindDeviceBean bindDeviceBean, final HekrUser.BindDeviceListener bindDeviceListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.BIND_DEVICE;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("devTid", bindDeviceBean.getDevTid());
        jsonObject.put("bindKey", bindDeviceBean.getBindKey());
        jsonObject.put("deviceName", bindDeviceBean.getDeviceName());
        jsonObject.put("desc", bindDeviceBean.getDesc());

        postHekrData(url, jsonObject.toString(), new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                bindDeviceListener.bindDeviceSuccess(JSON.parseObject(object.toString(), DeviceBean.class));
            }

            @Override
            public void getFail(int errorCode) {
                bindDeviceListener.bindDeviceFail(errorCode);
            }
        });
    }


    /**
     * 4.1.2 列举设备列表--1
     *
     * @param getDevicesListener 回调接口
     */
    public void getDevices(HekrUser.GetDevicesListener getDevicesListener) {
        getDevices(null, null, getDevicesListener);
    }

    /**
     * 4.1.2 列举设备列表--1
     *
     * @param getDevicesListener 回调接口
     */
    public void getDevices(int page, int size, HekrUser.GetDevicesListener getDevicesListener) {
        getDevices(String.valueOf(page), String.valueOf(size), getDevicesListener);
    }


    /**
     * 4.1.2 列举设备列表--1
     *
     * @param getDevicesListener 回调接口
     */
    private void getDevices(String page, String size, final HekrUser.GetDevicesListener getDevicesListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.BIND_DEVICE;
        if (!TextUtils.isEmpty(page) && !TextUtils.isEmpty(size)) {
            url = url + "?page=" + page + "&size=" + size;
        }
        getHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                List<DeviceBean> lists = JSON.parseArray(object.toString(), DeviceBean.class);
                getDevicesListener.getDevicesSuccess(lists);
            }

            @Override
            public void getFail(int errorCode) {
                getDevicesListener.getDevicesFail(errorCode);
            }
        });
    }


    /**
     * 4.1.3 删除设备--1
     *
     * @param devTid        设备ID
     * @param bindKey       绑定码
     * @param deleteDevices 回调接口
     */
    public void deleteDevice(@NotNull String devTid, @NotNull String bindKey, final HekrUser.DeleteDeviceListener deleteDevices) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.BIND_DEVICE + "/" + devTid + "?bindKey=" + bindKey;
        //http://user.openapi.hekr.me/device/{devTid}?bindKey={bindKey};
        deleteHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                deleteDevices.deleteDeviceSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                deleteDevices.deleteDeviceFail(errorCode);
            }
        });
    }

    /**
     * 4.1.4 更改设备名称/描述 --1
     *
     * @param devTid               设备ID
     * @param deviceName           设备名称  长度[1, 128]
     * @param desc                 设备描述 长度[1, 128]
     * @param renameDeviceListener 回调接口
     */
    public void renameDevice(@NotNull String devTid, @NotNull String ctrlKey, @NotNull String deviceName, String desc, final HekrUser.RenameDeviceListener renameDeviceListener) {
        // "http://user.openapi.hekr.me/device/{devTid}"
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.BIND_DEVICE + "/" + devTid;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("deviceName", deviceName);
        jsonObject.put("ctrlKey", ctrlKey);
        if (!android.text.TextUtils.isEmpty(desc)) {
            jsonObject.put("desc", desc);
        }
        patchHekrData(url, jsonObject.toString(), new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                renameDeviceListener.renameDeviceSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                renameDeviceListener.renameDeviceFail(errorCode);
            }
        });
    }


    /**
     * 4.1.5 获取当前局域网内所有设备绑定状态<br>
     * 只返回正确的devTid/bindKey对应的设备绑定状态，所以返回里的元素数量会少于提交里的元素数量。<br>
     * 后续操作按照4.1.1执行
     * {@link #bindDevice(BindDeviceBean, HekrUser.BindDeviceListener)}
     *
     * @param devTid                设备ID
     * @param bindKey               绑定码
     * @param getBindStatusListener 回调接口
     */
    public void deviceBindStatus(String devTid, String bindKey, final HekrUser.GetBindStatusListener getBindStatusListener) {
        //http://user.openapi.hekr.me/deviceBindStatus
        final JSONObject obj = new JSONObject();
        obj.put("devTid", devTid);
        obj.put("bindKey", bindKey);
        JSONArray array = new JSONArray();
        array.add(obj);
        deviceBindStatus(array, getBindStatusListener);
    }


    /**
     * 4.1.5 获取当前局域网内所有设备绑定状态<br>
     * 只返回正确的devTid/bindKey对应的设备绑定状态，所以返回里的元素数量会少于提交里的元素数量。<br>
     * 后续操作按照4.1.1执行
     * {@link #bindDevice(BindDeviceBean, HekrUser.BindDeviceListener)}
     *
     * @param array                 [ {"bindKey" : "xxxxx", "devTid" : "ESP_test"},... }]
     * @param getBindStatusListener 回调接口{@link HekrUser.GetBindStatusListener}
     */
    public void deviceBindStatus(JSONArray array, final HekrUser.GetBindStatusListener getBindStatusListener) {

        //http://user.openapi.hekr.me/deviceBindStatus
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.DEVICE_BIND_STATUS;
        postHekrData(url, array.toString(), new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                getBindStatusListener.getStatusSuccess(JSONArray.parseArray(object.toString(), DeviceStatusBean.class));
            }

            @Override
            public void getFail(int errorCode) {
                getBindStatusListener.getStatusFail(errorCode);
            }
        });
    }


    /**
     * 4.1.5 获取当前局域网内所有设备绑定状态&&如果可以绑定直接就进行绑定操作<br>
     * 4.1.1 绑定设备
     * * {@link #bindDevice(BindDeviceBean, HekrUser.BindDeviceListener)}<br>
     * 只返回正确的devTid/bindKey对应的设备绑定状态，所以返回里的元素数量会少于提交里的元素数量。<br>
     * 如果返回的数据可以进行绑定，该接口就直接就进行绑定操作，如果返回4.1.5接口失败和4.1.1接口失败，则直接失败的两个回调
     *
     * @param devTid                       设备ID
     * @param bindKey                      绑定码
     * @param getBindStatusAndBindListener 回调接口 {@link HekrUser.GetBindStatusAndBindListener}
     */
    public void deviceBindStatusAndBind(final String devTid, final String bindKey, final HekrUser.GetBindStatusAndBindListener getBindStatusAndBindListener) {
        try {


            //http://user.openapi.hekr.me/deviceBindStatus
            final JSONObject obj = new JSONObject();
            obj.put("devTid", devTid);
            obj.put("bindKey", bindKey);
            JSONArray array = new JSONArray();
            array.add(obj);
            deviceBindStatus(array, new HekrUser.GetBindStatusListener() {
                @Override
                public void getStatusSuccess(List<DeviceStatusBean> deviceStatusBeanLists) {
                    //成功后回调
                    getBindStatusAndBindListener.getStatusSuccess(deviceStatusBeanLists);
                    //直接进行绑定操作
                    if (deviceStatusBeanLists != null && !deviceStatusBeanLists.isEmpty()) {
                        DeviceStatusBean deviceStatusBean = deviceStatusBeanLists.get(0);
                        if (deviceStatusBean.isForceBind() || !deviceStatusBean.isBindToUser()) {
                            String name = (deviceStatusBean.getCidName().substring(deviceStatusBean.getCidName().indexOf("/") + 1));
                            BindDeviceBean bindDeviceBean = new BindDeviceBean(devTid, bindKey, name, mContext.getString(R.string.app_name));
                            bindDevice(bindDeviceBean, new HekrUser.BindDeviceListener() {
                                @Override
                                public void bindDeviceSuccess(DeviceBean deviceBean) {
                                    getBindStatusAndBindListener.bindDeviceSuccess(deviceBean);
                                }

                                @Override
                                public void bindDeviceFail(int errorCode) {
                                    getBindStatusAndBindListener.bindDeviceFail(errorCode);
                                }
                            });
                        }
                    }
                }

                @Override
                public void getStatusFail(int errorCode) {
                    getBindStatusAndBindListener.getStatusFail(errorCode);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * 4.2.1 添加目录
     *
     * @param folderName        目录名称
     * @param addFolderListener 回调接口{@link HekrUser.AddFolderListener}
     */
    public void addFolder(@NotNull String folderName, final HekrUser.AddFolderListener addFolderListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.FOLDER;
        JSONObject obj = new JSONObject();
        obj.put("folderName", folderName);
        postHekrData(url, obj.toString(), new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                addFolderListener.addFolderSuccess(JSON.parseObject(object.toString(), FolderBean.class));
            }

            @Override
            public void getFail(int errorCode) {
                addFolderListener.addFolderFail(errorCode);
            }
        });

    }

    /**
     * 4.2.2 列举目录
     *
     * @param page 页数
     */
    public void getFolder(int page, final HekrUser.GetFolderListsListener getFolderListsListener) {
        //http://user.openapi.hekr.me/folder?folderId=xxx,xxx1&page=1&size=1
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.FOLDER + "?page=" + page;
        getHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                getFolderListsListener.getSuccess(JSON.parseArray(object.toString(), FolderListBean.class));
            }

            @Override
            public void getFail(int errorCode) {
                getFolderListsListener.getFail(errorCode);
            }
        });

    }

    /**
     * 4.2.3 修改目录名称
     *
     * @param newFolderName        新目录名字
     * @param folderId             目录ID
     * @param renameFolderListener 回调接口
     */
    public void renameFolder(String newFolderName, String folderId, final HekrUser.RenameFolderListener renameFolderListener) {
        JSONObject object = new JSONObject();
        object.put("newFolderName", newFolderName);
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.FOLDER + "/" + folderId;

        putHekrData(url, object.toJSONString(), new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                renameFolderListener.renameSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                renameFolderListener.renameFail(errorCode);
            }
        });
    }


    /**
     * 4.2.4 删除全部自定义目录
     *
     * @param deleteFolderListener 回调
     */
    public void deleteFolder(final HekrUser.DeleteFolderListener deleteFolderListener) {
        deleteFolder(null, deleteFolderListener);
    }

    /**
     * 4.2.4 删除目录
     * 即使目录下有设备也可以删除，后续动作是把这些设备挪到根目录下。
     * 注意，如果不指定folderId参数，会删除全部自定义目录。
     *
     * @param folderId             目录ID
     * @param deleteFolderListener 回调
     */
    public void deleteFolder(String folderId, final HekrUser.DeleteFolderListener deleteFolderListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.FOLDER;
        if (!TextUtils.isEmpty(folderId)) {
            url += "/" + folderId;
        }
        deleteHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                deleteFolderListener.deleteSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                deleteFolderListener.deleteFail(errorCode);
            }
        });
    }

    /**
     * 4.2.5 将设备挪到指定目录
     *
     * @param folderId                目录id
     * @param ctrlKey                 设备控制码
     * @param devTid                  设备ID
     * @param devicePutFolderListener 回调接口
     */
    public void devicesPutFolder(String folderId, String ctrlKey, String devTid, final HekrUser.DevicePutFolderListener devicePutFolderListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.FOLDER + "/" + folderId;
        JSONObject obj = new JSONObject();
        obj.put("devTid", devTid);
        obj.put("ctrlKey", ctrlKey);
        postHekrData(url, obj.toJSONString(), new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                devicePutFolderListener.putSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                devicePutFolderListener.putFail(errorCode);
            }
        });
    }

    /**
     * 4.2.6 将设备从目录挪到根目录下
     *
     * @param folderId                目录ID
     * @param ctrlKey                 设备ID
     * @param devTid                  设备控制码
     * @param devicePutFolderListener 回调方法
     */
    public void folderToRoot(String folderId, String ctrlKey, String devTid, final HekrUser.DevicePutFolderListener devicePutFolderListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.FOLDER + "/" + folderId + "/" + devTid + "?ctrlKey=" + ctrlKey;
        deleteHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                devicePutFolderListener.putSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                devicePutFolderListener.putFail(errorCode);
            }
        });
    }

    /**
     * 4.3.2 反向授权创建
     * <p>
     * 1. 创建授权二维码URL--1
     * </p>
     *
     * @param createOAuthQRCodeListener 回调接口
     */
    public void oAuthCreateCode(OAuthBean oAuthBean, final HekrUser.CreateOAuthQRCodeListener createOAuthQRCodeListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + "authorization/reverse/authUrl";
        postHekrData(url, JSON.toJSONString(oAuthBean), new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                String reverseAuthorizationTemplateId = JSON.parseObject(object.toString()).getString("reverseAuthorizationTemplateId");
                createOAuthQRCodeListener.createSuccess(ConstantsUtil.UrlUtil.OAuthUrl + reverseAuthorizationTemplateId);
            }

            @Override
            public void getFail(int errorCode) {
                createOAuthQRCodeListener.createFail(errorCode);
            }
        });
    }

    /**
     * 4.3.2 反向授权创建
     * <p>
     * 2. 被授权用户扫描二维码--1
     * </p>
     *
     * @param reverseAuthorizationTemplateId 回调接口
     * @param registerOAuthQRCodeListener    授权id
     */
    public void registerAuth(String reverseAuthorizationTemplateId, final HekrUser.RegisterOAuthQRCodeListener registerOAuthQRCodeListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.OAuthREGISTER + "?reverseTemplateId=" + reverseAuthorizationTemplateId;
        postHekrData(url, null, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                registerOAuthQRCodeListener.registerSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                registerOAuthQRCodeListener.registerFail(errorCode);
            }
        });

    }

    /**
     * 4.3.2 反向授权创建
     * <p>
     * 3. 授权用户收到被授权者的请求--1
     * </p>
     *
     * @param devTid               可选
     * @param page                 0
     * @param size                 1
     * @param reverseRegisterId    可选
     * @param getOauthInfoListener 回调接口
     */
    public void getOAuthInfoRequest(String devTid, int page, int size, String reverseRegisterId, final HekrUser.GetOauthInfoListener getOauthInfoListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.OAuthREGISTER + "?page=" + page + "&size=" + size;
        if (!TextUtils.isEmpty(devTid)) {
            url = url + "&devTid=" + devTid;
        }
        if (!TextUtils.isEmpty(reverseRegisterId)) {
            url = url + "&reverseRegisterId=" + reverseRegisterId;
        }
        getHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                getOauthInfoListener.getOauthInfoSuccess(JSON.parseArray(object.toString(), OAuthRequestBean.class));
            }

            @Override
            public void getFail(int errorCode) {
                getOauthInfoListener.getOauthInfoFail(errorCode);
            }
        });
    }

    /**
     * 4.3.2 反向授权创建
     * <p>
     * 4. 授权用户同意--1
     * </p>
     *
     * @param devTid             必选
     * @param ctrlKey            必选
     * @param reverseRegisterId  必选 通过4.3.2-3接口拿到的数据
     * @param agreeOauthListener 回调接口
     */
    public void agreeOAuth(String devTid, String ctrlKey, String reverseRegisterId, final HekrUser.AgreeOauthListener agreeOauthListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + "authorization/reverse?devTid=" + devTid + "&ctrlKey=" + ctrlKey + "&reverseRegisterId=" + reverseRegisterId;
        postHekrData(url, null, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                agreeOauthListener.AgreeOauthSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                agreeOauthListener.AgreeOauthFail(errorCode);
            }
        });
    }

    /**
     * 4.3.2 反向授权创建
     * <p>
     * 5. 授权用户拒绝
     * </p>
     *
     * @param devTid              必选
     * @param ctrlKey             必选
     * @param grantee             必选 通过4.3.2-3接口拿到的数据
     * @param reverseRegisterId   必选 通过4.3.2-3接口拿到的数据！
     * @param refuseOAuthListener 回调接口
     */
    public void refuseOAuth(@NotNull String devTid, @NotNull String ctrlKey, @NotNull String grantee, @NotNull String reverseRegisterId, final HekrUser.RefuseOAuthListener refuseOAuthListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + "authorization/reverse/register/" + reverseRegisterId + "?devTid=" +
                devTid + "&uid=" + grantee + "&ctrlKey=" + ctrlKey;
        deleteHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                refuseOAuthListener.refuseOauthSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                refuseOAuthListener.refuseOauthFail(errorCode);
            }
        });
    }

    /**
     * 4.3.4 取消授权--1
     *
     * @param grantor 授权用户uid
     * @param ctrlKey 被授权用户uid，多个使用逗号分隔；当不提交该参数时，表示授权者删除该设备上对所有被授权者的授权关系
     * @param grantee 控制码
     * @param devTid  设备ID
     */
    public void cancelOAuth(String grantor, String ctrlKey, String grantee, String devTid, final HekrUser.CancelOAuthListener cancelOAuthListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + "authorization?grantor=" + grantor + "&ctrlKey=" + ctrlKey + "&grantee=" + grantee + "&devTid=" + devTid;
        deleteHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                cancelOAuthListener.CancelOAuthListener();
            }

            @Override
            public void getFail(int errorCode) {
                cancelOAuthListener.CancelOauthFail(errorCode);
            }
        });
    }


    /**
     * 4.3.5 列举授权信息/被授权者调用
     *
     * @param grantor 授权用户uid
     * @param ctrlKey 控制码
     * @param devTid  设备ID
     * @param grantee 被授权用户uid，多个使用逗号分隔；当不提交该参数时，表示列举该设备上所有的授权关系；被授权者调用时不得为空，且其值为当前调用用户的uid
     */
    public void getOAuthList(String grantor, String ctrlKey, String devTid, String grantee, final HekrUser.GetOAuthListener getOAuthListener) {
        _getOAuthList(grantor, ctrlKey, devTid, grantee, getOAuthListener);
    }


    /**
     * 4.3.5 列举授权信息/授权者调用
     *
     * @param grantor          授权用户uid
     * @param ctrlKey          控制码
     * @param devTid           设备ID
     * @param getOAuthListener 回调接口
     */
    public void getOAuthList(String grantor, String ctrlKey, String devTid, final HekrUser.GetOAuthListener getOAuthListener) {
        _getOAuthList(grantor, ctrlKey, devTid, null, getOAuthListener);
    }

    /**
     * 4.3.5 列举授权信息
     *
     * @param grantor          授权用户uid
     * @param ctrlKey          控制码
     * @param devTid           设备ID
     * @param grantee          被授权用户uid，多个使用逗号分隔；当不提交该参数时，表示列举该设备上所有的授权关系；被授权者调用时不得为空，且其值为当前调用用户的uid
     * @param getOAuthListener 回调接口
     */
    private void _getOAuthList(String grantor, String ctrlKey, String devTid, String grantee, final HekrUser.GetOAuthListener getOAuthListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + "authorization?grantor=" + grantor + "&ctrlKey=" + ctrlKey + "&devTid=" + devTid;
        if (!TextUtils.isEmpty(grantee)) {
            url = url + "&grantee=" + grantee;
        }
        getHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                getOAuthListener.getOAuthListSuccess(JSON.parseArray(object.toString(), OAuthListBean.class));
            }

            @Override
            public void getFail(int errorCode) {
                getOAuthListener.getOAuthListFail(errorCode);
            }
        });
    }

    /**
     * 4.5.1 获取用户档案
     *
     * @param getProfileListener 回调接口
     */
    public void getProfile(final HekrUser.GetProfileListener getProfileListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.PROFILE;
        getHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                ProfileBean profileBean = JSONObject.parseObject(object.toString(), ProfileBean.class);
                setUserCache(profileBean);
                getProfileListener.getProfileSuccess(profileBean);
            }

            @Override
            public void getFail(int errorCode) {
                getProfileListener.getProfileFail(errorCode);
            }
        });

    }

    /**
     * 4.5.2 更新用户档案 --1
     *
     * @param jsonObject         用户实体类
     * @param setProfileListener 回调
     */
    public void setProfile(@NotNull final JSONObject jsonObject, final HekrUser.SetProfileListener setProfileListener) {
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.PROFILE;
        putHekrData(url, jsonObject.toString(), new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                setProfileListener.setProfileSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                setProfileListener.setProfileFail(errorCode);
            }
        });
    }


    /**
     * 4.5.14 上传文件 --1
     *
     * @param uri                绝对地址
     * @param uploadFileListener 回调
     */
    public void uploadFile(@NotNull final String uri, final HekrUser.UploadFileListener uploadFileListener) {
        File file = new File(uri);
        RequestParams params = new RequestParams();
        try {
            params.put("file", file);
            params.put("name", "a.png");
            params.put("type", "image/png");

            String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.USER_FILE;
            HekrHttpUtil.postFileReFreshToken(mContext, JWT_TOKEN, refresh_TOKEN, url, params, new HekrUser.GetHekrDataWithTokenProgressListener() {
                @Override
                public void getDataSuccess(Object object) {
                    uploadFileListener.uploadFileSuccess(JSONObject.parseObject(object.toString(), FileBean.class));
                }

                @Override
                public void getToken(JWTBean jwtBean) {
                    setTokenWIthCache(jwtBean);
                }

                @Override
                public void getDataFail(int errorCode) {
                    uploadFileListener.uploadFileFail(errorCode);
                }


                @Override
                public void getDataProgress(int progress) {
                    uploadFileListener.uploadProgress(progress);
                }
            });

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * 4.6.1 天气实况 --1
     *
     * @param getWeatherListener 回调
     */
    public void getWeather(String location, final HekrUser.GetWeatherListener getWeatherListener) {
        String time = String.valueOf(System.currentTimeMillis());
        byte[] bytes = (MD5Util.md5(time + "HekrNetwork1607" + time));
        StringBuilder ret = new StringBuilder(bytes.length << 1);
        for (byte aByte : bytes) {
            ret.append(Character.forDigit((aByte >> 4) & 0xf, 16));
            ret.append(Character.forDigit(aByte & 0xf, 16));
        }
        android.util.Log.d(TAG, "getWeather: " + ret.toString());
        String language;
        switch (HekrCodeUtil.getLanguage(mContext)) {
            case HekrCodeUtil.LANGUAGE_zh_Hans:
                language = "zh-Hans";
                break;
            case HekrCodeUtil.LANGUAGE_zh_Hant:
                language = "zh-Hant";
                break;
            case HekrCodeUtil.LANGUAGE_en:
                language = "en";
                break;
            default:
                language = "zh-Hans";
                break;
        }


        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.UAA_WEATHER + location + "&sign=" + ret.toString() + "&timestamp=" + time + "&language=" + language;
        getHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                String results = JSONArray.parseArray(JSONObject.parseObject(object.toString()).getString("results")).get(0).toString();
                JSONObject obj = JSONObject.parseObject(results);
                WeatherBeanResultsNow now = JSONObject.parseObject(obj.getString("now"), WeatherBeanResultsNow.class);
                getWeatherListener.getWeatherSuccess(now, JSONObject.parseObject(obj.getString("location")).getString("name"));
            }

            @Override
            public void getFail(int errorCode) {
                getWeatherListener.getWeatherFail(errorCode);
            }
        });

    }

    /**
     * 4.7.2 列举群组
     *
     * @param groupId 群组id
     */
    public void getGroup(int groupId, final HekrUser.GetGroupListener getGroupListener) {
        getGroup(String.valueOf(groupId), getGroupListener);
    }


    /**
     * 4.7.2 列举群组
     */
    public void getGroup(final HekrUser.GetGroupListener getGroupListener) {
        getGroup(null, getGroupListener);
    }


    /**
     * 4.7.2 列举群组
     *
     * @param groupId 群组id
     */
    private void getGroup(@NotNull String groupId, final HekrUser.GetGroupListener getGroupListener) {
        //"http://user.openapi.hekr.me/group?groupId=xxx"
        String url = ConstantsUtil.UrlUtil.BASE_USER_URL + ConstantsUtil.UrlUtil.UAA_GROUP;
        if (!TextUtils.isEmpty(groupId)) {
            url = url + "?groupId=" + groupId;
        }
        getHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                List<GroupBean> groupBeen = JSONArray.parseArray(object.toString(), GroupBean.class);
                getGroupListener.getGroupSuccess(groupBeen);
            }

            @Override
            public void getFail(int errorCode) {
                getGroupListener.getGroupFail(errorCode);
            }
        });
    }


    /**
     * 5.1 判断设备模块固件是否需要升级
     *
     * @param devTid           设备id
     * @param productPublicKey 产品公开码
     * @param binType          固件类型
     * @param binVer           固件版本
     */
    public void checkFirmwareUpdate(@NotNull String devTid, @NotNull String productPublicKey, @NotNull String binType, @NotNull String binVer, final HekrUser.CheckFwUpdateListener checkFwUpdateListener) {
        final JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("devTid", devTid);
        jsonObject.put("productPublicKey", productPublicKey);
        jsonObject.put("binType", binType);
        jsonObject.put("binVer", binVer);
        jsonArray.add(jsonObject);
        String url = ConstantsUtil.UrlUtil.BASE_CONSOLE_URL + ConstantsUtil.UrlUtil.CHECK_FW_UPDATE;
        postHekrData(url, jsonArray.toJSONString(), new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                try {
                    org.json.JSONArray jsonArray1 = new org.json.JSONArray(object.toString());
                    if (jsonArray.isEmpty()) {
                        checkFwUpdateListener.checkNotNeedUpdate();
                    } else {
                        org.json.JSONObject jsonObject1 = (org.json.JSONObject) jsonArray1.get(0);
                        if (jsonObject1.getBoolean("update")) {
                            FirmwareBean firmwareBean = JSON.parseObject(jsonObject1.getString("devFirmwareOTARawRuleVO"), FirmwareBean.class);
                            checkFwUpdateListener.checkNeedUpdate(firmwareBean);
                        } else {
                            checkFwUpdateListener.checkNotNeedUpdate();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    checkFwUpdateListener.checkNotNeedUpdate();
                }
            }

            @Override
            public void getFail(int errorCode) {
                checkFwUpdateListener.checkFail(errorCode);
            }
        });
    }


    /**
     * 5.2 根据pid获取企业资讯
     */
    public void getNewsByPid(HekrUser.GetInfoListener getInfoListener) {
        getNewsByPid(null, null, getInfoListener);
    }

    /**
     * 5.2 根据pid获取企业资讯
     */
    public void getNewsByPid(int page, int size, HekrUser.GetInfoListener getInfoListener) {
        getNewsByPid(String.valueOf(page), String.valueOf(size), getInfoListener);
    }


    /**
     * 5.2 根据pid获取企业资讯
     */
    private void getNewsByPid(String page, String size, final HekrUser.GetInfoListener getInfoListener) {
        String url = ConstantsUtil.UrlUtil.BASE_CONSOLE_URL + "external/vc/getByPid?pid=" + pid;
        if (!TextUtils.isEmpty(page) && !TextUtils.isEmpty(size)) {
            url = url + "?&page=" + page + "&size=" + size;
        }
        getHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                getInfoListener.getInfoSuccess(JSON.parseObject(object.toString(), NewsBean.class));
            }

            @Override
            public void getFail(int errorCode) {
                getInfoListener.getInfoFail(errorCode);
            }
        });

    }

    /**
     * 5.5 售后管理 - 针对设备反馈问题
     *
     * @param content          反馈内容
     * @param feedbackListener 回调接口
     */
    public void feedback(String content, final HekrUser.FeedbackListener feedbackListener) {
        String url = ConstantsUtil.UrlUtil.BASE_CONSOLE_URL + "external/feedback";
        JSONObject jsonObject = new JSONObject();
        if (TextUtils.isEmpty(getUserCache().getEmail())) {
            jsonObject.put("UserNumber", getUserCache().getPhoneNumber());
        } else {
            jsonObject.put("UserNumber", getUserCache().getEmail());
        }
        jsonObject.put("title", "蜂鸟Android反馈");
        jsonObject.put("content", content);
        postHekrData(url, jsonObject.toJSONString(), new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                feedbackListener.feedbackSuccess();
            }

            @Override
            public void getFail(int errorCode) {
                feedbackListener.feedFail(errorCode);
            }
        });

    }

    /**
     * 5.10 获取默认演示设备
     */
    public void getdefaultStatic(final HekrUser.GetDefaultDevicesListener getDefaultDevices) {
        //http://console.openapi.hekr.me/external/device/default/static
        String url = ConstantsUtil.UrlUtil.BASE_CONSOLE_URL + ConstantsUtil.UrlUtil.DEFAULT_STATIC;
        getHekrData(url, new GetHekrDataListener() {
            @Override
            public void getSuccess(Object object) {
                getDefaultDevices.getSuccess(JSON.parseArray(object.toString(), DeviceBean.class));
            }

            @Override
            public void getFail(int errorCode) {
                getDefaultDevices.getFail(errorCode);
            }
        });
    }


    /**
     * 将用户的bean的数据保存下来
     *
     * @param userBean 用户实体类
     */
    private void setUserCache(UserBean userBean) {
        try {
            //把此token保存下来
            SpCache.putString("JWT_TOKEN", userBean.getJWT_TOKEN());
            SpCache.putString("HEKR_USER_NAME", userBean.getUsername());
            // SpCache.putString("HEKR_USER_PWD", userBean.getPassword());
            SpCache.putString("HEKR_PID", userBean.getPid());
            SpCache.putString("refresh_TOKEN", userBean.getRefresh_token());
        } catch (Exception e) {
            e.printStackTrace();
            android.util.Log.e(TAG, "setUserCacheError");
        }

    }


    /**
     * 退出登录
     */
    public void userLogout() {
        //退出APP之后关闭所有的请求！
        BaseHttpUtil.getClient().cancelAllRequests(true);
        SpCache.putString("JWT_TOKEN", "");
        SpCache.putString("refresh_TOKEN", "");
        SpCache.putString("HEKR_USER_NAME", "");
        SpCache.putString("HEKR_PID", "");
        JWT_TOKEN = null;
        refresh_TOKEN = null;
        SpCache.putString("PhoneNumber", "");
        SpCache.putString("email", "");
        SpCache.putString("lastName", "");
        SpCache.putString("gender", "");
        //停止webSocket
        EventBus.getDefault().post(new WsSwitchEvent(1));
        //清除掉缓存
        //DataCleanManager.clearAllCache(mContext);
        SpCache.clear();
    }

    /**
     * @return 返回用户token
     */
    public String getJWT_TOKEN() {
        if (TextUtils.isEmpty(JWT_TOKEN)) {
            return SpCache.getString("JWT_TOKEN", "");
        } else {
            return JWT_TOKEN;
        }
    }

    private String getRefreshToken() {
        if (android.text.TextUtils.isEmpty(refresh_TOKEN)) {
            return SpCache.getString("refresh_TOKEN", "");
        } else {
            return refresh_TOKEN;
        }
    }


    /**
     * 将获取到的token保存下来
     */
    private void setTokenWIthCache(JWTBean jwtBean) {
        this.JWT_TOKEN = jwtBean.getAccess_token();
        this.refresh_TOKEN = jwtBean.getRefresh_token();
        //把此token保存下来
        this.userId = TokenToUid();
        if (!TextUtils.isEmpty(JWT_TOKEN)) {
            SpCache.putString("JWT_TOKEN", JWT_TOKEN);
        }
        if (!TextUtils.isEmpty(refresh_TOKEN)) {
            SpCache.putString("refresh_TOKEN", refresh_TOKEN);
        }
    }

    /**
     * 将获取到的用户信息保存下来
     */
    private void setUserCache(ProfileBean profileBean) {
        try {
            if (!TextUtils.isEmpty(profileBean.getPhoneNumber())) {
                SpCache.putString("PhoneNumber", profileBean.getPhoneNumber());
            }

            if (!TextUtils.isEmpty(profileBean.getEmail())) {
                SpCache.putString("email", profileBean.getEmail());
            }

            if (!TextUtils.isEmpty(profileBean.getLastName())) {
                SpCache.putString("lastName", profileBean.getLastName());
            }

            if (!TextUtils.isEmpty(profileBean.getGender())) {
                SpCache.putString("gender", profileBean.getGender());
            }
            if (!TextUtils.isEmpty(profileBean.getAvatarUrl().getSmall())) {
                SpCache.putString("avatarUrl", profileBean.getAvatarUrl().getSmall());
            }
            if (!TextUtils.isEmpty(profileBean.getAge())) {
                SpCache.putString("age", profileBean.getAge());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 拿到缓存到本地的用户档案
     *
     * @return 用户档案
     */
    public ProfileBean getUserCache() {
        ProfileBean profileBean = new ProfileBean();
        profileBean.setPhoneNumber(SpCache.getString("PhoneNumber", ""));
        profileBean.setEmail(SpCache.getString("email", ""));
        profileBean.setLastName(SpCache.getString("lastName", mContext.getString(R.string.visitor)));
        profileBean.setGender(SpCache.getString("gender", ""));
        ProfileBean.AvatarUrl avatarUrl = new ProfileBean.AvatarUrl();
        avatarUrl.setSmall(SpCache.getString("avatarUrl", ""));
        profileBean.setAvatarUrl(avatarUrl);
        profileBean.setAge(SpCache.getString("age", "21"));
        return profileBean;
    }


    /**
     * 获取UID
     *
     * @return uid
     */
    public String getUserId() {
        if (TextUtils.isEmpty(userId)) {
            return TokenToUid();
        } else {
            return userId;
        }
    }


    /**
     * 提取出来UID
     *
     * @return uid
     */
    private String TokenToUid() {
        if (getJWT_TOKEN().contains(".")) {
            String[] strs = getJWT_TOKEN().split("\\.");
            if (strs.length == 3 && !TextUtils.isEmpty(strs[1])) {
                JSONObject uidObj = JSONObject.parseObject(new String(Base64.decode(strs[1], Base64.DEFAULT)));
                if (uidObj.containsKey("uid")) {
                    return uidObj.getString("uid");
                }
            }
        }
        return null;
    }


    /**
     * hekrHttpGet  <br>此接口可自动管理token
     *
     * @param url                 url
     * @param getHekrDataListener 回调方法
     */
    public void getHekrData(String url, final GetHekrDataListener getHekrDataListener) {
        HekrHttpUtil.getDataReFreshToken(mContext, JWT_TOKEN, refresh_TOKEN, url, new HekrUser.GetHekrDataWithTokenListener() {
            @Override
            public void getDataSuccess(Object object) {
                getHekrDataListener.getSuccess(object);
            }

            @Override
            public void getToken(JWTBean jwtBean) {
                setTokenWIthCache(jwtBean);
            }

            @Override
            public void getDataFail(int errorCode) {
                getHekrDataListener.getFail(errorCode);
            }
        });
    }

    /**
     * hekrHttpPost <br>此接口可自动管理token
     *
     * @param url                 url
     * @param entity              entity
     * @param getHekrDataListener 回调
     */
    public void postHekrData(String url, String entity, final GetHekrDataListener getHekrDataListener) {
        HekrHttpUtil.postDataReFreshToken(mContext, JWT_TOKEN, refresh_TOKEN, url, entity, new HekrUser.GetHekrDataWithTokenListener() {
            @Override
            public void getDataSuccess(Object object) {
                getHekrDataListener.getSuccess(object);
            }

            @Override
            public void getToken(JWTBean jwtBean) {
                setTokenWIthCache(jwtBean);
            }

            @Override
            public void getDataFail(int errorCode) {
                getHekrDataListener.getFail(errorCode);
            }
        });
    }


    /**
     * hekrHttpPut <br>此接口可自动管理token
     *
     * @param url                 url
     * @param entity              entity
     * @param getHekrDataListener 回调
     */
    public void putHekrData(String url, String entity, final GetHekrDataListener getHekrDataListener) {
        HekrHttpUtil.putDataRefreshToken(mContext, JWT_TOKEN, refresh_TOKEN, url, entity, new HekrUser.GetHekrDataWithTokenListener() {
            @Override
            public void getDataSuccess(Object object) {
                getHekrDataListener.getSuccess(object);
            }

            @Override
            public void getToken(JWTBean jwtBean) {
                setTokenWIthCache(jwtBean);
            }

            @Override
            public void getDataFail(int errorCode) {
                getHekrDataListener.getFail(errorCode);
            }
        });
    }


    /**
     * deleteHttpDelete <br>此接口可自动管理token
     *
     * @param url                 url
     * @param getHekrDataListener 回调
     */
    public void deleteHekrData(String url, final GetHekrDataListener getHekrDataListener) {
        HekrHttpUtil.deleteDataReFreshToken(mContext, JWT_TOKEN, refresh_TOKEN, url, new HekrUser.GetHekrDataWithTokenListener() {
            @Override
            public void getDataSuccess(Object object) {
                getHekrDataListener.getSuccess(object);
            }

            @Override
            public void getToken(JWTBean jwtBean) {
                setTokenWIthCache(jwtBean);
            }

            @Override
            public void getDataFail(int errorCode) {
                getHekrDataListener.getFail(errorCode);
            }
        });
    }

    /**
     * deleteHttpPatch
     * <br>此接口可自动管理token
     *
     * @param url                 url
     * @param entity              entity
     * @param getHekrDataListener 回调
     */
    public void patchHekrData(String url, String entity, final GetHekrDataListener getHekrDataListener) {
        HekrHttpUtil.patchDataToken(mContext, JWT_TOKEN, refresh_TOKEN, url, entity, new HekrUser.GetHekrDataWithTokenListener() {
            @Override
            public void getDataSuccess(Object object) {
                getHekrDataListener.getSuccess(object);
            }

            @Override
            public void getToken(JWTBean jwtBean) {
                setTokenWIthCache(jwtBean);
            }

            @Override
            public void getDataFail(int errorCode) {
                getHekrDataListener.getFail(errorCode);
            }
        });
    }

    /**
     * get
     */
    public interface GetHekrDataListener {
        void getSuccess(Object object);

        void getFail(int errorCode);
    }

}

