package me.hekr.hummingbird.action;

import me.hekr.hummingbird.bean.DefaultDeviceBean;
import me.hekr.hummingbird.bean.DeviceBean;
import me.hekr.hummingbird.bean.DeviceStatusBean;
import me.hekr.hummingbird.bean.FileBean;
import me.hekr.hummingbird.bean.FirmwareBean;
import me.hekr.hummingbird.bean.FolderBean;
import me.hekr.hummingbird.bean.FolderListBean;
import me.hekr.hummingbird.bean.GroupBean;
import me.hekr.hummingbird.bean.JWTBean;
import me.hekr.hummingbird.bean.MOAuthBean;
import me.hekr.hummingbird.bean.NewsBean;
import me.hekr.hummingbird.bean.OAuthListBean;
import me.hekr.hummingbird.bean.OAuthRequestBean;
import me.hekr.hummingbird.bean.ProfileBean;
import me.hekr.hummingbird.bean.WeatherBeanResultsNow;

import java.util.List;

/**
 * Created by jin123d on 2016/4/8.
 **/
public class HekrUser {
    /**
     * 注册回调
     *
     * @author jin123d
     */
    public interface RegisterListener {
        /**
         * 注册成功
         *
         * @param uid 用户ID
         */
        void registerSuccess(String uid);

        /**
         * 注册失败
         *
         * @param errorCode 错误信息
         */
        void registerFail(int errorCode);
    }

    /**
     * 登录回调
     *
     * @author jin123d
     */
    public interface LoginListener {
        /**
         * 登录成功
         */

        void loginSuccess(String str);

        /**
         * 注册失败
         *
         * @param errorCode 错误信息
         */
        void loginFail(int errorCode);
    }


    /**
     * 校验验证码回调
     *
     * @author jin123d
     */
    public interface CheckVerifyCodeListener {

        /**
         * 校验验证码成功，返回成功字段
         *
         * @param phoneNumber 用户手机号
         * @param verifyCode  验证码
         * @param token       此token在注册时要使用
         * @param expireTime  token过期时间,此token的过期时间，在此时间之前没有注册，token会过期，必须重新获取、校验验证码
         */
        void checkVerifyCodeSuccess(String phoneNumber, String verifyCode, String token, String expireTime);

        /**
         * 发送验证码失败,返回错误信息
         *
         * @param errorCode 错误信息
         */
        void checkVerifyCodeFail(int errorCode);
    }

    /**
     * 绑定设备
     */
    public interface BindDeviceListener {

        /**
         * 绑定设备成功
         *
         * @param deviceBean 设备实体类
         */
        void bindDeviceSuccess(DeviceBean deviceBean);

        void bindDeviceFail(int errorCode);
    }

    /**
     * 获取列表
     */
    public interface GetDevicesListener {
        /**
         * 获取设备成功
         *
         * @param devicesLists 设备列表
         */
        void getDevicesSuccess(List<DeviceBean> devicesLists);

        void getDevicesFail(int errorCode);
    }

    /**
     * 删除设备
     */
    public interface DeleteDeviceListener {
        /**
         * 删除设备成功
         */
        void deleteDeviceSuccess();

        /**
         * 删除设备失败
         *
         * @param errorCode 错误信息
         */
        void deleteDeviceFail(int errorCode);
    }


    /**
     * 改名
     */
    public interface RenameDeviceListener {
        /**
         * 改名成功
         */
        void renameDeviceSuccess();

        /**
         * 改名失败
         *
         * @param errorCode 错误信息
         */
        void renameDeviceFail(int errorCode);
    }


    /**
     * 获取验证码接口
     */
    public interface GetVerifyCodeListener {

        /**
         * 获取验证码成功
         */
        void getVerifyCodeSuccess();

        /**
         * 获取验证码失败
         *
         * @param errorCode 错误信息
         */
        void getVerifyCodeFail(int errorCode);
    }


    /**
     * 4.2.1 添加目录
     */
    public interface AddFolderListener {
        void addFolderSuccess(FolderBean folderBean);

        void addFolderFail(int errorCode);
    }

    /**
     * 4.2.3 修改目录名称
     */
    public interface RenameFolderListener {
        void renameSuccess();

        void renameFail(int errorCode);
    }

    /**
     * 4.2.4 删除目录
     */
    public interface DeleteFolderListener {
        void deleteSuccess();

        void deleteFail(int errorCode);
    }

    /**
     * 4.2.5 将设备挪到指定目录
     * <p>
     * 除了根目录，一个目录下最多可以放置512个设备
     * </p>
     */
    public interface DevicePutFolderListener {
        void putSuccess();

        void putFail(int errorCode);
    }


    /**
     * 发送重置密码邮件接口
     */
    public interface SendResetPwdEmailListener {
        void sendSuccess();

        void sendFail(int errorCode);
    }


    /**
     * 更改密码接口
     */
    public interface ChangePwdListener {
        void changeSuccess();

        void changeFail(int errorCode);
    }

    /**
     * 重置密码接口
     */
    public interface ResetPwdListener {
        void resetSuccess();

        void resetFail(int errorCode);
    }


    /**
     * 获取到数据以及刷新token的接口
     */
    public interface GetHekrDataWithTokenListener {
        void getDataSuccess(Object object);

        void getToken(JWTBean jwtBean);

        void getDataFail(int errorCode);
    }

    /**
     * 获取到数据以及刷新token的接口
     */
    public interface GetHekrDataWithTokenProgressListener {
        void getDataSuccess(Object object);

        void getToken(JWTBean jwtBean);

        void getDataFail(int errorCode);

        void getDataProgress(int progress);
    }

    /**
     * 修改手机号
     */
    public interface ChangePhoneNumberListener {
        void changePhoneNumberSuccess();

        void changePhoneNumberFail(int errorCode);
    }

    /**
     * 重新发送确认邮件
     */
    public interface ReSendVerifiedEmailListener {
        void reSendVerifiedEmailSuccess();

        void reSendVerifiedEmailFail(int errorCode);
    }

    /**
     * 4.5.1 获取用户档案
     */
    public interface GetProfileListener {
        void getProfileSuccess(ProfileBean profileBean);

        void getProfileFail(int errorCode);
    }


    /**
     * 4.5.2 更新用户档案
     */
    public interface SetProfileListener {
        void setProfileSuccess();

        void setProfileFail(int errorCode);
    }

    /***
     * 4.6.1 天气实况
     */
    public interface GetWeatherListener {
        void getWeatherSuccess(WeatherBeanResultsNow now, String location);

        void getWeatherFail(int errorCode);
    }


    /**
     * 4.5.14 上传文件
     */
    public interface UploadFileListener {
        void uploadFileSuccess(FileBean fileBean);

        void uploadFileFail(int errorCode);

        void uploadProgress(int progress);
    }

    /**
     * 3.13 移动端OAuth
     */
    public interface MOAuthListener {
        void mOAuthSuccess(MOAuthBean moAuthBean);

        void mOAuthSuccess(JWTBean jwtBean);

        void mOAuthFail(int errorCode);
    }

    /**
     * 3.14 将OAuth账号和主账号绑定
     */
    public interface BindOAuthListener {
        void bindSuccess();

        void bindFail(int errorCode);
    }

    /**
     * 3.11 发送修改邮箱邮件
     */
    public interface SendChangeEmailListener {
        void sendChangeEmailSuccess();

        void sendChangeEmailFail(int errorCode);
    }

    /**
     * 4.7.2 列举群组
     */
    public interface GetGroupListener {

        void getGroupSuccess(List<GroupBean> groupBeen);

        void getGroupFail(int errorCode);
    }

    /**
     * 4.3.2 反向授权创建
     * <p>
     * 1. 创建授权二维码URL
     * </p>
     */
    public interface CreateOAuthQRCodeListener {
        void createSuccess(String url);

        void createFail(int errorCode);
    }


    /**
     * 4.3.2 反向授权创建
     * <p>
     * 2. 被授权用户扫描二维码
     * </p>
     */
    public interface RegisterOAuthQRCodeListener {
        void registerSuccess();

        void registerFail(int errorCode);
    }


    /**
     * 4.3.2 反向授权创建
     * <p>
     * 3. 授权用户收到被授权者的请求
     * </p>
     */
    public interface GetOauthInfoListener {
        void getOauthInfoSuccess(List<OAuthRequestBean> lists);

        void getOauthInfoFail(int errorCode);
    }


    /**
     * 4.3.2 反向授权创建
     * <p>
     * 4. 授权用户同意
     * </p>
     */
    public interface AgreeOauthListener {
        void AgreeOauthSuccess();

        void AgreeOauthFail(int errorCode);
    }


    /**
     * 4.3.2 反向授权创建
     * <p>
     * 4. 授权用户拒绝
     * </p>
     */
    public interface RefuseOAuthListener {
        void refuseOauthSuccess();

        void refuseOauthFail(int errorCode);
    }


    /**
     * 4.3.4 取消授权
     */
    public interface CancelOAuthListener {
        void CancelOAuthListener();

        void CancelOauthFail(int errorCode);
    }

    /**
     * 4.3.5 列举授权信息
     */
    public interface GetOAuthListener {
        void getOAuthListSuccess(List<OAuthListBean> lists);

        void getOAuthListFail(int errorCode);
    }


    /**
     * 5.2 根据pid获取企业资讯
     */
    public interface GetInfoListener {
        void getInfoSuccess(NewsBean newsBean);

        void getInfoFail(int errorCode);
    }

    /**
     * 5.5 售后管理 - 针对设备反馈问题
     */
    public interface FeedbackListener {
        void feedbackSuccess();

        void feedFail(int errorCode);
    }

    /**
     * 4.1.5 获取当前局域网内所有设备绑定状态
     */
    public interface GetBindStatusListener {
        void getStatusSuccess(List<DeviceStatusBean> deviceStatusBeanLists);

        void getStatusFail(int errorCode);
    }


    /**
     * 4.1.5 获取当前局域网内所有设备绑定状态&&进行绑定
     */
    public interface GetBindStatusAndBindListener {
        void getStatusSuccess(List<DeviceStatusBean> deviceStatusBeanLists);

        void getStatusFail(int errorCode);

        void bindDeviceSuccess(DeviceBean deviceBean);

        void bindDeviceFail(int errorCode);
    }


    /**
     * 4.2.2 列举目录
     */
    public interface GetFolderListsListener {
        void getSuccess(List<FolderListBean> folderList);

        void getFail(int errorCode);
    }


    /**
     * 5.1 判断设备模块固件是否需要升级
     */
    public interface CheckFwUpdateListener {

        void checkNotNeedUpdate();

        void checkNeedUpdate(FirmwareBean firmwareBean);

        void checkFail(int errorCode);
    }

    /**
     * 5.10 获取默认演示设备
     */
    public interface GetDefaultDevicesListener {
        void getSuccess(List<DeviceBean> list);

        void getFail(int errorCode);
    }
}
