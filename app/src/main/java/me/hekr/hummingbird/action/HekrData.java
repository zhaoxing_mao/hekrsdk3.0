package me.hekr.hummingbird.action;

/**
 * Created by hekr_xm on 2016/5/9.
 * 命令发送回调
 **/
public class HekrData {

    public interface dataReceiverListener{

        void onReceiveSuccess(String msg);

        void onReceiveTimeout();
    }
}
