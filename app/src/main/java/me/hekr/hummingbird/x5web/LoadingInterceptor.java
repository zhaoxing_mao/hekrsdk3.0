package me.hekr.hummingbird.x5web;

/**
 * Created by jin123d on 2016/4/14.
 **/
public interface LoadingInterceptor {
    void interceptor(String loadingUrl);
}
