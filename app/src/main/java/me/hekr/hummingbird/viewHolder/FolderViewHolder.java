package me.hekr.hummingbird.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import me.hekr.hummingbird.R;


/**
 * Created by jin123d on 2016/2/29.
 **/
public class FolderViewHolder extends RecyclerView.ViewHolder {

    public TextView tv_name;
    public TextView tv_number;


    public FolderViewHolder(View view) {
        super(view);
        tv_name = (TextView) view.findViewById(R.id.tv_name);
        tv_number = (TextView) view.findViewById(R.id.tv_folder_number);
    }

}
