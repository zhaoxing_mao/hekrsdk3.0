package me.hekr.hummingbird.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.ui.CircleImageView;


/**
 * Created by jin123d on 2016/05/20.
 **/
public class GroupViewHolder extends RecyclerView.ViewHolder {

    public TextView tv_name;
    public ImageView img_icon;
    public TextView tv_number;

    public GroupViewHolder(View view) {
        super(view);
        tv_name = (TextView) view.findViewById(R.id.tv_name);
        img_icon = (ImageView) view.findViewById(R.id.img_icon);
        tv_number = (TextView) view.findViewById(R.id.tv_number);

    }
}
