package me.hekr.hummingbird.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import me.hekr.hummingbird.ui.CircleImageView;

import me.hekr.hummingbird.R;


/**
 * Created by jin123d on 2016/2/29.
 **/
public class DevicesViewHolder extends RecyclerView.ViewHolder {

    public TextView tv;
    public ImageView img_icon;
    public CircleImageView online_bg;
    public TextView tv_state;

    public DevicesViewHolder(View view) {
        super(view);
        tv = (TextView) view.findViewById(R.id.tv_name);
        img_icon = (ImageView) view.findViewById(R.id.img_icon);
        online_bg = (CircleImageView) view.findViewById(R.id.online_bg);
        tv_state = (TextView) view.findViewById(R.id.tv_state);

    }
}
