package me.hekr.hummingbird.bean;

import me.hekr.hummingbird.util.LANUtil;

/**
 * Created by hekr_xm on 2016/5/10.
 * 局域网发送命令
 **/
public class LanUtilBean {
    private FindDeviceBean findDeviceBean;
    private LANUtil lanUtil;

    public LanUtilBean(FindDeviceBean findDeviceBean, LANUtil lanUtil) {
        this.findDeviceBean = findDeviceBean;
        this.lanUtil = lanUtil;
    }

    public FindDeviceBean getFindDeviceBean() {
        return findDeviceBean;
    }

    public void setFindDeviceBean(FindDeviceBean findDeviceBean) {
        this.findDeviceBean = findDeviceBean;
    }

    public LANUtil getLanUtil() {
        return lanUtil;
    }

    public void setLanUtil(LANUtil lanUtil) {
        this.lanUtil = lanUtil;
    }

    @Override
    public String toString() {
        return "LanUtilBean{" +
                "findDeviceBean=" + findDeviceBean +
                ", lanUtil=" + lanUtil +
                '}';
    }
}
