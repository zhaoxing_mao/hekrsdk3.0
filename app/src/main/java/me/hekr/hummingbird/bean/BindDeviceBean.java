package me.hekr.hummingbird.bean;

/**
 * Created by hekr_jds on 2016/4/12.
 **/
public class BindDeviceBean {


    private String devTid;
    private String deviceName;
    private String bindKey;
    private String desc;

    public String getDevTid() {
        return devTid;
    }

    public void setDevTid(String devTid) {
        this.devTid = devTid;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getBindKey() {
        return bindKey;
    }

    public void setBindKey(String bindKey) {
        this.bindKey = bindKey;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @param devTid     设备ID
     * @param deviceName 绑定码
     * @param bindKey    长度[1,128] 设备名称(可选)
     * @param desc       长度[1,128] 设备描述 (可选)
     */
    public BindDeviceBean(String devTid, String bindKey, String deviceName, String desc) {
        this.desc = desc;
        this.devTid = devTid;
        this.deviceName = deviceName;
        this.bindKey = bindKey;
    }

    @Override
    public String toString() {
        return "BindDeviceBean{" +
                "devTid='" + devTid + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", bindKey='" + bindKey + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
