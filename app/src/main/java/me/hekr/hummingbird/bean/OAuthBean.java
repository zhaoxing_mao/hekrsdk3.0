package me.hekr.hummingbird.bean;

/**
 * Created by hekr_jds on 5/11 0011.
 **/
public class OAuthBean {

    /**
     * grantor : uid1
     * devTid : ESP_xxx
     * ctrlKey : xxxxxx
     * expire : 300
     * mode : ALL
     * desc : xxxx
     * enableScheduler : true
     * enableIFTTT : true
     */

    private String grantor;
    private String devTid;
    private String ctrlKey;
    private int expire;
    private String mode;
    private String desc;
    private boolean enableScheduler;
    private boolean enableIFTTT;

    public OAuthBean() {
    }

    public String getGrantor() {
        return grantor;
    }

    public void setGrantor(String grantor) {
        this.grantor = grantor;
    }

    public String getDevTid() {
        return devTid;
    }

    public void setDevTid(String devTid) {
        this.devTid = devTid;
    }

    public String getCtrlKey() {
        return ctrlKey;
    }

    public void setCtrlKey(String ctrlKey) {
        this.ctrlKey = ctrlKey;
    }

    public int getExpire() {
        return expire;
    }

    public void setExpire(int expire) {
        this.expire = expire;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean isEnableScheduler() {
        return enableScheduler;
    }

    public void setEnableScheduler(boolean enableScheduler) {
        this.enableScheduler = enableScheduler;
    }

    public boolean isEnableIFTTT() {
        return enableIFTTT;
    }

    public void setEnableIFTTT(boolean enableIFTTT) {
        this.enableIFTTT = enableIFTTT;
    }

    public OAuthBean(String grantor, String devTid, String ctrlKey, int expire, String mode, String desc) {
        this.grantor = grantor;
        this.devTid = devTid;
        this.ctrlKey = ctrlKey;
        this.expire = expire;
        this.mode = mode;
        this.desc = desc;
    }

    public OAuthBean(String grantor, String devTid, String ctrlKey, int expire, String mode, String desc, boolean enableScheduler, boolean enableIFTTT) {
        this.grantor = grantor;
        this.devTid = devTid;
        this.ctrlKey = ctrlKey;
        this.expire = expire;
        this.mode = mode;
        this.desc = desc;
        this.enableScheduler = enableScheduler;
        this.enableIFTTT = enableIFTTT;
    }

    @Override
    public String toString() {
        return "OAuthBean{" +
                "grantor='" + grantor + '\'' +
                ", devTid='" + devTid + '\'' +
                ", ctrlKey='" + ctrlKey + '\'' +
                ", expire=" + expire +
                ", mode='" + mode + '\'' +
                ", desc='" + desc + '\'' +
                ", enableScheduler=" + enableScheduler +
                ", enableIFTTT=" + enableIFTTT +
                '}';
    }
}
