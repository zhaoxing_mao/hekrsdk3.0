package me.hekr.hummingbird.bean;

import me.hekr.hummingbird.action.HekrData;
import me.hekr.hummingbird.x5web.X5WebView;

import org.json.JSONObject;

/**
 * Created by hekr_xm on 2016/5/9.
 * 命令过滤器
 **/
public class FilterBean {

    private X5WebView objectWeakReference;
    private JSONObject filter;
    private long timeStamp;
    private boolean once;
    private HekrData.dataReceiverListener dataReceiverListener;

    public FilterBean(X5WebView objectWeakReference,long timeStamp, JSONObject filter, boolean once, HekrData.dataReceiverListener dataReceiverListener) {
        this.objectWeakReference=objectWeakReference;
        this.timeStamp = timeStamp;
        this.filter = filter;
        this.once = once;
        this.dataReceiverListener = dataReceiverListener;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public JSONObject getFilter() {
        return filter;
    }

    public void setFilter(JSONObject filter) {
        this.filter = filter;
    }

    public boolean isOnce() {
        return once;
    }

    public void setOnce(boolean once) {
        this.once = once;
    }

    public HekrData.dataReceiverListener getDataReceiverListener() {
        return dataReceiverListener;
    }

    public void setDataReceiverListener(HekrData.dataReceiverListener dataReceiverListener) {
        this.dataReceiverListener = dataReceiverListener;
    }

    public X5WebView getObjectWeakReference() {
        return objectWeakReference;
    }

    public void setObjectWeakReference(X5WebView objectWeakReference) {
        this.objectWeakReference = objectWeakReference;
    }

    @Override
    public String toString() {
        return "FilterBean{" +
                "filter=" + filter +
                ", timeStamp=" + timeStamp +
                ", once=" + once +
                '}';
    }
}
