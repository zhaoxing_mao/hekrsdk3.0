package me.hekr.hummingbird.bean;

/**
 * Created by hekr_jds on 2016/4/12.
 **/
public class ErrorMsgBean {

    /**
     * code : 3400005
     * desc : Verify code send too many
     * timestamp : 1460431878014
     */

    private int code;
    private String desc;
    private long timestamp;


    public ErrorMsgBean() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public ErrorMsgBean(int code, String desc, long timestamp) {
        this.code = code;
        this.desc = desc;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "ErrorMsgBean{" +
                "code=" + code +
                ", desc='" + desc + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
