package me.hekr.hummingbird.bean;

/**
 * Created by hekr_jds on 5/20 0020.
 **/
public class ConfigBean {

    /**
     * AppId : 1812570319
     * AppKey : 1812570319
     * AppSecurit : 8e977f35c127d288b953f4004332aefa
     */

    private String AppId;
    private String AppKey;
    private String AppSecurit;

    public ConfigBean() {
    }

    public String getAppId() {
        return AppId;
    }

    public void setAppId(String AppId) {
        this.AppId = AppId;
    }

    public String getAppKey() {
        return AppKey;
    }

    public void setAppKey(String AppKey) {
        this.AppKey = AppKey;
    }

    public String getAppSecurit() {
        return AppSecurit;
    }

    public void setAppSecurit(String AppSecurit) {
        this.AppSecurit = AppSecurit;
    }

    @Override
    public String toString() {
        return "ConfigBean{" +
                "AppId='" + AppId + '\'' +
                ", AppKey='" + AppKey + '\'' +
                ", AppSecurit='" + AppSecurit + '\'' +
                '}';
    }
}
