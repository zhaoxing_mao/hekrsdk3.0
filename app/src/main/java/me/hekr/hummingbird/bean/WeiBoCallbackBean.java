package me.hekr.hummingbird.bean;

/**
 * Created by hekr_xm on 2015/12/30.
 * 微博第三方登录返回信息
 **/
public class WeiBoCallbackBean {

    private String userId;
    private String accessToken;
    private String refreshToken;
    private String phone_num;
    private String expirationDate;

    public WeiBoCallbackBean() {
    }

    public WeiBoCallbackBean(String userId, String accessToken, String refreshToken, String phone_num, String expirationDate) {
        this.userId = userId;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.phone_num = phone_num;
        this.expirationDate = expirationDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(String phone_num) {
        this.phone_num = phone_num;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return "WeiBoCallbackBean{" +
                "userId='" + userId + '\'' +
                ", accessToken='" + accessToken + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                ", phone_num='" + phone_num + '\'' +
                ", expirationDate='" + expirationDate + '\'' +
                '}';
    }
}
