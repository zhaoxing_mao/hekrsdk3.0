package me.hekr.hummingbird.bean;

import java.util.List;

/**
 * Created by hekr_jds on 5/13 0013.
 **/
public class NewsBean {


    /**
     * page : 1
     * size : 50
     * totalResults : 2
     * totalPages : 1
     * first : true
     * last : true
     * result : [{"id":"573439c0e4b00e068514564a","authorName":"朱义生","updateTime":1463040448308,"title":"测试推送内容是否为空","infoTags":["测试"],"infoContent":"http://7xsk3w.com1.z0.glb.clouddn.com/00000000000/ffptGiru.html","status":1},{"id":"5732d901e4b04466fc15cf82","authorName":"小伟","updateTime":1462950145470,"title":"欢迎使用蜂鸟APP","infoTags":["火火"],"infoContent":"http://7xsk3w.com1.z0.glb.clouddn.com/00000000000/kCfAXCW1.html","status":1}]
     */

    private int page;
    private int size;
    private int totalResults;
    private int totalPages;
    private boolean first;
    private boolean last;
    /**
     * id : 573439c0e4b00e068514564a
     * authorName : 朱义生
     * updateTime : 1463040448308
     * title : 测试推送内容是否为空
     * infoTags : ["测试"]
     * infoContent : http://7xsk3w.com1.z0.glb.clouddn.com/00000000000/ffptGiru.html
     * status : 1
     */

    private List<Result> result;

    public NewsBean() {
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public static class Result {
        private String id;
        private String authorName;
        private long updateTime;
        private String title;
        private String infoContent;
        private int status;
        private List<String> infoTags;

        public Result() {
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAuthorName() {
            return authorName;
        }

        public void setAuthorName(String authorName) {
            this.authorName = authorName;
        }

        public long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(long updateTime) {
            this.updateTime = updateTime;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getInfoContent() {
            return infoContent;
        }

        public void setInfoContent(String infoContent) {
            this.infoContent = infoContent;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public List<String> getInfoTags() {
            return infoTags;
        }

        public void setInfoTags(List<String> infoTags) {
            this.infoTags = infoTags;
        }
    }
}
