package me.hekr.hummingbird.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.bean.OAuthListBean;


/**
 * Created by hekr_jin123d on 2016/3/31.
 **/
public class AuthAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<OAuthListBean> lists;
    private Context context;

    public AuthAdapter() {
    }

    public void setLists(List<OAuthListBean> lists) {
        this.lists = lists;
    }

    public AuthAdapter(List<OAuthListBean> lists, Context context) {
        this.lists = lists;
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (lists != null && !lists.isEmpty()) {
            return lists.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        if (lists != null && !lists.isEmpty()) {
            return lists.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OAuthListBean oAuthListBean = lists.get(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.activity_list_auth_item_device, parent, false);
            holder.device_name = (TextView) convertView.findViewById(R.id.username);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag(); //取出ViewHolder对象
        }

        if (oAuthListBean != null && !TextUtils.isEmpty(oAuthListBean.getGranteeName())) {
            //Log.i("oAuthListBean","oAuthListBean.getGranteeName():"+oAuthListBean.getGranteeName());
            holder.device_name.setText(oAuthListBean.getGranteeName());
        } else {
            holder.device_name.setText(context.getString(R.string.visitor));
        }
        return convertView;
    }

    public final class ViewHolder {
        public TextView device_name;
    }
}
