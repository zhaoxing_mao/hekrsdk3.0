package me.hekr.hummingbird.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.litesuits.common.assist.Toastor;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.activity.WebViewActivity;
import me.hekr.hummingbird.bean.DeviceAndFolderBean;
import me.hekr.hummingbird.bean.DeviceBean;
import me.hekr.hummingbird.bean.FolderBean;
import me.hekr.hummingbird.event.DeviceEvent;
import me.hekr.hummingbird.itemTouchHelper.FolderItemTouchHelperCallback;
import me.hekr.hummingbird.itemTouchHelper.FolderItemTouchMoveListener;
import me.hekr.hummingbird.itemTouchHelper.ItemTouchMoveListener;
import me.hekr.hummingbird.itemTouchHelper.OnStartDragListener;
import me.hekr.hummingbird.ui.RecyclerViewEmptySupport;
import me.hekr.hummingbird.util.HekrAlert;
import me.hekr.hummingbird.util.HekrCodeUtil;
import me.hekr.hummingbird.util.Validator;
import me.hekr.hummingbird.viewHolder.DevicesViewHolder;
import me.hekr.hummingbird.viewHolder.DividerGridItemDecoration;
import me.hekr.hummingbird.viewHolder.FolderViewHolder;
import me.hekr.hummingbird.viewHolder.GroupViewHolder;

/**
 * Created by jin123d on 2016/2/29.
 **/
public class DevicesAndFolderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemTouchMoveListener {
    private List<DeviceBean> lists;

    private LayoutInflater inflater;
    private Context context;
    private DisplayImageOptions options;
    public static final int TYPE_DEVICES = 0;
    public static final int TYPE_FOLDER = 1;
    public static final int TYPE_GROUP = 2;
    private HekrUserAction hekrUserAction;
    private Toastor toastor;
    private int mUpY;
    private Dialog dialog;


    public DevicesAndFolderAdapter(Context context, List<DeviceBean> lists) {
        this.lists = lists;
        this.context = context;
        toastor = new Toastor(context);
        hekrUserAction = HekrUserAction.getInstance(context);
        inflater = LayoutInflater.from(context);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.ic_hekr_logo)
                .showImageOnFail(R.mipmap.ic_hekr_logo)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)//设置为RGB565比起默认的ARGB_8888要节省大量的内存
                .delayBeforeLoading(100)//载入图片前稍做延时可以提高整体滑动的流畅度
                .build();

    }

    public void setList(List<DeviceBean> list) {
        this.lists = list;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (TYPE_DEVICES == viewType) {
            View itemView_d = inflater.inflate(R.layout.layout_item_devices, parent, false);
            return new DevicesViewHolder(itemView_d);
        } else if (TYPE_FOLDER == viewType) {
            View itemView_f = inflater.inflate(R.layout.layout_item_folder, parent, false);
            return new FolderViewHolder(itemView_f);
        } else {
            View itemView_g = inflater.inflate(R.layout.layout_item_group, parent, false);
            return new GroupViewHolder(itemView_g);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final DeviceBean deviceBean = lists.get(position);
        final int viewType = getItemViewType(position);
        DeviceBean deviceBeanGroup = null;
        switch (viewType) {
            case TYPE_DEVICES:
                bindDevicesItem(deviceBean, (DevicesViewHolder) holder);
                break;
            case TYPE_FOLDER:
                FolderViewHolder viewHolder_f = (FolderViewHolder) holder;
                viewHolder_f.tv_name.setText(deviceBean.getDeviceAndFolderBean().getFolderName());
                viewHolder_f.tv_number.setText(String.valueOf(deviceBean.getDeviceAndFolderBean().getLists().size()));
                break;
            case TYPE_GROUP:
                GroupViewHolder viewHolder_g = (GroupViewHolder) holder;
                String url;
                viewHolder_g.tv_name.setText(deviceBean.getGroupBean().getGroupName());
                viewHolder_g.tv_number.setText(String.valueOf(deviceBean.getGroupBean().getDeviceList().size()));
                if (!deviceBean.getGroupBean().getDeviceList().isEmpty()) {
                    //遍历拿到图标！
                    String DevTid = deviceBean.getGroupBean().getDeviceList().get(0).getDevTid();
                    for (DeviceBean deviceBean_g : lists) {
                        if (TextUtils.equals(DevTid, deviceBean_g.getDevTid())) {
                            url = deviceBean_g.getAndroidH5Page() + "&group=true";
                            deviceBean_g.setAndroidH5Page(url);
                            deviceBeanGroup = deviceBean_g;
                            break;
                        }
                    }
                }
                ImageLoader imageLoader = ImageLoader.getInstance();
                if (deviceBeanGroup != null) {
                    imageLoader.displayImage(deviceBeanGroup.getLogo(), viewHolder_g.img_icon, options);
                }

                break;
        }

        final DeviceBean finalDeviceBeanGroup = deviceBeanGroup;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TYPE_DEVICES == viewType) {
                    if (deviceBean.isOnline()) {
                        EventBus.getDefault().postSticky(new DeviceEvent(deviceBean));
                        context.startActivity(new Intent(context, WebViewActivity.class));
                    } else {
                        toastor.showSingletonToast(context.getString(R.string.device_offline));
                    }
                } else if (TYPE_FOLDER == viewType) {
                    folderDialog(deviceBean);
                } else if (TYPE_GROUP == viewType) {
                    //群控！
                    EventBus.getDefault().postSticky(new DeviceEvent(finalDeviceBeanGroup));
                    context.startActivity(new Intent(context, WebViewActivity.class));

                }
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });

    }


    @Override
    public int getItemCount() {
        return lists.size();
    }

    /**
     * 根据不同的目录的不同
     */
    @Override
    public int getItemViewType(int position) {
        if (lists.get(position).getDeviceAndFolderBean() != null) {
            return TYPE_FOLDER;
        } else if (lists.get(position).getGroupBean() != null) {
            return TYPE_GROUP;
        } else {
            return TYPE_DEVICES;
        }

    }

    /**
     * 绑定设备item
     */
    private void bindDevicesItem(DeviceBean deviceBean, DevicesViewHolder viewHolder) {
        viewHolder.itemView.setClickable(true);
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(deviceBean.getLogo(), viewHolder.img_icon, options);
        if (TextUtils.isEmpty(deviceBean.getDeviceName())) {
            String cidName = deviceBean.getCidName();
            viewHolder.tv.setText(cidName.substring(cidName.indexOf("/") + 1));
        } else {
            viewHolder.tv.setText(deviceBean.getDeviceName());
        }
        if (deviceBean.isOnline()) {
            viewHolder.itemView.setAlpha(1.0f);
            viewHolder.tv_state.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.itemView.setAlpha(0.6f);
            viewHolder.tv_state.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onItemMoveOver(RecyclerView.ViewHolder viewHolder, int fromPosition, int toPosition) {
        if (getItemViewType(fromPosition) == TYPE_DEVICES) {
            if (getItemViewType(toPosition) == TYPE_DEVICES) {
                //目标位置为设备，新建文件夹
                merge(fromPosition, toPosition);
            } else if (getItemViewType(toPosition) == TYPE_FOLDER) {
                devicePutFolder(fromPosition, toPosition);
            }
        }
    }


    /**
     * 将device放到folder下面
     *
     * @param fromPosition 起始id
     * @param toPosition   目录id
     */
    private void devicePutFolder(final int fromPosition, int toPosition) {
        final DeviceBean toDeviceBean = lists.get(toPosition);
        DeviceBean fromDeiceBean = lists.get(fromPosition);
        hekrUserAction.devicesPutFolder(toDeviceBean.getDeviceAndFolderBean().getFolderId(), fromDeiceBean.getCtrlKey(), fromDeiceBean.getDevTid(), new HekrUser.DevicePutFolderListener() {
            @Override
            public void putSuccess() {
                //放置成功
                toDeviceBean.getDeviceAndFolderBean().getLists().add(lists.get(fromPosition));
                lists.remove(fromPosition);
                DevicesAndFolderAdapter.this.notifyDataSetChanged();
            }

            @Override
            public void putFail(int errorCode) {
                toastor.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
            }
        });
    }


    /**
     * 将指定的id放置到目录里面
     *
     * @param deviceBean 设备
     * @param folderBean 目录
     */
    private void put(final DeviceBean deviceBean, FolderBean folderBean) {
        hekrUserAction.devicesPutFolder(folderBean.getFolderId(), deviceBean.getCtrlKey(), deviceBean.getDevTid(), new HekrUser.DevicePutFolderListener() {
            @Override
            public void putSuccess() {
                //放置成功
                lists.remove(deviceBean);
                DevicesAndFolderAdapter.this.notifyDataSetChanged();
            }

            @Override
            public void putFail(int errorCode) {
                DevicesAndFolderAdapter.this.notifyDataSetChanged();
                toastor.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
            }
        });
    }


    /**
     * addFolder
     *
     * @param fromPosition 第一个设备的positing
     * @param toPosition   第二个设备的posting
     */
    private void merge(final int fromPosition, final int toPosition) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_remove_item, null);
        final AppCompatEditText editText = (AppCompatEditText) view.findViewById(R.id.et_folder_name);
        editText.setFocusable(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.new_folder))
                .setView(view)
                .setPositiveButton(context.getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String newName = editText.getText().toString().trim();
                        if (Validator.isLimitEx(newName) || TextUtils.isEmpty(newName)) {
                            toastor.showSingleLongToast(R.string.please_input_valid);
                        } else if (TextUtils.isEmpty(editText.getText().toString().trim())) {
                            HekrAlert.hekrAlert(context, context.getString(R.string.empty_folder));
                        } else {
                            hekrUserAction.addFolder(editText.getText().toString().trim(), new HekrUser.AddFolderListener() {
                                @Override
                                public void addFolderSuccess(FolderBean folderBean) {
                                    DeviceBean fromDevice = lists.get(fromPosition);
                                    DeviceBean toDevice = lists.get(toPosition);
                                    List<DeviceBean> devices = new ArrayList<>();
                                    //folder创建成功之后将两个设备都添加进去
                                    put(fromDevice, folderBean);
                                    put(toDevice, folderBean);

                                    devices.add(fromDevice);
                                    devices.add(toDevice);

                                    lists.add(new DeviceBean(new DeviceAndFolderBean(folderBean.getFolderId(), folderBean.getFolderName(), devices)));
                                    DevicesAndFolderAdapter.this.notifyDataSetChanged();
                                }

                                @Override
                                public void addFolderFail(int errorCode) {
                                    //创建folder失败
                                    HekrAlert.hekrAlert(context, HekrCodeUtil.errorCode2Msg(errorCode));
                                }
                            });
                        }

                    }
                })
                .setNegativeButton(context.getString(R.string.negative_button), null);
        builder.create().show();
    }

    /**
     * 点开dialog
     *
     * @param deviceBean 将当前的itemBean传入即可
     */
    private void folderDialog(final DeviceBean deviceBean) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_folder_dialog, null, false);
        final EditText editText = (EditText) view.findViewById(R.id.title);
        final RecyclerViewEmptySupport mRecyclerView = (RecyclerViewEmptySupport) view.findViewById(R.id.mRecyclerView);
        //默认设置布局为3列
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 3);
        mRecyclerView.addItemDecoration(new DividerGridItemDecoration(context));
        mRecyclerView.setLayoutManager(gridLayoutManager);
        // mRecyclerView.setAdapter(dialogAdapter(deviceBean));
        FolderAdapter adapter = new FolderAdapter(context, deviceBean, mRecyclerView);
        mRecyclerView.setAdapter(adapter);
        move(adapter, mRecyclerView);
        mUpY = 0;
        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                switch (e.getAction()) {
                    case MotionEvent.ACTION_UP:
                        mUpY = (int) e.getY();
                        //toastor.showSingletonToast(String.valueOf(mRecyclerView.indexOfChild(mRecyclerView.findChildViewUnder(e.getX(), e.getY()))));
                        break;
                }
                return false;
            }
        });

        //----------------
        editText.setText(deviceBean.getDeviceAndFolderBean().getFolderName());
        dialog = new AlertDialog.Builder(context)
                .setView(view).create();
        dialog.getWindow().setBackgroundDrawable(new BitmapDrawable());
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                String newName = editText.getText().toString().trim();
                if (Validator.isLimitEx(newName) || TextUtils.isEmpty(newName)) {
                    toastor.showSingleLongToast(R.string.please_input_valid);
                } else if (!TextUtils.equals(deviceBean.getDeviceAndFolderBean().getFolderName(), newName)) {
                    hekrUserAction.renameFolder(editText.getText().toString().trim(), deviceBean.getDeviceAndFolderBean().getFolderId(), new HekrUser.RenameFolderListener() {
                        @Override
                        public void renameSuccess() {
                            deviceBean.getDeviceAndFolderBean().setFolderName(editText.getText().toString().trim());
                            DevicesAndFolderAdapter.this.notifyDataSetChanged();
                        }

                        @Override
                        public void renameFail(int errorCode) {
                            toastor.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
                        }
                    });
                }
            }
        });
    }


    /**
     * dialog拖动效果
     */
    private void move(FolderAdapter adapter, RecyclerView mRecyclerView) {
        ItemTouchHelper.Callback callback = new FolderItemTouchHelperCallback(adapter, (OnStartDragListener) context);
        //绑定到控件中
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
    }


    /**
     * folderAdapter
     */
    public class FolderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements FolderItemTouchMoveListener {
        private List<DeviceBean> devicesLists;

        private LayoutInflater inflater;
        private Context context;
        private DeviceBean deviceBean;
        private View view;


        public FolderAdapter(Context context, DeviceBean deviceBean, View view) {
            this.deviceBean = deviceBean;
            this.devicesLists = deviceBean.getDeviceAndFolderBean().getLists();
            this.context = context;
            this.inflater = LayoutInflater.from(context);
            this.view = view;
        }

        public void setList(List<DeviceBean> list) {
            this.devicesLists = list;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView_d = inflater.inflate(R.layout.layout_item_devices, parent, false);
            return new DevicesViewHolder(itemView_d);

        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
            final DeviceBean deviceBean = devicesLists.get(position);
            bindDevicesItem(deviceBean, (DevicesViewHolder) holder);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deviceBean.isOnline()) {
                        EventBus.getDefault().postSticky(new DeviceEvent(deviceBean));
                        context.startActivity(new Intent(context, WebViewActivity.class));
                    } else {
                        toastor.showSingletonToast(R.string.device_offline);
                    }
                }
            });

        }


        @Override
        public int getItemCount() {
            return devicesLists.size();
        }


        @Override
        public void onItemMoveOver(RecyclerView.ViewHolder viewHolder, final int fromPosition) {
            //<0表示拖出到屏幕上方 >view高表示拖到下方
            if (mUpY < 0 || mUpY > view.getHeight()) {
                final String folderId = deviceBean.getDeviceAndFolderBean().getFolderId();
                //将设备从目录移动到根目录
                hekrUserAction.folderToRoot(folderId, devicesLists.get(fromPosition).getCtrlKey(), devicesLists.get(fromPosition).getDevTid(), new HekrUser.DevicePutFolderListener() {
                    @Override
                    public void putSuccess() {
                        lists.add(devicesLists.get(fromPosition));
                        devicesLists.remove(devicesLists.get(fromPosition));
                        DevicesAndFolderAdapter.this.notifyDataSetChanged();
                        FolderAdapter.this.notifyDataSetChanged();

                        if (devicesLists.isEmpty()) {
                            //如果list被清空，则直接把目录给删除！
                            hekrUserAction.deleteFolder(folderId, new HekrUser.DeleteFolderListener() {
                                @Override
                                public void deleteSuccess() {
                                    lists.remove(deviceBean);
                                    if (dialog != null && dialog.isShowing()) {
                                        dialog.dismiss();
                                    }
                                    DevicesAndFolderAdapter.this.notifyDataSetChanged();
                                    FolderAdapter.this.notifyDataSetChanged();
                                }

                                @Override
                                public void deleteFail(int errorCode) {
                                    toastor.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
                                }
                            });
                        }
                    }

                    @Override
                    public void putFail(int errorCode) {
                        toastor.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
                    }
                });
            }
        }
    }

}
