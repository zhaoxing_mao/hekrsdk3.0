package me.hekr.hummingbird.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import me.hekr.hummingbird.R;

public class GalleryAdapter extends
		RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

	public interface OnItemClickListener {
		void onItemClick(View view, int position);
	}

	private OnItemClickListener mOnItemClickListener;

	public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
		this.mOnItemClickListener = mOnItemClickListener;
	}

	private LayoutInflater mInflater;
	private List<String> data;
	private DisplayImageOptions options;

	public GalleryAdapter(Context context, List<String> data) {
		mInflater = LayoutInflater.from(context);
		this.data = data;

		options = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.showImageOnFail(R.mipmap.appicon)
				.showImageForEmptyUri(R.mipmap.appicon)
				.showImageOnLoading(R.mipmap.appicon)
				.bitmapConfig(Bitmap.Config.RGB_565)//设置为RGB565比起默认的ARGB_8888要节省大量的内存
				.delayBeforeLoading(100)//载入图片前稍做延时可以提高整体滑动的流畅度
				.build();
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public ViewHolder(View arg0) {
			super(arg0);
		}
		ImageView mImg;
	}

	@Override
	public int getItemCount() {
		return data.size();
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
		View view = mInflater.inflate(R.layout.activity_index_gallery_item,
				viewGroup, false);
		ViewHolder viewHolder = new ViewHolder(view);

		viewHolder.mImg = (ImageView) view
				.findViewById(R.id.id_index_gallery_item_image);
		return viewHolder;
	}

	@Override
	public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

		if(data!=null&&!data.isEmpty()) {
			ImageLoader imageLoader = ImageLoader.getInstance();
			imageLoader.displayImage(data.get(i), viewHolder.mImg, options);
		}

		if (mOnItemClickListener != null) {
			viewHolder.itemView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v)
				{
					mOnItemClickListener.onItemClick(viewHolder.itemView, i);
				}
			});

		}

	}

}
