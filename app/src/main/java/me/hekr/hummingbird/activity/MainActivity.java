package me.hekr.hummingbird.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;

import com.igexin.sdk.PushManager;
import com.litesuits.android.log.Log;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.service.DiscoveryService;
import me.hekr.hummingbird.service.WebSocketService;

/**
 * updated by xm on 16/4/28.
 **/
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
    }

    private void initData() {

        //SqliteHelper sqliteHelper = new SqliteHelper(this, "device_list_db", null, 1);
        startService(new Intent(MainActivity.this, DiscoveryService.class));
        //startService(new Intent(MainActivity.this, DownLoadService.class));
        //startService(new Intent(MainActivity.this, LANService.class));

        if (TextUtils.isEmpty(HekrUserAction.getInstance(this).getJWT_TOKEN())) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                }
            }, 500);

        } else {
            if(!PushManager.getInstance().isPushTurnedOn(MainActivity.this)) {
                PushManager.getInstance().turnOnPush(MainActivity.this);
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startService(new Intent(MainActivity.this, WebSocketService.class));
                    startActivity(new Intent(MainActivity.this, ScrollingActivity.class));
                    finish();
                }
            }, 500);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("MainActivity", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("MainActivity", "onDestroy");
    }
}
