package me.hekr.hummingbird.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;

import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.fragment.InputUserNameFragment;
import me.hekr.hummingbird.ui.TitleBar;

import me.hekr.hummingbird.R;

public class ForgotPwdActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pwd);
    }

    protected void initData() {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        InputUserNameFragment fragment = new InputUserNameFragment();
        fragmentTransaction.add(R.id.fg, fragment);
        fragmentTransaction.commit();
    }

    protected void initView() {
        TitleBar titleBar = (TitleBar) findViewById(R.id.title);
        if (titleBar != null) {
            titleBar.setBack(new TitleBar.BackListener() {
                @Override
                public void click() {
                    finish();
                }
            });
        }
    }
}
