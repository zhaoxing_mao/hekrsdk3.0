package me.hekr.hummingbird.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.igexin.sdk.PushManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.bean.ProfileBean;
import me.hekr.hummingbird.event.LogoutEvent;
import me.hekr.hummingbird.event.RefreshEvent;
import me.hekr.hummingbird.ui.CircleImageView;
import me.hekr.hummingbird.util.ConstantsUtil;
import me.hekr.hummingbird.util.DensityUtils;


public class PersonalCenterActivity extends BaseActivity implements View.OnClickListener {
    //private MyToast myToast;
    public Toolbar toolbar;
    public RelativeLayout message;
    public RelativeLayout security;
    public RelativeLayout setting;

    private CircleImageView circleImageView;
    private TextView tv_user_name;
    public Button btn_logout;
    private HekrUserAction hekrUserAction;
    private DisplayImageOptions options;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_center);
        EventBus.getDefault().register(this);
    }

    protected void initData() {
        toolbar.setNavigationIcon(R.drawable.ic_left_white);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)//设置为RGB565比起默认的ARGB_8888要节省大量的内存
                .build();

        message.setOnClickListener(this);
        security.setOnClickListener(this);
        setting.setOnClickListener(this);
        circleImageView.setOnClickListener(this);
        btn_logout.setOnClickListener(this);
        tv_user_name.setOnClickListener(this);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appBar);
            if (appBarLayout != null) {
                appBarLayout.setPadding(0, DensityUtils.dp2px(this, 25), 0, 0);
            }
        }
        hekrUserAction = HekrUserAction.getInstance(this);
        updateUI();

    }

    private void updateUI() {
        ProfileBean profileBean = hekrUserAction.getUserCache();
        String logo = profileBean.getAvatarUrl().getSmall();
        //如果本地有头像，则直接加载
        if (TextUtils.isEmpty(logo)) {
            getUserInfo();
        } else {
            ImageLoader.getInstance().displayImage(logo, circleImageView, options);
            tv_user_name.setText(profileBean.getLastName());
        }
    }

    protected void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        message = (RelativeLayout) findViewById(R.id.message);
        security = (RelativeLayout) findViewById(R.id.security);
        setting = (RelativeLayout) findViewById(R.id.setting);
        circleImageView = (CircleImageView) findViewById(R.id.img_user_icon);
        tv_user_name = (TextView) findViewById(R.id.tv_user_name);
        btn_logout = (Button) findViewById(R.id.btn_logout);
        AppCompatTextView tv_title = (AppCompatTextView) findViewById(R.id.tv_title);
        if (tv_title != null) {
            tv_title.setText("");
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_user_name:
            case R.id.img_user_icon:
                startActivity(new Intent(this, UserInformationActivity.class));
                break;
            case R.id.message:
                start(NewsActivity.class);
                break;
            case R.id.security:
                start(UserSecurityActivity.class);
                break;
            case R.id.setting:
                start(SettingActivity.class);
                break;
            case R.id.btn_logout:
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle(getString(R.string.app_name));
                alert.setMessage(getString(R.string.logout));
                alert.setPositiveButton(getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        hekrUserAction.userLogout();
                        EventBus.getDefault().post(new LogoutEvent(true));
                        PushManager.getInstance().turnOffPush(PersonalCenterActivity.this);
                        PushManager.getInstance().unBindAlias(PersonalCenterActivity.this, hekrUserAction.getUserId(), true);
                        finish();
                    }
                });
                alert.setNegativeButton(getString(R.string.negative_button), null).create().show();
                break;
        }
    }


    private void start(Class c) {
        startActivity(new Intent(PersonalCenterActivity.this, c));
    }


    private void getUserInfo() {
        hekrUserAction.getProfile(new HekrUser.GetProfileListener() {
                                      @Override
                                      public void getProfileSuccess(ProfileBean profileBean) {
                                          try {
                                              if (profileBean.getAvatarUrl().getSmall() != null) {
                                                  String img_logo = profileBean.getAvatarUrl().getSmall();
                                                  ImageLoader.getInstance().displayImage(img_logo, circleImageView, options);
                                              }
                                              if (!TextUtils.isEmpty(profileBean.getLastName())) {
                                                  tv_user_name.setText(profileBean.getLastName());
                                              }
                                          } catch (Exception e) {
                                              e.printStackTrace();
                                          }
                                      }

                                      @Override
                                      public void getProfileFail(int errorCode) {

                                      }
                                  }

        );
    }


    @Subscribe
    public void onEvent(RefreshEvent event) {
        switch (event.getRefreshTag()) {
            case RefreshEvent.REFRESH_USER:
                updateUI();
                break;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
