package me.hekr.hummingbird.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.litesuits.common.assist.Toastor;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.bean.DeviceBean;
import me.hekr.hummingbird.bean.OAuthBean;
import me.hekr.hummingbird.event.DeviceEvent;
import me.hekr.hummingbird.ui.CustomProgress;
import me.hekr.hummingbird.ui.TitleBar;
import me.hekr.hummingbird.util.WindowUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Hashtable;

import me.hekr.hummingbird.R;

public class ShareActivity extends BaseActivity {

    private static final String TAG = "ShareActivity";
    private static int QR_WIDTH = 0;
    private static int QR_HEIGHT = 0;
    private ImageView qr_code;
    public TitleBar titleBar;
    private HekrUserAction hekrUserAction;
    public DeviceBean deviceBean;
    private CustomProgress customProgress;
    private Toastor toastor;
    private LinearLayout qr_code_ry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
    }

    protected void initView() {
        qr_code = (ImageView) findViewById(R.id.qr_code_img);
        titleBar = (TitleBar) findViewById(R.id.title);
        qr_code_ry= (LinearLayout) findViewById(R.id.qr_code_ry);
        titleBar.setBack(new TitleBar.BackListener() {
            @Override
            public void click() {
                finish();
            }
        });
        customProgress = CustomProgress.show(this, false, null);
        toastor=new Toastor(this);
    }

    protected void initData() {
        QR_WIDTH = (int) (WindowUtil.getScreenWidth(this) * 0.6);
        QR_HEIGHT = (int) (WindowUtil.getScreenWidth(this) * 0.6);
        hekrUserAction = HekrUserAction.getInstance(ShareActivity.this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceEvent event) {
        // UI updates must run on MainThread
        deviceBean = event.getDeviceBean();
        Log.i(TAG, "deviceBean:" + deviceBean.toString());
        //更新UI
        hekrUserAction.oAuthCreateCode(new OAuthBean(hekrUserAction.getUserId(), deviceBean.getDevTid(), deviceBean.getCtrlKey(), 2147483647, "ALL", deviceBean.getDesc()), new HekrUser.CreateOAuthQRCodeListener() {
            @Override
            public void createSuccess(String reverseAuthorizationTemplateId) {
                Log.i(TAG, "reverseAuthorizationTemplateId:" + reverseAuthorizationTemplateId);
                if (!TextUtils.isEmpty(reverseAuthorizationTemplateId)) {
                    createQRImage(reverseAuthorizationTemplateId);
                }
            }

            @Override
            public void createFail(int errorCode) {
                Log.i(TAG, "errorCode:" + errorCode);
                customProgress.dismiss();
                toastor.showSingletonToast(getString(R.string.qr_code_create_fail));
                qr_code_ry.setVisibility(View.GONE);
                //finish();
            }
        });

    }

    public void createQRImage(String url) {
        try {
            //判断URL合法性
            if (url == null || "".equals(url) || url.length() < 1) {
                return;
            }
            Hashtable<EncodeHintType, String> hints = new Hashtable<>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            //图像数据转换，使用了矩阵转换
            BitMatrix bitMatrix = new QRCodeWriter().encode(url, BarcodeFormat.QR_CODE, QR_WIDTH, QR_HEIGHT, hints);
            int[] pixels = new int[QR_WIDTH * QR_HEIGHT];
            //下面这里按照二维码的算法，逐个生成二维码的图片，
            //两个for循环是图片横列扫描的结果
            for (int y = 0; y < QR_HEIGHT; y++) {
                for (int x = 0; x < QR_WIDTH; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * QR_WIDTH + x] = 0xff000000;
                    }
                    //加上下面有白边
//                    else {
//                    pixels[y * QR_WIDTH + x] = 0xFFFFFFFF;
//                    }
                }
            }
            //生成二维码图片的格式，使用ARGB_8888
            Bitmap bitmap = Bitmap.createBitmap(QR_WIDTH, QR_HEIGHT, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, QR_WIDTH, 0, 0, QR_WIDTH, QR_HEIGHT);
            //显示到一个ImageView上面
            if (QR_WIDTH != 0 && QR_HEIGHT != 0) {
                qr_code.setImageBitmap(bitmap);
                customProgress.dismiss();
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
        if(customProgress!=null&&customProgress.isShowing()){
            customProgress.dismiss();
        }
    }
}
