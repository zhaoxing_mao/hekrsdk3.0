package me.hekr.hummingbird.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.TextUtils;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.bean.ProfileBean;
import me.hekr.hummingbird.fragment.ChangeEmailFragment;
import me.hekr.hummingbird.fragment.ChangePhoneFragment;
import me.hekr.hummingbird.ui.CustomProgress;
import me.hekr.hummingbird.ui.TitleBar;

import com.litesuits.common.assist.Toastor;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.util.HekrCodeUtil;

public class ChangeBindActivity extends BaseActivity {
    CustomProgress customProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind);
    }


    protected void initData() {
        customProgress = CustomProgress.show(this, true, null);
        final FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        HekrUserAction hekrUserAction = HekrUserAction.getInstance(this);
        hekrUserAction.getProfile(new HekrUser.GetProfileListener() {
            @Override
            public void getProfileSuccess(ProfileBean profileBean) {
                customProgress.dismiss();
                if (!TextUtils.isEmpty(profileBean.getPhoneNumber())) {
                    fragmentTransaction.replace(R.id.fg, ChangePhoneFragment.newInstance(profileBean.getPhoneNumber()));
                    fragmentTransaction.commit();
                } else if (!TextUtils.isEmpty(profileBean.getEmail())) {
                    fragmentTransaction.replace(R.id.fg, ChangeEmailFragment.newInstance(profileBean.getEmail()));
                    fragmentTransaction.commit();
                } else {
                    finish();
                }
            }

            @Override
            public void getProfileFail(int errorCode) {
                customProgress.dismiss();
                new Toastor(ChangeBindActivity.this).showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
                ChangeBindActivity.this.finish();
            }
        });


    }

    protected void initView() {
        TitleBar titleBar = (TitleBar) findViewById(R.id.title);
        if (titleBar != null) {
            titleBar.setBack(new TitleBar.BackListener() {
                @Override
                public void click() {
                    finish();
                }
            });
        }
    }


}
