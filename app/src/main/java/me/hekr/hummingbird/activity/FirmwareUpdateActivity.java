package me.hekr.hummingbird.activity;


import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.litesuits.android.log.Log;
import com.litesuits.common.assist.Toastor;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.action.HekrData;
import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.bean.DeviceBean;
import me.hekr.hummingbird.bean.FirmwareBean;
import me.hekr.hummingbird.event.ClearFilterEvent;
import me.hekr.hummingbird.event.DeviceEvent;
import me.hekr.hummingbird.ui.CustomProgress;
import me.hekr.hummingbird.ui.PercentageRing;
import me.hekr.hummingbird.ui.TitleBar;
import me.hekr.hummingbird.util.MsgUtil;
import me.hekr.hummingbird.x5web.X5WebView;

public class FirmwareUpdateActivity extends BaseActivity implements View.OnClickListener {

    public static final String TAG = "FirmwareUpdateActivity";
    private HekrUserAction hekrUserAction;
    private DeviceBean deviceBean;
    private TextView now_firmware_version;
    private TextView update_status;
    private PercentageRing mPercentageRing;
    private FirmwareBean mfirmwareBean;
    private Toastor toastor;

    private int isNeedUpdate = 0;
    private X5WebView currentView;
    private CustomProgress customProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firmware_update);
        EventBus.getDefault().register(this);
    }

    protected void initView() {
        TitleBar titleBar = (TitleBar) findViewById(R.id.title);
        if (titleBar != null) {
            titleBar.setBack(new TitleBar.BackListener() {
                @Override
                public void click() {
                    finish();
                }
            });
        }

        now_firmware_version = (TextView) findViewById(R.id.firmware_version_content);
        update_status = (TextView) findViewById(R.id.update_status);
        mPercentageRing = (PercentageRing) findViewById(R.id.circle);
        toastor = new Toastor(this);
        update_status.setOnClickListener(this);

        customProgress = CustomProgress.show(this, false, null);
    }

    protected void initData() {
        hekrUserAction = HekrUserAction.getInstance(this);
        currentView = new X5WebView(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceEvent event) {
        // UI updates must run on MainThread
        deviceBean = event.getDeviceBean();

        if (deviceBean != null) {
            if (!TextUtils.isEmpty(deviceBean.getBinVersion())) {
                now_firmware_version.setText(deviceBean.getBinVersion());
            }

            if (!TextUtils.isEmpty(deviceBean.getDevTid()) &&
                    !TextUtils.isEmpty(deviceBean.getProductPublicKey()) &&
                    !TextUtils.isEmpty(deviceBean.getBinType()) &&
                    !TextUtils.isEmpty(deviceBean.getBinVersion())) {

                hekrUserAction.checkFirmwareUpdate(deviceBean.getDevTid(), deviceBean.getProductPublicKey(), deviceBean.getBinType(), deviceBean.getBinVersion(), new HekrUser.CheckFwUpdateListener() {
                    @Override
                    public void checkNotNeedUpdate() {
                        isNeedUpdate = 0;
                        update_status.setText(getString(R.string.activity_firmware_is_updated));
                        update_status.setClickable(false);
                        customProgress.dismiss();
                    }

                    @Override
                    public void checkNeedUpdate(FirmwareBean firmwareBean) {
                        isNeedUpdate = 1;
                        switchTextView(1);
                        mfirmwareBean = firmwareBean;
                        customProgress.dismiss();
                    }

                    @Override
                    public void checkFail(int errorCode) {
                        customProgress.dismiss();
                        toastor.showSingletonToast(getString(R.string.activity_firmware_get_information_fail));
                    }
                });
            }
        }

    }

    private void switchTextView(int type) {
        if (type == 1) {
            if (deviceBean.isOnline()) {
                update_status.setBackgroundResource(R.drawable.firmware_btn_bg_press);
                update_status.setText(getString(R.string.activity_firmware_update));
                update_status.setTextColor(ContextCompat.getColor(this, R.color.firmware_update_text_color));
                update_status.setClickable(true);
            } else {
                update_status.setText(getString(R.string.activity_firmware_update));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.update_status:
                if (deviceBean.isOnline()) {
                    if (isNeedUpdate == 1) {
                        if (deviceBean != null && mfirmwareBean != null) {
                            JSONObject devUpgradeObject = new JSONObject();
                            JSONObject params = new JSONObject();

                            JSONObject receiveDevUpgradeObject = new JSONObject();
                            JSONObject receiveParams = new JSONObject();

                            try {
                                params.put("devTid", deviceBean.getDevTid());
                                params.put("ctrlKey", deviceBean.getCtrlKey());
                                params.put("binUrl", mfirmwareBean.getBinUrl());
                                params.put("md5", mfirmwareBean.getMd5());
                                params.put("binType", mfirmwareBean.getLatestBinType());
                                params.put("binVer", mfirmwareBean.getLatestBinVer());
                                params.put("size", mfirmwareBean.getSize());

                                devUpgradeObject.put("action", "devUpgrade");
                                devUpgradeObject.put("params", params);


                                receiveParams.put("devTid", deviceBean.getDevTid());
                                receiveParams.put("upgradeProgress", null);
                                receiveParams.put("upgradeState", null);

                                receiveDevUpgradeObject.put("action", "devUpgrade");
                                receiveDevUpgradeObject.put("params", receiveParams);

                                isNeedUpdate = 0;

                                //发送升级命令
                                MsgUtil.sendMsg(currentView, deviceBean.getDevTid(), devUpgradeObject, new HekrData.dataReceiverListener() {
                                    @Override
                                    public void onReceiveSuccess(String msg) {
                                        Log.i(TAG, "msg:" + msg);
                                        if (!TextUtils.isEmpty(msg)) {
                                            try {
                                                if (TextUtils.equals(new JSONObject(msg).getString("action"), "devUpgradeResp") &&
                                                        TextUtils.equals(new JSONObject(msg).getJSONObject("params").getString("devTid"), deviceBean.getDevTid()) &&
                                                        TextUtils.equals(new JSONObject(msg).getJSONObject("params").getString("ctrlKey"), deviceBean.getCtrlKey())) {
                                                    toastor.showSingletonToast(getString(R.string.activity_firmware_update_is_send_success));
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onReceiveTimeout() {
                                        Log.i(TAG, "msg:" + "超时");
                                    }
                                });

                                //监听升级过程
                                MsgUtil.receiveMsg(currentView, receiveDevUpgradeObject, new HekrData.dataReceiverListener() {
                                    @Override
                                    public void onReceiveSuccess(String msg) {
                                        Log.i(TAG, "msg:" + msg);
                                        if (!TextUtils.isEmpty(msg)) {
                                            try {
                                                JSONObject backDevUpgradeStatus = new JSONObject(msg);

                                                if (backDevUpgradeStatus.has("upgradeState")) {
                                                    switch (backDevUpgradeStatus.getInt("upgradeState")) {
                                                        //成功
                                                        case 0:
                                                            mPercentageRing.setVisibility(View.GONE);
                                                            update_status.setText(getString(R.string.activity_firmware_is_updated));
                                                            update_status.setClickable(false);

                                                            break;
                                                        //下载中
                                                        case 1:
                                                            if (backDevUpgradeStatus.has("upgradeProgress")) {
                                                                mPercentageRing.setVisibility(View.VISIBLE);
                                                                mPercentageRing.setTargetPercent(backDevUpgradeStatus.getInt("upgradeProgress"));
                                                                update_status.setClickable(false);
                                                            }
                                                            break;
                                                        //固件校验失败
                                                        case 2:
                                                            mPercentageRing.setVisibility(View.GONE);
                                                            switchTextView(1);
                                                            toastor.showSingletonToast(getString(R.string.activity_firmware_update_fail_check_fail));
                                                            isNeedUpdate = 1;
                                                            break;
                                                        //超时
                                                        case 3:
                                                            mPercentageRing.setVisibility(View.GONE);
                                                            switchTextView(1);
                                                            toastor.showSingletonToast(getString(R.string.activity_firmware_update_fail_timeout));
                                                            isNeedUpdate = 1;
                                                            break;
                                                        //连接失败
                                                        case 4:
                                                            mPercentageRing.setVisibility(View.GONE);
                                                            switchTextView(1);
                                                            toastor.showSingletonToast(getString(R.string.activity_firmware_update_fail_connect_fail));
                                                            isNeedUpdate = 1;
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onReceiveTimeout() {

                                    }
                                });

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    toastor.showSingletonToast(getString(R.string.device_offline));
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onStop() {
        EventBus.getDefault().post(new ClearFilterEvent(true, currentView));
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
