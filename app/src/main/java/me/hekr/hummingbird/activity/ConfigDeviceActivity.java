package me.hekr.hummingbird.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.adapter.GalleryAdapter;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.bean.DeviceBean;
import me.hekr.hummingbird.bean.DeviceStatusBean;
import me.hekr.hummingbird.bean.FindDeviceBean;
import me.hekr.hummingbird.event.ConfigEvent;
import me.hekr.hummingbird.ui.ConfirmView;
import me.hekr.hummingbird.ui.MyRecyclerView;
import me.hekr.hummingbird.ui.TitleBar;
import me.hekr.hummingbird.util.HekrCodeUtil;
import me.hekr.hummingbird.util.HekrConfig;
import me.hekr.hummingbird.util.SpCache;
import me.hekr.hummingbird.util.UIDscDevUtil;
import me.hekr.hummingbird.util.WindowUtil;

import com.litesuits.common.assist.Toastor;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import me.hekr.hummingbird.R;

/**
 * Created by hekr_xm on 2016/4/20.
 **/
public class ConfigDeviceActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "ConfigDeviceActivity";

    //根据网络wifi变换切换textView ssid
    private BroadcastReceiver connectionReceiver;

    private EditText ssid;
    private EditText pwd_input;
    private CheckBox watchPass;
    private SwitchCompat switchCompat;

    private View view;
    private AlertDialog dialog;
    public ImageView config_back_dialog_img;
    private ConfirmView confirmView;
    private TextView config_tip_tv;
    private TextView config_fail_help;
    private Button again;
    private Button complete;
    public Button device_connect_btn;
    public Toastor toastor;

    private List<String> m_Data = new ArrayList<>();

    private HekrUserAction hekrUserAction;

    private HekrConfig hekrConfig;
    private UIDscDevUtil uiDscDevUtil;
    //0表示开始进入配网
    private int clickConnectBtnFlag;

    public MyRecyclerView mRecyclerView;
    private GalleryAdapter mAdapter;

    private ArrayList<FindDeviceBean> discover = new ArrayList<>();
    private ArrayList<DeviceBean> device = new ArrayList<>();

    private WifiManager.MulticastLock lock = null;

    //1状态下，按对话框左上角x按钮将取消回调
    //0状态下，一次配网过程已经结束，点击不取消回调
    private int isInConfig=0;

    //发现设备并且至少有一个已经成功绑定，界面跳出完成按钮，用户点击完成按钮之后将主动跳转到主界面，在回调成功里面标识是否该跳回主界面
    //0状态:默认用户配网时间到并且在配网时间内成功绑定至少一个设备
    //1状态:用户点击完成按钮回到主界面，在成功回调中不再再次跳转至主界面操作
    private int completeClickFlag=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_device);
    }

    private void initToolBar() {
        TitleBar titleBar = (TitleBar) findViewById(R.id.config_title);
        if (titleBar != null) {
            titleBar.setBack(new TitleBar.BackListener() {
                @Override
                public void click() {
                    finish();
                }
            });
        }
    }

    private void initDialogView() {

        view = LayoutInflater.from(ConfigDeviceActivity.this).inflate(R.layout.dialog_config_device, (ViewGroup) findViewById(R.id.dialog));
        dialog = new AlertDialog.Builder(ConfigDeviceActivity.this)
                .setCancelable(false)
                .setView(view).create();
        Window window = dialog.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.alpha = 0.9f;
        window.setAttributes(lp);

        config_back_dialog_img = (ImageView) view.findViewById(R.id.config_dialog_back);
        config_back_dialog_img.setOnClickListener(this);

        again = (Button) view.findViewById(R.id.again);
        again.setOnClickListener(this);

        complete = (Button) view.findViewById(R.id.complete);
        complete.setOnClickListener(this);

        config_fail_help = (TextView) view.findViewById(R.id.config_fail_help);
        config_fail_help.setOnClickListener(this);

        config_tip_tv = (TextView) view.findViewById(R.id.config_tip_tv);

        confirmView = (ConfirmView) view.findViewById(R.id.confirmView);

        mRecyclerView = (MyRecyclerView) view.findViewById(R.id.id_recyclerview_horizontal);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        mRecyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new GalleryAdapter(this, m_Data);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.setOnItemScrollChangeListener(new MyRecyclerView.OnItemScrollChangeListener() {
            @Override
            public void onChange(View view, int position) {

            }
        });

        mAdapter.setOnItemClickListener(new GalleryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                /*Toast.makeText(getApplicationContext(), position + "", Toast.LENGTH_SHORT)
                        .show();*/
            }
        });
    }

    private void initCheckBox() {

        watchPass.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (watchPass.isChecked()) {
                  /* show the password*/
                    pwd_input.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                  /* hide the password */
                    pwd_input.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

    }

    protected void initView() {
        initToolBar();
        initDialogView();
        toastor = new Toastor(this);
        ssid = (EditText) findViewById(R.id.ssid);
        pwd_input = (EditText) findViewById(R.id.pwd_input);
        watchPass = (CheckBox) findViewById(R.id.watch_password);
        initCheckBox();

        switchCompat = (SwitchCompat) findViewById(R.id.config_switchCompat);

        device_connect_btn = (Button) findViewById(R.id.device_connect_btn);
        if (device_connect_btn != null) {
            device_connect_btn.setOnClickListener(this);
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    /**
     * 数据初始化
     * 1、eventBus初始化
     * 2、网络监听广播初始化
     * 3、网络操作(绑定设备)初始化
     * 4、配网udp发送工具类
     * 5、点击开始配网按钮标识
     * 6、发现服务工具类
     * 7、获取wifi组播锁
     */
    protected void initData() {
        EventBus.getDefault().register(this);
        createReceiver();

        hekrUserAction = HekrUserAction.getInstance(this);
        hekrConfig = new HekrConfig();
        clickConnectBtnFlag = 0;
        uiDscDevUtil = new UIDscDevUtil();
        discoverCallBack();

        WifiManager manager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        if (lock == null) {
            lock = manager.createMulticastLock("localWifi");
            lock.setReferenceCounted(true);
        }
    }

    /**
     * 启动发现服务之后的数据回调
     * 1、设备回调(新设备、更新bindkey的设备)
     * 2、成功回调(发现总时间内至少发现一个设备)
     * 3、失败回调(发现时间内一个设备都未发现)
     */
    private void discoverCallBack() {
        uiDscDevUtil.setListener(new UIDscDevUtil.DeviceListCallBack() {

            @Override
            public void callBackDevice(final FindDeviceBean findDeviceBean) {

                Log.i(TAG, "回调发现设备:" + findDeviceBean.toString());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hekrUserAction.deviceBindStatusAndBind(findDeviceBean.getDevTid(), findDeviceBean.getBindKey(), new HekrUser.GetBindStatusAndBindListener() {
                            @Override
                            public void getStatusSuccess(List<DeviceStatusBean> deviceStatusBeanLists) {
                                DeviceStatusBean deviceStatusBean=deviceStatusBeanLists.get(0);
                                Log.i(TAG,"deviceStatusBean:"+deviceStatusBean.toString());
                                if(deviceStatusBean.isForceBind()||(!deviceStatusBean.isBindToUser())) {
                                    discover.add(findDeviceBean);
                                }else{
                                    toastor.showSingleLongToast(getString(R.string.activity_config_discovery_device_tip)+deviceStatusBean.getDevTid()+getString(R.string.activity_config_already_in_other_account));
                                }
                            }

                            @Override
                            public void getStatusFail(int errorCode) {
                                Log.i(TAG, "getStatusFail:"+findDeviceBean.getDevTid()+">>>" + HekrCodeUtil.errorCode2Msg(errorCode));
                            }

                            @Override
                            public void bindDeviceSuccess(DeviceBean deviceBean) {
                                //com.litesuits.android.log.Log.d(TAG, "logoUrl: " + deviceBean.getLogo());
                                EventBus.getDefault().post(new ConfigEvent(deviceBean));
                            }

                            @Override
                            public void bindDeviceFail(int errorCode) {
                                Log.i(TAG, "bindDeviceFail:"+findDeviceBean.getDevTid()+">>>" + HekrCodeUtil.errorCode2Msg(errorCode));
                                //toastor.showSingletonToast("bindDeviceFail: " + errorMsg);
                            }
                        });
                    }
                });
            }

            @Override
            public void callBackFail() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        clickConnectBtnFlag = 0;
                        btnSwitch(2);
                        confirmView.animatedWithState(ConfirmView.State.Fail);
                        if (discover.isEmpty()) {
                            config_tip_tv.setText(getResources().getString(R.string.activity_config_not_discovery));
                            toastor.showSingleLongToast(getString(R.string.activity_config_local_without_useful_bind_device));
                            isInConfig=0;
                        }
                    }
                });
                if (lock != null) {
                    lock.release();
                }
                Log.i("hekrxmconfig","回掉未发现");
            }

            @Override
            public void callBackSuccess() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("hekrxmconfig","回掉已发现");
                        clickConnectBtnFlag = 0;
                        Log.i(TAG, "搜索list:" + discover.toString());
                        Log.i(TAG, "绑定成功list:" + device.toString());
                        isInConfig=0;

                        if (!device.isEmpty()) {
                            config_tip_tv.setText(getResources().getString(R.string.activity_config_success_bind_device));
                            confirmView.animatedWithState(ConfirmView.State.Success);
                            btnSwitch(1);
                            if(completeClickFlag!=1) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(ConfigDeviceActivity.this, ScrollingActivity.class);
                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(i);
                                        finish();
                                    }
                                }, 2000);
                            }

                        } else {
                            btnSwitch(2);
                            config_tip_tv.setText(getResources().getString(R.string.activity_config_discovery_but_not_bind_success));
                            confirmView.animatedWithState(ConfirmView.State.Fail);
                            if (discover.isEmpty()) {
                                config_tip_tv.setText(getResources().getString(R.string.activity_config_not_discovery));
                                toastor.showSingleLongToast(getString(R.string.activity_config_local_without_useful_bind_device));
                            }
                        }
                    }
                });

                if (lock != null) {
                    lock.release();
                }
            }
        });
    }

    /**
     * @param type 1、显示完成
     *             2、显示重试
     *             3、dialog界面btn、text全部隐藏
     */
    private void btnSwitch(int type) {
        switch (type) {
            case 1:
                complete.setVisibility(View.VISIBLE);
                again.setVisibility(View.GONE);
                complete.setClickable(true);
                again.setClickable(false);
                config_fail_help.setVisibility(View.GONE);
                break;

            case 2:
                again.setVisibility(View.VISIBLE);
                complete.setVisibility(View.GONE);
                again.setClickable(true);
                complete.setClickable(false);
                config_fail_help.setVisibility(View.VISIBLE);
                view.findViewById(R.id.id_recyclerview_horizontal).setVisibility(View.GONE);
                break;

            case 3:
                complete.setVisibility(View.GONE);
                again.setVisibility(View.GONE);
                complete.setClickable(false);
                again.setClickable(false);
                config_fail_help.setVisibility(View.GONE);
                view.findViewById(R.id.id_recyclerview_horizontal).setVisibility(View.GONE);
                break;

            default:
                break;
        }
    }

    /**
     * 点击配网按钮之后界面操作
     */
    private void startConfigUI() {
        dialog.show();
        dialog.getWindow().setLayout((int) (WindowUtil.getScreenWidth(this) * 0.95), (int) (WindowUtil.getScreenHeight(this) * 0.6));
        confirmView.animatedWithState(ConfirmView.State.Progressing);
        Animation textAnim = AnimationUtils.loadAnimation(ConfigDeviceActivity.this, R.anim.textanim);
        config_tip_tv.startAnimation(textAnim);

        Animation circleViewAnim = AnimationUtils.loadAnimation(ConfigDeviceActivity.this, R.anim.circleviewanim);
        confirmView.startAnimation(circleViewAnim);

        config_tip_tv.setText(getResources().getString(R.string.activity_config_connecting));
        clickConnectBtnFlag = 1;
        device.clear();
        discover.clear();

        btnSwitch(3);

        if (switchCompat.isChecked()) {
            SpCache.putString(ssid.getText().toString(), pwd_input.getText().toString());
        } else {
            SpCache.putString(ssid.getText().toString(), "");
        }

        isInConfig=1;
    }

    /**
     * 点击配网按钮之后
     * 1、发送ssid &&pwd
     * 2、启动发现服务
     */
    private void config() {
        if (lock != null) {
            lock.acquire();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                //Log.i("hekrxmconfig","hekrxmconfig配网...");
                hekrConfig.config(ssid.getText().toString(), pwd_input.getText().toString(), 60);
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                //Log.i("hekrxmconfig","hekrxmconfig发现...");
                uiDscDevUtil.startSearch(60);
            }
        }).start();

    }

    /**
     * @return 当前网络是否是wifi
     */
    private boolean netWorkCheck() {
        ConnectivityManager mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = mConnectivityManager.getActiveNetworkInfo();
        return netInfo != null && netInfo.isAvailable() && netInfo.getType() == ConnectivityManager.TYPE_WIFI;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.device_connect_btn:
                if (clickConnectBtnFlag == 0) {
                    //EventBus.getDefault().post(new StartDiscoverEvent(false));
                    Log.i(TAG,"pwd_input:"+pwd_input.getText().toString());
                    if(!TextUtils.isEmpty(pwd_input.getText().toString())) {
                        if(netWorkCheck()) {
                            startConfigUI();
                            config();
                        }else{
                            toastor.showSingletonToast(getResources().getString(R.string.activity_please_connect_wifi));
                        }

                    }else{
                        AlertDialog.Builder alert = new AlertDialog.Builder(this);
                        alert.setTitle(getResources().getString(R.string.app_name));
                        alert.setMessage(getResources().getString(R.string.activity_config_pwd_null_tip));
                        alert.setPositiveButton(getResources().getString(R.string.dialog_ok_tip), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(netWorkCheck()) {
                                    startConfigUI();
                                    config();
                                }else{
                                    toastor.showSingletonToast(getResources().getString(R.string.activity_please_connect_wifi));
                                }
                            }
                        });
                        alert.setNegativeButton(getResources().getString(R.string.dialog_cancel_tip), null).create().show();
                    }
                }
                break;

            case R.id.config_dialog_back:
                uiDscDevUtil.stopSearch();
                if(hekrConfig!=null){
                    hekrConfig.stop();
                }
                //进入配网isInConfig为1
                if(isInConfig==1) {
                    uiDscDevUtil.cancelCallback();
                    isInConfig=0;
                }
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                    clickConnectBtnFlag = 0;
                }
                break;

            case R.id.again:
                if(netWorkCheck()) {
                    startConfigUI();
                    config();
                }else{
                    toastor.showSingletonToast(getResources().getString(R.string.activity_please_connect_wifi));
                }
                break;

            case R.id.complete:
                uiDscDevUtil.cancelCallback();
                completeClickFlag=1;
                Intent i = new Intent(ConfigDeviceActivity.this, ScrollingActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                break;

            case R.id.config_fail_help:
                Intent intent = new Intent();
                intent.putExtra("number", 3);
                intent.setClass(ConfigDeviceActivity.this, AnswerActivity.class);
                startActivity(intent);
                break;

            default:
                break;
        }
    }

    //EventBus 接收到绑定成功的设备
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserEvent(ConfigEvent event) {
        if (event != null) {
            Log.i(TAG, "configEvent:" + event.getDeviceBean().toString());
            device.add(event.getDeviceBean());
            view.findViewById(R.id.id_recyclerview_horizontal).setVisibility(View.VISIBLE);
            m_Data.add(event.getDeviceBean().getLogo());
            mAdapter.notifyDataSetChanged();
            btnSwitch(1);
        }
    }

    /**
     * 监听网络变化
     */
    public void createReceiver() {
        // 创建网络监听广播
        if (connectionReceiver == null) {
            connectionReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                        ConnectivityManager mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = mConnectivityManager.getActiveNetworkInfo();
                        if (netInfo != null && netInfo.isAvailable()) {
                            WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                            String nowWifi = wifiManager.getConnectionInfo().getSSID().replace("\"", "");
                            if (!TextUtils.isEmpty(nowWifi)) {
                                ssid.setText(nowWifi);
                                if (!TextUtils.isEmpty(SpCache.getString(nowWifi, ""))) {
                                    pwd_input.setText(SpCache.getString(nowWifi, ""));
                                    switchCompat.setChecked(true);
                                } else {
                                    pwd_input.setText("");
                                    switchCompat.setChecked(false);
                                }
                            }
                        }
                        else{
                            ssid.setText("");
                            pwd_input.setText("");
                        }
                    }
                }
            };
            // 注册网络监听广播
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(connectionReceiver, intentFilter);
        }
    }

    //点击EditText之外隐藏输入框
    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null && v != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        return getWindow().superDispatchTouchEvent(ev) || onTouchEvent(ev);
    }

    public boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            return !(event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom);
            // 点击的是输入框区域，保留点击EditText的事件
        }
        return false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        btnSwitch(3);
        clickConnectBtnFlag = 0;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (connectionReceiver != null) {
            unregisterReceiver(connectionReceiver);
        }
    }
}
