package me.hekr.hummingbird.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.ui.TitleBar;

import me.hekr.hummingbird.R;

public class AnswerActivity extends BaseActivity {
    private TextView tv_answer;
    private String str;
    private TitleBar titleBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer);
    }

    protected void initData() {

        String[] name = {getString(R.string.Q1), getString(R.string.Q2), getString(R.string.Q3), getString(R.string.Q4), getString(R.string.Q5), getString(R.string.Q6)};
        Intent intent = getIntent();
        int _number = intent.getIntExtra("number", 0);
        titleBar.setTitle(name[_number].substring(3, name[_number].length()));
        switch (_number) {
            case 0:
                str = getString(R.string.A1);
                break;
            case 1:
                str = getString(R.string.A2);
                break;
            case 2:
                str = getString(R.string.A3);
                break;
            case 3:
                str = getString(R.string.A4);
                break;
            case 4:
                str = getString(R.string.A5);
                break;
            case 5:
                str = getString(R.string.A6);
                break;
            default:
                break;
        }

        tv_answer.setText(str);
    }

    protected void initView() {
        tv_answer = (TextView) findViewById(R.id.tv_answer);
        titleBar = (TitleBar) findViewById(R.id.title);
        titleBar.setBack(new TitleBar.BackListener() {
            @Override
            public void click() {
                finish();
            }
        });
    }
}
