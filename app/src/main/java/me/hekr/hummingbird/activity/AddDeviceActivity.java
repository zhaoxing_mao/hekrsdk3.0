package me.hekr.hummingbird.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.ui.TitleBar;

import com.litesuits.android.log.Log;
import com.litesuits.common.assist.Toastor;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.util.ConstantsUtil;

public class AddDeviceActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "AddDeviceActivity";
    private static final int SCAN_AUTH_WHAT = 10002;
    private static final String CONFIG_HELP_URL = ConstantsUtil.UrlUtil.BASE_CONFIG_HElP_URL;

    public TitleBar titleBar;
    public View view;
    public Dialog dialog;
    public ImageView back_dialog_img;
    public TextView config_tip_tv;
    public Button nextStep;
    public Toastor toastor;
    private HekrUserAction hekrUserAction;

    public RelativeLayout wifi_ry;
    public RelativeLayout scan_ry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_device);

        initView();
        initData();
    }

    private void initView() {

        titleBar = (TitleBar) findViewById(R.id.add_title);

        view = LayoutInflater.from(AddDeviceActivity.this).inflate(R.layout.dialog_add_device_tip, (ViewGroup) findViewById(R.id.dialog));
        dialog = new AlertDialog.Builder(AddDeviceActivity.this)
                .setView(view).create();
        Window window = dialog.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.alpha = 0.9f;
        window.setAttributes(lp);

        nextStep = (Button) view.findViewById(R.id.nextStep);
        nextStep.setOnClickListener(this);

        back_dialog_img = (ImageView) view.findViewById(R.id.dialog_back);
        back_dialog_img.setOnClickListener(this);

        config_tip_tv = (TextView) view.findViewById(R.id.config_tip_tv);
        config_tip_tv.setOnClickListener(this);

        wifi_ry = (RelativeLayout) findViewById(R.id.wifi_ry);
        if (wifi_ry != null) {
            wifi_ry.setOnClickListener(this);
        }
        scan_ry = (RelativeLayout) findViewById(R.id.scan_ry);
        if (scan_ry != null) {
            scan_ry.setOnClickListener(this);
        }
        toastor = new Toastor(this);
    }

    private void initData() {

        titleBar.setBack(new TitleBar.BackListener() {
            @Override
            public void click() {
                finish();
            }
        });

        hekrUserAction = HekrUserAction.getInstance(AddDeviceActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.wifi_ry:

                if (!isFinishing()) {
                    dialog.show();
                }
                break;

            case R.id.nextStep:
                startActivity(new Intent(AddDeviceActivity.this, ConfigDeviceActivity.class));
                break;

            case R.id.dialog_back:
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                break;

            case R.id.config_tip_tv:
                Intent browser = new Intent(Intent.ACTION_VIEW, Uri.parse(CONFIG_HELP_URL));
                startActivity(browser);
                break;

            case R.id.scan_ry:
                startActivityForResult(new Intent(this, CaptureActivity.class), SCAN_AUTH_WHAT);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SCAN_AUTH_WHAT:
                if (data != null && resultCode == Activity.RESULT_OK) {
                    Bundle extras = data.getExtras();
                    String result = extras.getString("result");
                    Log.i(TAG, "result:" + result);
                    //toastor.showSingletonToast("result:"+result);

                    String reallyResult = "";

                    if (!TextUtils.isEmpty(result) && result.contains("=") && result.startsWith(ConstantsUtil.UrlUtil.BASE_QR_CODE_URL)) {
                        int start = result.indexOf("token=");
                        reallyResult = result.substring(start + 6);
                    }
                    if (!TextUtils.isEmpty(reallyResult)) {
                        Log.i(TAG, "reallyResult:" + reallyResult);
                    }

                    if (!TextUtils.isEmpty(reallyResult)) {
                        hekrUserAction.registerAuth(reallyResult, new HekrUser.RegisterOAuthQRCodeListener() {
                            @Override
                            public void registerSuccess() {
                                toastor.showSingletonToast(getResources().getString(R.string.activity_after_scan_tip));
                            }

                            @Override
                            public void registerFail(int errorCode) {
                                Log.i(TAG, "errorCode:" + errorCode);
                                toastor.showSingletonToast(getString(R.string.already_auth));
                            }
                        });
                    } else {
                        toastor.showSingletonToast(getResources().getString(R.string.activity_not_support_this_code));
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
