package me.hekr.hummingbird.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.litesuits.common.assist.Network;
import com.litesuits.common.assist.Toastor;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.ui.TitleBar;
import me.hekr.hummingbird.util.HekrCodeUtil;


import me.hekr.hummingbird.R;

public class FeedbackActivity extends BaseActivity implements View.OnClickListener {

    private Toastor myToast;
    private EditText input_edt;
    private HekrUserAction hekrUserAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
    }

    protected void initView() {
        TitleBar titleBar = (TitleBar) findViewById(R.id.title);
        if (titleBar != null) {
            titleBar.setBack(new TitleBar.BackListener() {
                @Override
                public void click() {
                    finish();
                }
            });
        }

        Button commit = (Button) findViewById(R.id.commit_btn);
        input_edt = (EditText) findViewById(R.id.feed_input);

        myToast = new Toastor(FeedbackActivity.this);

        if (commit != null) {
            commit.setOnClickListener(this);
        }
    }

    protected void initData() {
        hekrUserAction = HekrUserAction.getInstance(this);
    }

    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null && v != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        return getWindow().superDispatchTouchEvent(ev) || onTouchEvent(ev);
    }

    public boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            return !(event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom);
            // 点击的是输入框区域，保留点击EditText的事件
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.commit_btn:
                if (!TextUtils.isEmpty(input_edt.getText())) {
                    if (Network.isConnected(this)) {
                        hekrUserAction.feedback(input_edt.getText().toString(), new HekrUser.FeedbackListener() {
                            @Override
                            public void feedbackSuccess() {
                                myToast.showSingletonToast(getResources().getString(R.string.activity_feedback_content_success));
                                finish();
                            }

                            @Override
                            public void feedFail(int errorCode) {
                                myToast.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
                            }

                        });
                    } else {
                        myToast.showSingletonToast(getResources().getString(R.string.activity_feedback_without_useful_network));
                    }
                } else {
                    myToast.showSingletonToast(getResources().getString(R.string.activity_feedback_content_null));
                }
                break;
            default:
                break;
        }
    }
}
