package me.hekr.hummingbird.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.ui.TitleBar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.hekr.hummingbird.R;

public class QuestionsActivity extends BaseActivity {
    private ListView lv_question;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);
    }

    protected void initData() {
        List<Map<String, Object>> list = new ArrayList<>();

        String[] name = {getString(R.string.Q1), getString(R.string.Q2), getString(R.string.Q3), getString(R.string.Q4), getString(R.string.Q5), getString(R.string.Q6)};

        for (int i = 0; i < 6; i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("text", name[i]);
            list.add(map);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(this, list, R.layout.layout_question_item, new String[]{"text"}, new int[]{R.id.tv_question});
        lv_question.setAdapter(simpleAdapter);

        lv_question.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("number", position);
                intent.setClass(QuestionsActivity.this, AnswerActivity.class);
                startActivity(intent);
            }
        });
    }

    protected void initView() {
        TitleBar titleBar = (TitleBar) findViewById(R.id.title);
        if (titleBar != null) {
            titleBar.setBack(new TitleBar.BackListener() {
                @Override
                public void click() {
                    finish();
                }
            });
        }
        lv_question = (ListView) findViewById(R.id.lv_question);
    }
}
