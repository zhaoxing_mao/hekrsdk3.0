package me.hekr.hummingbird.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.igexin.sdk.PushManager;
import com.litesuits.common.assist.Toastor;

import org.greenrobot.eventbus.EventBus;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.event.LogoutEvent;
import me.hekr.hummingbird.ui.CustomProgress;
import me.hekr.hummingbird.ui.TitleBar;
import me.hekr.hummingbird.util.HekrAlert;
import me.hekr.hummingbird.util.HekrCodeUtil;

public class ResetPwdActivity extends BaseActivity {
    private EditText et_old_name;
    private EditText et_new_name;
    private EditText et_new_name_2;
    private Toastor toastor;
    private HekrUserAction hekrUserAction;
    private CustomProgress customProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pwd);
    }

    protected void initData() {
        hekrUserAction = HekrUserAction.getInstance(this);
        toastor = new Toastor(this);
    }

    protected void initView() {
        initTitle();
        et_old_name = (EditText) findViewById(R.id.et_old_name);
        et_new_name = (EditText) findViewById(R.id.et_new_name);
        et_new_name_2 = (EditText) findViewById(R.id.et_new_name_2);
        Button btn_set = (Button) findViewById(R.id.btn_set);
        if (btn_set != null) {
            btn_set.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String old_name = et_old_name.getText().toString().trim();
                    String new_name = et_new_name.getText().toString().trim();
                    String new_name_2 = et_new_name_2.getText().toString().trim();
                    if (TextUtils.isEmpty(old_name) || TextUtils.isEmpty(new_name) || TextUtils.isEmpty(new_name_2)) {
                        toastor.showSingletonToast(getString(R.string.full_info));
                    } else if (TextUtils.equals(new_name, old_name)) {
                        toastor.showSingletonToast(getString(R.string.same_old_new));
                    } else if (!TextUtils.equals(new_name, new_name_2)) {
                        toastor.showSingletonToast(getString(R.string.same_new_new));
                    } else {
                        if (new_name.length() >= 6 && !new_name.contains(" ")) {
                            customProgress = CustomProgress.show(ResetPwdActivity.this, false, null);
                            hekrUserAction.changePassword(new_name, old_name, new HekrUser.ChangePwdListener() {
                                @Override
                                public void changeSuccess() {
                                    customProgress.dismiss();
                                    alert();
                                }

                                @Override
                                public void changeFail(int errorCode) {
                                    customProgress.dismiss();
                                    HekrAlert.hekrAlert(ResetPwdActivity.this, HekrCodeUtil.errorCode2Msg(errorCode));
                                }
                            });
                        } else {
                            toastor.showSingletonToast(getString(R.string.pwd_least_six_number));
                        }
                    }
                }
            });
        }
    }

    private void initTitle() {
        TitleBar titleBar = (TitleBar) findViewById(R.id.title);
        if (titleBar != null) {
            titleBar.setBack(new TitleBar.BackListener() {
                @Override
                public void click() {
                    finish();
                }
            });
        }
    }

    private void alert() {
        new AlertDialog.Builder(ResetPwdActivity.this).setTitle(R.string.app_name).setMessage(R.string.reset_pwd_success).setPositiveButton(R.string.positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                hekrUserAction.userLogout();
                EventBus.getDefault().post(new LogoutEvent(true));
                PushManager.getInstance().turnOffPush(ResetPwdActivity.this);
                PushManager.getInstance().unBindAlias(ResetPwdActivity.this, hekrUserAction.getUserId(), true);
                finish();

            }
        }).setCancelable(false).create().show();
    }

}
