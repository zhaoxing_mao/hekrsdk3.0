package me.hekr.hummingbird.activity;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.fragment.OAuthBindEmailFragment;
import me.hekr.hummingbird.fragment.OAuthBindPhoneFragment;
import me.hekr.hummingbird.ui.TitleBar;

import me.hekr.hummingbird.R;

public class OAuthBindActivity extends BaseActivity implements View.OnClickListener {
    private String bindToken = null;
    private RelativeLayout layout_phone, layout_mail;
    private TextView tv_phone, tv_mail;
    private ImageView img_phone, img_mail;
    private OAuthBindPhoneFragment oAuthBindPhoneFragment;
    private OAuthBindEmailFragment oAuthBindEmailFragment;
    private FragmentTransaction fragmentTransaction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oauth_bind);
        setNumber(0);
    }

    protected void initData() {
        Intent intent = getIntent();
        bindToken = intent.getStringExtra("bindToken");
        if (TextUtils.isEmpty(bindToken)) {
            finish();
        }
        layout_phone.setOnClickListener(this);
        layout_mail.setOnClickListener(this);
    }

    protected void initView() {
        TitleBar titleBar = (TitleBar) findViewById(R.id.title);
        if (titleBar != null) {
            titleBar.setBack(new TitleBar.BackListener() {
                @Override
                public void click() {
                    finish();
                }
            });
        }

        layout_phone = (RelativeLayout) findViewById(R.id.layout_phone);
        layout_mail = (RelativeLayout) findViewById(R.id.layout_mail);
        tv_phone = (TextView) findViewById(R.id.tv_phone_number);
        tv_mail = (TextView) findViewById(R.id.tv_email);
        img_phone = (ImageView) findViewById(R.id.img_phone);
        img_mail = (ImageView) findViewById(R.id.img_email);
    }


    private void setNumber(int what) {
        fragmentTransaction = getFragmentManager().beginTransaction();
        if (what == 0) {
            tv_phone.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            tv_mail.setTextColor(ContextCompat.getColor(this, R.color.divider));
            img_phone.setBackgroundResource(R.color.colorPrimary);
            img_mail.setBackgroundResource(R.color.divider);
            fragmentTransaction.replace(R.id.fg, getPhoneFragment());
            fragmentTransaction.commit();
        } else {
            tv_phone.setTextColor(ContextCompat.getColor(this, R.color.divider));
            tv_mail.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            img_phone.setBackgroundResource(R.color.divider);
            img_mail.setBackgroundResource(R.color.colorPrimary);
            fragmentTransaction.replace(R.id.fg, getEmailFragment());
            fragmentTransaction.commit();
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_phone:
                setNumber(0);
                break;
            case R.id.layout_mail:
                setNumber(1);
                break;
        }
    }

    private OAuthBindPhoneFragment getPhoneFragment() {
        if (oAuthBindPhoneFragment == null) {
            oAuthBindPhoneFragment = OAuthBindPhoneFragment.newInstance(bindToken);
        }
        return oAuthBindPhoneFragment;
    }


    private OAuthBindEmailFragment getEmailFragment() {
        if (oAuthBindEmailFragment == null) {
            oAuthBindEmailFragment = OAuthBindEmailFragment.newInstance(bindToken);
        }
        return oAuthBindEmailFragment;
    }
}
