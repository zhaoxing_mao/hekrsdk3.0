package me.hekr.hummingbird.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.ui.TitleBar;

import com.litesuits.android.log.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import me.hekr.hummingbird.R;

public class PhoneInformationActivity extends BaseActivity {

    private TextView phone_content;
    public TitleBar titleBar;
    public Button copy;
    private ClipboardManager myClipboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_information);
    }

    protected void initView() {
        phone_content = (TextView) findViewById(R.id.phone_content);
        titleBar = (TitleBar) findViewById(R.id.title);
        titleBar.setBack(new TitleBar.BackListener() {
            @Override
            public void click() {
                finish();
            }
        });

        copy = (Button) findViewById(R.id.copy);
    }

    protected void initData() {
        phone_content.setText(printSystemInfo());
        myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copy();
            }
        });
    }

    public static String printSystemInfo() {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String time = dateFormat.format(date);
        StringBuilder sb = new StringBuilder();
        sb.append("_______  系统信息  ").append(time).append(" ______________");
        sb.append("\nID                 :").append(Build.ID);
        sb.append("\nBRAND              :").append(Build.BRAND);
        sb.append("\nMODEL              :").append(Build.MODEL);
        sb.append("\nRELEASE            :").append(Build.VERSION.RELEASE);
        sb.append("\nSDK                :").append(Build.VERSION.SDK);
        sb.append("\n");
        sb.append("\n_______ OTHER _______");
        sb.append("\nBOARD              :").append(Build.BOARD);
        sb.append("\nPRODUCT            :").append(Build.PRODUCT);
        sb.append("\nDEVICE             :").append(Build.DEVICE);
        sb.append("\nFINGERPRINT        :").append(Build.FINGERPRINT);
        sb.append("\nHOST               :").append(Build.HOST);
        sb.append("\nTAGS               :").append(Build.TAGS);
        sb.append("\nTYPE               :").append(Build.TYPE);
        sb.append("\nTIME               :").append(Build.TIME);
        sb.append("\nINCREMENTAL        :").append(Build.VERSION.INCREMENTAL);
        sb.append("\n");
        sb.append("\n_______ CUPCAKE-3 _______");
        if (Build.VERSION.SDK_INT >= 3) {
            sb.append("\nDISPLAY            :").append(Build.DISPLAY);
            sb.append("\n");
        }

        sb.append("\n_______ DONUT-4 _______");
        if (Build.VERSION.SDK_INT >= 4) {
            sb.append("\nSDK_INT            :").append(Build.VERSION.SDK_INT);
            sb.append("\nMANUFACTURER       :").append(Build.MANUFACTURER);
            sb.append("\nBOOTLOADER         :").append(Build.BOOTLOADER);
            sb.append("\nCPU_ABI            :").append(Build.CPU_ABI);
            sb.append("\nCPU_ABI2           :").append(Build.CPU_ABI2);
            sb.append("\nHARDWARE           :").append(Build.HARDWARE);
            sb.append("\nUNKNOWN            :").append("unknown");
            sb.append("\nCODENAME           :").append(Build.VERSION.CODENAME);
            sb.append("\n");
        }
        sb.append("\n_______ GINGERBREAD-9 _______");
        if (Build.VERSION.SDK_INT >= 9) {
            sb.append("\nSERIAL             :").append(Build.SERIAL);
        }

        //Log.i("PhoneInformationActivity", sb.toString());
        return sb.toString();
    }

    public void copy() {
        String text = phone_content.getText().toString();
        ClipData myClip = ClipData.newPlainText("text", text);
        myClipboard.setPrimaryClip(myClip);
        Toast.makeText(getApplicationContext(), "Text Copied",
                Toast.LENGTH_SHORT).show();
    }
}
