package me.hekr.hummingbird.activity;

import android.app.Activity;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.bean.FileBean;
import me.hekr.hummingbird.bean.ProfileBean;
import me.hekr.hummingbird.event.RefreshEvent;
import me.hekr.hummingbird.ui.CircleImageView;
import me.hekr.hummingbird.ui.CustomProgress;
import me.hekr.hummingbird.ui.TitleBar;
import me.hekr.hummingbird.util.BaseHttpUtil;
import me.hekr.hummingbird.util.HekrCodeUtil;
import me.hekr.hummingbird.util.ImageUtil;

import com.litesuits.common.assist.Toastor;
import com.litesuits.common.utils.BitmapUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.greenrobot.eventbus.EventBus;

import me.hekr.hummingbird.R;

public class UserInformationActivity extends BaseActivity implements View.OnClickListener {
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 10002;
    private static final int PICK_ACTIVITY_REQUEST_CODE = 10003;
    private static final String TAG = "UserInformationActivity";
    public RelativeLayout layout_user_img;
    public RelativeLayout layout_user_name;
    public RelativeLayout layout_sex;
    public RelativeLayout layout_age;
    private Toastor toastor;
    private HekrUserAction hekrUserAction;
    private CircleImageView img_user_icon;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TextView tv_user_name;
    private TextView tv_user_age;
    private TextView tv_user_sex;
    private ProfileBean userBean;
    private CustomProgress customProgress;
    private DisplayImageOptions options;
    private int sexItem = 0;


    @Override
    protected void initView() {
        TitleBar titleBar = (TitleBar) findViewById(R.id.title);
        if (titleBar != null) {
            titleBar.setBack(new TitleBar.BackListener() {
                @Override
                public void click() {
                    finish();
                }
            });
        }
        // setSupportActionBar(titleBar.getToolbar());
        layout_user_img = (RelativeLayout) findViewById(R.id.user_img);
        layout_user_name = (RelativeLayout) findViewById(R.id.user_name);
        layout_sex = (RelativeLayout) findViewById(R.id.user_sex);
        layout_age = (RelativeLayout) findViewById(R.id.user_age);
        img_user_icon = (CircleImageView) findViewById(R.id.img_user_icon);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.sw);
        tv_user_name = (TextView) findViewById(R.id.tv_user_name);
        tv_user_age = (TextView) findViewById(R.id.tv_user_age);
        tv_user_sex = (TextView) findViewById(R.id.tv_user_sex);
        layout_user_img.setOnClickListener(this);
        layout_user_name.setOnClickListener(this);
        layout_sex.setOnClickListener(this);
        layout_age.setOnClickListener(this);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getUserInfo();
            }
        });
    }

    @Override
    protected void initData() {
        toastor = new Toastor(this);
        hekrUserAction = HekrUserAction.getInstance(this);

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)//设置为RGB565比起默认的ARGB_8888要节省大量的内存
                .build();
        userBean = hekrUserAction.getUserCache();
        updateUI(userBean);
        //getUserInfo();
    }

    /**
     * 更新ui
     */
    private void updateUI(ProfileBean profileBean) {
        try {
            if (!TextUtils.isEmpty(profileBean.getAvatarUrl().getSmall())) {
                String img_logo = profileBean.getAvatarUrl().getSmall();
                ImageLoader.getInstance().displayImage(img_logo, img_user_icon, options);
            }

            if (!TextUtils.isEmpty(profileBean.getLastName())) {
                tv_user_name.setText(profileBean.getLastName());
            }
            if (!TextUtils.isEmpty(profileBean.getAge())) {
                tv_user_age.setText(profileBean.getAge());
            }
            if (!TextUtils.isEmpty(profileBean.getGender())) {
                switch (profileBean.getGender()) {
                    case "MAN":
                        tv_user_sex.setText(R.string.man);
                        sexItem = 0;
                        break;
                    case "WOMAN":
                        tv_user_sex.setText(R.string.woman);
                        sexItem = 1;
                        break;
                    case "UNKNOWN":
                        tv_user_sex.setText(R.string.sex_setting);
                        sexItem = 0;
                    default:
                        tv_user_sex.setText(R.string.sex_setting);
                        sexItem = 0;
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_information);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.user_name:
                setName();
                break;
            case R.id.user_img:
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setItems(new String[]{getString(R.string.camera), getString(R.string.pic), getString(R.string.negative_button)}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                takePhoto();
                                break;
                            case 1:
                                pickPhoto();
                                break;
                            case 2:
                                break;
                        }
                    }
                });
                builder.create().show();
                break;
            case R.id.user_sex:
                final String[] sexs = new String[]{getString(R.string.man), getString(R.string.woman)};
                AlertDialog.Builder builder_sex = new AlertDialog.Builder(this);
                builder_sex.setSingleChoiceItems(sexs, sexItem, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setSex(which);
                        dialog.dismiss();
                    }
                }).create().show();
                break;
            case R.id.user_age:
                setAge();
                break;
        }
    }


    //拍照获取图片
    private void takePhoto() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        } else {
            toastor.showSingletonToast(getString(R.string.please_put_sd));
        }
    }

    //从相册中取图片
    private void pickPhoto() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_ACTIVITY_REQUEST_CODE);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    Bundle extras = data.getExtras();
                    Bitmap bitmap = (Bitmap) extras.get("data");
                    bitmap = ImageUtil.zoomImage(bitmap, 200, 200);
                    uploadBitmap(bitmap);
                }
                break;

            case PICK_ACTIVITY_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    String picPath = "";
                    String path;
                    Cursor cursor = null;
                    try {
                        String[] pojo = {MediaStore.Images.Media.DATA};
                        Uri uri = data.getData();
                        CursorLoader cursorLoader = new CursorLoader(this, uri, pojo, null, null, null);
                        cursor = cursorLoader.loadInBackground();
                        cursor.moveToFirst();
                        path = cursor.getString(cursor.getColumnIndex(pojo[0]));
                        if (path != null && path.length() > 0) {
                            picPath = path;
                        }
                    } catch (Exception e) {
                        picPath = "";
                    } finally {
                        if (cursor != null) {
                            cursor.close();
                        }
                    }

                    if (!TextUtils.isEmpty(picPath)) {
                        String formatStr;
                        if (!TextUtils.isEmpty(picPath)) {
                            int last = picPath.lastIndexOf(".");
                            formatStr = picPath.substring(last + 1);
                            String qian = picPath.substring(0, last) + ".";
                            Bitmap bitmap = BitmapUtil.getSmallBitmap(qian + formatStr, 200, 200);
                            uploadBitmap(bitmap);
                        }

                    }
                }
                break;
        }
    }

    private void uploadBitmap(Bitmap bitmap) {
        final long time = System.currentTimeMillis();
        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED) && bitmap != null &&
                new ImageUtil().createPNG(bitmap, String.valueOf(time))) {
            final String action_path = Environment.getExternalStorageDirectory().getPath() + "/" + time + ".png";
            customProgress = CustomProgress.show(this, false, null);
            upload(action_path);
        }
    }

    /**
     * 上传文件
     *
     * @param action_path 路径
     */
    private void upload(String action_path) {
        hekrUserAction.uploadFile(action_path, new HekrUser.UploadFileListener() {
            @Override
            public void uploadFileSuccess(FileBean fileBean) {
                Log.d(TAG, "uploadFileSuccess: " + fileBean.getFileCDNUrl());
                ImageLoader.getInstance().displayImage(fileBean.getFileCDNUrl(), img_user_icon, options);
                toastor.showSingletonToast(getString(R.string.upload_success));

                JSONObject jsonObject = new JSONObject();
                JSONObject jsonObject1 = new JSONObject();

                jsonObject.put("small", fileBean.getFileCDNUrl());
                jsonObject1.put("avatarUrl", jsonObject);

                Log.d(TAG, "uploadFileSuccess: " + jsonObject1.toString());
                setInfo(jsonObject1);

                mSwipeRefreshLayout.setRefreshing(false);
                customProgress.dismiss();

            }

            @Override
            public void uploadFileFail(int errorCode) {
                customProgress.dismiss();
                toastor.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
            }

            @Override
            public void uploadProgress(int progress) {
                Log.d(TAG, "uploadProgress: " + progress);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * 获取用户信息！
     */
    private void getUserInfo() {
        hekrUserAction.getProfile(new HekrUser.GetProfileListener() {
            @Override
            public void getProfileSuccess(ProfileBean profileBean) {
                userBean = profileBean;
                updateUI(userBean);
                EventBus.getDefault().post(new RefreshEvent(RefreshEvent.REFRESH_USER));
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void getProfileFail(int errorCode) {
                mSwipeRefreshLayout.setRefreshing(false);
                toastor.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.user_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                //保存
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * 设置姓名！
     */
    private void setName() {
        View view = LayoutInflater.from(this).inflate(R.layout.layout_remove_item, null);
        final AppCompatEditText editText = (AppCompatEditText) view.findViewById(R.id.et_folder_name);
        if (userBean != null) {
            editText.setText(userBean.getLastName());
        }
        editText.setFocusable(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.update_user_name))
                .setView(view)
                .setPositiveButton(getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String new_name = editText.getText().toString().trim();
                        if (TextUtils.isEmpty(new_name)) {
                            toastor.showSingletonToast(getString(R.string.username_null));
                        } else if (!TextUtils.equals(new_name, userBean.getLastName())) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("lastName", editText.getText().toString().trim());
                            setInfo(jsonObject);
                        }
                    }
                })
                .setNegativeButton(getString(R.string.negative_button), null);
        builder.create().show();
    }

    /**
     * 设置年龄！
     */
    private void setAge() {
        View view = LayoutInflater.from(this).inflate(R.layout.layout_age, null);
        final NumberPicker numberPicker = (NumberPicker) view.findViewById(R.id.number_picker);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(100);
        numberPicker.setValue(Integer.valueOf(tv_user_age.getText().toString()));
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.update_user_age))
                .setView(view)
                .setPositiveButton(getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (numberPicker.getValue() != Integer.valueOf(tv_user_age.getText().toString())) {
                            JSONObject obj = new JSONObject();
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("age", numberPicker.getValue());
                            obj.put("extraProperties", jsonObject);
                            setInfo(obj);
                        }

                    }
                })
                .setNegativeButton(getString(R.string.negative_button), null);
        builder.create().show();
    }


    /**
     * 设置性别
     */
    private void setSex(int sex) {
        if (sex != sexItem) {
            JSONObject jsonObject = new JSONObject();
            switch (sex) {
                case 0:
                    sexItem = 0;
                    jsonObject.put("gender", "MAN");
                    break;
                case 1:
                    sexItem = 1;
                    jsonObject.put("gender", "WOMAN");
                    break;
                case 2:
                    sexItem = 0;
                    jsonObject.put("gender", "UNKNOWN");
                    break;
                default:
                    sexItem = 0;
                    jsonObject.put("gender", "UNKNOWN");
                    break;
            }
            setInfo(jsonObject);
        }
    }


    /**
     * 保存用户信息
     *
     * @param jsonObject
     */
    private void setInfo(JSONObject jsonObject) {
        hekrUserAction.setProfile(jsonObject, new HekrUser.SetProfileListener() {
            @Override
            public void setProfileSuccess() {
                toastor.showSingletonToast(getString(R.string.save_success));
                getUserInfo();
            }

            @Override
            public void setProfileFail(int errorCode) {
                toastor.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BaseHttpUtil.getClient().cancelRequests(this, true);
    }
}