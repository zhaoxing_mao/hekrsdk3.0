package me.hekr.hummingbird.activity;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.litesuits.common.assist.Toastor;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;
import com.zhy.base.adapter.ViewHolder;
import com.zhy.base.adapter.recyclerview.CommonAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.adapter.DevicesAndFolderAdapter;
import me.hekr.hummingbird.bean.DeviceBean;
import me.hekr.hummingbird.bean.FolderListBean;
import me.hekr.hummingbird.bean.GroupBean;
import me.hekr.hummingbird.bean.ProfileBean;
import me.hekr.hummingbird.bean.WeatherBeanResultsNow;
import me.hekr.hummingbird.event.DeviceEvent;
import me.hekr.hummingbird.event.LogoutEvent;
import me.hekr.hummingbird.event.NetworkEvent;
import me.hekr.hummingbird.event.RefreshEvent;
import me.hekr.hummingbird.fragment.SlidingMenuFragment;
import me.hekr.hummingbird.itemTouchHelper.ItemTouchHelperCallback;
import me.hekr.hummingbird.itemTouchHelper.OnStartDragListener;
import me.hekr.hummingbird.service.WebSocketService;
import me.hekr.hummingbird.ui.CircleImageView;
import me.hekr.hummingbird.ui.CustomProgress;
import me.hekr.hummingbird.ui.PushHandler;
import me.hekr.hummingbird.util.ConstantsUtil;
import me.hekr.hummingbird.util.HekrCodeUtil;
import me.hekr.hummingbird.viewHolder.DividerGridItemDecoration;

public class ScrollingActivity extends AppCompatActivity implements OnStartDragListener, View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = "ScrollingActivity";
    private RecyclerView mRecyclerView;
    private Toolbar toolbar;

    public SwipeRefreshLayout swipeRefreshLayout;
    private DevicesAndFolderAdapter adapter;
    private ItemTouchHelper mItemTouchHelper;

    private Toastor myToast;
    private int recyclerViewNumber = 3;

    private HekrUserAction hekrUserAction;

    private TextView tv_temperature;
    private TextView tv_humidity;
    private TextView tv_name;

    private List<DeviceBean> devicesLists;
    private String location;
    private NavigationView navigationView;
    private CircleImageView img_user_icon;
    private TextView tv_user_name;
    private ImageView img_weather;

    private TextView tv_online, tv_offline;
    private int online = 0, offline = 0;
    private boolean isSwip = false;
    private Vibrator mVibrator;
    private DisplayImageOptions options;
    private boolean firstLoad = false;
    private String user_name, icon_logo;
    private final List<FolderListBean> folderListBeanArrayList = new ArrayList<>();
    private boolean addMoreFlag = false; //能否继续上拉刷新
    private int mPage = 0; //当前刷新的设备page
    private CustomProgress customProgress;
    private TextView tv_suggestion;
    private SlidingMenu slidingMenu;
    private NestedScrollView nestedScrollView;
    private RecyclerView virtual_recycler_view;
    private long firsTime = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slidingmenu);
        EventBus.getDefault().register(this);
        initView();
        initData();
        move();
    }

    /**
     * 侧边栏的一些数据
     */
    private void initDrawerView() {
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
        }
        View headerView = navigationView.getHeaderView(0);
        img_user_icon = (CircleImageView) headerView.findViewById(R.id.user_logo);
        tv_user_name = (TextView) headerView.findViewById(R.id.tv_user_name);
        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent();
                intent.putExtra("name", user_name);
                intent.putExtra("icon", icon_logo);*//*
                intent.setClass(ScrollingActivity.this, PersonalCenterActivity.class);*/
                start(PersonalCenterActivity.class);
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer,
                toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                navigationView.getMenu().getItem(0).setChecked(true);
            }

        };


        if (drawer != null) {
            drawer.setDrawerListener(toggle);
        }
        toggle.syncState();
    }


    //侧边栏 以fragment的形式出现
    private void initSlidingMenu() {
       /* if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.layout_main);
            if (linearLayout != null) {
                linearLayout.setPadding(0, DensityUtils.dp2px(this, 25), 0, 0);
            }
            mRecyclerView.setClipToPadding(false);
        }*/


        slidingMenu = new SlidingMenu(ScrollingActivity.this);
        slidingMenu.setMode(SlidingMenu.LEFT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        slidingMenu.setMenu(R.layout.layout_left_menu);
        slidingMenu.setBehindOffsetRes(R.dimen.sliding_menu_off_set);
        slidingMenu.setId(R.id.sliding_menu);
        slidingMenu.setBackgroundResource(R.color.sliding_menu_bg);
        SlidingMenuFragment slidingMenuFragment = new SlidingMenuFragment();
        getFragmentManager().beginTransaction().replace(R.id.slidingmenumain, slidingMenuFragment).commit();

    }


    private void initDisplay() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        if (size.x >= 800) {
            recyclerViewNumber = 4;
        } else {
            recyclerViewNumber = 3;
        }
    }

    //初始化
    private void initView() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("");
        }
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingMenu.toggle();
            }
        });
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        mRecyclerView = (RecyclerView) findViewById(R.id.conversation);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.sw);
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        }
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EventBus.getDefault().post(new RefreshEvent(RefreshEvent.REFRESH_USER));
                getDevices();
                getWeather(location);
            }
        });
        if (appBarLayout != null) {
            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    isSwip = verticalOffset >= 0;
                    swipeRefreshLayout.setEnabled(isSwip);

                }
            });
        }
        customProgress = CustomProgress.show(this, true, null);
        //实例化其他的一些界面
        initWeatherView();
        initDevicesNumberView();
        initVirtualView();
        //initDrawerView();
        initSlidingMenu();

    }


    /**
     * 数量预览
     */
    private void initDevicesNumberView() {
        tv_online = (TextView) findViewById(R.id.tv_devices_online_number);
        tv_offline = (TextView) findViewById(R.id.tv_devices_offline_number);
    }

    /**
     * 天气界面
     */
    private void initWeatherView() {
        tv_humidity = (TextView) findViewById(R.id.tv_humidity);
        tv_temperature = (TextView) findViewById(R.id.tv_temperature);
        tv_name = (TextView) findViewById(R.id.tv_name);
        img_weather = (ImageView) findViewById(R.id.img_weather);
        tv_suggestion = (TextView) findViewById(R.id.suggestion);
    }


    /**
     * title
     */
    private void initVirtualView() {
        AppCompatTextView tv_title = (AppCompatTextView) findViewById(R.id.tv_title);
        if (tv_title != null) {
            tv_title.setOnClickListener(this);
        }

        nestedScrollView = (NestedScrollView) findViewById(R.id.virtual_devices);
        virtual_recycler_view = (RecyclerView) findViewById(R.id.virtual_recycler_view);
        //默认设置布局为3列
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, recyclerViewNumber);
        virtual_recycler_view.setLayoutManager(gridLayoutManager);
    }


    private void initData() {

        myToast = new Toastor(ScrollingActivity.this);
        hekrUserAction = HekrUserAction.getInstance(this);
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)//设置为RGB565比起默认的ARGB_8888要节省大量的内存
                .delayBeforeLoading(100)//载入图片前稍做延时可以提高整体滑动的流畅度
                .build();


        //默认设置布局为3列
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, recyclerViewNumber);
        mRecyclerView.addItemDecoration(new DividerGridItemDecoration(this));
        mRecyclerView.setLayoutManager(gridLayoutManager);
        devicesLists = new ArrayList<>();
        adapter = new DevicesAndFolderAdapter(ScrollingActivity.this, devicesLists);
        mRecyclerView.setAdapter(adapter);
        mVibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        //获取定位信息！
        initLocation();
        getDevices();
        bindPush();
        //deleteRemoveAllFolder();


       /* mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItems = gridLayoutManager.findLastVisibleItemPosition();
                if (dy > 0 && visibleItems > adapter.getItemCount() - 5 && addMoreFlag) {
                    Log.d(TAG, "will loadNewFeeds");
                }
            }
        });*/

    }

    /**
     * 获取位置
     */
    private void initLocation() {

        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Battery_Saving
        );//可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setCoorType("bd09ll");//可选，默认gcj02，设置返回的定位结果坐标系
        option.setScanSpan(0);//可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setIsNeedLocationDescribe(true);//可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        option.setIsNeedLocationPoiList(true);//可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        option.setIgnoreKillProcess(false);//可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        option.setEnableSimulateGps(false);//可选，默认false，设置是否需要过滤gps仿真结果，默认需要

        final LocationClient mLocationClient = new LocationClient(getApplicationContext());     //声明LocationClient类
        mLocationClient.setLocOption(option);
        //注册监听函数
        mLocationClient.registerLocationListener(new BDLocationListener() {
            @Override
            public void onReceiveLocation(BDLocation bdLocation) {
                if (bdLocation.getLocType() == 62) {
                    location = "beijing";
                    getWeather(location);
                } else {
                    Double latitude = bdLocation.getLatitude();
                    Double longitude = bdLocation.getLongitude();
                    String location = latitude + ":" + longitude;
                    getWeather(location);
                }
                mLocationClient.stop();
            }
        });
        mLocationClient.start();
    }

    /**
     * 获取设备
     */
    private void getDevices() {

        hekrUserAction.getDevices(0, 20, new HekrUser.GetDevicesListener() {
            @Override
            public void getDevicesSuccess(final List<DeviceBean> list) {


                if (list.isEmpty()) {
                    getDefaultDevices();
                    devicesLists.clear();
                    adapter.notifyDataSetChanged();
                    online = 0;
                    offline = 0;
                } else {
                    if (customProgress != null) {
                        customProgress.dismiss();
                    }
                    //虚拟设备界面
                    mRecyclerView.setVisibility(View.VISIBLE);
                    nestedScrollView.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);

                    if (list.size() < 20) {
                        addMoreFlag = false;
                    } else {
                        addMoreFlag = true;
                        mPage++;
                    }
                    addMoreFlag = list.size() >= 20;
                    mPage = 0;
                    online = 0;
                    offline = 0;
                    for (DeviceBean deviceBean : list) {
                        if (deviceBean.isOnline()) {
                            online += 1;
                        } else {
                            offline += 1;
                        }
                    }
                    devicesLists.clear();
                    devicesLists.addAll(HekrCodeUtil.Folder2Lists(list));
                    adapter.notifyDataSetChanged();
                }

                tv_offline.setText(getString(R.string.offline) + String.valueOf(offline) + getString(R.string.number));
                tv_online.setText(getString(R.string.online) + String.valueOf(online) + getString(R.string.number));
                //群组
                getGroup();
                //个人信息
                // getUserInfo();
                //删除空目录！
                deleteNullFolder(0);

/*
                //数据同步2s之后折叠appbar
                if (!firstLoad) {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            appBarLayout.setExpanded(false);
                            if (swipeRefreshLayout.isRefreshing()) {
                                swipeRefreshLayout.setRefreshing(false);
                                firstLoad = true;
                            }
                        }
                    }, 2000);
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                    firstLoad = true;
                }*/

            }

            @Override
            public void getDevicesFail(int errorCode) {
                if (customProgress != null) {
                    customProgress.dismiss();
                }
                swipeRefreshLayout.setRefreshing(false);
                myToast.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
            }
        });

    }

    /**
     * 获取群控信息
     */
    private void getGroup() {
        hekrUserAction.getGroup(new HekrUser.GetGroupListener() {
            @Override
            public void getGroupSuccess(List<GroupBean> groupBeans) {
                Log.d(TAG, "getGroupSuccess: " + groupBeans);
                for (GroupBean groupBean : groupBeans) {
                    devicesLists.add(new DeviceBean(groupBean));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void getGroupFail(int errorCode) {
                //拉取失败！
                myToast.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
            }
        });
    }

    /**
     * 获取天气
     *
     * @param location 坐标
     */
    private void getWeather(String location) {
        hekrUserAction.getWeather(location, new HekrUser.GetWeatherListener() {
            @Override
            public void getWeatherSuccess(WeatherBeanResultsNow now, String location) {
                swipeRefreshLayout.setRefreshing(false);
                if (customProgress != null) {
                    customProgress.dismiss();
                }
                tv_temperature.setText(now.getTemperature() + "℃");
                tv_humidity.setText(getText(R.string.humidity) + now.getHumidity() + "%");
                tv_name.setText(location);
                img_weather.setImageDrawable(HekrCodeUtil.getWeatherDrawable(ScrollingActivity.this, now.getCode()));
                tv_suggestion.setText(HekrCodeUtil.getSuggestion(now));
            }

            @Override
            public void getWeatherFail(int errorCode) {
                swipeRefreshLayout.setRefreshing(false);
                if (customProgress != null) {
                    customProgress.dismiss();
                }
                myToast.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
            }
        });
    }

    /**
     * 获取用户信息
     */
    private void getUserInfo() {
        hekrUserAction.getProfile(new HekrUser.GetProfileListener() {
            @Override
            public void getProfileSuccess(ProfileBean profileBean) {
                try {
                    if (profileBean.getAvatarUrl().getSmall() != null) {
                        icon_logo = profileBean.getAvatarUrl().getSmall();
                        ImageLoader.getInstance().displayImage(icon_logo, img_user_icon, options);
                    }
                    if (!TextUtils.isEmpty(profileBean.getLastName())) {
                        user_name = profileBean.getLastName();
                        tv_user_name.setText(user_name);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void getProfileFail(int errorCode) {
                myToast.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_title:
                swipeRefreshLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(true);
                    }
                });
                getDevices();
                getWeather(location);
                break;

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.menu_add:
                if (devicesLists != null && !devicesLists.isEmpty()) {
                    EventBus.getDefault().postSticky(new DeviceEvent(devicesLists.get(0)));
                }
                start(AddDeviceActivity.class);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        int id = item.getItemId();
        if (id == R.id.home) {
            if (drawer != null) {
                drawer.closeDrawer(GravityCompat.START);
            }
        } else if (id == R.id.devices) {
            start(DevicesManageActivity.class);
        } else if (id == R.id.about) {
            start(AboutActivity.class);
        }
        return true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);//反注册EventBus
    }


    @Subscribe
    public void onEvent(LogoutEvent event) {
        // UI updates must run on MainThread
        if (event.isLogout()) {
            Intent intent = new Intent(this, ScrollingActivity.class);
            intent.putExtra(ConstantsUtil.LOGOUT_FLAG_STRING, ConstantsUtil.LOGOUT_FLAG_INT);
            startActivity(intent);
        }
    }


    @Subscribe
    public void onEvent(RefreshEvent event) {
        switch (event.getRefreshTag()) {
            case RefreshEvent.REFRESH_DEVICE:
                getDevices();
                break;
            case RefreshEvent.REFRESH_USER:
                //getUserInfo();
                break;
        }
    }

    @Subscribe
    public void onEvent(NetworkEvent event) {
        switch (event.getNetStatus()) {
            case WebSocketService.NETWORK_AVAILABLE:
                //getDevices();
                break;
        }
    }

    private void move() {
        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(adapter, this);
        //绑定到控件中
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
    }


    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
        Log.d(TAG, "开始拖拽");
        swipeRefreshLayout.setEnabled(false);
        mVibrator.vibrate(25);
    }

    @Override
    public void onEndDrag(RecyclerView.ViewHolder viewHolder) {
        if (isSwip) {
            swipeRefreshLayout.setEnabled(true);
        }
        Log.d(TAG, "结束拖拽");
    }


    @Override
    public void onBackPressed() {
       /* DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/
        if (slidingMenu.isMenuShowing()) {
            slidingMenu.toggle();
        } else {
            long secondTime = System.currentTimeMillis();
            if (secondTime - firsTime > 2000) {
                myToast.showSingletonToast(getString(R.string.exit));
                firsTime = System.currentTimeMillis();
            } else {
                MobclickAgent.onKillProcess(this);
                finish();
            }
        }

    }


    /**
     * 推送
     */
    private void bindPush() {
        new PushHandler(ScrollingActivity.this).messageHandler(hekrUserAction);
    }

    private void start(Class c) {
        startActivity(new Intent(ScrollingActivity.this, c));
    }


    /**
     * 删除空目录
     *
     * @param page
     */
    private void deleteNullFolder(final int page) {
        hekrUserAction.getFolder(page, new HekrUser.GetFolderListsListener() {
                    @Override
                    public void getSuccess(List<FolderListBean> folderList) {
                        int i = page;
                        for (FolderListBean folderBean_temp : folderList) {
                            if (!TextUtils.equals(folderBean_temp.getFolderId(), "0") && folderBean_temp.getDevTidList().isEmpty()) {
                                folderListBeanArrayList.add(folderBean_temp);
                            }
                        }
                        if (folderList.size() < 20) {
                            Log.d(TAG, "空目录有" + folderListBeanArrayList.size());
                            deleteFolder(folderListBeanArrayList, 0);
                        } else {
                            deleteNullFolder(++i);
                        }
                    }

                    @Override
                    public void getFail(int errorCode) {
                        folderListBeanArrayList.clear();
                    }
                }

        );
    }

    /**
     * 循环将list中的folder删除干净！
     */

    private void deleteFolder(final List<FolderListBean> lists, final int position) {
        if (!lists.isEmpty() && position < lists.size()) {
            hekrUserAction.deleteFolder(lists.get(position).getFolderId(), new HekrUser.DeleteFolderListener() {
                @Override
                public void deleteSuccess() {
                    int i = position;
                    deleteFolder(lists, ++i);
                }

                @Override
                public void deleteFail(int errorCode) {
                    folderListBeanArrayList.clear();
                }
            });
        } else {
            folderListBeanArrayList.clear();
        }
    }


    /**
     * 加载更多获取设备
     */
    private void getMoreDevices(int page) {
        //如果可以继续加载
        if (addMoreFlag) {
            hekrUserAction.getDevices(page, 20, new HekrUser.GetDevicesListener() {
                @Override
                public void getDevicesSuccess(final List<DeviceBean> hekrLists) {
                    //判断是否需要继续加载 =20 则可以继续加载！
                    if (hekrLists.size() < 20) {
                        addMoreFlag = false;
                    } else {
                        //如果能继续加载,则page直接加1
                        addMoreFlag = true;
                        mPage++;
                    }
                    for (DeviceBean deviceBean : hekrLists) {
                        if (deviceBean.isOnline()) {
                            online += 1;
                        } else {
                            offline += 1;
                        }
                    }
                    devicesLists.clear();
                    devicesLists.addAll(HekrCodeUtil.Folder2Lists(hekrLists));
                    adapter.notifyDataSetChanged();

                    tv_offline.setText(getString(R.string.offline) + String.valueOf(offline) + getString(R.string.number));
                    tv_online.setText(getString(R.string.online) + String.valueOf(online) + getString(R.string.number));

                }

                @Override
                public void getDevicesFail(int errorCode) {
                    swipeRefreshLayout.setRefreshing(false);
                    myToast.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
                }
            });
        }
    }


    /**
     * 获取默认演示设备
     */
    private void getDefaultDevices() {
        hekrUserAction.getdefaultStatic(new HekrUser.GetDefaultDevicesListener() {
            @Override
            public void getSuccess(List<DeviceBean> list) {
                swipeRefreshLayout.setRefreshing(false);
                if (customProgress != null) {
                    customProgress.dismiss();
                }
                //虚拟设备界面
                mRecyclerView.setVisibility(View.GONE);
                nestedScrollView.setVisibility(View.VISIBLE);

                virtual_recycler_view.setAdapter(new CommonAdapter<DeviceBean>(ScrollingActivity.this, R.layout.layout_item_default_devices, list) {
                    @Override
                    public void convert(ViewHolder holder, final DeviceBean defaultDeviceBean) {
                        holder.setText(R.id.tv_name, defaultDeviceBean.getDeviceName());
                        ImageView img_logo = holder.getView(R.id.img_icon);
                        ImageLoader.getInstance().displayImage(defaultDeviceBean.getLogo(), img_logo, options);
                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DeviceBean deviceBean = new DeviceBean(defaultDeviceBean.getLogo(), defaultDeviceBean.getAndroidH5Page(), defaultDeviceBean.getIosH5Page(), defaultDeviceBean.getDeviceName(), defaultDeviceBean.getDesc());
                                EventBus.getDefault().postSticky(new DeviceEvent(deviceBean));
                                start(WebViewActivity.class);
                            }
                        });
                    }
                });
                swipeRefreshLayout.setRefreshing(false);
            }


            @Override
            public void getFail(int errorCode) {
                swipeRefreshLayout.setRefreshing(false);
                if (customProgress != null) {
                    customProgress.dismiss();
                }
                myToast.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        int flag = intent.getIntExtra(ConstantsUtil.LOGOUT_FLAG_STRING, 0);
        if (flag == 1) {
            //说明账号退出！
            finish();
            start(LoginActivity.class);
        }
    }
}