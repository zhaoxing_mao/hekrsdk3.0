package me.hekr.hummingbird.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.litesuits.common.assist.Toastor;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zhy.base.adapter.ViewHolder;
import com.zhy.base.adapter.abslistview.CommonAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.bean.DeviceBean;
import me.hekr.hummingbird.event.DeviceEvent;
import me.hekr.hummingbird.event.RefreshEvent;
import me.hekr.hummingbird.ui.CircleImageView;
import me.hekr.hummingbird.ui.EndlessListView;
import me.hekr.hummingbird.ui.TitleBar;
import me.hekr.hummingbird.util.HekrCodeUtil;

public class DevicesManageActivity extends BaseActivity {
    private TitleBar titleBar;
    private static final String TAG = "DevicesManageActivity";
    private ListView lv_devices;
    private HekrUserAction hekrUserAction;
    private List<DeviceBean> lists;
    private Toastor toastor;
    private SwipeRefreshLayout swipeRefreshLayout;
    private DisplayImageOptions options;
    private CommonAdapter adapter;
    private boolean addMoreFlag = false; //能否继续上拉刷新
    private int mPage = 0; //当前刷新的设备page

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices_manage);
        EventBus.getDefault().register(this);
        getDevices();
    }

    protected void initData() {
        toastor = new Toastor(this);
        lists = new ArrayList<>();
        hekrUserAction = HekrUserAction.getInstance(this);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.ic_hekr_logo)
                .showImageOnFail(R.mipmap.ic_hekr_logo)
                .showImageForEmptyUri(R.mipmap.ic_hekr_logo)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)//设置为RGB565比起默认的ARGB_8888要节省大量的内存
                .delayBeforeLoading(100)//载入图片前稍做延时可以提高整体滑动的流畅度
                .build();
        adapter = new CommonAdapter<DeviceBean>(DevicesManageActivity.this, R.layout.layout_devices_item, lists) {
            @Override
            public void convert(ViewHolder holder, DeviceBean deviceBean) {
                TextView device_name = holder.getView(R.id.tv_dev_name);
                //TextView device_cid = holder.getView(R.id.tv_dev_cid);
                ImageView img_logo = holder.getView(R.id.img_icon);
                TextView tv_online = holder.getView(R.id.tv_online);
                CircleImageView img_online = holder.getView(R.id.online_bg);
                String cidName = deviceBean.getCidName();
                if (TextUtils.isEmpty(deviceBean.getDeviceName())) {
                    device_name.setText(cidName.substring(cidName.indexOf("/") + 1));
                } else {
                    device_name.setText(deviceBean.getDeviceName());
                }
                //holder.setText(R.id.tv_dev_cid, cidName.substring(cidName.indexOf("/") + 1));
                holder.setText(R.id.tv_dev_cid, cidName);
                ImageLoader.getInstance().displayImage(deviceBean.getLogo(), img_logo, options);
                if (deviceBean.isOnline()) {
                    tv_online.setText(R.string.online);
                    img_online.setImageDrawable(ContextCompat.getDrawable(DevicesManageActivity.this, R.color.devices_online));

                } else {
                    tv_online.setText(R.string.offline);
                    img_online.setImageDrawable(ContextCompat.getDrawable(DevicesManageActivity.this, R.color.devices_offline));
                }
            }
        };
      /*  lv_devices.setListener(new EndlessListView.EndlessListener() {
            @Override
            public void loadData() {
                getMoreDevices(mPage);
            }
        });*/
        lv_devices.setAdapter(adapter);
    }

    protected void initView() {
        titleBar = (TitleBar) findViewById(R.id.title);
        titleBar.setBack(new TitleBar.BackListener() {
            @Override
            public void click() {
                finish();
            }
        });
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.sw);
        lv_devices = (ListView) findViewById(R.id.lv_devices);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDevices();
            }
        });
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
        lv_devices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EventBus.getDefault().postSticky(new DeviceEvent(lists.get(position)));
                startActivity(new Intent(DevicesManageActivity.this, DeviceDetailActivity.class));
            }
        });
    }

    private void getDevices() {
        hekrUserAction.getDevices(0, 20, new HekrUser.GetDevicesListener() {
            @Override
            public void getDevicesSuccess(final List<DeviceBean> devicesLists) {
                if (devicesLists.size() < 20 || !devicesLists.isEmpty()) {
                    //lv_devices.setLoadFlag(false);
                    addMoreFlag = false;
                } else {
                    //如果能继续加载,则page直接加1
                    //lv_devices.setLoadFlag(true);
                    addMoreFlag = true;
                    mPage++;
                }

                lists.clear();
                lists.addAll(devicesLists);
                adapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void getDevicesFail(int errorCode) {
                swipeRefreshLayout.setRefreshing(false);
                toastor.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
            }
        });
    }

    @Subscribe
    public void onEvent(RefreshEvent event) {
        switch (event.getRefreshTag()) {
            case RefreshEvent.REFRESH_DEVICE:
                getDevices();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    /**
     * 加载更多获取设备
     */
    private void getMoreDevices(int page) {
        //如果可以继续加载
        if (addMoreFlag) {
            hekrUserAction.getDevices(page, 20, new HekrUser.GetDevicesListener() {
                @Override
                public void getDevicesSuccess(final List<DeviceBean> devicesLists) {
                    //判断是否需要继续加载 =20 则可以继续加载！
                    if (devicesLists.size() < 20 || !devicesLists.isEmpty()) {
                        //lv_devices.setLoadFlag(false);
                        addMoreFlag = false;
                    } else {
                        addMoreFlag = true;
                        //如果能继续加载,则page直接加1
                        //lv_devices.setLoadFlag(true);
                        mPage++;
                    }
                    lists.addAll(HekrCodeUtil.Folder2Lists(devicesLists));
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void getDevicesFail(int errorCode) {
                    swipeRefreshLayout.setRefreshing(false);
                    toastor.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
                }
            });
        }
    }

}
