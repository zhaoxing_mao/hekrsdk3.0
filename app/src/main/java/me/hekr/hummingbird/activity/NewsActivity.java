package me.hekr.hummingbird.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.bean.NewsBean;
import me.hekr.hummingbird.ui.TitleBar;

import com.litesuits.common.assist.Toastor;
import com.zhy.base.adapter.ViewHolder;
import com.zhy.base.adapter.recyclerview.CommonAdapter;
import com.zhy.base.adapter.recyclerview.DividerItemDecoration;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.util.HekrCodeUtil;

public class NewsActivity extends BaseActivity {
    private Toastor toastor;
    private HekrUserAction hekrUserAction;
    private RecyclerView recyclerViewNews;
    private List<NewsBean.Result> lists = new ArrayList<>();
    private TextView tv_nothing;


    private CommonAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        getNews();
    }


    protected void initData() {
        toastor = new Toastor(this);
        hekrUserAction = HekrUserAction.getInstance(this);

        adapter = new CommonAdapter<NewsBean.Result>(NewsActivity.this, R.layout.layout_item_news, lists) {
            @Override
            public void convert(ViewHolder holder, final NewsBean.Result result) {
                holder.setText(R.id.tv_title, result.getTitle());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                holder.setText(R.id.tv_time, sdf.format(new Date(result.getUpdateTime())));
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        toastor.showSingletonToast(result.getInfoContent());
                    }
                });
            }
        };

        recyclerViewNews.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNews();
            }
        });
    }


    protected void initView() {
        TitleBar titleBar = (TitleBar) findViewById(R.id.title);
        if (titleBar != null) {
            titleBar.setBack(new TitleBar.BackListener() {
                @Override
                public void click() {
                    finish();
                }
            });
        }
        recyclerViewNews = (RecyclerView) findViewById(R.id.lv_news);
        recyclerViewNews.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewNews.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL_LIST));
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.sw);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
        tv_nothing = (TextView) findViewById(R.id.tv_nothing);
    }

    private void getNews() {
        hekrUserAction.getNewsByPid(new HekrUser.GetInfoListener() {
            @Override
            public void getInfoSuccess(NewsBean newsBean) {
                try {

                    if (newsBean.getResult().isEmpty()) {
                        swipeRefreshLayout.setVisibility(View.INVISIBLE);
                        tv_nothing.setVisibility(View.VISIBLE);
                    } else {
                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                        tv_nothing.setVisibility(View.INVISIBLE);

                        lists.clear();
                        lists.addAll(newsBean.getResult());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                adapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void getInfoFail(int errorCode) {
                toastor.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
}
