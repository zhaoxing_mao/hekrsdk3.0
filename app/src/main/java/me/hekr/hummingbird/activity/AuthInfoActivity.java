package me.hekr.hummingbird.activity;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.adapter.AuthAdapter;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.bean.DeviceBean;
import me.hekr.hummingbird.bean.OAuthListBean;
import me.hekr.hummingbird.event.DeviceEvent;
import me.hekr.hummingbird.ui.TitleBar;
import me.hekr.hummingbird.util.DensityUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import me.hekr.hummingbird.R;

public class AuthInfoActivity extends BaseActivity {

    private static final String TAG = "AuthInfoActivity";
    private TextView deviceManage_name_tv;
    private SwipeMenuListView swipeMenuListView;

    private AuthAdapter authAdapter;
    private List<OAuthListBean> authList = new ArrayList<>();

    private HekrUserAction hekrUserAction;
    private DeviceBean deviceBean;
    private TextView device_share_tip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
    }

    protected void initView() {
        deviceManage_name_tv = (TextView) findViewById(R.id.deviceManage_name_tv);
        swipeMenuListView = (SwipeMenuListView) findViewById(R.id.swipeMenuListView);
        TitleBar titleBar = (TitleBar) findViewById(R.id.title);
        device_share_tip= (TextView) findViewById(R.id.device_share_tip);
        if (titleBar != null) {
            titleBar.setBack(new TitleBar.BackListener() {
                @Override
                public void click() {
                    finish();
                }
            });
        }
    }

    protected void initData() {

        hekrUserAction = HekrUserAction.getInstance(this);

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(DensityUtils.dp2px(AuthInfoActivity.this, 90));
                // set a icon
                deleteItem.setIcon(R.drawable.ic_delete_forever_white_24dp);

                //deleteItem.setTitle("取消授权");
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        swipeMenuListView.setMenuCreator(creator);

        authAdapter = new AuthAdapter(authList, AuthInfoActivity.this);
        swipeMenuListView.setAdapter(authAdapter);

        swipeMenuListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        AlertDialog.Builder alert = new AlertDialog.Builder(AuthInfoActivity.this);
                        alert.setMessage(getString(R.string.activity_authInfo_cancel_title_tip));
                        alert.setPositiveButton(getString(R.string.dialog_ok_tip), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                hekrUserAction.cancelOAuth(authList.get(position).getGrantor(), authList.get(position).getCtrlKey(),
                                        authList.get(position).getGrantee(), deviceBean.getDevTid(), new HekrUser.CancelOAuthListener() {
                                            @Override
                                            public void CancelOAuthListener() {
                                                authList.remove(position);
                                                authAdapter.notifyDataSetChanged();
                                            }

                                            @Override
                                            public void CancelOauthFail(int errorCode) {

                                            }
                                        });
                            }
                        });
                        alert.setNegativeButton(getString(R.string.negative_button), null).create().show();

                        break;
                }
                return false;
            }
        });

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceEvent event) {
        // UI updates must run on MainThread

        deviceBean = event.getDeviceBean();

        Log.i(TAG, "deviceBean:" + deviceBean.toString());
        //更新UI
        if (TextUtils.equals(deviceBean.getOwnerUid(), hekrUserAction.getUserId())) {
            device_share_tip.setText(getString(R.string.activity_authInfo_deviceShare_tip));
            deviceManage_name_tv.setText(hekrUserAction.getUserCache().getLastName());
            hekrUserAction.getOAuthList(deviceBean.getOwnerUid(), deviceBean.getCtrlKey(), deviceBean.getDevTid(), new HekrUser.GetOAuthListener() {
                @Override
                public void getOAuthListSuccess(List<OAuthListBean> lists) {

                    Log.i(TAG, "list:" + lists);
                    authList.clear();
                    authList.addAll(lists);
                    authAdapter.notifyDataSetChanged();
                }

                @Override
                public void getOAuthListFail(int errorCode) {

                }
            });
        } else {
            device_share_tip.setText("");
            hekrUserAction.getOAuthList(deviceBean.getOwnerUid(), deviceBean.getCtrlKey(), deviceBean.getDevTid(), hekrUserAction.getUserId(), new HekrUser.GetOAuthListener() {
                @Override
                public void getOAuthListSuccess(List<OAuthListBean> lists) {

                    if (!lists.isEmpty()) {
                        deviceManage_name_tv.setText(lists.get(0).getGrantorName());
                    }
                }

                @Override
                public void getOAuthListFail(int errorCode) {

                }
            });
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

}
