package me.hekr.hummingbird.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.litesuits.common.assist.Network;
import com.litesuits.common.assist.Toastor;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.bean.DeviceBean;
import me.hekr.hummingbird.event.DeviceEvent;
import me.hekr.hummingbird.util.HekrJSSDK;

public class WebViewActivity extends AppCompatActivity {
    private static final String TAG = "WebViewActivity";
    //private String url = null;
    //private String h5Zip = null;
    private HekrJSSDK hekrJSSDK;
    RelativeLayout layout;
    private DeviceBean deviceBean;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.doX5WebViewConstruction();
        EventBus.getDefault().register(this);
    }

    private void initData() {
        Toastor toaster = new Toastor(this);
        if (Network.isConnected(WebViewActivity.this)) {
            if (deviceBean != null) {
                if (!TextUtils.isEmpty(deviceBean.getAndroidH5Page())) {
                    hekrJSSDK = HekrJSSDK.getInstance(this, this, layout, deviceBean);
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                            | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                            | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                            | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

                } else {
                    toaster.showSingletonToast(getString(R.string.activity_webView_not_control_tip));
                    finish();
                }
            } else {
                toaster.showSingletonToast(getString(R.string.activity_webView_not_useful_device));
                finish();
            }
        } else {
            toaster.showSingletonToast(getString(R.string.activity_webView_not_useful_network));
            finish();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        //hekrJSSDK.onNewIntent(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        hekrJSSDK.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onDestroy() {
        //ViewGroup view = (ViewGroup) getWindow().getDecorView();
        //view.removeAllViews();
        if (hekrJSSDK != null) {
            hekrJSSDK.onDestroy();
        }
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (hekrJSSDK != null) {
            hekrJSSDK.onBackPressed();
        } else {
            finish();
        }
    }


    private void doX5WebViewConstruction() {
        layout = new RelativeLayout(this);
        setContentView(layout);

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceEvent event) {
        deviceBean = event.getDeviceBean();
        Log.d(TAG, "onEvent: " + deviceBean.toString());
        initData();
    }

    @Override
    public void onStart() {
        super.onStart();
        //EventBus.getDefault().register(this);
    }


    @Override
    public void onStop() {
        //EventBus.getDefault().unregister(this);
        hekrJSSDK.onStop();
        super.onStop();
    }


}
