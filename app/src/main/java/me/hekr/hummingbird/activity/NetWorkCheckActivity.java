package me.hekr.hummingbird.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.event.PingMessageEvent;
import me.hekr.hummingbird.ui.TitleBar;

public class NetWorkCheckActivity extends BaseActivity {

    private static final String TAG = "NetWorkCheckActivity";
    private BroadcastReceiver connectionReceiver;

    public TextView network_connect_tv;
    public TextView network_type_tv;
    public TextView network_local_ip_tv;

    public TextView user_open_content;
    public TextView uaa_open_content;
    public TextView asia_dev_content;
    public TextView asia_app_content;
    public TextView ok_tip;

    public TitleBar titleBar;

    private ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
    private String[] pingStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_net_work_check);
    }

    protected void initView() {
        network_connect_tv = (TextView) findViewById(R.id.network_connect_content);
        network_type_tv = (TextView) findViewById(R.id.network_type_content);
        network_local_ip_tv = (TextView) findViewById(R.id.network_local_ip_content);
        user_open_content = (TextView) findViewById(R.id.user_open_content);
        uaa_open_content = (TextView) findViewById(R.id.uaa_open_content);
        asia_dev_content = (TextView) findViewById(R.id.asia_dev_content);
        asia_app_content = (TextView) findViewById(R.id.asia_app_content);
        ok_tip = (TextView) findViewById(R.id.ok_tip);

        titleBar = (TitleBar) findViewById(R.id.title);
        titleBar.setBack(new TitleBar.BackListener() {
            @Override
            public void click() {
                finish();
            }
        });
    }

    protected void initData() {
        createReceiver();
        EventBus.getDefault().register(this);
        pingStr = new String[]{"user.openapi.hekr.me", "uaa.openapi.hekr.me", "asia.dev.hekr.me", "asia.app.hekr.me"};
    }

    //EventBus 接受数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserEvent(PingMessageEvent event) {

        if (event != null && !TextUtils.isEmpty(event.getStr())) {
            Log.i(TAG, event.getStr());
            if (event.getStr().startsWith("PING user.openapi.hekr.me")) {
                if (event.getStr().contains(")")) {
                    int lastIndex = event.getStr().indexOf(")");
                    String ip = event.getStr().substring(0, lastIndex + 1);
                    if (event.getStr().contains("time=") && event.getStr().contains("ms---")) {
                        int startIndexTime = event.getStr().indexOf("time=");
                        int lastIndexTime = event.getStr().indexOf("ms---");
                        String time = event.getStr().substring(startIndexTime + 5, lastIndexTime + 2);
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append(ip).append(" ").append(time);
                        user_open_content.setText(stringBuilder.toString());
                    } else {
                        user_open_content.setText(ip);
                    }
                }
            }
            if (event.getStr().startsWith("PING uaa.openapi.hekr.me")) {
                if (event.getStr().contains(")")) {
                    int lastIndex = event.getStr().indexOf(")");
                    String ip = event.getStr().substring(0, lastIndex + 1);
                    if (event.getStr().contains("time=") && event.getStr().contains("ms---")) {
                        int startIndexTime = event.getStr().indexOf("time=");
                        int lastIndexTime = event.getStr().indexOf("ms---");
                        String time = event.getStr().substring(startIndexTime + 5, lastIndexTime + 2);
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append(ip).append(" ").append(time);
                        uaa_open_content.setText(stringBuilder.toString());
                    } else {
                        uaa_open_content.setText(ip);
                    }
                }
            }
            if (event.getStr().startsWith("PING asia.dev.hekr.me")) {
                if (event.getStr().contains(")")) {
                    int lastIndex = event.getStr().indexOf(")");
                    String ip = event.getStr().substring(0, lastIndex + 1);
                    if (event.getStr().contains("time=") && event.getStr().contains("ms---")) {
                        int startIndexTime = event.getStr().indexOf("time=");
                        int lastIndexTime = event.getStr().indexOf("ms---");
                        String time = event.getStr().substring(startIndexTime + 5, lastIndexTime + 2);
                        asia_dev_content.setText(ip + " " + time);
                    } else {
                        asia_dev_content.setText(ip);
                    }
                }
            }
            if (event.getStr().startsWith("PING asia.app.hekr.me")) {
                if (event.getStr().contains(")")) {
                    int lastIndex = event.getStr().indexOf(")");
                    String ip = event.getStr().substring(0, lastIndex + 1);
                    if (event.getStr().contains("time=") && event.getStr().contains("ms---")) {
                        int startIndexTime = event.getStr().indexOf("time=");
                        int lastIndexTime = event.getStr().indexOf("ms---");
                        String time = event.getStr().substring(startIndexTime + 5, lastIndexTime + 2);
                        asia_app_content.setText(ip + " " + time);
                    } else {
                        asia_app_content.setText(ip);
                    }
                    ok_tip.setText(getResources().getString(R.string.activity_NetWorkCheck_test_success));
                }
            }
        }

    }

    private void createReceiver() {
        // 创建网络监听广播
        connectionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String action = intent.getAction();
                if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {

                    ConnectivityManager mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = mConnectivityManager.getActiveNetworkInfo();
                    if (netInfo != null && netInfo.isAvailable()) {

                        network_connect_tv.setText(getResources().getString(R.string.activity_NetWorkCheck_network_is_connect));
                        ok_tip.setText(getResources().getString(R.string.activity_NetWorkCheck_testing));
                        for (final String aPingStr : pingStr) {
                            singleThreadExecutor.execute(new Runnable() {
                                @Override
                                public void run() {
                                    ping(aPingStr);
                                }
                            });
//                            new Thread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    ping(aPingStr);
//                                }
//                            }).start();
                        }

                        //网络连接
                        if (netInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                            network_type_tv.setText(getResources().getString(R.string.activity_NetWorkCheck_network_type_is_WiFi));
                            WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
                            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                            // 获取32位整型IP地址
                            int ipAddress = wifiInfo.getIpAddress();

                            //返回整型地址转换成“*.*.*.*”地址
                            network_local_ip_tv.setText(String.format("%d.%d.%d.%d",
                                    (ipAddress & 0xff), (ipAddress >> 8 & 0xff),
                                    (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff)));

                        } else if (netInfo.getType() == ConnectivityManager.TYPE_ETHERNET) {


                        } else if (netInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                            //3g网络
                            network_type_tv.setText(getResources().getString(R.string.activity_NetWorkCheck_network_type_is_mobile));
                            try {
                                for (Enumeration<NetworkInterface> en = NetworkInterface
                                        .getNetworkInterfaces(); en.hasMoreElements(); ) {
                                    NetworkInterface information = en.nextElement();
                                    for (Enumeration<InetAddress> enumIpAddress = information
                                            .getInetAddresses(); enumIpAddress.hasMoreElements(); ) {
                                        InetAddress inetAddress = enumIpAddress.nextElement();
                                        if (!inetAddress.isLoopbackAddress()
                                                && inetAddress instanceof Inet4Address) {
                                            // if (!inetAddress.isLoopbackAddress() && inetAddress
                                            // instanceof Inet6Address) {
                                            network_local_ip_tv.setText(inetAddress.getHostAddress());
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        //网络断开
                        network_type_tv.setText("");
                        network_connect_tv.setText(getResources().getString(R.string.activity_NetWorkCheck_network_is_not_connect));
                        ok_tip.setText(getResources().getString(R.string.activity_NetWorkCheck_network_is_disconnect));
                        user_open_content.setText("");
                        uaa_open_content.setText("");
                        asia_dev_content.setText("");
                        asia_app_content.setText("");
                        singleThreadExecutor.shutdownNow();

                    }
                }
            }
        };
        // 注册网络监听广播
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(connectionReceiver, intentFilter);
    }

    public String ping(String str) {
        String result = "";
        Process p;
        try {
            //ping -c 3 -w 100  中  ，-c 是指ping的次数 3是指ping 3次 ，-w 100  以秒为单位指定超时间隔，是指超时时间为100秒
            p = Runtime.getRuntime().exec("ping -c 1 -w 100 " + str);
            int status = p.waitFor();
            InputStream input = p.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(input));
            StringBuilder buffer = new StringBuilder();
            String line;
            while ((line = in.readLine()) != null) {
                buffer.append(line);
            }
            Log.i(TAG, "Return ============" + buffer.toString());
            String pingBack = buffer.toString();
            if (status == 0) {
                result = "success";
                EventBus.getDefault().post(new PingMessageEvent(pingBack));
            } else {
                result = "fail";
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (connectionReceiver != null) {
            unregisterReceiver(connectionReceiver);
            connectionReceiver = null;
        }
        EventBus.getDefault().unregister(this);
    }
}
