package me.hekr.hummingbird.activity;

import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.litesuits.common.assist.Toastor;
import com.litesuits.common.utils.PackageUtil;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.ui.TitleBar;
import me.hekr.hummingbird.util.HekrCodeUtil;


public class AboutActivity extends BaseActivity {
    private int pressNumber = 0;
    private long firsTime = 0;
    private Toastor myToast;
    public TitleBar titleBar;
    private HekrUserAction hekrUserAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

    @Override
    protected void initView() {
        titleBar = (TitleBar) findViewById(R.id.title);

        myToast = new Toastor(AboutActivity.this);
        AppCompatTextView tv_version = (AppCompatTextView) findViewById(R.id.tv_version);
        if (tv_version != null) {
            tv_version.setText(getString(R.string.app_name) + PackageUtil.getAppPackageInfo(this).versionName);
        }
        if (tv_version != null) {
            tv_version.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    long secondTime = System.currentTimeMillis();
                    if (secondTime - firsTime > 2000) {
                        pressNumber = 0;
                        firsTime = System.currentTimeMillis();
                    } else {
                        pressNumber++;
                        if (pressNumber >= 2 && pressNumber < 5) {
                            myToast.showSingletonToast(5 - pressNumber + "");
                        }
                        if (pressNumber > 4) {
                            pressNumber = 0;
                            // Dialog();
                            deleteRemoveAllFolder();
                        }
                    }
                }
            });
        }

        titleBar.setBack(new TitleBar.BackListener() {
            @Override
            public void click() {
                finish();
            }
        });
    }


    @Override
    protected void initData() {
        hekrUserAction = HekrUserAction.getInstance(this);
    }


    private void deleteRemoveAllFolder() {
        hekrUserAction.deleteFolder(new HekrUser.DeleteFolderListener() {
            @Override
            public void deleteSuccess() {
                myToast.showSingletonToast("已清除全部自定义目录！");
            }

            @Override
            public void deleteFail(int errorCode) {
                myToast.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
            }
        });
    }

}
