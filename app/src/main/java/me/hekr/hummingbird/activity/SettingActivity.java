package me.hekr.hummingbird.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.ui.TitleBar;
import me.hekr.hummingbird.util.DataCleanManager;

import com.litesuits.common.assist.Toastor;

import me.hekr.hummingbird.R;

public class SettingActivity extends BaseActivity implements View.OnClickListener {
    private TitleBar titleBar;
    private RelativeLayout layout_question;
    private RelativeLayout layout_feedback;
    private RelativeLayout layout_cache;
    private RelativeLayout layout_about;
    private RelativeLayout layout_network_check;
    private RelativeLayout layout_phone_information;
    private Toastor toastor;
    private TextView tv_catch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
    }

    protected void initData() {
        toastor = new Toastor(this);
        titleBar.setBack(new TitleBar.BackListener() {
            @Override
            public void click() {
                finish();
            }
        });
        layout_question.setOnClickListener(this);
        layout_feedback.setOnClickListener(this);
        layout_cache.setOnClickListener(this);
        layout_about.setOnClickListener(this);
        layout_network_check.setOnClickListener(this);
        layout_phone_information.setOnClickListener(this);
        try {
            tv_catch.setText(DataCleanManager.getTotalCacheSize(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void initView() {
        titleBar = (TitleBar) findViewById(R.id.title);
        layout_question = (RelativeLayout) findViewById(R.id.layout_question);
        layout_feedback = (RelativeLayout) findViewById(R.id.layout_feedback);
        layout_cache = (RelativeLayout) findViewById(R.id.layout_cache);
        layout_about = (RelativeLayout) findViewById(R.id.layout_about);
        layout_network_check = (RelativeLayout) findViewById(R.id.layout_network_help);
        layout_phone_information = (RelativeLayout) findViewById(R.id.layout_phone_information);
        tv_catch = (TextView) findViewById(R.id.tv_catch);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_question:
                start(QuestionsActivity.class);
                break;
            case R.id.layout_feedback:
                start(FeedbackActivity.class);
                break;
            case R.id.layout_about:
                start(AboutActivity.class);
                break;
            case R.id.layout_cache:
                DataCleanManager.clearAllCache(this);
                toastor.showSingletonToast(getString(R.string.clear_cache));
                try {
                    tv_catch.setText(DataCleanManager.getTotalCacheSize(this));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.layout_network_help:
                start(NetWorkCheckActivity.class);
                break;
            case R.id.layout_phone_information:
                start(PhoneInformationActivity.class);
                break;
        }
    }


    private void start(Class clazz) {
        startActivity(new Intent(this, clazz));
    }
}
