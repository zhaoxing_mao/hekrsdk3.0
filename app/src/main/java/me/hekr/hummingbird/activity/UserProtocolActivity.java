package me.hekr.hummingbird.activity;

import android.os.Bundle;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.ui.TitleBar;

public class UserProtocolActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_protocol);
    }

    @Override
    protected void initView() {
        TitleBar titleBar = (TitleBar) findViewById(R.id.title);
        if (titleBar != null) {
            titleBar.setBack(new TitleBar.BackListener() {
                @Override
                public void click() {
                    finish();
                }
            });
        }
    }

    @Override
    protected void initData() {

    }
}
