package me.hekr.hummingbird.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.ui.TitleBar;

import me.hekr.hummingbird.R;

public class UserSecurityActivity extends BaseActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_security);
    }

    protected void initView() {
        RelativeLayout layout_bind = (RelativeLayout) findViewById(R.id.layout_bind);
        RelativeLayout layout_pwd = (RelativeLayout) findViewById(R.id.layout_pwd);
        if (layout_bind != null) {
            layout_bind.setOnClickListener(this);
        }
        if (layout_pwd != null) {
            layout_pwd.setOnClickListener(this);
        }
        TitleBar titleBar = (TitleBar) findViewById(R.id.title);
        if (titleBar != null) {
            titleBar.setBack(new TitleBar.BackListener() {
                @Override
                public void click() {
                    finish();
                }
            });
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_bind:
                startActivity(ChangeBindActivity.class);
                break;
            case R.id.layout_pwd:
                startActivity(ResetPwdActivity.class);
                break;
        }
    }

    private void startActivity(Class clazz) {
        startActivity(new Intent(this, clazz));
    }
}
