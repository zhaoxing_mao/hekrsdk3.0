package me.hekr.hummingbird.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.application.BaseActivity;
import me.hekr.hummingbird.bean.DeviceBean;
import me.hekr.hummingbird.event.DeviceEvent;
import me.hekr.hummingbird.event.RefreshEvent;
import me.hekr.hummingbird.ui.CircleImageView;
import me.hekr.hummingbird.util.DensityUtils;

import com.litesuits.common.assist.Toastor;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import me.hekr.hummingbird.R;

public class DeviceDetailActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "DeviceDetailActivity";
    private RelativeLayout layout_auth;
    private RelativeLayout layout_share;
    private RelativeLayout layout_update;
    private RelativeLayout layout_cid;
    private RelativeLayout layout_ed;
    private HekrUserAction hekrUserAction;
    private Toastor toastor;
    public Toolbar toolbar;
    private DeviceBean deviceBean;
    private TextView tv_cid;
    private CircleImageView img_user_icon;
    private TextView tv_user_name;
    private Button btn_delete;
    private Button btn_ctrl;
    private DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_detail);
    }


    protected void initData() {
        hekrUserAction = HekrUserAction.getInstance(this);
        toastor = new Toastor(this);
        layout_auth.setOnClickListener(this);
        layout_share.setOnClickListener(this);
        layout_update.setOnClickListener(this);
        layout_ed.setOnClickListener(this);
        layout_cid.setOnClickListener(this);
        toolbar.setNavigationIcon(R.drawable.ic_left_white);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btn_delete.setOnClickListener(this);
        btn_ctrl.setOnClickListener(this);

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)//设置为RGB565比起默认的ARGB_8888要节省大量的内存
                .build();

    }

    protected void initView() {
        layout_auth = (RelativeLayout) findViewById(R.id.layout_auth);
        layout_share = (RelativeLayout) findViewById(R.id.layout_share);
        layout_update = (RelativeLayout) findViewById(R.id.layout_update);
        layout_cid = (RelativeLayout) findViewById(R.id.layout_cid);
        layout_ed = (RelativeLayout) findViewById(R.id.layout_ed);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appBar);
            if (appBarLayout != null) {
                appBarLayout.setPadding(0, DensityUtils.dp2px(this, 25), 0, 0);
            }
        }

        tv_cid = (TextView) findViewById(R.id.tv_cid);
        img_user_icon = (CircleImageView) findViewById(R.id.img_user_icon);
        tv_user_name = (TextView) findViewById(R.id.tv_user_name);
        btn_delete = (Button) findViewById(R.id.btn_delete_device);
        btn_ctrl = (Button) findViewById(R.id.btn_ctrl_device);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_cid:
                break;
            case R.id.layout_auth:
                if (TextUtils.equals(deviceBean.getOwnerUid(), hekrUserAction.getUserId())) {
                    start(ShareActivity.class);
                } else {
                    toastor.showSingletonToast("您不是该设备属主!");
                }
                break;
            case R.id.layout_share:

                start(AuthInfoActivity.class);
                break;
            case R.id.layout_update:
                start(FirmwareUpdateActivity.class);
                break;
            case R.id.layout_ed:
                View view = LayoutInflater.from(this).inflate(R.layout.layout_remove_item, null);
                final AppCompatEditText editText = (AppCompatEditText) view.findViewById(R.id.et_folder_name);
                editText.setText(tv_user_name.getText().toString().trim());
                editText.setHint(tv_user_name.getText().toString().trim());
                editText.setFocusable(true);
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.update_device_name))
                        .setView(view)
                        .setPositiveButton(getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                hekrUserAction.renameDevice(deviceBean.getDevTid(), deviceBean.getCtrlKey(), editText.getText().toString().trim(), null, new HekrUser.RenameDeviceListener() {
                                    @Override
                                    public void renameDeviceSuccess() {
                                        toastor.showSingletonToast(getString(R.string.success_update));
                                        deviceBean.setDeviceName(editText.getText().toString().trim());
                                        EventBus.getDefault().post(new RefreshEvent(RefreshEvent.REFRESH_DEVICE));
                                        tv_user_name.setText(editText.getText().toString().trim());
                                    }

                                    @Override
                                    public void renameDeviceFail(int errorMsg) {
                                        toastor.showSingletonToast(getString(R.string.fail_update) + errorMsg);
                                    }
                                });
                            }
                        })
                        .setNegativeButton(getString(R.string.negative_button), null);
                builder.create().show();
                break;
            case R.id.btn_delete_device:
                AlertDialog.Builder alert = new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.delete_device))

                        .setMessage(deviceBean.getDeviceName() + "\n" + deviceBean.getDevTid())
                        .setPositiveButton(getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (deviceBean.getOwnerUid().equals(hekrUserAction.getUserId())) {
                                    delete_device();
                                } else {
                                    delete_oauth();
                                }
                            }
                        })
                        .setNegativeButton(getString(R.string.negative_button), null);
                alert.create().show();
                break;
            case R.id.btn_ctrl_device:
                if (deviceBean.isOnline()) {
                    EventBus.getDefault().postSticky(new DeviceEvent(deviceBean));
                    start(WebViewActivity.class);
                } else {
                    toastor.showSingletonToast(getString(R.string.device_offline));
                }
                break;
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(DeviceEvent event) {
        // UI updates must run on MainThread
        deviceBean = event.getDeviceBean();
        //更新UI
        updateUI();
    }

    private void updateUI() {
        tv_cid.setText(deviceBean.getCidName());
        tv_user_name.setText(deviceBean.getDeviceName());
        if (TextUtils.isEmpty(deviceBean.getDeviceName())) {
            String cidName = deviceBean.getCidName();
            tv_user_name.setText(cidName.substring(cidName.indexOf("/") + 1));
        } else {
            tv_user_name.setText(deviceBean.getDeviceName());
        }


        ImageLoader.getInstance().displayImage(deviceBean.getLogo(), img_user_icon, options);


        if (!TextUtils.equals(deviceBean.getOwnerUid(), hekrUserAction.getUserId())) {
            layout_auth.setVisibility(View.GONE);
        }
    }

    private void start(Class clazz) {
        startActivity(new Intent(this, clazz));
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void delete_device() {
        hekrUserAction.deleteDevice(deviceBean.getDevTid(), deviceBean.getBindKey(), new HekrUser.DeleteDeviceListener() {
            @Override
            public void deleteDeviceSuccess() {
                toastor.showSingletonToast(getString(R.string.success_delete_device));
                EventBus.getDefault().post(new RefreshEvent(RefreshEvent.REFRESH_DEVICE));
                DeviceDetailActivity.this.finish();
            }

            @Override
            public void deleteDeviceFail(int errorMsg) {
                toastor.showSingletonToast(getString(R.string.fail_delete_device) + errorMsg);
            }
        });
    }

    private void delete_oauth() {
        hekrUserAction.cancelOAuth(deviceBean.getOwnerUid(), deviceBean.getCtrlKey(), hekrUserAction.getUserId(), deviceBean.getDevTid(), new HekrUser.CancelOAuthListener() {
            @Override
            public void CancelOAuthListener() {
                toastor.showSingletonToast(getString(R.string.success_delete_device));
                EventBus.getDefault().post(new RefreshEvent(RefreshEvent.REFRESH_DEVICE));
                DeviceDetailActivity.this.finish();
            }

            @Override
            public void CancelOauthFail(int errorCode) {
                toastor.showSingletonToast(getString(R.string.fail_delete_device) + errorCode);
            }
        });
    }
}
