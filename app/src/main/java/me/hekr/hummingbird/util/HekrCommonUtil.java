package me.hekr.hummingbird.util;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import me.hekr.hummingbird.R;

/**
 * Created by hekr_jds on 5/20 0020.
 **/
public class HekrCommonUtil {


    /**
     * 读取R.raw.hekr.json
     */
    public static String convertStreamToString(Context mContext, int id) {
        InputStream is = mContext.getResources().openRawResource(id);
        String s = "";
        try {
            Scanner scanner = new Scanner(is, "UTF-8").useDelimiter("\\A");
            if (scanner.hasNext()) s = scanner.next();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }
}
