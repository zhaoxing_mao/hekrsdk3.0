package me.hekr.hummingbird.util;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import org.jetbrains.annotations.NotNull;

import me.hekr.hummingbird.R;

/**
 * Created by hekr_jds on 5/27 0027.
 **/
public class HekrAlert {

    /**
     * 错误码
     *
     * @param title   标题
     * @param message message
     */
    public static void hekrAlert(@NotNull Context context, String title, @NotNull String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        if (!TextUtils.isEmpty(title)) {
            alertDialog.setTitle(title);
        } else {
            alertDialog.setTitle(context.getString(R.string.app_name));
        }
        alertDialog.setPositiveButton(context.getString(R.string.positive_button), null).setMessage(message).create().show();
    }


    /**
     * 错误码
     *
     * @param message message
     */
    public static void hekrAlert(@NotNull Context context, @NotNull String message) {
        hekrAlert(context, null, message);
    }


    /**
     * 错误码
     *
     * @param errorCode 错误码
     */
    public static void hekrAlert(@NotNull Context context, int errorCode) {
        hekrAlert(context, HekrCodeUtil.errorCode2Msg(errorCode));
    }
}
