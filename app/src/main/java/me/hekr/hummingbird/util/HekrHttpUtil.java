package me.hekr.hummingbird.util;

import android.content.Context;

import com.alibaba.fastjson.JSONObject;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.bean.JWTBean;
import me.hekr.hummingbird.event.LogoutEvent;

import com.litesuits.common.assist.Toastor;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.EventBus;

import cz.msebera.android.httpclient.Header;
import me.hekr.hummingbird.R;

/**
 * Created by jin123d on 4/23 0023.
 **/
public class HekrHttpUtil {


    /**
     * 带token的get
     * 并且支持刷新token
     *
     * @param context             context
     * @param JWT_TOKEN           指令格式中的参数token，是APP调用 认证授权API -> 登录 接口返回的JWT Token，有效期为12小时。如果APP和云端建立通道前token已过期，云端会提示token无效；如果联网之后连接不断，即使token过期，APP还能继续控制设备。
     * @param ReFresh_Token       刷新token
     * @param url                 构造网址
     * @param getHekrDataListener 回调方法
     */
    public static void getDataReFreshToken(final Context context, final String JWT_TOKEN, final String ReFresh_Token, final String url, final HekrUser.GetHekrDataWithTokenListener getHekrDataListener) {
        BaseHttpUtil.getDataToken(context, JWT_TOKEN, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                getHekrDataListener.getDataSuccess(byte2Str(bytes));
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                if (getTimeOutFlag(i, bytes)) {
                    HekrCodeUtil.getErrorCode(i, bytes);
                    refreshToken(context, ReFresh_Token, new HekrHttpUtil.RefreshTokenListener() {
                        @Override
                        public void refreshSuccess(JWTBean jwtBean) {
                            getHekrDataListener.getToken(jwtBean);
                            BaseHttpUtil.getDataToken(context, jwtBean.getAccess_token(), jwtBean.getRefresh_token(), new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                                    getHekrDataListener.getDataSuccess(byte2Str(bytes));
                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    getHekrDataListener.getDataFail(HekrCodeUtil.getErrorCode(i, bytes));
                                }
                            });
                            // getDataReFreshToken(context, jwtBean.getAccess_token(), jwtBean.getRefresh_token(), url, getHekrDataListener);
                        }

                        @Override
                        public void refreshFail(int i, Header[] headers, byte[] bytes) {
                            getHekrDataListener.getDataFail(ConstantsUtil.ErrorCode.TOKEN_TIME_OUT);
                        }
                    });
                } else {
                    getHekrDataListener.getDataFail(HekrCodeUtil.getErrorCode(i, bytes));
                }

            }
        });
    }

    /**
     * 带token的网络post  runInUI
     * 支持刷新token
     *
     * @param context             context
     * @param JWT_TOKEN           指令格式中的参数token，是APP调用 认证授权API -> 登录 接口返回的JWT Token，有效期为12小时。如果APP和云端建立通道前token已过期，云端会提示token无效；如果联网之后连接不断，即使token过期，APP还能继续控制设备。
     * @param url                 构造网址
     * @param entity              String JSON形式的string
     * @param getHekrDataListener 回调
     */
    public static void postDataReFreshToken(final Context context, final String JWT_TOKEN, final String ReFresh_Token, final String url, final String entity, final HekrUser.GetHekrDataWithTokenListener getHekrDataListener) {
        BaseHttpUtil.postDataToken(context, JWT_TOKEN, url, entity, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                getHekrDataListener.getDataSuccess(byte2Str(bytes));
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                if (getTimeOutFlag(i, bytes)) {
                    HekrCodeUtil.getErrorCode(i, bytes);
                    refreshToken(context, ReFresh_Token, new HekrHttpUtil.RefreshTokenListener() {
                        @Override
                        public void refreshSuccess(JWTBean jwtBean) {
                            getHekrDataListener.getToken(jwtBean);
                            BaseHttpUtil.postDataToken(context, jwtBean.getAccess_token(), url, entity, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                                    getHekrDataListener.getDataSuccess(byte2Str(bytes));
                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    getHekrDataListener.getDataFail(HekrCodeUtil.getErrorCode(i, bytes));
                                }
                            });
                            //postDataReFreshToken(context, jwtBean.getAccess_token(), jwtBean.getRefresh_token(), url, entity, getHekrDataListener);
                        }

                        @Override
                        public void refreshFail(int i, Header[] headers, byte[] bytes) {
                            getHekrDataListener.getDataFail(ConstantsUtil.ErrorCode.TOKEN_TIME_OUT);
                        }
                    });
                } else {
                    getHekrDataListener.getDataFail(HekrCodeUtil.getErrorCode(i, bytes));
                }

            }
        });
    }


    /**
     * 带token的DELETE
     * 支持刷新token
     *
     * @param context             context
     * @param JWT_TOKEN           指令格式中的参数token，是APP调用 认证授权API -> 登录 接口返回的JWT Token，有效期为12小时。如果APP和云端建立通道前token已过期，云端会提示token无效；如果联网之后连接不断，即使token过期，APP还能继续控制设备。
     * @param url                 构造网址
     * @param getHekrDataListener 回调方法
     */
    public static void deleteDataReFreshToken(final Context context, final String JWT_TOKEN, final String ReFresh_Token, final String url, final HekrUser.GetHekrDataWithTokenListener getHekrDataListener) {
        BaseHttpUtil.deleteDataToken(context, JWT_TOKEN, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                getHekrDataListener.getDataSuccess(byte2Str(bytes));
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                if (getTimeOutFlag(i, bytes)) {
                    HekrCodeUtil.getErrorCode(i, bytes);
                    refreshToken(context, ReFresh_Token, new HekrHttpUtil.RefreshTokenListener() {
                        @Override
                        public void refreshSuccess(JWTBean jwtBean) {
                            getHekrDataListener.getToken(jwtBean);
                            BaseHttpUtil.deleteDataToken(context, jwtBean.getAccess_token(), url, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                                    getHekrDataListener.getDataSuccess(byte2Str(bytes));
                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    getHekrDataListener.getDataFail(HekrCodeUtil.getErrorCode(i, bytes));
                                }
                            });
                            //deleteDataReFreshToken(context, jwtBean.getAccess_token(), jwtBean.getRefresh_token(), url, getHekrDataListener);
                        }

                        @Override
                        public void refreshFail(int i, Header[] headers, byte[] bytes) {
                            getHekrDataListener.getDataFail(ConstantsUtil.ErrorCode.TOKEN_TIME_OUT);
                        }
                    });
                } else {
                    getHekrDataListener.getDataFail(HekrCodeUtil.getErrorCode(i, bytes));
                }
            }
        });
    }


    /**
     * 带token的PATCH
     * 支持刷新token
     *
     * @param context             context
     * @param JWT_TOKEN           指令格式中的参数token，是APP调用 认证授权API -> 登录 接口返回的JWT Token，有效期为12小时。如果APP和云端建立通道前token已过期，云端会提示token无效；如果联网之后连接不断，即使token过期，APP还能继续控制设备。
     * @param ReFresh_Token       刷新token
     * @param url                 构造网址
     * @param entity              请求体
     * @param getHekrDataListener 回调方法
     */
    public static void patchDataToken(final Context context, final String JWT_TOKEN, final String ReFresh_Token, final String url, final String entity, final HekrUser.GetHekrDataWithTokenListener getHekrDataListener) {
        BaseHttpUtil.patchDataToken(context, JWT_TOKEN, url, entity, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                getHekrDataListener.getDataSuccess(byte2Str(bytes));

            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                if (getTimeOutFlag(i, bytes)) {
                    HekrCodeUtil.getErrorCode(i, bytes);
                    refreshToken(context, ReFresh_Token, new HekrHttpUtil.RefreshTokenListener() {
                        @Override
                        public void refreshSuccess(JWTBean jwtBean) {
                            getHekrDataListener.getToken(jwtBean);
                            BaseHttpUtil.patchDataToken(context, jwtBean.getAccess_token(), url, entity, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                                    getHekrDataListener.getDataSuccess(byte2Str(bytes));
                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    getHekrDataListener.getDataFail(HekrCodeUtil.getErrorCode(i, bytes));
                                }
                            });
                            //patchDataToken(context, jwtBean.getAccess_token(), jwtBean.getRefresh_token(), url, entity, getHekrDataListener);
                        }

                        @Override
                        public void refreshFail(int i, Header[] headers, byte[] bytes) {
                            getHekrDataListener.getDataFail(ConstantsUtil.ErrorCode.TOKEN_TIME_OUT);
                        }
                    });
                } else {
                    getHekrDataListener.getDataFail(HekrCodeUtil.getErrorCode(i, bytes));
                }
            }
        });
    }


    /**
     * 带token的PUT
     *
     * @param context             context
     * @param JWT_TOKEN           指令格式中的参数token，是APP调用 认证授权API -> 登录 接口返回的JWT Token，有效期为12小时。如果APP和云端建立通道前token已过期，云端会提示token无效；如果联网之后连接不断，即使token过期，APP还能继续控制设备。
     * @param url                 构造网址
     * @param entity              请求体
     * @param getHekrDataListener 回调方法
     */
    public static void putDataRefreshToken(final Context context, String JWT_TOKEN, final String ReFresh_Token, final String url, final String entity, final HekrUser.GetHekrDataWithTokenListener getHekrDataListener) {
        BaseHttpUtil.putDataToken(context, JWT_TOKEN, url, entity, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                getHekrDataListener.getDataSuccess(byte2Str(bytes));
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                if (getTimeOutFlag(i, bytes)) {
                    HekrCodeUtil.getErrorCode(i, bytes);
                    refreshToken(context, ReFresh_Token, new HekrHttpUtil.RefreshTokenListener() {
                        @Override
                        public void refreshSuccess(JWTBean jwtBean) {
                            getHekrDataListener.getToken(jwtBean);
                            BaseHttpUtil.putDataToken(context, jwtBean.getAccess_token(), url, entity, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                                    getHekrDataListener.getDataSuccess(byte2Str(bytes));
                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    getHekrDataListener.getDataFail(HekrCodeUtil.getErrorCode(i, bytes));
                                }
                            });
                            //putDataRefreshToken(context, jwtBean.getAccess_token(), jwtBean.getRefresh_token(), url, entity, getHekrDataListener);
                        }

                        @Override
                        public void refreshFail(int i, Header[] headers, byte[] bytes) {
                            getHekrDataListener.getDataFail(ConstantsUtil.ErrorCode.TOKEN_TIME_OUT);
                        }
                    });
                } else {
                    getHekrDataListener.getDataFail(HekrCodeUtil.getErrorCode(i, bytes));
                }
            }
        });
    }


    /**
     * 带token的网络post  runInUI
     * 支持刷新token
     *
     * @param context             context
     * @param JWT_TOKEN           指令格式中的参数token，是APP调用 认证授权API -> 登录 接口返回的JWT Token，有效期为12小时。如果APP和云端建立通道前token已过期，云端会提示token无效；如果联网之后连接不断，即使token过期，APP还能继续控制设备。
     * @param ReFresh_Token       刷新方法
     * @param url                 构造网址
     * @param params              表单
     * @param getHekrDataListener 回调
     */
    public static void postFileReFreshToken(final Context context, String JWT_TOKEN, final String ReFresh_Token, final String url, final RequestParams params, final HekrUser.GetHekrDataWithTokenProgressListener getHekrDataListener) {
        BaseHttpUtil.postFileToken(context, JWT_TOKEN, url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                getHekrDataListener.getDataSuccess(byte2Str(bytes));
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);
                int count = (int) ((bytesWritten * 1.0 / totalSize) * 100);
                // 上传进度显示
                getHekrDataListener.getDataProgress(count);
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                if (getTimeOutFlag(i, bytes)) {
                    HekrCodeUtil.getErrorCode(i, bytes);
                    refreshToken(context, ReFresh_Token, new HekrHttpUtil.RefreshTokenListener() {
                        @Override
                        public void refreshSuccess(JWTBean jwtBean) {
                            getHekrDataListener.getToken(jwtBean);
                            //为了防止死循环，只拉一次接口!
                            BaseHttpUtil.postFileToken(context, jwtBean.getAccess_token(), url, params, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                                    getHekrDataListener.getDataSuccess(byte2Str(bytes));
                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    getHekrDataListener.getDataFail(HekrCodeUtil.getErrorCode(i, bytes));
                                }
                            });
                            //postFileReFreshToken(context, jwtBean.getAccess_token(), jwtBean.getRefresh_token(), url, params, getHekrDataListener);
                        }

                        @Override
                        public void refreshFail(int i, Header[] headers, byte[] bytes) {
                            getHekrDataListener.getDataFail(ConstantsUtil.ErrorCode.TOKEN_TIME_OUT);
                        }
                    });
                } else {
                    getHekrDataListener.getDataFail(HekrCodeUtil.getErrorCode(i, bytes));
                }

            }
        });
    }

    private static String byte2Str(byte[] bytes) {
        if (bytes == null) {
            return null;
        } else {
            return new String(bytes);
        }
    }


    /**
     * 刷新token
     *
     * @param context              context
     * @param ReFresh_Token        RefreshToken
     * @param refreshTokenListener 回调接口
     */
    private static void refreshToken(final Context context, String ReFresh_Token, final HekrHttpUtil.RefreshTokenListener refreshTokenListener) {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("refresh_token", ReFresh_Token);
        String refresh_url = ConstantsUtil.UrlUtil.BASE_UAA_URL + ConstantsUtil.UrlUtil.UAA_REFRESH_TOKEN;
        BaseHttpUtil.postData(context, refresh_url, jsonObject.toJSONString(), new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                refreshTokenListener.refreshSuccess(JSONObject.parseObject(new String(bytes), JWTBean.class));

            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                if (bytes != null && bytes.length > 0) {
                    android.util.Log.e("HEKR_SDK_ERROR", new String(bytes));
                }
                refreshTokenListener.refreshFail(i, headers, bytes);
                //此时token过期，直接强制退出APP
                logout(context);
            }
        });
    }

    /**
     * 刷新token接口
     */
    public interface RefreshTokenListener {
        void refreshSuccess(JWTBean jwtBean);

        void refreshFail(int i, Header[] headers, byte[] bytes);
    }


    /**
     * 退出APP
     */
    private static void logout(Context context) {
        //此时token过期，直接强制退出APP
        new Toastor(context).showSingletonToast(context.getString(R.string.Token_expired));
        HekrUserAction.getInstance(context).userLogout();
        EventBus.getDefault().post(new LogoutEvent(true));
    }


    /**
     * 判断是否token过期
     */
    private static boolean getTimeOutFlag(int i, byte[] bytes) {
        //{"code":3400014,"desc":"Your account has already been linked to a QQ account","timestamp":1465715832304}
        //云端sb接口403
        return i == 403 && (HekrCodeUtil.getErrorCode(i, bytes) != 3400014);
    }
}
