/**
 *
 * Copyright (c) Hekr.me Co.ltd Hangzhou, 2013-2015
 * 
 * 杭州第九区科技（氦氪科技）有限公司拥有该文件的使用、复制、修改和分发的许可权
 * 如果你想得到更多信息，请访问 <http://www.hekr.me>
 *
 * Hekr Co.ltd Hangzhou owns permission to use, copy, modify and
 * distribute this documentation.
 * For more information, please see <http://www.hekr.me>
 * 
 */

package me.hekr.hummingbird.util;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 
 * HekrConfig.java created by durianskh at Oct 30, 2015 6:49:19 PM 这里对类或者接口作简要描述
 *
 * @author durianskh
 * @version 1.0
 * 
 */

public class HekrConfig {

	/**
	 * 原子变量，决定是否需要继续发送ssid和password
	 */
	private AtomicBoolean isSsidAndPassOK = new AtomicBoolean(false);

	/**
	 * 原子变量，判断线程是否全部停止
	 */
	//private AtomicBoolean done = new AtomicBoolean(false);

	/**
	 * 原子变量，返回的最终结果
	 */
	private AtomicReference<DatagramPacket> result = null;


	public HekrConfig() {

	}

	/**
	 * 获取发送经历的时间
	 * 
	 * @param beginTime
	 * @return
	 */
	private long getPassTime(long beginTime) {
		return System.currentTimeMillis() - beginTime;
	}

	/**
	 * 获取需要等待的时间，该变量变化不大，有时间可以多测试，写一个更好的公式
	 * 
	 * @param passTime
	 * @param length
	 * @return
	 */
	private long getSleepTime(long passTime, int length) {
		long param = passTime / 1000 - 3 > 0 ? passTime / 1000 - 3 : 0;
		return 100 / length * (1 + param / 6);
	}

	/**
	 * 具体执行配置的过程，启动两个线程，一个线程用于发送ssid和pass
	 * 
	 * @param ssid
	 * @param password
	 * @return
	 */
	public Object config(final String ssid, final String password,int number) {
		isSsidAndPassOK.set(false);
		// 发送ssid和pass的线程
		new Thread() {
			public void run() {
				byte[] data = (ssid + '\0' + password + '\0').getBytes();
				long beginTime = System.currentTimeMillis();
				long passTime = getPassTime(beginTime);
				while (!isSsidAndPassOK.get()) {
					long sleepTime;
					if (passTime > 1000) {
						sleepTime = getSleepTime(passTime, data.length);
					} else {
						sleepTime = getSleepTime(passTime, data.length + 1);
					}
					try {
						UDPConfig.hekrconfig(ssid + "", password + "", (int) sleepTime);
						//Log.i("HekrConfig","发送ssid"+ssid+"&&password"+password+"udp发包休眠时间:"+sleepTime*100);
						Thread.sleep(sleepTime);
					} catch (Exception e) {
					}
                    passTime = getPassTime(beginTime);
				}
			}
		}.start();

		int count = 0;
		try {
			// 120*500毫秒是1分钟，这是配置的总体超时时间
			// 在1分钟内，不断去判断配置是否成功
			while ((!isSsidAndPassOK.get()) && count < number) {
				// 每次判断之后，主线程休眠1000毫秒
				Thread.sleep(1000);
				count++;
			}
		} catch (Exception e) {
		}

		//Log.i("hekrxmconfig","hekrxmconfig停止发包...");
		// timeout, stopping the two threads
		isSsidAndPassOK.set(true);
		// 等待所有的线程停止
		//while (!done.get()) {
		//}

		return result;
	}

    public void stop() {
        isSsidAndPassOK.set(true);
    }

	/**
	 * hekrconfig配置
	 * 
	 * @param ssid
	 * @param password
	 * @throws IOException
	 */
	public static void hekrconfig(String ssid, String password) throws IOException {
		UDPConfig.hekrconfig(ssid, password, 0);
	}
}
