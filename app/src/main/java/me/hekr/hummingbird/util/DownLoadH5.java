package me.hekr.hummingbird.util;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by hekr_xm on 2016/4/22.
 **/
public class DownLoadH5 {

    private static final String TAG="DownLoadH5";
    private Context context;
    private ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();

    public DownLoadH5(Context context) {
        this.context = context;
    }

    public void startDownLoadH5(final String url, final String pageFileName){
        singleThreadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                com.litesuits.android.log.Log.i(TAG,"启动下载线程!");
                downloadAllAssets(url,pageFileName);
            }
        });
    }

    private void downloadAllAssets( String url,String pageFileName) {
        // Temp folder for holding asset during download
        Log.i("zx","download");
        File zipDir =  ExternalStorage.getSDCacheDir(context, "tmp");
        // File path to store .zip file before unzipping
        File zipFile = new File( zipDir.getPath() + "/temp.zip" );
        // Folder to hold unzipped output
        File outputDir = ExternalStorage.getSDCacheDir( context, pageFileName );

        try {
            DownloadFile.download( url, zipFile, zipDir );
            unzipFile( zipFile, outputDir );
        } finally {
            zipFile.delete();
        }
    }

    /**
     * Unpack .zip file.
     *
     * @param zipFile
     * @param destination
     */
    protected void unzipFile( File zipFile, File destination ) {
        Log.i("zx","unzip");
        DecompressZip decomp = new DecompressZip( zipFile.getPath(),
                destination.getPath() + File.separator );
        decomp.unzip();
    }
}
