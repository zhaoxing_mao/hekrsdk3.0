package me.hekr.hummingbird.util;

import me.hekr.hummingbird.action.HekrData;
import me.hekr.hummingbird.application.MyApplication;
import me.hekr.hummingbird.bean.CtrlBean;
import me.hekr.hummingbird.event.WebServiceSendEvent;
import me.hekr.hummingbird.x5web.X5WebView;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by hekr_xm on 2016/5/3.
 **/
public class MsgUtil {

    private static final String TAG = "MsgUtil";

    public static void sendMsg(X5WebView x5WebView, String devTid, JSONObject protocol, HekrData.dataReceiverListener dataReceiverListener) {

        if (MyApplication.lanList != null && !MyApplication.lanList.isEmpty()) {

            //EventBus.getDefault().post(new LanServiceSendEvent(5, new CtrlBean(x5WebView,devTid, protocol, dataReceiverListener)));
                EventBus.getDefault().post(new WebServiceSendEvent(ConstantsUtil.ServiceCode.DATA_SEND_WHAT, new CtrlBean(x5WebView,devTid, protocol, dataReceiverListener)));
        } else {
            EventBus.getDefault().post(new WebServiceSendEvent(ConstantsUtil.ServiceCode.DATA_SEND_WHAT, new CtrlBean(x5WebView,devTid, protocol, dataReceiverListener)));
        }
    }


    public static void receiveMsg(X5WebView x5WebView, JSONObject filter, HekrData.dataReceiverListener dataReceiverListener) {

        if (MyApplication.lanList != null && !MyApplication.lanList.isEmpty()) {

            try {
                //EventBus.getDefault().post(new LanServiceSendEvent(6, new CtrlBean(x5WebView,filter.getJSONObject("params").getString("devTid"), filter, dataReceiverListener)));
                EventBus.getDefault().post(new WebServiceSendEvent(ConstantsUtil.ServiceCode.DATA_RECEIVE_WHAT, new CtrlBean(x5WebView,filter.getJSONObject("params").getString("devTid"), filter, dataReceiverListener)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            try {
                EventBus.getDefault().post(new WebServiceSendEvent(ConstantsUtil.ServiceCode.DATA_RECEIVE_WHAT, new CtrlBean(x5WebView,filter.getJSONObject("params").getString("devTid"), filter, dataReceiverListener)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
