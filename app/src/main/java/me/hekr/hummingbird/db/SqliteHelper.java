package me.hekr.hummingbird.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by OkamiHsu on 16/4/7.
 */
public class SqliteHelper extends SQLiteOpenHelper {


    public SqliteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE IF NOT EXISTS devices " +
                "  ( " +
                "     id               INTEGER PRIMARY KEY NOT NULL UNIQUE, " +
                "     devicename       VARCHAR(50), " +
                "     tokentype        INTEGER, " +
                "     bintype          VARCHAR(20), " +
                "     devtid           VARCHAR(20), " +
                "     servicehost      VARCHAR(20), " +
                "     ctrlkey          VARCHAR(50), " +
                "     logo             VARCHAR(250), " +
                "     currentlogintime DATETIME, " +
                "     owneruid         VARCHAR(20), " +
                "     ssid             VARCHAR(50), " +
                "     granted          BOOLEAN, " +
                "     eventrulemap     VARCHAR(250), " +
                "     workmodetype     VARCHAR(20), " +
                "     timermap         VARCHAR(50), " +
                "     wanip            VARCHAR(20), " +
                "     lastlogintime    DATETIME, " +
                "     connectip        VARCHAR, " +
                "     productpublickey VARCHAR(20), " +
                "     androidh5page    VARCHAR(250), " +
                "     online           BOOLEAN, " +
                "     setschedulertask BOOLEAN, " +
                "     lastupdatetime   DATETIME, " +
                "     serviceport      INTEGER, " +
                "     firstlogintime   DATETIME, " +
                "     folderid         VARCHAR(20), " +
                "     sdkver           VARCHAR(20), " +
                "     description      VARCHAR(50), " +
                "     binversion       VARCHAR(20), " +
                "     cid              VARCHAR(20), " +
                "     devsharenum      INTEGER, " +
                "     iosh5page        VARCHAR(250), " +
                "     bindkey          VARCHAR(20), " +
                "     lastheartbeat    DATETIME, " +
                "     h5version        INTEGER(10), " +
                "     h5downtime       DATETIME, " +
                "     h5local          VARCHAR(50) " +
                "  ) ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS device_log");
        onCreate(db);

    }
}
