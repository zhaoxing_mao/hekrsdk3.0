package me.hekr.hummingbird.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;

import com.litesuits.common.assist.Toastor;

import org.jetbrains.annotations.NotNull;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.util.HekrAlert;
import me.hekr.hummingbird.util.HekrCodeUtil;

public class ChangePhoneGetCodeFragment extends Fragment {
    private EditText et_code;
    private Button btn_next;
    private TimeCount timeCount;
    private TextView tv_get_code;
    private HekrUserAction hekrUserAction;
    private Toastor toastor;
    private String phoneNumber;


    public static ChangePhoneGetCodeFragment newInstance(String phone) {
        ChangePhoneGetCodeFragment fragment = new ChangePhoneGetCodeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("phone", phone);
        fragment.setArguments(bundle);
        return fragment;
    }

    public ChangePhoneGetCodeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_change_phone_get_code, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initData();
        getCode();
    }

    private void initData() {
        toastor = new Toastor(getActivity());
        hekrUserAction = HekrUserAction.getInstance(getActivity());
        timeCount = new TimeCount(60000, 1000);
        phoneNumber = getArguments().getString("phone");
        tv_get_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCode();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                String code = et_code.getText().toString().trim();
                if (TextUtils.isEmpty(code)) {
                    HekrAlert.hekrAlert(getActivity(), getString(R.string.please_input_code));
                } else {
                    checkCode(phoneNumber, code);
                }
            }
        });
    }

    private void initView(View view) {
        et_code = (EditText) view.findViewById(R.id.et_code);
        btn_next = (Button) view.findViewById(R.id.btn_next);
        tv_get_code = (TextView) view.findViewById(R.id.tv_get_code);
    }


    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            tv_get_code.setClickable(false);
            tv_get_code.setEnabled(false);
            tv_get_code.setText(millisUntilFinished / 1000 + "s");
        }

        @Override
        public void onFinish() {
            tv_get_code.setEnabled(true);
            tv_get_code.setText(getString(R.string.get_code));
            tv_get_code.setClickable(true);
        }
    }

    /**
     * 校验手机号是否正确
     */
    private void checkCode(String phoneNumber, String code) {
        hekrUserAction.checkVerifyCode(phoneNumber, code, new HekrUser.CheckVerifyCodeListener() {
            @Override
            public void checkVerifyCodeSuccess(String phoneNumber, String verifyCode, String token, String expireTime) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fg, ChangePhoneBindFragment.newInstance(token));
                fragmentTransaction.commit();
            }

            @Override
            public void checkVerifyCodeFail(int errorCode) {
                HekrAlert.hekrAlert(getActivity(), errorCode);
            }
        });
    }

    private void getCode() {
        hekrUserAction.getVerifyCode(phoneNumber, HekrUserAction.CODE_TYPE_CHANGE_PHONE, new HekrUser.GetVerifyCodeListener() {
            @Override
            public void getVerifyCodeSuccess() {
                toastor.showSingletonToast(getString(R.string.success_send));
                timeCount.start();
            }

            @Override
            public void getVerifyCodeFail(int errorCode) {
                HekrAlert.hekrAlert(getActivity(), errorCode);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timeCount != null) {
            timeCount.cancel();
            timeCount = null;
        }
    }
}
