package me.hekr.hummingbird.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;

import com.litesuits.common.assist.Toastor;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.util.HekrAlert;
import me.hekr.hummingbird.util.HekrCodeUtil;

public class ChangeEmailFragment extends Fragment {
    private HekrUserAction hekrUserAction;
    private Toastor toastor;


    public static ChangeEmailFragment newInstance(String email) {
        ChangeEmailFragment fragment = new ChangeEmailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("email", email);
        fragment.setArguments(bundle);
        return fragment;
    }

    public ChangeEmailFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_change_mail, container, false);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hekrUserAction = HekrUserAction.getInstance(getActivity());
        toastor = new Toastor(getActivity());
        final String email = getArguments().getString("email");
        TextView tv_email = (TextView) view.findViewById(R.id.tv_email);
        tv_email.setText(getString(R.string.my_email) + email);
        Button btn_next = (Button) view.findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email != null) {
                    hekrUserAction.sendChangeEmailStep1Email(email, new HekrUser.SendChangeEmailListener() {
                        @Override
                        public void sendChangeEmailSuccess() {
                            toastor.showSingletonToast(getString(R.string.re_send_mail));
                        }

                        @Override
                        public void sendChangeEmailFail(int errorCode) {
                            HekrAlert.hekrAlert(getActivity(), errorCode);
                        }
                    });
                }
            }
        });
    }

}
