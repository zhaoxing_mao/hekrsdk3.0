package me.hekr.hummingbird.fragment;

import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;

import com.litesuits.common.assist.Toastor;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.event.ViewpageEvent;
import me.hekr.hummingbird.util.ConstantsUtil;
import me.hekr.hummingbird.util.HekrAlert;
import me.hekr.hummingbird.util.HekrCodeUtil;
import me.hekr.hummingbird.util.SpCache;
import me.hekr.hummingbird.util.Validator;

public class OAuthBindEmailFragment extends Fragment {
    private static final String TAG = "InputUserNameFragment";
    private EditText et_pwd;
    private EditText et_mail;
    private Button btn_ok;
    private HekrUserAction hekrUserAction;
    private Toastor toastor;
    private String bindToken;


    public static OAuthBindEmailFragment newInstance(@NotNull String bindToken) {
        OAuthBindEmailFragment fragment = new OAuthBindEmailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("bindToken", bindToken);
        fragment.setArguments(bundle);
        return fragment;
    }

    public OAuthBindEmailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_oauth_bind_email, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initData();
    }

    private void initData() {
        toastor = new Toastor(getActivity());
        hekrUserAction = HekrUserAction.getInstance(getActivity());
        bindToken = getArguments().getString("bindToken");
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final String email = et_mail.getText().toString().trim();
                String pwd = et_pwd.getText().toString().trim();
                if (TextUtils.isEmpty(email) || TextUtils.isEmpty(pwd)) {
                    toastor.showSingletonToast(getString(R.string.full_info));
                } else if (Validator.isEmail(email)) {
                    hekrUserAction.registerByEmail(email, pwd, new HekrUser.RegisterListener() {
                        @Override
                        public void registerSuccess(String uid) {
                            new AlertDialog.Builder(getActivity()).setTitle(R.string.email_register_title).
                                    setMessage(getString(R.string.email_register_success)).setPositiveButton(getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    SpCache.putString(ConstantsUtil.SP_TEMP_ACCOUNT, email);
                                    SpCache.putString(ConstantsUtil.SP_TEMP_TOKEN, bindToken);
                                    getActivity().finish();
                                }
                            }).create().show();
                        }

                        @Override
                        public void registerFail(int errorCode) {
                            if (errorCode == 3400008) {
                                registed();
                            } else {
                                HekrAlert.hekrAlert(getActivity(), errorCode);
                            }
                        }
                    });

                } else {
                    toastor.showSingletonToast(getString(R.string.error_email));
                }

            }
        });
    }

    private void initView(View view) {
        et_pwd = (EditText) view.findViewById(R.id.et_pwd);
        btn_ok = (Button) view.findViewById(R.id.btn_ok);
        et_mail = (EditText) view.findViewById(R.id.et_mail);
    }


    /**
     * 已经注册！
     */
    private void registed() {
        new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.app_name)).setMessage(getString(R.string.registered)).
                setPositiveButton(getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String email = et_mail.getText().toString().trim();
                        SpCache.putString(ConstantsUtil.SP_TEMP_ACCOUNT, email);
                        SpCache.putString(ConstantsUtil.SP_TEMP_TOKEN, bindToken);
                        getActivity().finish();
                        EventBus.getDefault().post(new ViewpageEvent(1, email));
                    }
                }).setNegativeButton(getString(R.string.negative_button), null).create().show();
    }


}
