package me.hekr.hummingbird.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.activity.AboutActivity;
import me.hekr.hummingbird.activity.DevicesManageActivity;
import me.hekr.hummingbird.activity.PersonalCenterActivity;
import me.hekr.hummingbird.activity.ScrollingActivity;
import me.hekr.hummingbird.bean.ProfileBean;
import me.hekr.hummingbird.event.RefreshEvent;
import me.hekr.hummingbird.ui.CircleImageView;
import me.hekr.hummingbird.util.ConstantsUtil;
import me.hekr.hummingbird.util.SpCache;

/**
 * Created by hekr_jds on 5/27 0027.
 **/
public class SlidingMenuFragment extends Fragment implements View.OnClickListener {
    private HekrUserAction hekrUserAction;
    private CircleImageView img_user_icon;
    private TextView tv_user_name;
    private DisplayImageOptions options;
    private LinearLayout linearLayout;
    private LinearLayout layout_home;
    private LinearLayout layout_devices;
    private LinearLayout layout_about;
    private SlidingMenu slidingMenu;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initData();
    }

    private void initData() {
        linearLayout.setOnClickListener(this);
        layout_home.setOnClickListener(this);
        layout_devices.setOnClickListener(this);
        layout_about.setOnClickListener(this);
    }

    private void initView(View view) {
        img_user_icon = (CircleImageView) view.findViewById(R.id.user_logo);
        tv_user_name = (TextView) view.findViewById(R.id.tv_user_name);
        hekrUserAction = HekrUserAction.getInstance(getActivity());
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)//设置为RGB565比起默认的ARGB_8888要节省大量的内存
                .delayBeforeLoading(100)//载入图片前稍做延时可以提高整体滑动的流畅度
                .build();
        getUserInfo();
        linearLayout = (LinearLayout) view.findViewById(R.id.header);
        layout_home = (LinearLayout) view.findViewById(R.id.home);
        layout_devices = (LinearLayout) view.findViewById(R.id.devices);
        layout_about = (LinearLayout) view.findViewById(R.id.about);
        slidingMenu = (SlidingMenu) getActivity().findViewById(R.id.sliding_menu);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.layout_menu, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe
    public void onEvent(RefreshEvent event) {
        switch (event.getRefreshTag()) {
            case RefreshEvent.REFRESH_USER:
                getUserInfo();
                break;
        }
    }


    /**
     * 获取用户信息
     */
    private void getUserInfo() {
        hekrUserAction.getProfile(new HekrUser.GetProfileListener() {
            @Override
            public void getProfileSuccess(ProfileBean profileBean) {
                try {
                    if (profileBean.getAvatarUrl().getSmall() != null) {
                        ImageLoader.getInstance().displayImage(profileBean.getAvatarUrl().getSmall(), img_user_icon, options);
                    }
                    if (!TextUtils.isEmpty(profileBean.getLastName())) {
                        tv_user_name.setText(profileBean.getLastName());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void getProfileFail(int errorCode) {
                //myToast.showSingletonToast(HekrCodeUtil.errorCode2Msg(errorCode));
            }
        });
    }

    private void start(Class clazz) {
        startActivity(new Intent(getActivity(), clazz));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.header:
                start(PersonalCenterActivity.class);
                break;
            case R.id.home:
                slidingMenu.toggle();
                break;
            case R.id.devices:
                start(DevicesManageActivity.class);
                break;
            case R.id.about:
                start(AboutActivity.class);
                break;
        }
    }
}
