package me.hekr.hummingbird.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.litesuits.common.assist.Toastor;

import me.hekr.hummingbird.R;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.util.HekrAlert;
import me.hekr.hummingbird.util.HekrCodeUtil;

public class ChangePhoneBindFragment extends Fragment {
    private static final String TAG = "InputUserNameFragment";
    private EditText et_code;
    private Button btn_ok;
    private TimeCount timeCount;
    private TextView tv_get_code;
    private HekrUserAction hekrUserAction;
    private Toastor toastor;
    private String token;
    private EditText et_user_name;


    public static ChangePhoneBindFragment newInstance(String token) {
        ChangePhoneBindFragment fragment = new ChangePhoneBindFragment();
        Bundle bundle = new Bundle();
        bundle.putString("token", token);
        fragment.setArguments(bundle);
        return fragment;
    }

    public ChangePhoneBindFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_change_phone_bind, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initData();
    }

    private void initData() {
        toastor = new Toastor(getActivity());
        hekrUserAction = HekrUserAction.getInstance(getActivity());
        timeCount = new TimeCount(60000, 1000);
        token = getArguments().getString("token");
        tv_get_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hekrUserAction.getVerifyCode(et_user_name.getText().toString(), HekrUserAction.REGISTER_TYPE_PHONE, new HekrUser.GetVerifyCodeListener() {
                    @Override
                    public void getVerifyCodeSuccess() {
                        toastor.showSingletonToast(getString(R.string.success_send));
                        timeCount.start();
                    }

                    @Override
                    public void getVerifyCodeFail(int errorCode) {
                        HekrAlert.hekrAlert(getActivity(), errorCode);
                    }
                });
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                String code = et_code.getText().toString().trim();
                String phone = et_user_name.getText().toString().trim();
                if (TextUtils.isEmpty(code) || TextUtils.isEmpty(phone)) {
                    HekrAlert.hekrAlert(getActivity(), getString(R.string.full_info));
                } else {
                    changePhone(phone, code, token);
                }
            }
        });
    }

    private void initView(View view) {
        et_code = (EditText) view.findViewById(R.id.et_code);
        btn_ok = (Button) view.findViewById(R.id.btn_ok);
        tv_get_code = (TextView) view.findViewById(R.id.tv_get_code);
        et_user_name = (EditText) view.findViewById(R.id.et_user_name);
    }


    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            tv_get_code.setClickable(false);
            tv_get_code.setText(millisUntilFinished / 1000 + "s");
        }

        @Override
        public void onFinish() {
            tv_get_code.setText(getString(R.string.get_code));
            tv_get_code.setClickable(true);

        }
    }

    /**
     * 修改手机号
     *
     * @param phoneNumber 手机号
     * @param verifyCode  验证码
     * @param token       上一步获取到token
     */
    private void changePhone(String phoneNumber, String verifyCode, final String token) {
        hekrUserAction.changePhoneNumber(token, verifyCode, phoneNumber, new HekrUser.ChangePhoneNumberListener() {
            @Override
            public void changePhoneNumberSuccess() {
                toastor.showSingletonToast(R.string.change_success);
                getActivity().finish();
            }

            @Override
            public void changePhoneNumberFail(int errorCode) {
                HekrAlert.hekrAlert(getActivity(), errorCode);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timeCount != null) {
            timeCount.cancel();
            timeCount = null;
        }
    }
}
