package me.hekr.hummingbird.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.igexin.sdk.PushManager;
import com.litesuits.android.log.Log;
import com.litesuits.common.assist.Toastor;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.sina.weibo.sdk.exception.WeiboException;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;

import io.fabric.sdk.android.Fabric;
import me.hekr.hummingbird.R;
import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.activity.ForgotPwdActivity;
import me.hekr.hummingbird.activity.OAuthBindActivity;
import me.hekr.hummingbird.activity.ScrollingActivity;
import me.hekr.hummingbird.application.MyApplication;
import me.hekr.hummingbird.bean.JWTBean;
import me.hekr.hummingbird.bean.MOAuthBean;
import me.hekr.hummingbird.event.AuthCodeEvent;
import me.hekr.hummingbird.event.ViewpageEvent;
import me.hekr.hummingbird.event.WsSwitchEvent;
import me.hekr.hummingbird.service.WebSocketService;
import me.hekr.hummingbird.ui.CustomProgress;
import me.hekr.hummingbird.ui.TwButton;
import me.hekr.hummingbird.util.AccessTokenKeeper;
import me.hekr.hummingbird.util.AuthConstant;
import me.hekr.hummingbird.util.ConstantsUtil;
import me.hekr.hummingbird.util.HekrAlert;
import me.hekr.hummingbird.util.HekrCodeUtil;
import me.hekr.hummingbird.util.SpCache;

public class LoginFragment extends Fragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "LoginFragment";

    private EditText et_user_name;
    private EditText et_pwd;
    private Button btn_login;
    private TextView tv_reset_pwd;
    private HekrUserAction hekrUserAction;
    private Toastor toastor;
    private ImageView img_qq;
    private ImageView img_weChat;
    private ImageView img_weiBo;
    private ImageView img_google;
    private ImageView img_facebook;
    private TwButton img_twitter;


    private int ssoAuthType = 0;

    //qq
    private Tencent mTencent;

    //微博
    public AuthInfo mAuthInfo;

    public Oauth2AccessToken mAccessToken;
    /**
     * 注意：SsoHandler 仅当 HekrSDK 支持 SSO 时有效
     */
    private SsoHandler mSsoHandler;

    //facebook
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    public AccessToken accessToken;
    public ProfileTracker profileTracker;

    //google
    private static final int RC_SIGN_IN = 9001;
    private static final int RC_GET_TOKEN = 9002;
    private GoogleApiClient mGoogleApiClient;
    private CustomProgress customProgress;
    private String SCOPE;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initTwitter();
        EventBus.getDefault().register(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.layout_login, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initData();

    }

    private void initQQLogin() {
        //qq
        mTencent = Tencent.createInstance(AuthConstant.QQ_APP_ID, getActivity().getApplicationContext());
    }

    private void initWeiBoLogin() {
        //weiBo
        mAuthInfo = new AuthInfo(getActivity(), AuthConstant.WEIBO_APP_KEY, AuthConstant.WEIBO_REDIRECT_URL, AuthConstant.WEIBO_SCOPE);

    }

    private void initTwitter() {
        TwitterAuthConfig authConfig = new TwitterAuthConfig(AuthConstant.TWITTER_KEY, AuthConstant.TWITTER_SECRET);
        Fabric.with(getActivity(), new TwitterCore(authConfig));
    }


    private void initGoogleLogin() {
        SCOPE = "audience:server:client_id:" + getString(R.string.server_client_id);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.DRIVE_APPFOLDER))
                .requestIdToken(getString(R.string.server_client_id))
                .requestServerAuthCode(getString(R.string.server_client_id))
                .requestEmail()
                .build();
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }
    }

    private void initFacebookLogin() {
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        FacebookSdk.setIsDebugEnabled(true);

        callbackManager = CallbackManager.Factory.create();

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
            }
        };
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                    Profile oldProfile,
                    Profile currentProfile) {
                // App code
            }
        };
        // If the access token is available already assign it.
        accessToken = AccessToken.getCurrentAccessToken();
        LoginManager.getInstance().registerCallback(callbackManager, new FaceBookListener());
    }


    private void initData() {
        hekrUserAction = HekrUserAction.getInstance(getActivity());
        toastor = new Toastor(getActivity());
        tv_reset_pwd.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        img_qq.setOnClickListener(this);
        img_weiBo.setOnClickListener(this);
        img_weChat.setOnClickListener(this);
        img_google.setOnClickListener(this);
        img_facebook.setOnClickListener(this);
        initAuthLogin();
    }

    /**
     * 初始化第三方登录
     */
    private void initAuthLogin() {
        initQQLogin();
        initWeiBoLogin();
        initFacebookLogin();
        validateServerClientID();
    }

    private void initView(View view) {
        et_user_name = (EditText) view.findViewById(R.id.et_user_name);
        et_pwd = (EditText) view.findViewById(R.id.et_pwd);
        tv_reset_pwd = (TextView) view.findViewById(R.id.tv_reset_pwd);
        btn_login = (Button) view.findViewById(R.id.btn_login);
        img_qq = (ImageView) view.findViewById(R.id.img_qq);
        img_weChat = (ImageView) view.findViewById(R.id.img_wechat);
        img_weiBo = (ImageView) view.findViewById(R.id.img_weibo);
        img_facebook = (ImageView) view.findViewById(R.id.img_facebook);
        img_google = (ImageView) view.findViewById(R.id.img_google_plus);
        img_twitter = (TwButton) view.findViewById(R.id.img_twitter);
        img_twitter.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterAuthToken authToken = result.data.getAuthToken();
                String certificate = authToken.token + "&certificateSecret=" + authToken.secret;
                oauthLogin(HekrUserAction.OAUTH_TWITTER, certificate);
            }

            @Override
            public void failure(TwitterException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /**
     * qq
     */
    class BaseUiListener implements IUiListener {

        @Override
        public void onComplete(Object response) {
            //V2.0版本，参数类型由JSONObject 改成了Object,具体类型参考api文档
            String code;
            Log.i(TAG, "QQ:BaseUiListener:onComplete:response:" + response.toString());
            if (!TextUtils.isEmpty(response.toString())) {
                code = response.toString();
                try {
                    String certificate = new JSONObject(code).getString("access_token");
                    //登录
                    oauthLogin(HekrUserAction.OAUTH_QQ, certificate);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Log.i(TAG, "QQ:onComplete返回值为空！");
            }
        }


        @Override
        public void onError(UiError e) {
            Log.i("onError:", "code:" + e.errorCode + ", msg:"
                    + e.errorMessage + ", detail:" + e.errorDetail);
            if (customProgress != null) {
                customProgress.dismiss();
            }
        }

        @Override
        public void onCancel() {
            if (customProgress != null) {
                customProgress.dismiss();
            }
        }

    }


    /**
     * Facebook
     */
    class FaceBookListener implements FacebookCallback<LoginResult> {
        @Override
        public void onSuccess(LoginResult loginResult) {
            oauthLogin(HekrUserAction.OAUTH_FACEBOOK, loginResult.getAccessToken().getToken());
        }

        @Override
        public void onCancel() {
            //toastor.showSingletonToast("facebook取消登录");
            if (customProgress != null) {
                customProgress.dismiss();
            }
        }

        @Override
        public void onError(FacebookException exception) {
            // toastor.showSingletonToast("facebook登录错误");
            if (customProgress != null) {
                customProgress.dismiss();
            }
        }

    }


    /**
     * google[START handleSignInResult]
     */
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.toString());
        if (result.isSuccess()) {
            final GoogleSignInAccount acct = result.getSignInAccount();
            String email = acct.getEmail();
            new GetAuthToken().execute(email);
        }
    }


    /**
     * qq 退出
     */
    public void qqLogout() {
        mTencent.logout(getActivity());
    }

    /**
     * 微博退出
     */
    public void weiBoLogout() {
        AccessTokenKeeper.clear(getActivity().getApplicationContext());
    }

    /**
     * facebook退出
     */
    public void faceBookLogout() {
        accessTokenTracker.stopTracking();
    }


    /**
     * google退出
     */
    private void googleSignOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                    }
                });
    }


    //weibo
    public class AuthListener implements WeiboAuthListener {

        @Override
        public void onComplete(Bundle values) {

            mAccessToken = Oauth2AccessToken.parseAccessToken(values);
            //Toast.makeText(LoginActivity.this, "微博确认登录返回的mAccessToken：" + mAccessToken.toString(), Toast.LENGTH_LONG).show();
            String realContext;
            if (mAccessToken.isSessionValid()) {
                AccessTokenKeeper.writeAccessToken(getActivity(), mAccessToken);
                String certificate = mAccessToken.getToken() + "&uid=" + mAccessToken.getUid();
                oauthLogin(HekrUserAction.OAUTH_SINA, certificate);
            }

        }

        @Override
        public void onWeiboException(WeiboException e) {

        }

        @Override
        public void onCancel() {

        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        img_twitter.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_GET_TOKEN) {
            // [START get_id_token]
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

        switch (ssoAuthType) {
            case 1:
                if (mTencent != null) {
                    mTencent.onActivityResultData(requestCode, resultCode, data, new BaseUiListener());
                    ssoAuthType = 0;
                }
            case 2:
                if (mSsoHandler != null) {
                    mSsoHandler.authorizeCallBack(requestCode, resultCode, data);
                    ssoAuthType = 0;
                }
                break;
            case 3:
                if (callbackManager != null) {
                    callbackManager.onActivityResult(requestCode, resultCode, data);
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                final String userName = et_user_name.getText().toString().trim();
                String pwd = et_pwd.getText().toString().trim();
                if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(pwd)) {
                    toastor.showSingletonToast(getString(R.string.input_username));
                } else {
                    customProgress = CustomProgress.show(getActivity(), false, null);
                    hekrUserAction.login(userName, pwd, new HekrUser.LoginListener() {
                        @Override
                        public void loginSuccess(String str) {
                            String temp_bind_email = SpCache.getString(ConstantsUtil.SP_TEMP_ACCOUNT, "");
                            String temp_bind_token = SpCache.getString(ConstantsUtil.SP_TEMP_TOKEN, "");
                            if (TextUtils.equals(userName, temp_bind_email) && !TextUtils.isEmpty(temp_bind_token)) {
                                hekrUserAction.bindOAuth(temp_bind_token, new HekrUser.BindOAuthListener() {
                                    @Override
                                    public void bindSuccess() {
                                        SpCache.putString(ConstantsUtil.SP_TEMP_ACCOUNT, "");
                                        SpCache.putString(ConstantsUtil.SP_TEMP_TOKEN, "");
                                        toastor.showSingletonToast(getString(R.string.bind_success));
                                        pushAndStart();
                                    }

                                    @Override
                                    public void bindFail(int errorCode) {
                                        SpCache.putString(ConstantsUtil.SP_TEMP_ACCOUNT, "");
                                        SpCache.putString(ConstantsUtil.SP_TEMP_TOKEN, "");
                                        toastor.showSingleLongToast(HekrCodeUtil.errorCode2Msg(errorCode));
                                        customProgress.dismiss();
                                        pushAndStart();
                                    }
                                });
                            } else {
                                pushAndStart();
                            }
                        }

                        @Override
                        public void loginFail(int errorCode) {
                            customProgress.dismiss();
                            if (errorCode == 3400009) {
                                new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.app_name)).setMessage(getString(R.string.email_registered))
                                        .setPositiveButton(getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                customProgress = CustomProgress.show(getActivity(), false, null);
                                                hekrUserAction.reSendVerifiedEmail(userName, new HekrUser.ReSendVerifiedEmailListener() {
                                                    @Override
                                                    public void reSendVerifiedEmailSuccess() {
                                                        customProgress.dismiss();
                                                        HekrAlert.hekrAlert(getActivity(), getString(R.string.email_register_title), getString(R.string.email_register_success));
                                                    }

                                                    @Override
                                                    public void reSendVerifiedEmailFail(int errorCode) {
                                                        customProgress.dismiss();
                                                        HekrAlert.hekrAlert(getActivity(), HekrCodeUtil.errorCode2Msg(errorCode));
                                                    }
                                                });
                                            }
                                        }).setNegativeButton(getString(R.string.negative_button), null).create().show();
                            } else {
                                HekrAlert.hekrAlert(getActivity(), errorCode);
                            }
                        }
                    });
                }
                break;
            case R.id.tv_reset_pwd:
                startActivity(new Intent(getActivity(), ForgotPwdActivity.class));
                break;
            case R.id.img_qq:
                //
                if (!mTencent.isSessionValid()) {
                    mTencent.login(this, "all", new BaseUiListener());
                    ssoAuthType = 1;
                }
                break;
            case R.id.img_wechat:
                //
                if (!MyApplication.api.isWXAppInstalled()) {
                    //提醒用户没有按照微信
                    toastor.showSingletonToast(getString(R.string.no_install_wechat));
                    return;
                }


                final SendAuth.Req req = new SendAuth.Req();
                req.scope = "snsapi_userinfo";
                req.state = "hekr_wx_login";
                if (MyApplication.api != null) {
                    MyApplication.api.sendReq(req);
                }
                break;
            case R.id.img_weibo:
                //
                ssoAuthType = 2;
                mSsoHandler = new SsoHandler(getActivity(), mAuthInfo);
                mSsoHandler.authorize(new AuthListener());
                break;
            case R.id.img_facebook:
                LoginManager.getInstance().logInWithReadPermissions(this,
                        Arrays.asList("user_friends", "user_status", "public_profile", "user_about_me"));
                ssoAuthType = 3;
                break;
            case R.id.img_google_plus:
                initGoogleLogin();
                getIdToken();
                break;
        }
    }

    /**
     * 第三方登录
     *
     * @param type        类型
     * @param certificate code
     */
    private void oauthLogin(int type, final String certificate) {
        customProgress = CustomProgress.show(getActivity(), false, null);
        hekrUserAction.OAuthLogin(type, certificate, new HekrUser.MOAuthListener() {
            @Override
            public void mOAuthSuccess(MOAuthBean moAuthBean) {
                Log.d(TAG, "mOAuthSuccess: " + moAuthBean.toString());
                String bindToken = moAuthBean.getBindToken();
                if (TextUtils.equals(bindToken, SpCache.getString(ConstantsUtil.SP_TEMP_TOKEN, ""))) {
                    toastor.showSingletonToast("请直接使用邮箱登录进行绑定！");
                } else {
                    SpCache.putString(ConstantsUtil.SP_TEMP_TOKEN, "");
                    SpCache.putString(ConstantsUtil.SP_TEMP_ACCOUNT, "");
                    //执行绑定操作
                    Intent intent = new Intent(getActivity(), OAuthBindActivity.class);
                    intent.putExtra("bindToken", bindToken);
                    getActivity().startActivity(intent);
                    customProgress.dismiss();
                }


            }

            @Override
            public void mOAuthSuccess(JWTBean jwtBean) {

                if (!PushManager.getInstance().isPushTurnedOn(getActivity())) {
                    PushManager.getInstance().turnOnPush(getActivity());
                }
                PushManager.getInstance().bindAlias(getActivity(), hekrUserAction.getUserId());

                //执行登录操作
                startActivity(new Intent(getActivity(), ScrollingActivity.class));
                if (customProgress != null) {
                    customProgress.dismiss();
                }
                getActivity().finish();
            }

            @Override
            public void mOAuthFail(int errorCode) {
                if (customProgress != null) {
                    customProgress.dismiss();
                }
                if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                    signOut();
                }
                HekrAlert.hekrAlert(getActivity(), errorCode);
            }
        });
    }

    @Override
    public void onDestroy() {
        if (customProgress != null) {
            customProgress.dismiss();
        }
        EventBus.getDefault().unregister(this);
        super.onDestroy();

    }


    @Subscribe
    public void onEvent(ViewpageEvent event) {
        if (event.getCurrentItem() == 1) {
            et_user_name.setText(event.getUserName());
        }
    }


    /**
     * 第三方的回调
     *
     * @param event 微博和微信的回调
     */
    @Subscribe
    public void onEvent(AuthCodeEvent event) {
        switch (event.getType()) {
            case AuthCodeEvent.TYPE_WEIXIN:
                String code = event.getCode();
                if (!TextUtils.isEmpty(code)) {
                    oauthLogin(HekrUserAction.OAUTH_WECHAT, code);
                }
                break;
//            case AuthCodeEvent.TYPE_WEIBO:
//                if (mSsoHandler != null) {
//                    mSsoHandler.authorizeCallBack(event.getRequestCode(), event.getResultCode(), event.getData());
//                    ssoAuthType = 0;
//                }
//                break;
        }

    }

    /**
     * Validates that there is a reasonable server client ID in strings.xml, this is only needed
     * to make sure users of this sample follow the README.
     */
    private void validateServerClientID() {
        String serverClientId = getString(R.string.server_client_id);
        String suffix = ".apps.googleusercontent.com";
        if (!serverClientId.trim().endsWith(suffix)) {
            String message = "Invalid server client ID in strings.xml, must end with " + suffix;
            Log.w(TAG, message);
            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        }
    }


    private void getIdToken() {
        // Show an account picker to let the user choose a Google account from the device.
        // If the GoogleSignInOptions only asks for IDToken and/or profile and/or email then no
        // consent screen will be shown here.
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_GET_TOKEN);
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            signOut();
        }
    }

    private void signOut() {
        try {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void pushAndStart() {

        if (!PushManager.getInstance().isPushTurnedOn(getActivity())) {
            PushManager.getInstance().turnOnPush(getActivity());
        }
        PushManager.getInstance().bindAlias(getActivity(), hekrUserAction.getUserId());
        getActivity().startService(new Intent(getActivity(), WebSocketService.class));
        EventBus.getDefault().post(new WsSwitchEvent(2));
        startActivity(new Intent(getActivity(), ScrollingActivity.class));

        if (customProgress != null) {
            customProgress.dismiss();
        }
        getActivity().finish();
    }


    /**
     * google登录
     */
    private class GetAuthToken extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                return GoogleAuthUtil.getToken(getActivity(), params[0], "oauth2:"
                        + Scopes.PLUS_LOGIN);
            } catch (UserRecoverableAuthException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Intent recover = e.getIntent();
                startActivityForResult(recover, 125);
                return "error -";
            } catch (IOException | GoogleAuthException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "error -";
            }
        }


        @Override
        protected void onPostExecute(String result) {
            android.util.Log.d(TAG, "onPostExecute: " + result);
            if (!result.startsWith("error -")) {
                oauthLogin(HekrUserAction.OAUTH_GOOGLE_PLUS, result);
            }
        }
    }


}
