package me.hekr.hummingbird.fragment;

import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.litesuits.common.assist.Toastor;

import org.greenrobot.eventbus.EventBus;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.activity.ScrollingActivity;
import me.hekr.hummingbird.activity.UserProtocolActivity;
import me.hekr.hummingbird.event.ViewpageEvent;
import me.hekr.hummingbird.ui.CustomProgress;
import me.hekr.hummingbird.util.DensityUtils;
import me.hekr.hummingbird.util.HekrAlert;
import me.hekr.hummingbird.util.HekrCodeUtil;
import me.hekr.hummingbird.util.Validator;

public class RegisterFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "RegisterFragment";
    private EditText et_user_name;
    private EditText et_code;
    private EditText et_pwd;
    private TextView tv_get_code;
    private HekrUserAction hekrUserAction;
    private Toastor toastor;
    private LinearLayout layout_pwd;
    private LinearLayout layout_code;
    //private String userToken;
    private TimeCount timeCount;
    private CustomProgress customProgress;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.layout_register, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initData();
    }

    private void initData() {
        hekrUserAction = HekrUserAction.getInstance(getActivity());
        toastor = new Toastor(getActivity());
        timeCount = new TimeCount(60000, 1000);
    }

    private void initView(View view) {
        et_user_name = (EditText) view.findViewById(R.id.et_user_name);
        et_code = (EditText) view.findViewById(R.id.et_code);
        et_pwd = (EditText) view.findViewById(R.id.et_pwd);
        tv_get_code = (TextView) view.findViewById(R.id.tv_get_code);
        Button btn_register = (Button) view.findViewById(R.id.btn_register);
        layout_pwd = (LinearLayout) view.findViewById(R.id.layout_pwd);
        layout_code = (LinearLayout) view.findViewById(R.id.layout_code);
        tv_get_code.setOnClickListener(this);
        btn_register.setOnClickListener(this);
        et_user_name.addTextChangedListener(watcher);
        TextView tv_user_protocol = (TextView) view.findViewById(R.id.tv_user_protocol);
        tv_user_protocol.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_get_code:
                new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.app_name)).setMessage(getString(R.string.code_send) + et_user_name.getText().toString().trim() + " ?")
                        .setPositiveButton(getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                hekrUserAction.getVerifyCode(et_user_name.getText().toString().trim(), HekrUserAction.CODE_TYPE_REGISTER, new HekrUser.GetVerifyCodeListener() {
                                    @Override
                                    public void getVerifyCodeSuccess() {
                                        toastor.showSingletonToast(getString(R.string.success_send));
                                        timeCount.start();
                                    }

                                    @Override
                                    public void getVerifyCodeFail(int errorCode) {
                                        if (errorCode == 3400008) {
                                            registed();
                                        } else {
                                            HekrAlert.hekrAlert(getActivity(), HekrCodeUtil.errorCode2Msg(errorCode));
                                        }
                                    }
                                });
                            }
                        }).setNegativeButton(getString(R.string.negative_button), null).create().show();

                break;
            case R.id.btn_register:
                String userName = et_user_name.getText().toString().trim();
                String pwd = et_pwd.getText().toString().trim();
                if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(pwd)) {
                    toastor.showSingletonToast(getString(R.string.alert_input));
                } else {
                    if (pwd.length() >= 6 && !pwd.contains(" ")) {
                        customProgress = CustomProgress.show(getActivity(), false, null);
                        if (Validator.isMobile(userName)) {
                            hekrUserAction.checkVerifyCode(et_user_name.getText().toString(), et_code.getText().toString().trim(), new HekrUser.CheckVerifyCodeListener() {
                                /**
                                 * @param phoneNumber 用户手机号
                                 * @param verifyCode  验证码
                                 * @param token       此token在注册时要使用
                                 * @param expireTime  token过期时间,此token的过期时间，在此时间之前没有注册，token会过期，必须重新获取、校验验证码
                                 */
                                @Override
                                public void checkVerifyCodeSuccess(String phoneNumber, String verifyCode, String token, String expireTime) {
                                    // userToken = token;
                                    registerByPhone(token);
                                }

                                @Override
                                public void checkVerifyCodeFail(int errorCode) {
                                    customProgress.dismiss();
                                    HekrAlert.hekrAlert(getActivity(), errorCode);
                                }
                            });
                        } else if (Validator.isEmail(userName)) {
                            registerByEmail();
                        } else {
                            HekrAlert.hekrAlert(getActivity(), getString(R.string.username_phone_mail));
                            customProgress.dismiss();
                        }
                    } else {
                        toastor.showSingletonToast(getString(R.string.pwd_least_six_number));
                    }
                }
                break;
            case R.id.tv_user_protocol:
                startActivity(new Intent(getActivity(), UserProtocolActivity.class));
                break;
        }

    }


    private TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // TODO Auto-generated method stub
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub

        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
            long time = 300;
            if (Validator.isMobile(s.toString()) && layout_code.getVisibility() == View.GONE) {
                layout_code.setVisibility(View.VISIBLE);
                AlphaAnimation aa = new AlphaAnimation(0, 1);
                aa.setDuration(time);
                layout_code.startAnimation(aa);
                ObjectAnimator animator = ObjectAnimator.ofFloat(layout_pwd, "translationY", DensityUtils.dp2px(getActivity(), 58));
                animator.setDuration(time);
                animator.start();
            } else {
                if (layout_code.getVisibility() == View.VISIBLE) {
                    AlphaAnimation aa = new AlphaAnimation(1, 0);
                    aa.setDuration(time);
                    layout_code.startAnimation(aa);
                    layout_code.setVisibility(View.GONE);
                    ObjectAnimator animator = ObjectAnimator.ofFloat(layout_pwd, "translationY", 0);
                    animator.setDuration(time);
                    animator.start();
                }
            }
        }
    };


    /**
     * 手机注册
     */
    private void registerByPhone(String userToken) {
        hekrUserAction.registerByPhone(et_user_name.getText().toString().trim(), et_pwd.getText().toString().trim(), userToken, new HekrUser.RegisterListener() {
            @Override
            public void registerSuccess(String uid) {
                toastor.showSingletonToast(getString(R.string.registe_success));
                login();
            }

            @Override
            public void registerFail(int errorCode) {
                customProgress.dismiss();
                HekrAlert.hekrAlert(getActivity(), errorCode);
            }
        });
    }

    /**
     * 邮箱注册
     */
    private void registerByEmail() {
        hekrUserAction.registerByEmail(et_user_name.getText().toString(), et_pwd.getText().toString(), new HekrUser.RegisterListener() {
            @Override
            public void registerSuccess(String uid) {
                customProgress.dismiss();
                HekrAlert.hekrAlert(getActivity(), getString(R.string.email_register_title), getString(R.string.email_register_success));
            }

            @Override
            public void registerFail(int errorCode) {
                customProgress.dismiss();
                if (errorCode == 3400008) {
                    registed();
                } else {
                    HekrAlert.hekrAlert(getActivity(), errorCode);
                }
            }
        });
    }


    /**
     * 登录
     */
    private void login() {
        hekrUserAction.login(et_user_name.getText().toString(), et_pwd.getText().toString(), new HekrUser.LoginListener() {
            @Override
            public void loginSuccess(String str) {
                customProgress.dismiss();
                startActivity(new Intent(getActivity(), ScrollingActivity.class));
            }

            @Override
            public void loginFail(int errorCode) {
                customProgress.dismiss();
                HekrAlert.hekrAlert(getActivity(), errorCode);
            }
        });
    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            tv_get_code.setClickable(false);
            tv_get_code.setText(millisUntilFinished / 1000 + "s");
        }

        @Override
        public void onFinish() {
            tv_get_code.setText(getString(R.string.get_code));
            tv_get_code.setClickable(true);
        }
    }

    /**
     * 已经注册！
     */
    private void registed() {
        new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.app_name)).setMessage(getString(R.string.registered)).
                setPositiveButton(getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EventBus.getDefault().post(new ViewpageEvent(1, et_user_name.getText().toString().trim()));
                    }
                }).setNegativeButton(getString(R.string.negative_button), null).create().show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timeCount != null) {
            timeCount.cancel();
            timeCount = null;
        }
    }


}
