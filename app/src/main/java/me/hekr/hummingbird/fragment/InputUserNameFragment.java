package me.hekr.hummingbird.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.util.HekrAlert;
import me.hekr.hummingbird.util.HekrCodeUtil;
import me.hekr.hummingbird.util.Validator;

import com.litesuits.common.assist.Toastor;

import org.jetbrains.annotations.NotNull;

import me.hekr.hummingbird.R;

public class InputUserNameFragment extends Fragment {
    private static final String TAG = "InputUserNameFragment";
    private EditText et_user_name;
    private Button btn_next;
    private Toastor toastor;
    HekrUserAction hekrUserAction;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_input_user_name, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initData();
    }

    private void initData() {
        toastor = new Toastor(getActivity());
        hekrUserAction = HekrUserAction.getInstance(getActivity());
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(et_user_name.getText().toString().trim())) {
                    toastor.showSingletonToast(R.string.error_user_name);
                } else {
                    setPwd(et_user_name.getText().toString().trim());
                }

            }
        });
    }

    private void initView(View view) {
        et_user_name = (EditText) view.findViewById(R.id.et_user_name);
        btn_next = (Button) view.findViewById(R.id.btn_next);
    }

    private void setPwd(final String userName) {
        final FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if (Validator.isEmail(userName)) {
            //重新发送激活邮件
            /*hekrUserAction.reSendVerifiedEmail(userName, new HekrUser.ReSendVerifiedEmailListener() {
                @Override
                public void reSendVerifiedEmailSuccess() {
                    RendMailFragment fragment = new RendMailFragment();
                    transaction.replace(R.id.fg, fragment);
                    transaction.commit();
                }

                @Override
                public void reSendVerifiedEmailFail(int errorCode) {
                    toastor.showSingletonToast("失败" + errorCode);
                }
            });*/
            hekrUserAction.sendResetPwdEmail(userName, new HekrUser.SendResetPwdEmailListener() {
                @Override
                public void sendSuccess() {
                    RendMailFragment fragment = new RendMailFragment();
                    transaction.replace(R.id.fg, fragment);
                    transaction.commit();
                }

                @Override
                public void sendFail(int errorCode) {
                    HekrAlert.hekrAlert(getActivity(), errorCode);
                }
            });

        } else if (Validator.isMobile(userName)) {
            //通过手机号重置
            hekrUserAction.getVerifyCode(userName, HekrUserAction.CODE_TYPE_RE_REGISTER, new HekrUser.GetVerifyCodeListener() {
                @Override
                public void getVerifyCodeSuccess() {
                    GetCodeFragment fragment = GetCodeFragment.newInstance(userName);
                    transaction.replace(R.id.fg, fragment);
                    transaction.commit();
                }

                @Override
                public void getVerifyCodeFail(int errorCode) {
                    HekrAlert.hekrAlert(getActivity(), errorCode);
                }
            });

        } else {
            HekrAlert.hekrAlert(getActivity(), getString(R.string.error_user_name));
        }
    }


}
