package me.hekr.hummingbird.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import me.hekr.hummingbird.R;

public class ChangePhoneFragment extends Fragment {
    private static final String TAG = "InputUserNameFragment";
    private Button btn_next;
    private TextView tv_phone_number;


    public static ChangePhoneFragment newInstance(String phone) {
        ChangePhoneFragment fragment = new ChangePhoneFragment();
        Bundle bundle = new Bundle();
        bundle.putString("phone", phone);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_change_phone, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initData();


    }

    private void initData() {
        final String phoneNumber = getArguments().getString("phone");
        tv_phone_number.setText(getString(R.string.my_phone) + phoneNumber);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fg, ChangePhoneGetCodeFragment.newInstance(phoneNumber));
                fragmentTransaction.commit();
            }
        });
    }

    private void initView(View view) {
        btn_next = (Button) view.findViewById(R.id.btn_next);
        tv_phone_number = (TextView) view.findViewById(R.id.tv_phone_number);
    }

}
