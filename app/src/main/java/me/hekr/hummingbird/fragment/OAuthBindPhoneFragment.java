package me.hekr.hummingbird.fragment;

import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import me.hekr.hummingbird.activity.ScrollingActivity;

import com.litesuits.common.assist.Toastor;

import org.greenrobot.eventbus.EventBus;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.event.ViewpageEvent;
import me.hekr.hummingbird.util.ConstantsUtil;
import me.hekr.hummingbird.util.HekrAlert;
import me.hekr.hummingbird.util.SpCache;
import me.hekr.hummingbird.util.Validator;

public class OAuthBindPhoneFragment extends Fragment {
    private static final String TAG = "InputUserNameFragment";
    private EditText et_code;
    private EditText et_pwd;
    private EditText et_phone;
    private Button btn_ok;
    private TimeCount timeCount;
    private TextView tv_get_code;
    private HekrUserAction hekrUserAction;
    private Toastor toastor;
    private String bindToken;


    public static OAuthBindPhoneFragment newInstance(String bindToken) {
        OAuthBindPhoneFragment fragment = new OAuthBindPhoneFragment();
        Bundle bundle = new Bundle();
        bundle.putString("bindToken", bindToken);
        fragment.setArguments(bundle);
        return fragment;
    }

    public OAuthBindPhoneFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_oauth_bind_phone, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initData();
    }

    private void initData() {
        toastor = new Toastor(getActivity());
        hekrUserAction = HekrUserAction.getInstance(getActivity());
        timeCount = new TimeCount(60000, 1000);
        bindToken = getArguments().getString("bindToken");
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        tv_get_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCode();
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (TextUtils.isEmpty(et_phone.getText().toString().trim()) || TextUtils.isEmpty(et_code.getText().toString().trim())) {
                    HekrAlert.hekrAlert(getActivity(), getString(R.string.full_info));
                } else {
                    hekrUserAction.checkVerifyCode(et_phone.getText().toString().trim(), et_code.getText().toString().trim(), new HekrUser.CheckVerifyCodeListener() {

                        @Override
                        public void checkVerifyCodeSuccess(String phoneNumber, String verifyCode, String token, String expireTime) {
                            //手机注册
                            registerByPhone(phoneNumber, token);
                        }


                        @Override
                        public void checkVerifyCodeFail(int errorCode) {
                            if (errorCode == 3400008) {
                                registed();
                            } else {
                                HekrAlert.hekrAlert(getActivity(), errorCode);
                            }
                        }
                    });
                }

            }
        });
    }

    private void initView(View view) {
        et_code = (EditText) view.findViewById(R.id.et_code);
        et_pwd = (EditText) view.findViewById(R.id.et_pwd);
        btn_ok = (Button) view.findViewById(R.id.btn_ok);
        tv_get_code = (TextView) view.findViewById(R.id.tv_get_code);
        et_phone = (EditText) view.findViewById(R.id.et_phone);
    }


    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            tv_get_code.setClickable(false);
            tv_get_code.setEnabled(false);
            tv_get_code.setText(millisUntilFinished / 1000 + "s");
        }

        @Override
        public void onFinish() {
            tv_get_code.setText(getString(R.string.get_code));
            tv_get_code.setEnabled(true);
            tv_get_code.setClickable(true);

        }
    }


    /**
     * 获取验证码
     */
    private void getCode() {
        if (!TextUtils.isEmpty(et_phone.getText().toString().trim())) {
            if (Validator.isMobile(et_phone.getText().toString().trim())) {
                hekrUserAction.getVerifyCode(et_phone.getText().toString().trim(), HekrUserAction.CODE_TYPE_REGISTER, new HekrUser.GetVerifyCodeListener() {
                    @Override
                    public void getVerifyCodeSuccess() {
                        toastor.showSingletonToast(getString(R.string.success_send));
                        timeCount.start();
                    }

                    @Override
                    public void getVerifyCodeFail(int errorCode) {
                        if (errorCode == 3400008) {
                            registed();
                        } else {
                            HekrAlert.hekrAlert(getActivity(), errorCode);
                        }
                    }
                });
            } else {
                toastor.showSingletonToast(getString(R.string.please_input_ok_phone_number));
            }
        } else {
            toastor.showSingletonToast(getString(R.string.please_input_phone_number));
        }
    }


    /**
     * 手机注册
     */
    private void registerByPhone(final String phoneNumber, String userToken) {
        if (!TextUtils.isEmpty(et_pwd.getText().toString().trim())) {
            hekrUserAction.registerByPhone(phoneNumber, et_pwd.getText().toString().trim(), userToken, new HekrUser.RegisterListener() {
                @Override
                public void registerSuccess(String uid) {
                    //注册成功，执行登录，登录完成了再绑定
                    login(phoneNumber, et_pwd.getText().toString());
                }

                @Override
                public void registerFail(int errorCode) {
                    HekrAlert.hekrAlert(getActivity(), errorCode);
                }
            });
        } else {
            toastor.showSingletonToast(getString(R.string.input_pwd));
        }
    }

    /**
     * 绑定操作
     */
    private void bind() {
        hekrUserAction.bindOAuth(bindToken, new HekrUser.BindOAuthListener() {
            @Override
            public void bindSuccess() {
                toastor.showSingletonToast(getString(R.string.bind_success));
                //绑定成功
                getActivity().startActivity(new Intent(getActivity(), ScrollingActivity.class));

            }

            @Override
            public void bindFail(int errorCode) {
                HekrAlert.hekrAlert(getActivity(), errorCode);
            }
        });
    }

    /**
     * 登录操作！
     *
     * @param phoneNumber 手机号码
     * @param pwd         密码
     */
    private void login(String phoneNumber, String pwd) {
        hekrUserAction.login(phoneNumber, pwd, new HekrUser.LoginListener() {
            @Override
            public void loginSuccess(String str) {
                bind();
            }

            @Override
            public void loginFail(int errorCode) {
                HekrAlert.hekrAlert(getActivity(), errorCode);
            }
        });
    }


    /**
     * 已经注册！
     */
    private void registed() {
        new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.app_name)).setMessage(getString(R.string.registered)).
                setPositiveButton(getString(R.string.positive_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String phone = et_phone.getText().toString().trim();
                        SpCache.putString(ConstantsUtil.SP_TEMP_ACCOUNT, phone);
                        SpCache.putString(ConstantsUtil.SP_TEMP_TOKEN, bindToken);
                        getActivity().finish();
                        EventBus.getDefault().post(new ViewpageEvent(1, phone));
                    }
                }).setNegativeButton(getString(R.string.negative_button), null).create().show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timeCount != null) {
            timeCount.cancel();
            timeCount = null;
        }
    }
}
