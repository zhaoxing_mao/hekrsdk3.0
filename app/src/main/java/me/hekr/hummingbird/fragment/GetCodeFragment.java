package me.hekr.hummingbird.fragment;

import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.litesuits.common.assist.Toastor;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.action.HekrUser;
import me.hekr.hummingbird.action.HekrUserAction;
import me.hekr.hummingbird.util.HekrAlert;
import me.hekr.hummingbird.util.HekrCodeUtil;

public class GetCodeFragment extends Fragment {
    private EditText et_code;
    private EditText et_pwd;
    private Button btn_ok;
    private TimeCount timeCount;
    private TextView tv_get_code;
    private HekrUserAction hekrUserAction;
    private Toastor toastor;
    private String phoneNumber;


    public static GetCodeFragment newInstance(String phone) {
        GetCodeFragment fragment = new GetCodeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("phone", phone);
        fragment.setArguments(bundle);
        return fragment;
    }

    public GetCodeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_get_code, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initData();
    }

    private void initData() {
        toastor = new Toastor(getActivity());
        hekrUserAction = HekrUserAction.getInstance(getActivity());
        timeCount = new TimeCount(60000, 1000);
        timeCount.start();
        phoneNumber = getArguments().getString("phone");
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        tv_get_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCode();
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final String pwd = et_pwd.getText().toString().trim();
                final String code = et_code.getText().toString().trim();
                if (TextUtils.isEmpty(phoneNumber) || TextUtils.isEmpty(pwd) || TextUtils.isEmpty(code)) {
                    HekrAlert.hekrAlert(getActivity(), getString(R.string.null_code_pwd));
                } else {
                    hekrUserAction.checkVerifyCode(phoneNumber, et_code.getText().toString().trim(), new HekrUser.CheckVerifyCodeListener() {
                        /**
                         * @param phoneNumber 用户手机号
                         * @param verifyCode  验证码
                         * @param token       此token在注册时要使用
                         * @param expireTime  token过期时间,此token的过期时间，在此时间之前没有注册，token会过期，必须重新获取、校验验证码
                         */
                        @Override
                        public void checkVerifyCodeSuccess(String phoneNumber, String verifyCode, String token, String expireTime) {
                            // userToken = token;
                            reSetPwd(phoneNumber, verifyCode, pwd);
                        }

                        @Override
                        public void checkVerifyCodeFail(int errorCode) {
                            HekrAlert.hekrAlert(getActivity(), errorCode);
                        }
                    });
                }
            }
        });
    }

    private void initView(View view) {
        et_code = (EditText) view.findViewById(R.id.et_code);
        et_pwd = (EditText) view.findViewById(R.id.et_pwd);
        btn_ok = (Button) view.findViewById(R.id.btn_ok);
        tv_get_code = (TextView) view.findViewById(R.id.tv_get_code);
    }


    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            tv_get_code.setClickable(false);
            tv_get_code.setEnabled(false);
            tv_get_code.setText(millisUntilFinished / 1000 + "s");
        }

        @Override
        public void onFinish() {
            tv_get_code.setText(getString(R.string.get_code));
            tv_get_code.setEnabled(true);
            tv_get_code.setClickable(true);

        }
    }

    private void reSetPwd(String phoneNumber, String verifyCode, String password) {
        hekrUserAction.resetPwd(phoneNumber, verifyCode, password, new HekrUser.ResetPwdListener() {
            @Override
            public void resetSuccess() {
                new AlertDialog.Builder(getActivity()).setTitle(getString(R.string.app_name)).setMessage(getString(R.string.change_pwd_success)).setPositiveButton(getString(R.string.i_know), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                }).setCancelable(false).create().show();
            }

            @Override
            public void resetFail(int errorCode) {
                HekrAlert.hekrAlert(getActivity(), errorCode);
            }
        });
    }

    private void getCode() {
        hekrUserAction.getVerifyCode(phoneNumber, HekrUserAction.CODE_TYPE_RE_REGISTER, new HekrUser.GetVerifyCodeListener() {
            @Override
            public void getVerifyCodeSuccess() {
                toastor.showSingletonToast(getString(R.string.success_send));
                timeCount.start();
            }

            @Override
            public void getVerifyCodeFail(int errorCode) {
                HekrAlert.hekrAlert(getActivity(), errorCode);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timeCount != null) {
            timeCount.cancel();
            timeCount = null;
        }
    }
}
