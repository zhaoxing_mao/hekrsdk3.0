package me.hekr.hummingbird.application;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import me.hekr.hummingbird.R;
import me.hekr.hummingbird.bean.FindDeviceBean;
import me.hekr.hummingbird.util.AuthConstant;
import me.hekr.hummingbird.util.HekrSDK;
import me.hekr.hummingbird.x5web.FirstLoadingX5Service;

import com.igexin.sdk.PushManager;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.smtt.sdk.QbSdk;

import java.util.ArrayList;

public class MyApplication extends Application {

    public static ArrayList<FindDeviceBean> lanList = new ArrayList<>();
    public static IWXAPI api;
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        new Thread(new Runnable() {
            @Override
            public void run() {
                api = WXAPIFactory.createWXAPI(getApplicationContext(), AuthConstant.WEIXIN_APP_ID, true);
                api.registerApp(AuthConstant.WEIXIN_APP_ID);
                context = getApplicationContext();

                HekrSDK.init(getApplicationContext(), R.raw.config);
                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                        .memoryCacheExtraOptions(80, 80)
                        .denyCacheImageMultipleSizesInMemory()
                        .writeDebugLogs()
                        .tasksProcessingOrder(QueueProcessingType.LIFO)
                        .build();
                ImageLoader.getInstance().init(config);

                if (!QbSdk.isTbsCoreInited()) {// preinit只需要调用一次，如果已经完成了初始化，那么就直接构造view
                    Intent intent = new Intent(getApplicationContext(), FirstLoadingX5Service.class);
                    startService(intent);
                }

                //推送
                PushManager.getInstance().initialize(getApplicationContext());
            }
        }).start();
    }
}
