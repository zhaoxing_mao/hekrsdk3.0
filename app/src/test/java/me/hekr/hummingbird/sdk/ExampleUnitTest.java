package me.hekr.hummingbird.sdk;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import me.hekr.hummingbird.util.Validator;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(true, Validator.isMobile("17011111111"));

    }
}